package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBool;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestContestSaveDeveloperInfo extends TLMethod<TLBool> {
    public static final int CLASS_ID = 0x9a5f6e95;

    protected int vkId;

    protected String name;

    protected String phoneNumber;

    protected int age;

    protected String city;

    public TLRequestContestSaveDeveloperInfo() {
    }

    public TLRequestContestSaveDeveloperInfo(int vkId, String name, String phoneNumber, int age,
            String city) {
        this.vkId = vkId;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.age = age;
        this.city = city;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLBool deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLBool)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLBool) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(vkId, stream);
        writeString(name, stream);
        writeString(phoneNumber, stream);
        writeInt(age, stream);
        writeString(city, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        vkId = readInt(stream);
        name = readTLString(stream);
        phoneNumber = readTLString(stream);
        age = readInt(stream);
        city = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += computeTLStringSerializedSize(name);
        size += computeTLStringSerializedSize(phoneNumber);
        size += SIZE_INT32;
        size += computeTLStringSerializedSize(city);
        return size;
    }

    @Override
    public String toString() {
        return "contest.saveDeveloperInfo#9a5f6e95";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getVkId() {
        return vkId;
    }

    public void setVkId(int vkId) {
        this.vkId = vkId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
