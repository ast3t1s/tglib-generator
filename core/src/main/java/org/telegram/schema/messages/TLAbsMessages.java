package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLChannelMessages: messages.channelMessages#99262e37</li>
 * <li>{@link TLMessages: messages.messages#8c718e87</li>
 * <li>{@link TLMessagesSlice: messages.messagesSlice#b446ae3</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsMessages extends TLObject {
    public TLAbsMessages() {
    }
}
