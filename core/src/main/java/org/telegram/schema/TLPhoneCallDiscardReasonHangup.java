package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLPhoneCallDiscardReasonHangup extends TLAbsPhoneCallDiscardReason {
    public static final int CLASS_ID = 0x57adc690;

    public TLPhoneCallDiscardReasonHangup() {
    }

    @Override
    public String toString() {
        return "phoneCallDiscardReasonHangup#57adc690";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
