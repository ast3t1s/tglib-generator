package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLPrivacyValueAllowAll extends TLAbsPrivacyRule {
    public static final int CLASS_ID = 0x65427b82;

    public TLPrivacyValueAllowAll() {
    }

    @Override
    public String toString() {
        return "privacyValueAllowAll#65427b82";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
