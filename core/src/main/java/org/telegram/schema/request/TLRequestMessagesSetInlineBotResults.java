package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBool;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsInputBotInlineResult;
import org.telegram.schema.TLInlineBotSwitchPM;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestMessagesSetInlineBotResults extends TLMethod<TLBool> {
    public static final int CLASS_ID = 0xeb5ea206;

    protected int flags;

    protected boolean gallery;

    protected boolean _private;

    protected long queryId;

    protected TLVector<TLAbsInputBotInlineResult> results;

    protected int cacheTime;

    protected String nextOffset;

    protected TLInlineBotSwitchPM switchPm;

    public TLRequestMessagesSetInlineBotResults() {
    }

    public TLRequestMessagesSetInlineBotResults(boolean gallery, boolean _private, long queryId,
            TLVector<TLAbsInputBotInlineResult> results, int cacheTime, String nextOffset,
            TLInlineBotSwitchPM switchPm) {
        this.gallery = gallery;
        this._private = _private;
        this.queryId = queryId;
        this.results = results;
        this.cacheTime = cacheTime;
        this.nextOffset = nextOffset;
        this.switchPm = switchPm;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLBool deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLBool)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLBool) response;
    }

    private void computeFlags() {
        flags = 0;
        flags = gallery ? (flags | 1) : (flags & ~1);
        flags = _private ? (flags | 2) : (flags & ~2);
        flags = nextOffset != null ? (flags | 4) : (flags & ~4);
        flags = switchPm != null ? (flags | 8) : (flags & ~8);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeLong(queryId, stream);
        writeTLVector(results, stream);
        writeInt(cacheTime, stream);
        if ((flags & 4) != 0) {
            if (nextOffset == null) throwNullFieldException("nextOffset" , flags);
            writeString(nextOffset, stream);
        }
        if ((flags & 8) != 0) {
            if (switchPm == null) throwNullFieldException("switchPm" , flags);
            writeTLObject(switchPm, stream);
        }
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        gallery = (flags & 1) != 0;
        _private = (flags & 2) != 0;
        queryId = readLong(stream);
        results = readTLVector(stream, context);
        cacheTime = readInt(stream);
        nextOffset = (flags & 4) != 0 ? readTLString(stream) : null;
        switchPm = (flags & 8) != 0 ? readTLObject(stream, context, TLInlineBotSwitchPM.class, TLInlineBotSwitchPM.CLASS_ID) : null;
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT64;
        size += results.computeSerializedSize();
        size += SIZE_INT32;
        if ((flags & 4) != 0) {
            if (nextOffset == null) throwNullFieldException("nextOffset" , flags);
            size += computeTLStringSerializedSize(nextOffset);
        }
        if ((flags & 8) != 0) {
            if (switchPm == null) throwNullFieldException("switchPm" , flags);
            size += switchPm.computeSerializedSize();
        }
        return size;
    }

    @Override
    public String toString() {
        return "messages.setInlineBotResults#eb5ea206";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getGallery() {
        return gallery;
    }

    public void setGallery(boolean gallery) {
        this.gallery = gallery;
    }

    public boolean getPrivate() {
        return _private;
    }

    public void setPrivate(boolean _private) {
        this._private = _private;
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public TLVector<TLAbsInputBotInlineResult> getResults() {
        return results;
    }

    public void setResults(TLVector<TLAbsInputBotInlineResult> results) {
        this.results = results;
    }

    public int getCacheTime() {
        return cacheTime;
    }

    public void setCacheTime(int cacheTime) {
        this.cacheTime = cacheTime;
    }

    public String getNextOffset() {
        return nextOffset;
    }

    public void setNextOffset(String nextOffset) {
        this.nextOffset = nextOffset;
    }

    public TLInlineBotSwitchPM getSwitchPm() {
        return switchPm;
    }

    public void setSwitchPm(TLInlineBotSwitchPM switchPm) {
        this.switchPm = switchPm;
    }
}
