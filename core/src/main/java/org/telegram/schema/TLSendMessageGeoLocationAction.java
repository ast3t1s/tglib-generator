package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLSendMessageGeoLocationAction extends TLAbsSendMessageAction {
    public static final int CLASS_ID = 0x176f8ba1;

    public TLSendMessageGeoLocationAction() {
    }

    @Override
    public String toString() {
        return "sendMessageGeoLocationAction#176f8ba1";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
