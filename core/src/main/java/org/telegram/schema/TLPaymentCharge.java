package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLPaymentCharge extends TLObject {
    public static final int CLASS_ID = 0xea02c27e;

    protected String id;

    protected String providerChargeId;

    public TLPaymentCharge() {
    }

    public TLPaymentCharge(String id, String providerChargeId) {
        this.id = id;
        this.providerChargeId = providerChargeId;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(id, stream);
        writeString(providerChargeId, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        id = readTLString(stream);
        providerChargeId = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(id);
        size += computeTLStringSerializedSize(providerChargeId);
        return size;
    }

    @Override
    public String toString() {
        return "paymentCharge#ea02c27e";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProviderChargeId() {
        return providerChargeId;
    }

    public void setProviderChargeId(String providerChargeId) {
        this.providerChargeId = providerChargeId;
    }
}
