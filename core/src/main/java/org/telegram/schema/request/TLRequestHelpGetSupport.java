package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.help.TLSupport;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestHelpGetSupport extends TLMethod<TLSupport> {
    public static final int CLASS_ID = 0x9cdf08cd;

    public TLRequestHelpGetSupport() {
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLSupport deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLSupport)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLSupport) response;
    }

    @Override
    public String toString() {
        return "help.getSupport#9cdf08cd";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
