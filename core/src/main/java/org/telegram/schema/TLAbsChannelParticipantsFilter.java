package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLChannelParticipantsAdmins: channelParticipantsAdmins#b4608969</li>
 * <li>{@link TLChannelParticipantsBots: channelParticipantsBots#b0d1865b</li>
 * <li>{@link TLChannelParticipantsKicked: channelParticipantsKicked#3c37bb7a</li>
 * <li>{@link TLChannelParticipantsRecent: channelParticipantsRecent#de3f3c79</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsChannelParticipantsFilter extends TLObject {
    public TLAbsChannelParticipantsFilter() {
    }
}
