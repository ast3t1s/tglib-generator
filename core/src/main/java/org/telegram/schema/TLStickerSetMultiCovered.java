package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLVector;

/**
 * @author Telegram Schema Generator
 */
public class TLStickerSetMultiCovered extends TLAbsStickerSetCovered {
    public static final int CLASS_ID = 0x3407e51b;

    protected TLStickerSet set;

    protected TLVector<TLAbsDocument> covers;

    public TLStickerSetMultiCovered() {
    }

    public TLStickerSetMultiCovered(TLStickerSet set, TLVector<TLAbsDocument> covers) {
        this.set = set;
        this.covers = covers;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(set, stream);
        writeTLVector(covers, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        set = readTLObject(stream, context, TLStickerSet.class, TLStickerSet.CLASS_ID);
        covers = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += set.computeSerializedSize();
        size += covers.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "stickerSetMultiCovered#3407e51b";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLStickerSet getSet() {
        return set;
    }

    public void setSet(TLStickerSet set) {
        this.set = set;
    }

    public TLVector<TLAbsDocument> getCovers() {
        return covers;
    }

    public void setCovers(TLVector<TLAbsDocument> covers) {
        this.covers = covers;
    }
}
