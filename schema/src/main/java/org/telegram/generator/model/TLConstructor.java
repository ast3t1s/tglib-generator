package org.telegram.generator.model;

import org.telegram.generator.util.Util;

import java.util.ArrayList;

public class TLConstructor implements Comparable<TLConstructor> {

    private final int id;
    private final String name;
    private final ArrayList<TLParameter> parameters;
    private final TLDefTypeModel.TLTypeRaw tlType;

    public TLConstructor(int id, String name, ArrayList<TLParameter> parameters, TLDefTypeModel.TLTypeRaw tlType) {
        this.id = id;
        this.name = name;
        this.parameters = parameters;
        this.tlType = tlType;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ArrayList<TLParameter> getParameters() {
        return parameters;
    }

    public TLDefTypeModel.TLTypeRaw getTlType() {
        return tlType;
    }

    @Override
    public int compareTo(TLConstructor o) {
        return name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return String.format("%s#%s -> %s", name, Util.hex(id), tlType);
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof TLConstructor
                && ((TLConstructor) other).getName().equals(name)
                && ((TLConstructor) other).id == id;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
