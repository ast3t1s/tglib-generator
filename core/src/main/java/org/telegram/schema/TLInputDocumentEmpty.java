package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputDocumentEmpty extends TLAbsInputDocument {
    public static final int CLASS_ID = 0x72f0eaae;

    public TLInputDocumentEmpty() {
    }

    @Override
    public String toString() {
        return "inputDocumentEmpty#72f0eaae";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    @Override
    public final boolean isEmpty() {
        return true;
    }

    @Override
    public final boolean isNotEmpty() {
        return false;
    }
}
