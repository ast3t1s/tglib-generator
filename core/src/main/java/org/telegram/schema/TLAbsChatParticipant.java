package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLChatParticipant: chatParticipant#c8d7493e</li>
 * <li>{@link TLChatParticipantAdmin: chatParticipantAdmin#e2d6e436</li>
 * <li>{@link TLChatParticipantCreator: chatParticipantCreator#da13538a</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsChatParticipant extends TLObject {
    public TLAbsChatParticipant() {
    }
}
