package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputDocumentFileLocation: inputDocumentFileLocation#430f0724</li>
 * <li>{@link TLInputEncryptedFileLocation: inputEncryptedFileLocation#f5235d55</li>
 * <li>{@link TLInputFileLocation: inputFileLocation#14637196</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputFileLocation extends TLObject {
    public TLAbsInputFileLocation() {
    }
}
