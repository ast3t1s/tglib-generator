package org.telegram.schema.upload;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLCdnFile: upload.cdnFile#a99fca4f</li>
 * <li>{@link TLCdnFileReuploadNeeded: upload.cdnFileReuploadNeeded#eea8e46e</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsCdnFile extends TLObject {
    public TLAbsCdnFile() {
    }
}
