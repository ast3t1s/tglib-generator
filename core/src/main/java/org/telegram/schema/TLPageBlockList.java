package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLVector;

/**
 * @author Telegram Schema Generator
 */
public class TLPageBlockList extends TLAbsPageBlock {
    public static final int CLASS_ID = 0x3a58c7f4;

    protected boolean ordered;

    protected TLVector<TLAbsRichText> items;

    public TLPageBlockList() {
    }

    public TLPageBlockList(boolean ordered, TLVector<TLAbsRichText> items) {
        this.ordered = ordered;
        this.items = items;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeBoolean(ordered, stream);
        writeTLVector(items, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        ordered = readTLBool(stream);
        items = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_BOOLEAN;
        size += items.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "pageBlockList#3a58c7f4";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getOrdered() {
        return ordered;
    }

    public void setOrdered(boolean ordered) {
        this.ordered = ordered;
    }

    public TLVector<TLAbsRichText> getItems() {
        return items;
    }

    public void setItems(TLVector<TLAbsRichText> items) {
        this.items = items;
    }
}
