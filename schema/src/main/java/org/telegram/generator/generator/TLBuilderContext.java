package org.telegram.generator.generator;

import com.squareup.javapoet.*;
import org.telegram.generator.model.TLAbsConstructor;
import org.telegram.generator.model.TLConstructor;
import org.telegram.generator.model.TLDefTypeModel;
import org.telegram.generator.model.TLMethod;
import org.telegram.generator.util.Util;

import javax.lang.model.element.Modifier;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static javax.lang.model.element.Modifier.PUBLIC;
import static org.telegram.generator.model.TLDefTypeModel.TLType;
import static org.telegram.generator.generator.GeneratorClassConst.*;
import static org.telegram.generator.util.TLStringUtils.className;
import static org.telegram.generator.util.TLStringUtils.getLastElement;

public class TLBuilderContext {

    private final HashMap<TLType, ClassName> typeClassNameMap = new HashMap<>();
    private final HashMap<TLConstructor, ClassName> constructorClassNameMap = new HashMap<>();

    private final HashMap<TLConstructor, Boolean> emptyConstructorAbstractedMap = new HashMap<>();
    private final ArrayList<TLConstructor> contextConstructors = new ArrayList<>();

    private TypeSpec.Builder apiClazz;

    private MethodSpec.Builder apiMethod;

    private TypeSpec.Builder apiWrapperClazz;

    private MethodSpec.Builder apiWrapperMethod;

    private ContextCallback callback;

    public TLBuilderContext(ContextCallback callback) {
        this.callback = callback;
        apiClazz = TypeSpec.interfaceBuilder(TELEGRAM_API_INTERFACE)
                .addModifiers(PUBLIC)
                .addJavadoc(DEFAULT_AUTHOR)
                .addAnnotation(AnnotationSpec.builder(SuppressWarnings.class)
                        .addMember("value", "{\"unused\", \"unchecked\", \"RedundantCast\"}")
                        .build());
        apiWrapperClazz = TypeSpec.classBuilder(TELEGRAM_API_WRAPPER)
                .addModifiers(PUBLIC, Modifier.ABSTRACT)
                .addJavadoc(DEFAULT_AUTHOR)
                .addSuperinterface(TYPE_TELEGRAM_API)
                .addAnnotation(AnnotationSpec.builder(SuppressWarnings.class)
                        .addMember("value", "{\"unused\", \"unchecked\", \"RedundantCast\"}")
                        .build());
    }

    public List<TLConstructor> getSortedConstructors() {
        return contextConstructors.stream().sorted().collect(Collectors.toList());
    }

    public int getCountOfConstructors() {
        return contextConstructors.size();
    }

    public TypeSpec getClazzTypeSpec() {
        return apiClazz.build();
    }

    public TypeSpec getClazzWrapperSpec() {
        return apiWrapperClazz.build();
    }

    public ClassName getClassConstructor(TLConstructor constructor) {
        return this.constructorClassNameMap.get(constructor);
    }

    public ClassName getClassType(TLType tlType) {
        return this.typeClassNameMap.get(tlType);
    }

    public void addMapConstructor(TLConstructor constructor, ClassName className) {
        this.constructorClassNameMap.put(constructor, className);
    }

    public void addTypeClass(TLType tlType, ClassName className) {
        this.typeClassNameMap.put(tlType, className);
    }

    public void addEmptyAbstractedConstructor(TLConstructor constructor, boolean isEmtpy) {
        this.emptyConstructorAbstractedMap.put(constructor, isEmtpy);
    }

    public void fillContextConstructors(List<TLConstructor> constructors) {
        this.contextConstructors.addAll(constructors);
    }

    public void updateApiWrapperClazz() {
        apiWrapperClazz.addMethod(MethodSpec.methodBuilder("executeRpcQuery")
                .addException(TYPE_RPC_EXCEPTION).addException(IOException.class)
                .addModifiers(PUBLIC, Modifier.ABSTRACT)
                .addTypeVariable(TypeVariableName.get("T", TYPE_TL_OBJECT))
                .returns(TypeVariableName.get("T"))
                .addParameter(ParameterizedTypeName.get(TYPE_TL_METHOD, TypeVariableName.get("T")), "method")
                .build());
    }

    public TypeSpec generateAbstractConstructorClass(TLAbsConstructor constructor) {
        String clazzName = className(constructor.getName(), true, false);
        TypeSpec.Builder clazz = TypeSpec.classBuilder(clazzName)
                .addModifiers(PUBLIC, Modifier.ABSTRACT)
                .superclass(TYPE_TL_OBJECT);

        callback.onGeneratedClass(clazz, ClassName.get("", clazzName), constructor.getName(), null, constructor.getParameters());

        List<TLConstructor> constructors = contextConstructors.stream()
                .filter(constructor1 -> constructor1.getTlType() == constructor.getTlType())
                .collect(Collectors.toList());

        clazz.addJavadoc("Abstraction level for the following constructors:\n")
                .addJavadoc("<ul>\n");

        constructors.forEach(tlConstructor -> clazz.addJavadoc("<li>{@link " +
                Util.last(constructorClassNameMap.get(tlConstructor).toString().split("\\."))
                + ": " + tlConstructor.getName() + "#" + Util.hex(tlConstructor.getId()) + "</li>\n"));

        clazz.addJavadoc("</ul>\n\n");

        if (constructor.isAbstractEmptyConstructor()) {
            TLConstructor nonEmptyConstructor = constructors.stream()
                    .filter(constructor12 -> !constructor12.getName().toLowerCase().endsWith("empty")).findAny().get();
            TLConstructor emptyConstructor = constructors.stream()
                    .filter(tlConstructor -> tlConstructor.getName().toLowerCase().endsWith("empty")).findAny().get();

            clazz.addMethod(MethodSpec.methodBuilder("isEmpty")
                    .addModifiers(PUBLIC, Modifier.ABSTRACT)
                    .returns(TypeName.BOOLEAN)
                    .build());

            clazz.addMethod(MethodSpec.methodBuilder("isNotEmpty")
                    .addModifiers(PUBLIC, Modifier.ABSTRACT)
                    .returns(TypeName.BOOLEAN)
                    .build());

            clazz.addMethod(MethodSpec.methodBuilder("getAs" + Util.uCamelCase(getLastElement(nonEmptyConstructor.getName().split("\\."))))
                    .addModifiers(PUBLIC)
                    .returns(constructorClassNameMap.get(nonEmptyConstructor))
                    .addStatement("return null")
                    .build());

            if (constructor.getName().toLowerCase().startsWith("input")) {
                clazz.addMethod(MethodSpec.methodBuilder("newEmpty")
                        .addModifiers(PUBLIC, Modifier.STATIC)
                        .returns(constructorClassNameMap.get(emptyConstructor))
                        .addStatement("return new $T()", constructorClassNameMap.get(emptyConstructor))
                        .build());

                clazz.addMethod(MethodSpec.methodBuilder("newNotEmpty")
                        .addModifiers(PUBLIC, Modifier.STATIC)
                        .returns(constructorClassNameMap.get(nonEmptyConstructor))
                        .addStatement("return new $T()", constructorClassNameMap.get(nonEmptyConstructor))
                        .build());
            }
        }

        return clazz
                .addJavadoc(DEFAULT_AUTHOR)
                .build();
    }

    public TypeSpec generateConstructorClass(TLConstructor constructor, ClassName superClass) {
        TypeSpec.Builder clazz = TypeSpec.classBuilder(className(constructor.getName(), false, false))
                .addModifiers(PUBLIC)
                .superclass(superClass);
        ClassName clazzTypeName = constructorClassNameMap.get(constructor);

        callback.onGeneratedClass(clazz, clazzTypeName, constructor.getName(), constructor.getId(), constructor.getParameters());

        if (emptyConstructorAbstractedMap.getOrDefault(constructor, false)) {
            clazz.addMethod(MethodSpec.methodBuilder("isEmpty")
                    .addModifiers(PUBLIC, Modifier.FINAL)
                    .addAnnotation(Override.class)
                    .returns(TypeName.BOOLEAN)
                    .addStatement("return $L", constructor.getName().toLowerCase().endsWith("empty"))
                    .build());

            clazz.addMethod(MethodSpec.methodBuilder("isNotEmpty")
                    .addModifiers(PUBLIC, Modifier.FINAL)
                    .addAnnotation(Override.class)
                    .returns(TypeName.BOOLEAN)
                    .addStatement("return $L", !constructor.getName().toLowerCase().endsWith("empty"))
                    .build());

            if (!constructor.getName().toLowerCase().endsWith("empty")) {
                clazz.addMethod(MethodSpec.methodBuilder("getAs" + Util.uCamelCase(Util.last(constructor.getName().split("\\."))))
                        .addModifiers(PUBLIC, Modifier.FINAL)
                        .addAnnotation(Override.class)
                        .returns(clazzTypeName)
                        .addStatement("return this")
                        .build());
            }
        }

        return clazz
                .addJavadoc(DEFAULT_AUTHOR)
                .build();
    }

    public TypeSpec generateMethodClass(TLMethod method) {
        String clazzName = className(method.getName(), false, true);
        TypeName responseType = getType(method.getTlType(), true);
        ClassName clazzTypeName = ClassName.get(PACKAGE_TL_API_REQUEST, clazzName);
        TypeSpec.Builder clazz = TypeSpec.classBuilder(clazzName)
                .addModifiers(PUBLIC)
                .addJavadoc(DEFAULT_AUTHOR)
                .superclass(ParameterizedTypeName.get(TYPE_TL_METHOD, responseType));

        MethodSpec.Builder deserializeResponseMethod = MethodSpec.methodBuilder("deserializeResponse")
                .addModifiers(PUBLIC)
                .addAnnotation(Override.class)
                .addAnnotation(AnnotationSpec.builder(SuppressWarnings.class)
                        .addMember("value", "{\"unchecked\", \"SimplifiableConditionalExpression\"}")
                        .build())
                .addParameter(InputStream.class, "stream")
                .addParameter(TYPE_TL_CONTEXT, "context")
                .addException(IOException.class)
                .returns(responseType);

        if (method.getTlType() instanceof TLDefTypeModel.TLTypeAny) {
            clazz.addTypeVariable(TypeVariableName.get("T", TYPE_TL_OBJECT));
            deserializeResponseMethod.addStatement("return query.deserializeResponse(stream, context)", TypeVariableName.get("T"));
        } else if (method.getTlType() instanceof TLDefTypeModel.TLTypeGeneric) {
            if (responseType == TYPE_TL_INT_VECTOR)
                deserializeResponseMethod.addStatement("return readTLIntVector(stream, context)");
            else if (responseType == TYPE_TL_LONG_VECTOR)
                deserializeResponseMethod.addStatement("return readTLLongVector(stream, context)");
            else if (responseType == TYPE_TL_STRING_VECTOR)
                deserializeResponseMethod.addStatement("return readTLStringVector(stream, context)");
            else
                deserializeResponseMethod.addStatement("return readTLVector(stream, context)");
        } else {
            deserializeResponseMethod.addStatement("final $T response = readTLObject(stream, context)", TYPE_TL_OBJECT)
                    .beginControlFlow("if (response == null)")
                    .addStatement("throw new $T(\"Unable to parse response\")", IOException.class)
                    .endControlFlow()
                    .beginControlFlow("if (!(response instanceof $T))", responseType instanceof ParameterizedTypeName ? ((ParameterizedTypeName) responseType).rawType : responseType)
                    .addStatement("throw new $T(\"Incorrect response type, expected \" + getClass().getCanonicalName() + \", found \" + response.getClass().getCanonicalName())", IOException.class)
                    .endControlFlow()
                    .addStatement("return ($T) response", responseType);
        }
        clazz.addMethod(deserializeResponseMethod.build());

        apiMethod = MethodSpec.methodBuilder(Util.lCamelCase(method.getName()))
                .addModifiers(PUBLIC, Modifier.ABSTRACT)
                .addException(TYPE_RPC_EXCEPTION)
                .addException(IOException.class)
                .returns(responseType);

        apiWrapperMethod = MethodSpec.methodBuilder(Util.lCamelCase(method.getName()))
                .addModifiers(PUBLIC).addAnnotation(Override.class)
                .addException(TYPE_RPC_EXCEPTION).addException(IOException.class)
                .returns(responseType)
                .addStatement("return ($T) executeRpcQuery(new $T($L))", responseType, clazzTypeName,
                        !method.getParameters().isEmpty() ? method.getParameters().stream()
                                .filter(tlParameter -> !(tlParameter.getTlType() instanceof TLDefTypeModel.TLTypeFlag))
                                .map(tlParameter -> Util.javaEscape(Util.lCamelCase(tlParameter.getName())))
                                .collect(Collectors.joining(", ")) : "");
        callback.onGeneratedClass(clazz, clazzTypeName, method.getName(), method.getId(), method.getParameters());
        apiClazz.addMethod(apiMethod.build());
        apiWrapperClazz.addMethod(apiWrapperMethod.build());
        apiMethod = null;
        apiWrapperMethod = null;

        return clazz.build();
    }

    public TypeName getType(TLType type, boolean boxedIntTl) {
        if (type instanceof TLDefTypeModel.TLTypeFunctional) {
            return ParameterizedTypeName.get(TYPE_TL_METHOD, TypeVariableName.get("T"));
        } else if (type instanceof TLDefTypeModel.TLTypeAny) {
            return TypeVariableName.get("T");
        } else if (type instanceof TLDefTypeModel.TLTypeFlag) {
            return TypeName.INT;
        } else if (type instanceof TLDefTypeModel.TLTypeConditional) {
            TypeName realTlType = getType(((TLDefTypeModel.TLTypeConditional) type).getRealType(), false);
            if (!(((TLDefTypeModel.TLTypeConditional) type).getRealType() instanceof TLDefTypeModel.TLTypeRaw) || !Arrays.asList("true", "false", "Bool").contains(((TLDefTypeModel.TLTypeRaw) ((TLDefTypeModel.TLTypeConditional) type).getRealType()).getName()))
                return realTlType.box();
            else
                return realTlType;
        } else if (type instanceof TLDefTypeModel.TLTypeGeneric) {
            TLDefTypeModel.TLTypeRaw tlType = ((TLDefTypeModel.TLTypeRaw) Arrays.stream(((TLDefTypeModel.TLTypeGeneric) type).getGenerics()).findFirst().get());
            String primitive = tlType.getName();
            if (primitive.equalsIgnoreCase("int")) {
                return TYPE_TL_INT_VECTOR;
            } else if (primitive.equalsIgnoreCase("long")) {
                return TYPE_TL_LONG_VECTOR;
            } else if (primitive.equalsIgnoreCase("string")) {
                return TYPE_TL_STRING_VECTOR;
            } else {
                return ParameterizedTypeName.get(TYPE_TL_VECTOR,  getType(tlType, true).box());
            }
        } else if (type instanceof TLDefTypeModel.TLTypeRaw) {
            String name = ((TLDefTypeModel.TLTypeRaw) type).getName();
            return getRawType(type, name, boxedIntTl);
        } else {
            throw new RuntimeException("Unsupported type " + type);
        }
    }

    private TypeName getRawType(TLType type, String name, boolean boxedIntTl) {
        switch (name) {
            case "int":
                return TypeName.INT;
            case "long":
                return TypeName.LONG;
            case "double":
                return TypeName.DOUBLE;
            case "float":
                return TypeName.FLOAT;
            case "string":
                return TypeName.get(String.class);
            case "bytes":
                return TYPE_TL_BYTES;
            case "Bool": case "true": case "false":
                return boxedIntTl ? TYPE_TL_BOOL : TypeName.BOOLEAN;
            default:
                if (typeClassNameMap.containsKey(type))
                    return typeClassNameMap.get(type);
                else
                    throw new RuntimeException("Unknown type " + type);
        }
    }

    public void updateApiWrapperState(TLType tlType, TypeName fieldType, String fieldName) {
        if (apiMethod != null) {
            apiMethod.addParameter(fieldType, fieldName);
            apiWrapperMethod.addParameter(fieldType, fieldName);
            if (tlType instanceof TLDefTypeModel.TLTypeFunctional) {
                apiMethod.addTypeVariable(TypeVariableName.get("T", TYPE_TL_OBJECT));
                apiWrapperMethod.addTypeVariable(TypeVariableName.get("T", TYPE_TL_OBJECT));
            }
        }
    }

    public MethodSpec generateGetter(String realName, String fieldName, TypeName fieldType) {
        return MethodSpec.methodBuilder("get" + Util.uCamelCase(realName))
                .addModifiers(Modifier.PUBLIC)
                .returns(fieldType)
                .addStatement("return $L", fieldName)
                .build();
    }

    public MethodSpec generateSetter(String realName, String fieldName, TypeName fieldType) {
        return MethodSpec.methodBuilder("set" + Util.uCamelCase(realName))
                .addModifiers(Modifier.PUBLIC)
                .addParameter(fieldType, fieldName)
                .addStatement("this.$L = $L", fieldName, fieldName)
                .build();
    }

}
