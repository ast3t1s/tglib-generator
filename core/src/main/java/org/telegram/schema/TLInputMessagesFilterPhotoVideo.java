package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputMessagesFilterPhotoVideo extends TLAbsMessagesFilter {
    public static final int CLASS_ID = 0x56e9f0e4;

    public TLInputMessagesFilterPhotoVideo() {
    }

    @Override
    public String toString() {
        return "inputMessagesFilterPhotoVideo#56e9f0e4";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
