package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLFeaturedStickersNotModified extends TLAbsFeaturedStickers {
    public static final int CLASS_ID = 0x4ede3cf;

    public TLFeaturedStickersNotModified() {
    }

    @Override
    public String toString() {
        return "messages.featuredStickersNotModified#4ede3cf";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
