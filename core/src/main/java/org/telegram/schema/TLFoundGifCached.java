package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLFoundGifCached extends TLAbsFoundGif {
    public static final int CLASS_ID = 0x9c750409;

    protected String url;

    protected TLAbsPhoto photo;

    protected TLAbsDocument document;

    public TLFoundGifCached() {
    }

    public TLFoundGifCached(String url, TLAbsPhoto photo, TLAbsDocument document) {
        this.url = url;
        this.photo = photo;
        this.document = document;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(url, stream);
        writeTLObject(photo, stream);
        writeTLObject(document, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        url = readTLString(stream);
        photo = readTLObject(stream, context, TLAbsPhoto.class, -1);
        document = readTLObject(stream, context, TLAbsDocument.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(url);
        size += photo.computeSerializedSize();
        size += document.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "foundGifCached#9c750409";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public TLAbsPhoto getPhoto() {
        return photo;
    }

    public void setPhoto(TLAbsPhoto photo) {
        this.photo = photo;
    }

    public TLAbsDocument getDocument() {
        return document;
    }

    public void setDocument(TLAbsDocument document) {
        this.document = document;
    }
}
