package org.telegram.schema.storage;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLFileMp3 extends TLAbsFileType {
    public static final int CLASS_ID = 0x528a0677;

    public TLFileMp3() {
    }

    @Override
    public String toString() {
        return "storage.fileMp3#528a0677";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
