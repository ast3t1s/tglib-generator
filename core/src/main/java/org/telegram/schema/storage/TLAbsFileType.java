package org.telegram.schema.storage;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLFileGif: storage.fileGif#cae1aadf</li>
 * <li>{@link TLFileJpeg: storage.fileJpeg#7efe0e</li>
 * <li>{@link TLFileMov: storage.fileMov#4b09ebbc</li>
 * <li>{@link TLFileMp3: storage.fileMp3#528a0677</li>
 * <li>{@link TLFileMp4: storage.fileMp4#b3cea0e4</li>
 * <li>{@link TLFilePartial: storage.filePartial#40bc6f52</li>
 * <li>{@link TLFilePdf: storage.filePdf#ae1e508d</li>
 * <li>{@link TLFilePng: storage.filePng#a4f63c0</li>
 * <li>{@link TLFileUnknown: storage.fileUnknown#aa963b05</li>
 * <li>{@link TLFileWebp: storage.fileWebp#1081464c</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsFileType extends TLObject {
    public TLAbsFileType() {
    }
}
