package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLPageFull: pageFull#d7a19d69</li>
 * <li>{@link TLPagePart: pagePart#8dee6c44</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsPage extends TLObject {
    public TLAbsPage() {
    }
}
