package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLLongVector;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLStickerPack extends TLObject {
    public static final int CLASS_ID = 0x12b299d4;

    protected String emoticon;

    protected TLLongVector documents;

    public TLStickerPack() {
    }

    public TLStickerPack(String emoticon, TLLongVector documents) {
        this.emoticon = emoticon;
        this.documents = documents;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(emoticon, stream);
        writeTLVector(documents, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        emoticon = readTLString(stream);
        documents = readTLLongVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(emoticon);
        size += documents.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "stickerPack#12b299d4";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getEmoticon() {
        return emoticon;
    }

    public void setEmoticon(String emoticon) {
        this.emoticon = emoticon;
    }

    public TLLongVector getDocuments() {
        return documents;
    }

    public void setDocuments(TLLongVector documents) {
        this.documents = documents;
    }
}
