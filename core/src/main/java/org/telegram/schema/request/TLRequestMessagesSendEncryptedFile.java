package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBytes;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLAbsInputEncryptedFile;
import org.telegram.schema.TLInputEncryptedChat;
import org.telegram.schema.messages.TLAbsSentEncryptedMessage;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestMessagesSendEncryptedFile extends TLMethod<TLAbsSentEncryptedMessage> {
    public static final int CLASS_ID = 0x9a901b66;

    protected TLInputEncryptedChat peer;

    protected long randomId;

    protected TLBytes data;

    protected TLAbsInputEncryptedFile file;

    public TLRequestMessagesSendEncryptedFile() {
    }

    public TLRequestMessagesSendEncryptedFile(TLInputEncryptedChat peer, long randomId,
            TLBytes data, TLAbsInputEncryptedFile file) {
        this.peer = peer;
        this.randomId = randomId;
        this.data = data;
        this.file = file;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLAbsSentEncryptedMessage deserializeResponse(InputStream stream, TLContext context)
            throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLAbsSentEncryptedMessage)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLAbsSentEncryptedMessage) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(peer, stream);
        writeLong(randomId, stream);
        writeTLBytes(data, stream);
        writeTLObject(file, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        peer = readTLObject(stream, context, TLInputEncryptedChat.class, TLInputEncryptedChat.CLASS_ID);
        randomId = readLong(stream);
        data = readTLBytes(stream, context);
        file = readTLObject(stream, context, TLAbsInputEncryptedFile.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += peer.computeSerializedSize();
        size += SIZE_INT64;
        size += computeTLBytesSerializedSize(data);
        size += file.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messages.sendEncryptedFile#9a901b66";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLInputEncryptedChat getPeer() {
        return peer;
    }

    public void setPeer(TLInputEncryptedChat peer) {
        this.peer = peer;
    }

    public long getRandomId() {
        return randomId;
    }

    public void setRandomId(long randomId) {
        this.randomId = randomId;
    }

    public TLBytes getData() {
        return data;
    }

    public void setData(TLBytes data) {
        this.data = data;
    }

    public TLAbsInputEncryptedFile getFile() {
        return file;
    }

    public void setFile(TLAbsInputEncryptedFile file) {
        this.file = file;
    }
}
