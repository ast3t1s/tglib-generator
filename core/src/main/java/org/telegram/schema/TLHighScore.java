package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLHighScore extends TLObject {
    public static final int CLASS_ID = 0x58fffcd0;

    protected int pos;

    protected int userId;

    protected int score;

    public TLHighScore() {
    }

    public TLHighScore(int pos, int userId, int score) {
        this.pos = pos;
        this.userId = userId;
        this.score = score;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(pos, stream);
        writeInt(userId, stream);
        writeInt(score, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        pos = readInt(stream);
        userId = readInt(stream);
        score = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT32;
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "highScore#58fffcd0";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
