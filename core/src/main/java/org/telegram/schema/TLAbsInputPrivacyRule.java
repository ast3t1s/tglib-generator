package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputPrivacyValueAllowAll: inputPrivacyValueAllowAll#184b35ce</li>
 * <li>{@link TLInputPrivacyValueAllowContacts: inputPrivacyValueAllowContacts#d09e07b</li>
 * <li>{@link TLInputPrivacyValueAllowUsers: inputPrivacyValueAllowUsers#131cc67f</li>
 * <li>{@link TLInputPrivacyValueDisallowAll: inputPrivacyValueDisallowAll#d66b66c9</li>
 * <li>{@link TLInputPrivacyValueDisallowContacts: inputPrivacyValueDisallowContacts#ba52007</li>
 * <li>{@link TLInputPrivacyValueDisallowUsers: inputPrivacyValueDisallowUsers#90110467</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputPrivacyRule extends TLObject {
    public TLAbsInputPrivacyRule() {
    }
}
