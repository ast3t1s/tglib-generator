package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputMessagesFilterRoundVideo extends TLAbsMessagesFilter {
    public static final int CLASS_ID = 0xb549da53;

    public TLInputMessagesFilterRoundVideo() {
    }

    @Override
    public String toString() {
        return "inputMessagesFilterRoundVideo#b549da53";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
