package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLDialogs: messages.dialogs#15ba6c40</li>
 * <li>{@link TLDialogsSlice: messages.dialogsSlice#71e094f3</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsDialogs extends TLObject {
    public TLAbsDialogs() {
    }
}
