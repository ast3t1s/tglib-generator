package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputReportReasonViolence extends TLAbsReportReason {
    public static final int CLASS_ID = 0x1e22c78d;

    public TLInputReportReasonViolence() {
    }

    @Override
    public String toString() {
        return "inputReportReasonViolence#1e22c78d";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
