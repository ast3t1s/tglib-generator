package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLPeerNotifySettingsEmpty extends TLAbsPeerNotifySettings {
    public static final int CLASS_ID = 0x70a68512;

    public TLPeerNotifySettingsEmpty() {
    }

    @Override
    public String toString() {
        return "peerNotifySettingsEmpty#70a68512";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    @Override
    public final boolean isEmpty() {
        return true;
    }

    @Override
    public final boolean isNotEmpty() {
        return false;
    }
}
