package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLAbsUser;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestAccountUpdateProfile extends TLMethod<TLAbsUser> {
    public static final int CLASS_ID = 0x78515775;

    protected int flags;

    protected String firstName;

    protected String lastName;

    protected String about;

    public TLRequestAccountUpdateProfile() {
    }

    public TLRequestAccountUpdateProfile(String firstName, String lastName, String about) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.about = about;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLAbsUser deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLAbsUser)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLAbsUser) response;
    }

    private void computeFlags() {
        flags = 0;
        flags = firstName != null ? (flags | 1) : (flags & ~1);
        flags = lastName != null ? (flags | 2) : (flags & ~2);
        flags = about != null ? (flags | 4) : (flags & ~4);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        if ((flags & 1) != 0) {
            if (firstName == null) throwNullFieldException("firstName" , flags);
            writeString(firstName, stream);
        }
        if ((flags & 2) != 0) {
            if (lastName == null) throwNullFieldException("lastName" , flags);
            writeString(lastName, stream);
        }
        if ((flags & 4) != 0) {
            if (about == null) throwNullFieldException("about" , flags);
            writeString(about, stream);
        }
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        firstName = (flags & 1) != 0 ? readTLString(stream) : null;
        lastName = (flags & 2) != 0 ? readTLString(stream) : null;
        about = (flags & 4) != 0 ? readTLString(stream) : null;
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        if ((flags & 1) != 0) {
            if (firstName == null) throwNullFieldException("firstName" , flags);
            size += computeTLStringSerializedSize(firstName);
        }
        if ((flags & 2) != 0) {
            if (lastName == null) throwNullFieldException("lastName" , flags);
            size += computeTLStringSerializedSize(lastName);
        }
        if ((flags & 4) != 0) {
            if (about == null) throwNullFieldException("about" , flags);
            size += computeTLStringSerializedSize(about);
        }
        return size;
    }

    @Override
    public String toString() {
        return "account.updateProfile#78515775";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }
}
