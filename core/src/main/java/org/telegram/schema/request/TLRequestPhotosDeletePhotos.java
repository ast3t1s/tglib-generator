package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLLongVector;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsInputPhoto;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestPhotosDeletePhotos extends TLMethod<TLLongVector> {
    public static final int CLASS_ID = 0x87cf7f2f;

    protected TLVector<TLAbsInputPhoto> id;

    public TLRequestPhotosDeletePhotos() {
    }

    public TLRequestPhotosDeletePhotos(TLVector<TLAbsInputPhoto> id) {
        this.id = id;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLLongVector deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        return readTLLongVector(stream, context);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(id, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        id = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += id.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "photos.deletePhotos#87cf7f2f";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLVector<TLAbsInputPhoto> getId() {
        return id;
    }

    public void setId(TLVector<TLAbsInputPhoto> id) {
        this.id = id;
    }
}
