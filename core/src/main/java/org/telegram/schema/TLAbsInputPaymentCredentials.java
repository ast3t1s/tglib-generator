package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputPaymentCredentials: inputPaymentCredentials#3417d728</li>
 * <li>{@link TLInputPaymentCredentialsSaved: inputPaymentCredentialsSaved#c10eb2cf</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputPaymentCredentials extends TLObject {
    public TLAbsInputPaymentCredentials() {
    }
}
