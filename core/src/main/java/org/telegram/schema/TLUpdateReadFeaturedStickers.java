package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateReadFeaturedStickers extends TLAbsUpdate {
    public static final int CLASS_ID = 0x571d2742;

    public TLUpdateReadFeaturedStickers() {
    }

    @Override
    public String toString() {
        return "updateReadFeaturedStickers#571d2742";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
