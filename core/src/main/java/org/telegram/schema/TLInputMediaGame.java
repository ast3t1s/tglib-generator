package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLInputMediaGame extends TLAbsInputMedia {
    public static final int CLASS_ID = 0xd33f43f3;

    protected TLAbsInputGame id;

    public TLInputMediaGame() {
    }

    public TLInputMediaGame(TLAbsInputGame id) {
        this.id = id;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(id, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        id = readTLObject(stream, context, TLAbsInputGame.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += id.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "inputMediaGame#d33f43f3";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsInputGame getId() {
        return id;
    }

    public void setId(TLAbsInputGame id) {
        this.id = id;
    }
}
