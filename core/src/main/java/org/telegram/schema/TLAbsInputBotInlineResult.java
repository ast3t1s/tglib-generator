package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputBotInlineResult: inputBotInlineResult#2cbbe15a</li>
 * <li>{@link TLInputBotInlineResultDocument: inputBotInlineResultDocument#fff8fdc4</li>
 * <li>{@link TLInputBotInlineResultGame: inputBotInlineResultGame#4fa417f2</li>
 * <li>{@link TLInputBotInlineResultPhoto: inputBotInlineResultPhoto#a8d864a7</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputBotInlineResult extends TLObject {
    protected TLAbsInputBotInlineMessage sendMessage;

    public TLAbsInputBotInlineResult() {
    }

    public TLAbsInputBotInlineMessage getSendMessage() {
        return sendMessage;
    }

    public void setSendMessage(TLAbsInputBotInlineMessage sendMessage) {
        this.sendMessage = sendMessage;
    }
}
