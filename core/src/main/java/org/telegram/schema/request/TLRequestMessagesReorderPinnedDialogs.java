package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBool;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsInputPeer;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestMessagesReorderPinnedDialogs extends TLMethod<TLBool> {
    public static final int CLASS_ID = 0x959ff644;

    protected int flags;

    protected boolean force;

    protected TLVector<TLAbsInputPeer> order;

    public TLRequestMessagesReorderPinnedDialogs() {
    }

    public TLRequestMessagesReorderPinnedDialogs(boolean force, TLVector<TLAbsInputPeer> order) {
        this.force = force;
        this.order = order;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLBool deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLBool)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLBool) response;
    }

    private void computeFlags() {
        flags = 0;
        flags = force ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeTLVector(order, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        force = (flags & 1) != 0;
        order = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += order.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messages.reorderPinnedDialogs#959ff644";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getForce() {
        return force;
    }

    public void setForce(boolean force) {
        this.force = force;
    }

    public TLVector<TLAbsInputPeer> getOrder() {
        return order;
    }

    public void setOrder(TLVector<TLAbsInputPeer> order) {
        this.order = order;
    }
}
