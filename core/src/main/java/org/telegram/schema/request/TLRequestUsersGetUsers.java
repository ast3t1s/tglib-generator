package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsInputUser;
import org.telegram.schema.TLAbsUser;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestUsersGetUsers extends TLMethod<TLVector<TLAbsUser>> {
    public static final int CLASS_ID = 0xd91a548;

    protected TLVector<TLAbsInputUser> id;

    public TLRequestUsersGetUsers() {
    }

    public TLRequestUsersGetUsers(TLVector<TLAbsInputUser> id) {
        this.id = id;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLVector<TLAbsUser> deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        return readTLVector(stream, context);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(id, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        id = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += id.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "users.getUsers#d91a548";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLVector<TLAbsInputUser> getId() {
        return id;
    }

    public void setId(TLVector<TLAbsInputUser> id) {
        this.id = id;
    }
}
