package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLIntVector;
import org.telegram.core.type.TLMethod;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestContactsExportCard extends TLMethod<TLIntVector> {
    public static final int CLASS_ID = 0x84e53737;

    public TLRequestContactsExportCard() {
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLIntVector deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        return readTLIntVector(stream, context);
    }

    @Override
    public String toString() {
        return "contacts.exportCard#84e53737";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
