package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLPrivacyKeyChatInvite extends TLAbsPrivacyKey {
    public static final int CLASS_ID = 0x500e6dfa;

    public TLPrivacyKeyChatInvite() {
    }

    @Override
    public String toString() {
        return "privacyKeyChatInvite#500e6dfa";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
