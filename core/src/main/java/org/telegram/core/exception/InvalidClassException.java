package org.telegram.core.exception;

public class InvalidClassException extends DeserializationException {

    public InvalidClassException(int foundId, int expectedId) {
        super("Invalid class id. Found " + Integer.toHexString(foundId)
                + ", expected: " + Integer.toHexString(expectedId));
    }

    public InvalidClassException(String message) {
        super(message);
    }
} 
