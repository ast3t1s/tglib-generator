package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLChannelParticipantsRecent extends TLAbsChannelParticipantsFilter {
    public static final int CLASS_ID = 0xde3f3c79;

    public TLChannelParticipantsRecent() {
    }

    @Override
    public String toString() {
        return "channelParticipantsRecent#de3f3c79";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
