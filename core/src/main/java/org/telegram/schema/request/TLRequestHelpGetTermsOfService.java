package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.help.TLTermsOfService;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestHelpGetTermsOfService extends TLMethod<TLTermsOfService> {
    public static final int CLASS_ID = 0x350170f3;

    public TLRequestHelpGetTermsOfService() {
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLTermsOfService deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLTermsOfService)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLTermsOfService) response;
    }

    @Override
    public String toString() {
        return "help.getTermsOfService#350170f3";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
