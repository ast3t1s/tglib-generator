package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLMessageMediaWebPage extends TLAbsMessageMedia {
    public static final int CLASS_ID = 0xa32dd600;

    protected TLAbsWebPage webpage;

    public TLMessageMediaWebPage() {
    }

    public TLMessageMediaWebPage(TLAbsWebPage webpage) {
        this.webpage = webpage;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(webpage, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        webpage = readTLObject(stream, context, TLAbsWebPage.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += webpage.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messageMediaWebPage#a32dd600";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsWebPage getWebpage() {
        return webpage;
    }

    public void setWebpage(TLAbsWebPage webpage) {
        this.webpage = webpage;
    }
}
