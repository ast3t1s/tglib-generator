package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLChatPhotoEmpty extends TLAbsChatPhoto {
    public static final int CLASS_ID = 0x37c1011c;

    public TLChatPhotoEmpty() {
    }

    @Override
    public String toString() {
        return "chatPhotoEmpty#37c1011c";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    @Override
    public final boolean isEmpty() {
        return true;
    }

    @Override
    public final boolean isNotEmpty() {
        return false;
    }
}
