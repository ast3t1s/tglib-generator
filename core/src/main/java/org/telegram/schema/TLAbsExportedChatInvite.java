package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLChatInviteEmpty: chatInviteEmpty#69df3769</li>
 * <li>{@link TLChatInviteExported: chatInviteExported#fc2e05bc</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsExportedChatInvite extends TLObject {
    public TLAbsExportedChatInvite() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLChatInviteExported getAsChatInviteExported() {
        return null;
    }
}
