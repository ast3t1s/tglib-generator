package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLSendMessageGamePlayAction extends TLAbsSendMessageAction {
    public static final int CLASS_ID = 0xdd6a8f48;

    public TLSendMessageGamePlayAction() {
    }

    @Override
    public String toString() {
        return "sendMessageGamePlayAction#dd6a8f48";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
