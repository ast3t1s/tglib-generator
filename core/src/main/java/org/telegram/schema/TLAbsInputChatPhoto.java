package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputChatPhoto: inputChatPhoto#8953ad37</li>
 * <li>{@link TLInputChatPhotoEmpty: inputChatPhotoEmpty#1ca48f57</li>
 * <li>{@link TLInputChatUploadedPhoto: inputChatUploadedPhoto#927c55b4</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputChatPhoto extends TLObject {
    public TLAbsInputChatPhoto() {
    }
}
