package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateBotInlineQuery extends TLAbsUpdate {
    public static final int CLASS_ID = 0x54826690;

    protected int flags;

    protected long queryId;

    protected int userId;

    protected String query;

    protected TLAbsGeoPoint geo;

    protected String offset;

    public TLUpdateBotInlineQuery() {
    }

    public TLUpdateBotInlineQuery(long queryId, int userId, String query, TLAbsGeoPoint geo,
            String offset) {
        this.queryId = queryId;
        this.userId = userId;
        this.query = query;
        this.geo = geo;
        this.offset = offset;
    }

    private void computeFlags() {
        flags = 0;
        flags = geo != null ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeLong(queryId, stream);
        writeInt(userId, stream);
        writeString(query, stream);
        if ((flags & 1) != 0) {
            if (geo == null) throwNullFieldException("geo" , flags);
            writeTLObject(geo, stream);
        }
        writeString(offset, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        queryId = readLong(stream);
        userId = readInt(stream);
        query = readTLString(stream);
        geo = (flags & 1) != 0 ? readTLObject(stream, context, TLAbsGeoPoint.class, -1) : null;
        offset = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT64;
        size += SIZE_INT32;
        size += computeTLStringSerializedSize(query);
        if ((flags & 1) != 0) {
            if (geo == null) throwNullFieldException("geo" , flags);
            size += geo.computeSerializedSize();
        }
        size += computeTLStringSerializedSize(offset);
        return size;
    }

    @Override
    public String toString() {
        return "updateBotInlineQuery#54826690";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public TLAbsGeoPoint getGeo() {
        return geo;
    }

    public void setGeo(TLAbsGeoPoint geo) {
        this.geo = geo;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }
}
