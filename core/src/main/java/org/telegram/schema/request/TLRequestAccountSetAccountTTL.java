package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBool;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLAccountDaysTTL;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestAccountSetAccountTTL extends TLMethod<TLBool> {
    public static final int CLASS_ID = 0x2442485e;

    protected TLAccountDaysTTL ttl;

    public TLRequestAccountSetAccountTTL() {
    }

    public TLRequestAccountSetAccountTTL(TLAccountDaysTTL ttl) {
        this.ttl = ttl;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLBool deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLBool)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLBool) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(ttl, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        ttl = readTLObject(stream, context, TLAccountDaysTTL.class, TLAccountDaysTTL.CLASS_ID);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += ttl.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "account.setAccountTTL#2442485e";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAccountDaysTTL getTtl() {
        return ttl;
    }

    public void setTtl(TLAccountDaysTTL ttl) {
        this.ttl = ttl;
    }
}
