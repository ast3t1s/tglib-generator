package org.telegram.schema.updates;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLChannelDifference: updates.channelDifference#2064674e</li>
 * <li>{@link TLChannelDifferenceEmpty: updates.channelDifferenceEmpty#3e11affb</li>
 * <li>{@link TLChannelDifferenceTooLong: updates.channelDifferenceTooLong#410dee07</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsChannelDifference extends TLObject {
    public TLAbsChannelDifference() {
    }
}
