package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLPhoneCallDiscardReasonBusy: phoneCallDiscardReasonBusy#faf7e8c9</li>
 * <li>{@link TLPhoneCallDiscardReasonDisconnect: phoneCallDiscardReasonDisconnect#e095c1a0</li>
 * <li>{@link TLPhoneCallDiscardReasonHangup: phoneCallDiscardReasonHangup#57adc690</li>
 * <li>{@link TLPhoneCallDiscardReasonMissed: phoneCallDiscardReasonMissed#85e42301</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsPhoneCallDiscardReason extends TLObject {
    public TLAbsPhoneCallDiscardReason() {
    }
}
