package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBytes;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.account.TLTmpPassword;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestAccountGetTmpPassword extends TLMethod<TLTmpPassword> {
    public static final int CLASS_ID = 0x4a82327e;

    protected TLBytes passwordHash;

    protected int period;

    public TLRequestAccountGetTmpPassword() {
    }

    public TLRequestAccountGetTmpPassword(TLBytes passwordHash, int period) {
        this.passwordHash = passwordHash;
        this.period = period;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLTmpPassword deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLTmpPassword)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLTmpPassword) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLBytes(passwordHash, stream);
        writeInt(period, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        passwordHash = readTLBytes(stream, context);
        period = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLBytesSerializedSize(passwordHash);
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "account.getTmpPassword#4a82327e";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLBytes getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(TLBytes passwordHash) {
        this.passwordHash = passwordHash;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }
}
