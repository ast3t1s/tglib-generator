package org.telegram.schema.help;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLNoAppUpdate extends TLAbsAppUpdate {
    public static final int CLASS_ID = 0xc45a6536;

    public TLNoAppUpdate() {
    }

    @Override
    public String toString() {
        return "help.noAppUpdate#c45a6536";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
