package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.lang.Integer;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.exception.RpcException;
import org.telegram.core.type.TLBool;
import org.telegram.core.type.TLBytes;
import org.telegram.core.type.TLIntVector;
import org.telegram.core.type.TLLongVector;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLStringVector;
import org.telegram.core.type.TLVector;
import org.telegram.schema.account.TLAbsPassword;
import org.telegram.schema.account.TLAuthorizations;
import org.telegram.schema.account.TLPasswordInputSettings;
import org.telegram.schema.account.TLPasswordSettings;
import org.telegram.schema.account.TLPrivacyRules;
import org.telegram.schema.account.TLTmpPassword;
import org.telegram.schema.auth.TLAuthorization;
import org.telegram.schema.auth.TLCheckedPhone;
import org.telegram.schema.auth.TLExportedAuthorization;
import org.telegram.schema.auth.TLPasswordRecovery;
import org.telegram.schema.auth.TLSentCode;
import org.telegram.schema.channels.TLChannelParticipant;
import org.telegram.schema.channels.TLChannelParticipants;
import org.telegram.schema.contacts.TLAbsBlocked;
import org.telegram.schema.contacts.TLAbsContacts;
import org.telegram.schema.contacts.TLAbsTopPeers;
import org.telegram.schema.contacts.TLFound;
import org.telegram.schema.contacts.TLImportedContacts;
import org.telegram.schema.contacts.TLLink;
import org.telegram.schema.contacts.TLResolvedPeer;
import org.telegram.schema.help.TLAbsAppUpdate;
import org.telegram.schema.help.TLInviteText;
import org.telegram.schema.help.TLSupport;
import org.telegram.schema.help.TLTermsOfService;
import org.telegram.schema.messages.TLAbsAllStickers;
import org.telegram.schema.messages.TLAbsChats;
import org.telegram.schema.messages.TLAbsDhConfig;
import org.telegram.schema.messages.TLAbsDialogs;
import org.telegram.schema.messages.TLAbsFeaturedStickers;
import org.telegram.schema.messages.TLAbsMessages;
import org.telegram.schema.messages.TLAbsRecentStickers;
import org.telegram.schema.messages.TLAbsSavedGifs;
import org.telegram.schema.messages.TLAbsSentEncryptedMessage;
import org.telegram.schema.messages.TLAbsStickerSetInstallResult;
import org.telegram.schema.messages.TLAffectedHistory;
import org.telegram.schema.messages.TLAffectedMessages;
import org.telegram.schema.messages.TLArchivedStickers;
import org.telegram.schema.messages.TLBotCallbackAnswer;
import org.telegram.schema.messages.TLBotResults;
import org.telegram.schema.messages.TLChatFull;
import org.telegram.schema.messages.TLFoundGifs;
import org.telegram.schema.messages.TLHighScores;
import org.telegram.schema.messages.TLMessageEditData;
import org.telegram.schema.messages.TLPeerDialogs;
import org.telegram.schema.messages.TLStickerSet;
import org.telegram.schema.payments.TLAbsPaymentResult;
import org.telegram.schema.payments.TLPaymentForm;
import org.telegram.schema.payments.TLPaymentReceipt;
import org.telegram.schema.payments.TLSavedInfo;
import org.telegram.schema.payments.TLValidatedRequestedInfo;
import org.telegram.schema.phone.TLPhoneCall;
import org.telegram.schema.photos.TLAbsPhotos;
import org.telegram.schema.photos.TLPhoto;
import org.telegram.schema.updates.TLAbsChannelDifference;
import org.telegram.schema.updates.TLAbsDifference;
import org.telegram.schema.updates.TLState;
import org.telegram.schema.upload.TLAbsCdnFile;
import org.telegram.schema.upload.TLAbsFile;
import org.telegram.schema.upload.TLWebFile;

/**
 * @author Telegram Schema Generator
 */
@SuppressWarnings({"unused", "unchecked", "RedundantCast"})
public interface TelegramApi {
    TLAbsUser accountChangePhone(String phoneNumber, String phoneCodeHash, String phoneCode) throws
            RpcException, IOException;

    TLBool accountCheckUsername(String username) throws RpcException, IOException;

    TLBool accountConfirmPhone(String phoneCodeHash, String phoneCode) throws RpcException,
            IOException;

    TLBool accountDeleteAccount(String reason) throws RpcException, IOException;

    TLAccountDaysTTL accountGetAccountTTL() throws RpcException, IOException;

    TLAuthorizations accountGetAuthorizations() throws RpcException, IOException;

    TLAbsPeerNotifySettings accountGetNotifySettings(TLAbsInputNotifyPeer peer) throws RpcException,
            IOException;

    TLAbsPassword accountGetPassword() throws RpcException, IOException;

    TLPasswordSettings accountGetPasswordSettings(TLBytes currentPasswordHash) throws RpcException,
            IOException;

    TLPrivacyRules accountGetPrivacy(TLAbsInputPrivacyKey key) throws RpcException, IOException;

    TLTmpPassword accountGetTmpPassword(TLBytes passwordHash, int period) throws RpcException,
            IOException;

    TLVector<TLAbsWallPaper> accountGetWallPapers() throws RpcException, IOException;

    TLBool accountRegisterDevice(int tokenType, String token) throws RpcException, IOException;

    TLBool accountReportPeer(TLAbsInputPeer peer, TLAbsReportReason reason) throws RpcException,
            IOException;

    TLBool accountResetAuthorization(long hash) throws RpcException, IOException;

    TLBool accountResetNotifySettings() throws RpcException, IOException;

    TLSentCode accountSendChangePhoneCode(boolean allowFlashcall, String phoneNumber,
            boolean currentNumber) throws RpcException, IOException;

    TLSentCode accountSendConfirmPhoneCode(boolean allowFlashcall, String hash,
            boolean currentNumber) throws RpcException, IOException;

    TLBool accountSetAccountTTL(TLAccountDaysTTL ttl) throws RpcException, IOException;

    TLPrivacyRules accountSetPrivacy(TLAbsInputPrivacyKey key,
            TLVector<TLAbsInputPrivacyRule> rules) throws RpcException, IOException;

    TLBool accountUnregisterDevice(int tokenType, String token) throws RpcException, IOException;

    TLBool accountUpdateDeviceLocked(int period) throws RpcException, IOException;

    TLBool accountUpdateNotifySettings(TLAbsInputNotifyPeer peer,
            TLInputPeerNotifySettings settings) throws RpcException, IOException;

    TLBool accountUpdatePasswordSettings(TLBytes currentPasswordHash,
            TLPasswordInputSettings newSettings) throws RpcException, IOException;

    TLAbsUser accountUpdateProfile(String firstName, String lastName, String about) throws
            RpcException, IOException;

    TLBool accountUpdateStatus(boolean offline) throws RpcException, IOException;

    TLAbsUser accountUpdateUsername(String username) throws RpcException, IOException;

    TLBool authBindTempAuthKey(long permAuthKeyId, long nonce, int expiresAt,
            TLBytes encryptedMessage) throws RpcException, IOException;

    TLBool authCancelCode(String phoneNumber, String phoneCodeHash) throws RpcException,
            IOException;

    TLAuthorization authCheckPassword(TLBytes passwordHash) throws RpcException, IOException;

    TLCheckedPhone authCheckPhone(String phoneNumber) throws RpcException, IOException;

    TLBool authDropTempAuthKeys(TLLongVector exceptAuthKeys) throws RpcException, IOException;

    TLExportedAuthorization authExportAuthorization(int dcId) throws RpcException, IOException;

    TLAuthorization authImportAuthorization(int id, TLBytes bytes) throws RpcException, IOException;

    TLAuthorization authImportBotAuthorization(int flags, int apiId, String apiHash,
            String botAuthToken) throws RpcException, IOException;

    TLBool authLogOut() throws RpcException, IOException;

    TLAuthorization authRecoverPassword(String code) throws RpcException, IOException;

    TLPasswordRecovery authRequestPasswordRecovery() throws RpcException, IOException;

    TLSentCode authResendCode(String phoneNumber, String phoneCodeHash) throws RpcException,
            IOException;

    TLBool authResetAuthorizations() throws RpcException, IOException;

    TLSentCode authSendCode(boolean allowFlashcall, String phoneNumber, boolean currentNumber,
            int apiId, String apiHash) throws RpcException, IOException;

    TLBool authSendInvites(TLStringVector phoneNumbers, String message) throws RpcException,
            IOException;

    TLAuthorization authSignIn(String phoneNumber, String phoneCodeHash, String phoneCode) throws
            RpcException, IOException;

    TLAuthorization authSignUp(String phoneNumber, String phoneCodeHash, String phoneCode,
            String firstName, String lastName) throws RpcException, IOException;

    TLBool botsAnswerWebhookJSONQuery(long queryId, TLDataJSON data) throws RpcException,
            IOException;

    TLDataJSON botsSendCustomRequest(String customMethod, TLDataJSON params) throws RpcException,
            IOException;

    TLBool channelsCheckUsername(TLAbsInputChannel channel, String username) throws RpcException,
            IOException;

    TLAbsUpdates channelsCreateChannel(boolean broadcast, boolean megagroup, String title,
            String about) throws RpcException, IOException;

    TLAbsUpdates channelsDeleteChannel(TLAbsInputChannel channel) throws RpcException, IOException;

    TLAffectedMessages channelsDeleteMessages(TLAbsInputChannel channel, TLIntVector id) throws
            RpcException, IOException;

    TLAffectedHistory channelsDeleteUserHistory(TLAbsInputChannel channel, TLAbsInputUser userId)
            throws RpcException, IOException;

    TLBool channelsEditAbout(TLAbsInputChannel channel, String about) throws RpcException,
            IOException;

    TLAbsUpdates channelsEditAdmin(TLAbsInputChannel channel, TLAbsInputUser userId,
            TLAbsChannelParticipantRole role) throws RpcException, IOException;

    TLAbsUpdates channelsEditPhoto(TLAbsInputChannel channel, TLAbsInputChatPhoto photo) throws
            RpcException, IOException;

    TLAbsUpdates channelsEditTitle(TLAbsInputChannel channel, String title) throws RpcException,
            IOException;

    TLAbsExportedChatInvite channelsExportInvite(TLAbsInputChannel channel) throws RpcException,
            IOException;

    TLExportedMessageLink channelsExportMessageLink(TLAbsInputChannel channel, int id) throws
            RpcException, IOException;

    TLAbsChats channelsGetAdminedPublicChannels() throws RpcException, IOException;

    TLAbsChats channelsGetChannels(TLVector<TLAbsInputChannel> id) throws RpcException, IOException;

    TLChatFull channelsGetFullChannel(TLAbsInputChannel channel) throws RpcException, IOException;

    TLAbsMessages channelsGetMessages(TLAbsInputChannel channel, TLIntVector id) throws
            RpcException, IOException;

    TLChannelParticipant channelsGetParticipant(TLAbsInputChannel channel, TLAbsInputUser userId)
            throws RpcException, IOException;

    TLChannelParticipants channelsGetParticipants(TLAbsInputChannel channel,
            TLAbsChannelParticipantsFilter filter, int offset, int limit) throws RpcException,
            IOException;

    TLAbsUpdates channelsInviteToChannel(TLAbsInputChannel channel, TLVector<TLAbsInputUser> users)
            throws RpcException, IOException;

    TLAbsUpdates channelsJoinChannel(TLAbsInputChannel channel) throws RpcException, IOException;

    TLAbsUpdates channelsKickFromChannel(TLAbsInputChannel channel, TLAbsInputUser userId,
            boolean kicked) throws RpcException, IOException;

    TLAbsUpdates channelsLeaveChannel(TLAbsInputChannel channel) throws RpcException, IOException;

    TLBool channelsReadHistory(TLAbsInputChannel channel, int maxId) throws RpcException,
            IOException;

    TLBool channelsReportSpam(TLAbsInputChannel channel, TLAbsInputUser userId, TLIntVector id)
            throws RpcException, IOException;

    TLAbsUpdates channelsToggleInvites(TLAbsInputChannel channel, boolean enabled) throws
            RpcException, IOException;

    TLAbsUpdates channelsToggleSignatures(TLAbsInputChannel channel, boolean enabled) throws
            RpcException, IOException;

    TLAbsUpdates channelsUpdatePinnedMessage(boolean silent, TLAbsInputChannel channel, int id)
            throws RpcException, IOException;

    TLBool channelsUpdateUsername(TLAbsInputChannel channel, String username) throws RpcException,
            IOException;

    TLBool contactsBlock(TLAbsInputUser id) throws RpcException, IOException;

    TLLink contactsDeleteContact(TLAbsInputUser id) throws RpcException, IOException;

    TLBool contactsDeleteContacts(TLVector<TLAbsInputUser> id) throws RpcException, IOException;

    TLIntVector contactsExportCard() throws RpcException, IOException;

    TLAbsBlocked contactsGetBlocked(int offset, int limit) throws RpcException, IOException;

    TLAbsContacts contactsGetContacts(String hash) throws RpcException, IOException;

    TLVector<TLContactStatus> contactsGetStatuses() throws RpcException, IOException;

    TLAbsTopPeers contactsGetTopPeers(boolean correspondents, boolean botsPm, boolean botsInline,
            boolean groups, boolean channels, int offset, int limit, int hash) throws RpcException,
            IOException;

    TLAbsUser contactsImportCard(TLIntVector exportCard) throws RpcException, IOException;

    TLImportedContacts contactsImportContacts(TLVector<TLInputPhoneContact> contacts,
            boolean replace) throws RpcException, IOException;

    TLBool contactsResetTopPeerRating(TLAbsTopPeerCategory category, TLAbsInputPeer peer) throws
            RpcException, IOException;

    TLResolvedPeer contactsResolveUsername(String username) throws RpcException, IOException;

    TLFound contactsSearch(String q, int limit) throws RpcException, IOException;

    TLBool contactsUnblock(TLAbsInputUser id) throws RpcException, IOException;

    TLBool contestSaveDeveloperInfo(int vkId, String name, String phoneNumber, int age, String city)
            throws RpcException, IOException;

    TLAbsUpdates helpGetAppChangelog(String prevAppVersion) throws RpcException, IOException;

    TLAbsAppUpdate helpGetAppUpdate() throws RpcException, IOException;

    TLCdnConfig helpGetCdnConfig() throws RpcException, IOException;

    TLConfig helpGetConfig() throws RpcException, IOException;

    TLInviteText helpGetInviteText() throws RpcException, IOException;

    TLNearestDc helpGetNearestDc() throws RpcException, IOException;

    TLSupport helpGetSupport() throws RpcException, IOException;

    TLTermsOfService helpGetTermsOfService() throws RpcException, IOException;

    TLBool helpSaveAppLog(TLVector<TLInputAppEvent> events) throws RpcException, IOException;

    TLBool helpSetBotUpdatesStatus(int pendingUpdatesCount, String message) throws RpcException,
            IOException;

    <T extends TLObject> T initConnection(int apiId, String deviceModel, String systemVersion,
            String appVersion, String langCode, TLMethod<T> query) throws RpcException, IOException;

    <T extends TLObject> T invokeAfterMsg(long msgId, TLMethod<T> query) throws RpcException,
            IOException;

    <T extends TLObject> T invokeAfterMsgs(TLLongVector msgIds, TLMethod<T> query) throws
            RpcException, IOException;

    <T extends TLObject> T invokeWithLayer(int layer, TLMethod<T> query) throws RpcException,
            IOException;

    <T extends TLObject> T invokeWithoutUpdates(TLMethod<T> query) throws RpcException, IOException;

    TLAbsEncryptedChat messagesAcceptEncryption(TLInputEncryptedChat peer, TLBytes gB,
            long keyFingerprint) throws RpcException, IOException;

    TLAbsUpdates messagesAddChatUser(int chatId, TLAbsInputUser userId, int fwdLimit) throws
            RpcException, IOException;

    TLAbsChatInvite messagesCheckChatInvite(String hash) throws RpcException, IOException;

    TLBool messagesClearRecentStickers(boolean attached) throws RpcException, IOException;

    TLAbsUpdates messagesCreateChat(TLVector<TLAbsInputUser> users, String title) throws
            RpcException, IOException;

    TLAbsUpdates messagesDeleteChatUser(int chatId, TLAbsInputUser userId) throws RpcException,
            IOException;

    TLAffectedHistory messagesDeleteHistory(boolean justClear, TLAbsInputPeer peer, int maxId)
            throws RpcException, IOException;

    TLAffectedMessages messagesDeleteMessages(boolean revoke, TLIntVector id) throws RpcException,
            IOException;

    TLBool messagesDiscardEncryption(int chatId) throws RpcException, IOException;

    TLBool messagesEditChatAdmin(int chatId, TLAbsInputUser userId, boolean isAdmin) throws
            RpcException, IOException;

    TLAbsUpdates messagesEditChatPhoto(int chatId, TLAbsInputChatPhoto photo) throws RpcException,
            IOException;

    TLAbsUpdates messagesEditChatTitle(int chatId, String title) throws RpcException, IOException;

    TLBool messagesEditInlineBotMessage(boolean noWebpage, TLInputBotInlineMessageID id,
            String message, TLAbsReplyMarkup replyMarkup, TLVector<TLAbsMessageEntity> entities)
            throws RpcException, IOException;

    TLAbsUpdates messagesEditMessage(boolean noWebpage, TLAbsInputPeer peer, int id, String message,
            TLAbsReplyMarkup replyMarkup, TLVector<TLAbsMessageEntity> entities) throws
            RpcException, IOException;

    TLAbsExportedChatInvite messagesExportChatInvite(int chatId) throws RpcException, IOException;

    TLAbsUpdates messagesForwardMessage(TLAbsInputPeer peer, int id, long randomId) throws
            RpcException, IOException;

    TLAbsUpdates messagesForwardMessages(boolean silent, boolean background, boolean withMyScore,
            TLAbsInputPeer fromPeer, TLIntVector id, TLLongVector randomId, TLAbsInputPeer toPeer)
            throws RpcException, IOException;

    TLAbsChats messagesGetAllChats(TLIntVector exceptIds) throws RpcException, IOException;

    TLAbsUpdates messagesGetAllDrafts() throws RpcException, IOException;

    TLAbsAllStickers messagesGetAllStickers(int hash) throws RpcException, IOException;

    TLArchivedStickers messagesGetArchivedStickers(boolean masks, long offsetId, int limit) throws
            RpcException, IOException;

    TLVector<TLAbsStickerSetCovered> messagesGetAttachedStickers(TLAbsInputStickeredMedia media)
            throws RpcException, IOException;

    TLBotCallbackAnswer messagesGetBotCallbackAnswer(boolean game, TLAbsInputPeer peer, int msgId,
            TLBytes data) throws RpcException, IOException;

    TLAbsChats messagesGetChats(TLIntVector id) throws RpcException, IOException;

    TLAbsChats messagesGetCommonChats(TLAbsInputUser userId, int maxId, int limit) throws
            RpcException, IOException;

    TLAbsDhConfig messagesGetDhConfig(int version, int randomLength) throws RpcException,
            IOException;

    TLAbsDialogs messagesGetDialogs(boolean excludePinned, int offsetDate, int offsetId,
            TLAbsInputPeer offsetPeer, int limit) throws RpcException, IOException;

    TLAbsDocument messagesGetDocumentByHash(TLBytes sha256, int size, String mimeType) throws
            RpcException, IOException;

    TLAbsFeaturedStickers messagesGetFeaturedStickers(int hash) throws RpcException, IOException;

    TLChatFull messagesGetFullChat(int chatId) throws RpcException, IOException;

    TLHighScores messagesGetGameHighScores(TLAbsInputPeer peer, int id, TLAbsInputUser userId)
            throws RpcException, IOException;

    TLAbsMessages messagesGetHistory(TLAbsInputPeer peer, int offsetId, int offsetDate,
            int addOffset, int limit, int maxId, int minId) throws RpcException, IOException;

    TLBotResults messagesGetInlineBotResults(TLAbsInputUser bot, TLAbsInputPeer peer,
            TLAbsInputGeoPoint geoPoint, String query, String offset) throws RpcException,
            IOException;

    TLHighScores messagesGetInlineGameHighScores(TLInputBotInlineMessageID id,
            TLAbsInputUser userId) throws RpcException, IOException;

    TLAbsAllStickers messagesGetMaskStickers(int hash) throws RpcException, IOException;

    TLMessageEditData messagesGetMessageEditData(TLAbsInputPeer peer, int id) throws RpcException,
            IOException;

    TLAbsMessages messagesGetMessages(TLIntVector id) throws RpcException, IOException;

    TLIntVector messagesGetMessagesViews(TLAbsInputPeer peer, TLIntVector id, boolean increment)
            throws RpcException, IOException;

    TLPeerDialogs messagesGetPeerDialogs(TLVector<TLAbsInputPeer> peers) throws RpcException,
            IOException;

    TLPeerSettings messagesGetPeerSettings(TLAbsInputPeer peer) throws RpcException, IOException;

    TLPeerDialogs messagesGetPinnedDialogs() throws RpcException, IOException;

    TLAbsRecentStickers messagesGetRecentStickers(boolean attached, int hash) throws RpcException,
            IOException;

    TLAbsSavedGifs messagesGetSavedGifs(int hash) throws RpcException, IOException;

    TLStickerSet messagesGetStickerSet(TLAbsInputStickerSet stickerset) throws RpcException,
            IOException;

    TLAbsWebPage messagesGetWebPage(String url, int hash) throws RpcException, IOException;

    TLAbsMessageMedia messagesGetWebPagePreview(String message) throws RpcException, IOException;

    TLBool messagesHideReportSpam(TLAbsInputPeer peer) throws RpcException, IOException;

    TLAbsUpdates messagesImportChatInvite(String hash) throws RpcException, IOException;

    TLAbsStickerSetInstallResult messagesInstallStickerSet(TLAbsInputStickerSet stickerset,
            boolean archived) throws RpcException, IOException;

    TLAbsUpdates messagesMigrateChat(int chatId) throws RpcException, IOException;

    TLBool messagesReadEncryptedHistory(TLInputEncryptedChat peer, int maxDate) throws RpcException,
            IOException;

    TLBool messagesReadFeaturedStickers(TLLongVector id) throws RpcException, IOException;

    TLAffectedMessages messagesReadHistory(TLAbsInputPeer peer, int maxId) throws RpcException,
            IOException;

    TLAffectedMessages messagesReadMessageContents(TLIntVector id) throws RpcException, IOException;

    TLVector<TLReceivedNotifyMessage> messagesReceivedMessages(int maxId) throws RpcException,
            IOException;

    TLLongVector messagesReceivedQueue(int maxQts) throws RpcException, IOException;

    TLBool messagesReorderPinnedDialogs(boolean force, TLVector<TLAbsInputPeer> order) throws
            RpcException, IOException;

    TLBool messagesReorderStickerSets(boolean masks, TLLongVector order) throws RpcException,
            IOException;

    TLBool messagesReportEncryptedSpam(TLInputEncryptedChat peer) throws RpcException, IOException;

    TLBool messagesReportSpam(TLAbsInputPeer peer) throws RpcException, IOException;

    TLAbsEncryptedChat messagesRequestEncryption(TLAbsInputUser userId, int randomId, TLBytes gA)
            throws RpcException, IOException;

    TLBool messagesSaveDraft(boolean noWebpage, Integer replyToMsgId, TLAbsInputPeer peer,
            String message, TLVector<TLAbsMessageEntity> entities) throws RpcException, IOException;

    TLBool messagesSaveGif(TLAbsInputDocument id, boolean unsave) throws RpcException, IOException;

    TLBool messagesSaveRecentSticker(boolean attached, TLAbsInputDocument id, boolean unsave) throws
            RpcException, IOException;

    TLAbsMessages messagesSearch(TLAbsInputPeer peer, String q, TLAbsMessagesFilter filter,
            int minDate, int maxDate, int offset, int maxId, int limit) throws RpcException,
            IOException;

    TLFoundGifs messagesSearchGifs(String q, int offset) throws RpcException, IOException;

    TLAbsMessages messagesSearchGlobal(String q, int offsetDate, TLAbsInputPeer offsetPeer,
            int offsetId, int limit) throws RpcException, IOException;

    TLAbsSentEncryptedMessage messagesSendEncrypted(TLInputEncryptedChat peer, long randomId,
            TLBytes data) throws RpcException, IOException;

    TLAbsSentEncryptedMessage messagesSendEncryptedFile(TLInputEncryptedChat peer, long randomId,
            TLBytes data, TLAbsInputEncryptedFile file) throws RpcException, IOException;

    TLAbsSentEncryptedMessage messagesSendEncryptedService(TLInputEncryptedChat peer, long randomId,
            TLBytes data) throws RpcException, IOException;

    TLAbsUpdates messagesSendInlineBotResult(boolean silent, boolean background, boolean clearDraft,
            TLAbsInputPeer peer, Integer replyToMsgId, long randomId, long queryId, String id)
            throws RpcException, IOException;

    TLAbsUpdates messagesSendMedia(boolean silent, boolean background, boolean clearDraft,
            TLAbsInputPeer peer, Integer replyToMsgId, TLAbsInputMedia media, long randomId,
            TLAbsReplyMarkup replyMarkup) throws RpcException, IOException;

    TLAbsUpdates messagesSendMessage(boolean noWebpage, boolean silent, boolean background,
            boolean clearDraft, TLAbsInputPeer peer, Integer replyToMsgId, String message,
            long randomId, TLAbsReplyMarkup replyMarkup, TLVector<TLAbsMessageEntity> entities)
            throws RpcException, IOException;

    TLBool messagesSetBotCallbackAnswer(boolean alert, long queryId, String message, String url,
            int cacheTime) throws RpcException, IOException;

    TLBool messagesSetBotPrecheckoutResults(boolean success, long queryId, String error) throws
            RpcException, IOException;

    TLBool messagesSetBotShippingResults(long queryId, String error,
            TLVector<TLShippingOption> shippingOptions) throws RpcException, IOException;

    TLBool messagesSetEncryptedTyping(TLInputEncryptedChat peer, boolean typing) throws
            RpcException, IOException;

    TLAbsUpdates messagesSetGameScore(boolean editMessage, boolean force, TLAbsInputPeer peer,
            int id, TLAbsInputUser userId, int score) throws RpcException, IOException;

    TLBool messagesSetInlineBotResults(boolean gallery, boolean _private, long queryId,
            TLVector<TLAbsInputBotInlineResult> results, int cacheTime, String nextOffset,
            TLInlineBotSwitchPM switchPm) throws RpcException, IOException;

    TLBool messagesSetInlineGameScore(boolean editMessage, boolean force,
            TLInputBotInlineMessageID id, TLAbsInputUser userId, int score) throws RpcException,
            IOException;

    TLBool messagesSetTyping(TLAbsInputPeer peer, TLAbsSendMessageAction action) throws
            RpcException, IOException;

    TLAbsUpdates messagesStartBot(TLAbsInputUser bot, TLAbsInputPeer peer, long randomId,
            String startParam) throws RpcException, IOException;

    TLAbsUpdates messagesToggleChatAdmins(int chatId, boolean enabled) throws RpcException,
            IOException;

    TLBool messagesToggleDialogPin(boolean pinned, TLAbsInputPeer peer) throws RpcException,
            IOException;

    TLBool messagesUninstallStickerSet(TLAbsInputStickerSet stickerset) throws RpcException,
            IOException;

    TLBool paymentsClearSavedInfo(boolean credentials, boolean info) throws RpcException,
            IOException;

    TLPaymentForm paymentsGetPaymentForm(int msgId) throws RpcException, IOException;

    TLPaymentReceipt paymentsGetPaymentReceipt(int msgId) throws RpcException, IOException;

    TLSavedInfo paymentsGetSavedInfo() throws RpcException, IOException;

    TLAbsPaymentResult paymentsSendPaymentForm(int msgId, String requestedInfoId,
            String shippingOptionId, TLAbsInputPaymentCredentials credentials) throws RpcException,
            IOException;

    TLValidatedRequestedInfo paymentsValidateRequestedInfo(boolean save, int msgId,
            TLPaymentRequestedInfo info) throws RpcException, IOException;

    TLPhoneCall phoneAcceptCall(TLInputPhoneCall peer, TLBytes gB, TLPhoneCallProtocol protocol)
            throws RpcException, IOException;

    TLPhoneCall phoneConfirmCall(TLInputPhoneCall peer, TLBytes gA, long keyFingerprint,
            TLPhoneCallProtocol protocol) throws RpcException, IOException;

    TLAbsUpdates phoneDiscardCall(TLInputPhoneCall peer, int duration,
            TLAbsPhoneCallDiscardReason reason, long connectionId) throws RpcException, IOException;

    TLDataJSON phoneGetCallConfig() throws RpcException, IOException;

    TLBool phoneReceivedCall(TLInputPhoneCall peer) throws RpcException, IOException;

    TLPhoneCall phoneRequestCall(TLAbsInputUser userId, int randomId, TLBytes gAHash,
            TLPhoneCallProtocol protocol) throws RpcException, IOException;

    TLBool phoneSaveCallDebug(TLInputPhoneCall peer, TLDataJSON debug) throws RpcException,
            IOException;

    TLAbsUpdates phoneSetCallRating(TLInputPhoneCall peer, int rating, String comment) throws
            RpcException, IOException;

    TLLongVector photosDeletePhotos(TLVector<TLAbsInputPhoto> id) throws RpcException, IOException;

    TLAbsPhotos photosGetUserPhotos(TLAbsInputUser userId, int offset, long maxId, int limit) throws
            RpcException, IOException;

    TLAbsUserProfilePhoto photosUpdateProfilePhoto(TLAbsInputPhoto id) throws RpcException,
            IOException;

    TLPhoto photosUploadProfilePhoto(TLAbsInputFile file) throws RpcException, IOException;

    TLAbsChannelDifference updatesGetChannelDifference(boolean force, TLAbsInputChannel channel,
            TLAbsChannelMessagesFilter filter, int pts, int limit) throws RpcException, IOException;

    TLAbsDifference updatesGetDifference(int pts, Integer ptsTotalLimit, int date, int qts) throws
            RpcException, IOException;

    TLState updatesGetState() throws RpcException, IOException;

    TLAbsCdnFile uploadGetCdnFile(TLBytes fileToken, int offset, int limit) throws RpcException,
            IOException;

    TLAbsFile uploadGetFile(TLAbsInputFileLocation location, int offset, int limit) throws
            RpcException, IOException;

    TLWebFile uploadGetWebFile(TLInputWebFileLocation location, int offset, int limit) throws
            RpcException, IOException;

    TLBool uploadReuploadCdnFile(TLBytes fileToken, TLBytes requestToken) throws RpcException,
            IOException;

    TLBool uploadSaveBigFilePart(long fileId, int filePart, int fileTotalParts, TLBytes bytes)
            throws RpcException, IOException;

    TLBool uploadSaveFilePart(long fileId, int filePart, TLBytes bytes) throws RpcException,
            IOException;

    TLUserFull usersGetFullUser(TLAbsInputUser id) throws RpcException, IOException;

    TLVector<TLAbsUser> usersGetUsers(TLVector<TLAbsInputUser> id) throws RpcException, IOException;
}
