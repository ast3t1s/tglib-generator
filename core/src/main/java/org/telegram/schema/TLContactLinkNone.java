package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLContactLinkNone extends TLAbsContactLink {
    public static final int CLASS_ID = 0xfeedd3ad;

    public TLContactLinkNone() {
    }

    @Override
    public String toString() {
        return "contactLinkNone#feedd3ad";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
