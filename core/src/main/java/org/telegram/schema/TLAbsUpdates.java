package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLUpdateShort: updateShort#78d4dec1</li>
 * <li>{@link TLUpdateShortChatMessage: updateShortChatMessage#16812688</li>
 * <li>{@link TLUpdateShortMessage: updateShortMessage#914fbf11</li>
 * <li>{@link TLUpdateShortSentMessage: updateShortSentMessage#11f1331c</li>
 * <li>{@link TLUpdates: updates#74ae4240</li>
 * <li>{@link TLUpdatesCombined: updatesCombined#725b04c3</li>
 * <li>{@link TLUpdatesTooLong: updatesTooLong#e317af7e</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsUpdates extends TLObject {
    public TLAbsUpdates() {
    }
}
