package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLTopPeerCategoryGroups extends TLAbsTopPeerCategory {
    public static final int CLASS_ID = 0xbd17a14a;

    public TLTopPeerCategoryGroups() {
    }

    @Override
    public String toString() {
        return "topPeerCategoryGroups#bd17a14a";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
