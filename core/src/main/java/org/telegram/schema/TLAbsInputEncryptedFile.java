package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputEncryptedFile: inputEncryptedFile#5a17b5e5</li>
 * <li>{@link TLInputEncryptedFileBigUploaded: inputEncryptedFileBigUploaded#2dc173c8</li>
 * <li>{@link TLInputEncryptedFileEmpty: inputEncryptedFileEmpty#1837c364</li>
 * <li>{@link TLInputEncryptedFileUploaded: inputEncryptedFileUploaded#64bd0306</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputEncryptedFile extends TLObject {
    public TLAbsInputEncryptedFile() {
    }
}
