package org.telegram.schema.storage;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLFileUnknown extends TLAbsFileType {
    public static final int CLASS_ID = 0xaa963b05;

    public TLFileUnknown() {
    }

    @Override
    public String toString() {
        return "storage.fileUnknown#aa963b05";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
