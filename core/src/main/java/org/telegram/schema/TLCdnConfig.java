package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;

/**
 * @author Telegram Schema Generator
 */
public class TLCdnConfig extends TLObject {
    public static final int CLASS_ID = 0x5725e40a;

    protected TLVector<TLCdnPublicKey> publicKeys;

    public TLCdnConfig() {
    }

    public TLCdnConfig(TLVector<TLCdnPublicKey> publicKeys) {
        this.publicKeys = publicKeys;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(publicKeys, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        publicKeys = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += publicKeys.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "cdnConfig#5725e40a";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLVector<TLCdnPublicKey> getPublicKeys() {
        return publicKeys;
    }

    public void setPublicKeys(TLVector<TLCdnPublicKey> publicKeys) {
        this.publicKeys = publicKeys;
    }
}
