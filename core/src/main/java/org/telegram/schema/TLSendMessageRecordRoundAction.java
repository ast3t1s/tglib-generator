package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLSendMessageRecordRoundAction extends TLAbsSendMessageAction {
    public static final int CLASS_ID = 0x88f27fbc;

    public TLSendMessageRecordRoundAction() {
    }

    @Override
    public String toString() {
        return "sendMessageRecordRoundAction#88f27fbc";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
