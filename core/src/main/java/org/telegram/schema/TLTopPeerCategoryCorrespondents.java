package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLTopPeerCategoryCorrespondents extends TLAbsTopPeerCategory {
    public static final int CLASS_ID = 0x637b7ed;

    public TLTopPeerCategoryCorrespondents() {
    }

    @Override
    public String toString() {
        return "topPeerCategoryCorrespondents#637b7ed";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
