package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLDraftMessageEmpty extends TLAbsDraftMessage {
    public static final int CLASS_ID = 0xba4baec5;

    public TLDraftMessageEmpty() {
    }

    @Override
    public String toString() {
        return "draftMessageEmpty#ba4baec5";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    @Override
    public final boolean isEmpty() {
        return true;
    }

    @Override
    public final boolean isNotEmpty() {
        return false;
    }
}
