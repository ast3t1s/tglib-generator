package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLVector;

/**
 * @author Telegram Schema Generator
 */
public class TLChannelMessagesFilter extends TLAbsChannelMessagesFilter {
    public static final int CLASS_ID = 0xcd77d957;

    protected int flags;

    protected boolean excludeNewMessages;

    protected TLVector<TLMessageRange> ranges;

    public TLChannelMessagesFilter() {
    }

    public TLChannelMessagesFilter(boolean excludeNewMessages, TLVector<TLMessageRange> ranges) {
        this.excludeNewMessages = excludeNewMessages;
        this.ranges = ranges;
    }

    private void computeFlags() {
        flags = 0;
        flags = excludeNewMessages ? (flags | 2) : (flags & ~2);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeTLVector(ranges, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        excludeNewMessages = (flags & 2) != 0;
        ranges = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += ranges.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "channelMessagesFilter#cd77d957";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getExcludeNewMessages() {
        return excludeNewMessages;
    }

    public void setExcludeNewMessages(boolean excludeNewMessages) {
        this.excludeNewMessages = excludeNewMessages;
    }

    public TLVector<TLMessageRange> getRanges() {
        return ranges;
    }

    public void setRanges(TLVector<TLMessageRange> ranges) {
        this.ranges = ranges;
    }

    @Override
    public final boolean isEmpty() {
        return false;
    }

    @Override
    public final boolean isNotEmpty() {
        return true;
    }

    @Override
    public final TLChannelMessagesFilter getAsChannelMessagesFilter() {
        return this;
    }
}
