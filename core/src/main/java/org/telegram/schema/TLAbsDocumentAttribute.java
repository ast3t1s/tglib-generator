package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLDocumentAttributeAnimated: documentAttributeAnimated#11b58939</li>
 * <li>{@link TLDocumentAttributeAudio: documentAttributeAudio#9852f9c6</li>
 * <li>{@link TLDocumentAttributeFilename: documentAttributeFilename#15590068</li>
 * <li>{@link TLDocumentAttributeHasStickers: documentAttributeHasStickers#9801d2f7</li>
 * <li>{@link TLDocumentAttributeImageSize: documentAttributeImageSize#6c37c15c</li>
 * <li>{@link TLDocumentAttributeSticker: documentAttributeSticker#6319d612</li>
 * <li>{@link TLDocumentAttributeVideo: documentAttributeVideo#ef02ce6</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsDocumentAttribute extends TLObject {
    public TLAbsDocumentAttribute() {
    }
}
