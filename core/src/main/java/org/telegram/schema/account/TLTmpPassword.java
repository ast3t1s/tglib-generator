package org.telegram.schema.account;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBytes;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLTmpPassword extends TLObject {
    public static final int CLASS_ID = 0xdb64fd34;

    protected TLBytes tmpPassword;

    protected int validUntil;

    public TLTmpPassword() {
    }

    public TLTmpPassword(TLBytes tmpPassword, int validUntil) {
        this.tmpPassword = tmpPassword;
        this.validUntil = validUntil;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLBytes(tmpPassword, stream);
        writeInt(validUntil, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        tmpPassword = readTLBytes(stream, context);
        validUntil = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLBytesSerializedSize(tmpPassword);
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "account.tmpPassword#db64fd34";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLBytes getTmpPassword() {
        return tmpPassword;
    }

    public void setTmpPassword(TLBytes tmpPassword) {
        this.tmpPassword = tmpPassword;
    }

    public int getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(int validUntil) {
        this.validUntil = validUntil;
    }
}
