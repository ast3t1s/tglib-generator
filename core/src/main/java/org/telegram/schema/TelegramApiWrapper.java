package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.exception.RpcException;
import org.telegram.core.type.TLBool;
import org.telegram.core.type.TLBytes;
import org.telegram.core.type.TLIntVector;
import org.telegram.core.type.TLLongVector;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLStringVector;
import org.telegram.core.type.TLVector;
import org.telegram.schema.account.TLAbsPassword;
import org.telegram.schema.account.TLAuthorizations;
import org.telegram.schema.account.TLPasswordInputSettings;
import org.telegram.schema.account.TLPasswordSettings;
import org.telegram.schema.account.TLPrivacyRules;
import org.telegram.schema.account.TLTmpPassword;
import org.telegram.schema.auth.TLAuthorization;
import org.telegram.schema.auth.TLCheckedPhone;
import org.telegram.schema.auth.TLExportedAuthorization;
import org.telegram.schema.auth.TLPasswordRecovery;
import org.telegram.schema.auth.TLSentCode;
import org.telegram.schema.channels.TLChannelParticipant;
import org.telegram.schema.channels.TLChannelParticipants;
import org.telegram.schema.contacts.TLAbsBlocked;
import org.telegram.schema.contacts.TLAbsContacts;
import org.telegram.schema.contacts.TLAbsTopPeers;
import org.telegram.schema.contacts.TLFound;
import org.telegram.schema.contacts.TLImportedContacts;
import org.telegram.schema.contacts.TLLink;
import org.telegram.schema.contacts.TLResolvedPeer;
import org.telegram.schema.help.TLAbsAppUpdate;
import org.telegram.schema.help.TLInviteText;
import org.telegram.schema.help.TLSupport;
import org.telegram.schema.help.TLTermsOfService;
import org.telegram.schema.messages.TLAbsAllStickers;
import org.telegram.schema.messages.TLAbsChats;
import org.telegram.schema.messages.TLAbsDhConfig;
import org.telegram.schema.messages.TLAbsDialogs;
import org.telegram.schema.messages.TLAbsFeaturedStickers;
import org.telegram.schema.messages.TLAbsMessages;
import org.telegram.schema.messages.TLAbsRecentStickers;
import org.telegram.schema.messages.TLAbsSavedGifs;
import org.telegram.schema.messages.TLAbsSentEncryptedMessage;
import org.telegram.schema.messages.TLAbsStickerSetInstallResult;
import org.telegram.schema.messages.TLAffectedHistory;
import org.telegram.schema.messages.TLAffectedMessages;
import org.telegram.schema.messages.TLArchivedStickers;
import org.telegram.schema.messages.TLBotCallbackAnswer;
import org.telegram.schema.messages.TLBotResults;
import org.telegram.schema.messages.TLChatFull;
import org.telegram.schema.messages.TLFoundGifs;
import org.telegram.schema.messages.TLHighScores;
import org.telegram.schema.messages.TLMessageEditData;
import org.telegram.schema.messages.TLPeerDialogs;
import org.telegram.schema.messages.TLStickerSet;
import org.telegram.schema.payments.TLAbsPaymentResult;
import org.telegram.schema.payments.TLPaymentForm;
import org.telegram.schema.payments.TLPaymentReceipt;
import org.telegram.schema.payments.TLSavedInfo;
import org.telegram.schema.payments.TLValidatedRequestedInfo;
import org.telegram.schema.phone.TLPhoneCall;
import org.telegram.schema.photos.TLAbsPhotos;
import org.telegram.schema.photos.TLPhoto;
import org.telegram.schema.request.TLRequestAccountChangePhone;
import org.telegram.schema.request.TLRequestAccountCheckUsername;
import org.telegram.schema.request.TLRequestAccountConfirmPhone;
import org.telegram.schema.request.TLRequestAccountDeleteAccount;
import org.telegram.schema.request.TLRequestAccountGetAccountTTL;
import org.telegram.schema.request.TLRequestAccountGetAuthorizations;
import org.telegram.schema.request.TLRequestAccountGetNotifySettings;
import org.telegram.schema.request.TLRequestAccountGetPassword;
import org.telegram.schema.request.TLRequestAccountGetPasswordSettings;
import org.telegram.schema.request.TLRequestAccountGetPrivacy;
import org.telegram.schema.request.TLRequestAccountGetTmpPassword;
import org.telegram.schema.request.TLRequestAccountGetWallPapers;
import org.telegram.schema.request.TLRequestAccountRegisterDevice;
import org.telegram.schema.request.TLRequestAccountReportPeer;
import org.telegram.schema.request.TLRequestAccountResetAuthorization;
import org.telegram.schema.request.TLRequestAccountResetNotifySettings;
import org.telegram.schema.request.TLRequestAccountSendChangePhoneCode;
import org.telegram.schema.request.TLRequestAccountSendConfirmPhoneCode;
import org.telegram.schema.request.TLRequestAccountSetAccountTTL;
import org.telegram.schema.request.TLRequestAccountSetPrivacy;
import org.telegram.schema.request.TLRequestAccountUnregisterDevice;
import org.telegram.schema.request.TLRequestAccountUpdateDeviceLocked;
import org.telegram.schema.request.TLRequestAccountUpdateNotifySettings;
import org.telegram.schema.request.TLRequestAccountUpdatePasswordSettings;
import org.telegram.schema.request.TLRequestAccountUpdateProfile;
import org.telegram.schema.request.TLRequestAccountUpdateStatus;
import org.telegram.schema.request.TLRequestAccountUpdateUsername;
import org.telegram.schema.request.TLRequestAuthBindTempAuthKey;
import org.telegram.schema.request.TLRequestAuthCancelCode;
import org.telegram.schema.request.TLRequestAuthCheckPassword;
import org.telegram.schema.request.TLRequestAuthCheckPhone;
import org.telegram.schema.request.TLRequestAuthDropTempAuthKeys;
import org.telegram.schema.request.TLRequestAuthExportAuthorization;
import org.telegram.schema.request.TLRequestAuthImportAuthorization;
import org.telegram.schema.request.TLRequestAuthImportBotAuthorization;
import org.telegram.schema.request.TLRequestAuthLogOut;
import org.telegram.schema.request.TLRequestAuthRecoverPassword;
import org.telegram.schema.request.TLRequestAuthRequestPasswordRecovery;
import org.telegram.schema.request.TLRequestAuthResendCode;
import org.telegram.schema.request.TLRequestAuthResetAuthorizations;
import org.telegram.schema.request.TLRequestAuthSendCode;
import org.telegram.schema.request.TLRequestAuthSendInvites;
import org.telegram.schema.request.TLRequestAuthSignIn;
import org.telegram.schema.request.TLRequestAuthSignUp;
import org.telegram.schema.request.TLRequestBotsAnswerWebhookJSONQuery;
import org.telegram.schema.request.TLRequestBotsSendCustomRequest;
import org.telegram.schema.request.TLRequestChannelsCheckUsername;
import org.telegram.schema.request.TLRequestChannelsCreateChannel;
import org.telegram.schema.request.TLRequestChannelsDeleteChannel;
import org.telegram.schema.request.TLRequestChannelsDeleteMessages;
import org.telegram.schema.request.TLRequestChannelsDeleteUserHistory;
import org.telegram.schema.request.TLRequestChannelsEditAbout;
import org.telegram.schema.request.TLRequestChannelsEditAdmin;
import org.telegram.schema.request.TLRequestChannelsEditPhoto;
import org.telegram.schema.request.TLRequestChannelsEditTitle;
import org.telegram.schema.request.TLRequestChannelsExportInvite;
import org.telegram.schema.request.TLRequestChannelsExportMessageLink;
import org.telegram.schema.request.TLRequestChannelsGetAdminedPublicChannels;
import org.telegram.schema.request.TLRequestChannelsGetChannels;
import org.telegram.schema.request.TLRequestChannelsGetFullChannel;
import org.telegram.schema.request.TLRequestChannelsGetMessages;
import org.telegram.schema.request.TLRequestChannelsGetParticipant;
import org.telegram.schema.request.TLRequestChannelsGetParticipants;
import org.telegram.schema.request.TLRequestChannelsInviteToChannel;
import org.telegram.schema.request.TLRequestChannelsJoinChannel;
import org.telegram.schema.request.TLRequestChannelsKickFromChannel;
import org.telegram.schema.request.TLRequestChannelsLeaveChannel;
import org.telegram.schema.request.TLRequestChannelsReadHistory;
import org.telegram.schema.request.TLRequestChannelsReportSpam;
import org.telegram.schema.request.TLRequestChannelsToggleInvites;
import org.telegram.schema.request.TLRequestChannelsToggleSignatures;
import org.telegram.schema.request.TLRequestChannelsUpdatePinnedMessage;
import org.telegram.schema.request.TLRequestChannelsUpdateUsername;
import org.telegram.schema.request.TLRequestContactsBlock;
import org.telegram.schema.request.TLRequestContactsDeleteContact;
import org.telegram.schema.request.TLRequestContactsDeleteContacts;
import org.telegram.schema.request.TLRequestContactsExportCard;
import org.telegram.schema.request.TLRequestContactsGetBlocked;
import org.telegram.schema.request.TLRequestContactsGetContacts;
import org.telegram.schema.request.TLRequestContactsGetStatuses;
import org.telegram.schema.request.TLRequestContactsGetTopPeers;
import org.telegram.schema.request.TLRequestContactsImportCard;
import org.telegram.schema.request.TLRequestContactsImportContacts;
import org.telegram.schema.request.TLRequestContactsResetTopPeerRating;
import org.telegram.schema.request.TLRequestContactsResolveUsername;
import org.telegram.schema.request.TLRequestContactsSearch;
import org.telegram.schema.request.TLRequestContactsUnblock;
import org.telegram.schema.request.TLRequestContestSaveDeveloperInfo;
import org.telegram.schema.request.TLRequestHelpGetAppChangelog;
import org.telegram.schema.request.TLRequestHelpGetAppUpdate;
import org.telegram.schema.request.TLRequestHelpGetCdnConfig;
import org.telegram.schema.request.TLRequestHelpGetConfig;
import org.telegram.schema.request.TLRequestHelpGetInviteText;
import org.telegram.schema.request.TLRequestHelpGetNearestDc;
import org.telegram.schema.request.TLRequestHelpGetSupport;
import org.telegram.schema.request.TLRequestHelpGetTermsOfService;
import org.telegram.schema.request.TLRequestHelpSaveAppLog;
import org.telegram.schema.request.TLRequestHelpSetBotUpdatesStatus;
import org.telegram.schema.request.TLRequestInitConnection;
import org.telegram.schema.request.TLRequestInvokeAfterMsg;
import org.telegram.schema.request.TLRequestInvokeAfterMsgs;
import org.telegram.schema.request.TLRequestInvokeWithLayer;
import org.telegram.schema.request.TLRequestInvokeWithoutUpdates;
import org.telegram.schema.request.TLRequestMessagesAcceptEncryption;
import org.telegram.schema.request.TLRequestMessagesAddChatUser;
import org.telegram.schema.request.TLRequestMessagesCheckChatInvite;
import org.telegram.schema.request.TLRequestMessagesClearRecentStickers;
import org.telegram.schema.request.TLRequestMessagesCreateChat;
import org.telegram.schema.request.TLRequestMessagesDeleteChatUser;
import org.telegram.schema.request.TLRequestMessagesDeleteHistory;
import org.telegram.schema.request.TLRequestMessagesDeleteMessages;
import org.telegram.schema.request.TLRequestMessagesDiscardEncryption;
import org.telegram.schema.request.TLRequestMessagesEditChatAdmin;
import org.telegram.schema.request.TLRequestMessagesEditChatPhoto;
import org.telegram.schema.request.TLRequestMessagesEditChatTitle;
import org.telegram.schema.request.TLRequestMessagesEditInlineBotMessage;
import org.telegram.schema.request.TLRequestMessagesEditMessage;
import org.telegram.schema.request.TLRequestMessagesExportChatInvite;
import org.telegram.schema.request.TLRequestMessagesForwardMessage;
import org.telegram.schema.request.TLRequestMessagesForwardMessages;
import org.telegram.schema.request.TLRequestMessagesGetAllChats;
import org.telegram.schema.request.TLRequestMessagesGetAllDrafts;
import org.telegram.schema.request.TLRequestMessagesGetAllStickers;
import org.telegram.schema.request.TLRequestMessagesGetArchivedStickers;
import org.telegram.schema.request.TLRequestMessagesGetAttachedStickers;
import org.telegram.schema.request.TLRequestMessagesGetBotCallbackAnswer;
import org.telegram.schema.request.TLRequestMessagesGetChats;
import org.telegram.schema.request.TLRequestMessagesGetCommonChats;
import org.telegram.schema.request.TLRequestMessagesGetDhConfig;
import org.telegram.schema.request.TLRequestMessagesGetDialogs;
import org.telegram.schema.request.TLRequestMessagesGetDocumentByHash;
import org.telegram.schema.request.TLRequestMessagesGetFeaturedStickers;
import org.telegram.schema.request.TLRequestMessagesGetFullChat;
import org.telegram.schema.request.TLRequestMessagesGetGameHighScores;
import org.telegram.schema.request.TLRequestMessagesGetHistory;
import org.telegram.schema.request.TLRequestMessagesGetInlineBotResults;
import org.telegram.schema.request.TLRequestMessagesGetInlineGameHighScores;
import org.telegram.schema.request.TLRequestMessagesGetMaskStickers;
import org.telegram.schema.request.TLRequestMessagesGetMessageEditData;
import org.telegram.schema.request.TLRequestMessagesGetMessages;
import org.telegram.schema.request.TLRequestMessagesGetMessagesViews;
import org.telegram.schema.request.TLRequestMessagesGetPeerDialogs;
import org.telegram.schema.request.TLRequestMessagesGetPeerSettings;
import org.telegram.schema.request.TLRequestMessagesGetPinnedDialogs;
import org.telegram.schema.request.TLRequestMessagesGetRecentStickers;
import org.telegram.schema.request.TLRequestMessagesGetSavedGifs;
import org.telegram.schema.request.TLRequestMessagesGetStickerSet;
import org.telegram.schema.request.TLRequestMessagesGetWebPage;
import org.telegram.schema.request.TLRequestMessagesGetWebPagePreview;
import org.telegram.schema.request.TLRequestMessagesHideReportSpam;
import org.telegram.schema.request.TLRequestMessagesImportChatInvite;
import org.telegram.schema.request.TLRequestMessagesInstallStickerSet;
import org.telegram.schema.request.TLRequestMessagesMigrateChat;
import org.telegram.schema.request.TLRequestMessagesReadEncryptedHistory;
import org.telegram.schema.request.TLRequestMessagesReadFeaturedStickers;
import org.telegram.schema.request.TLRequestMessagesReadHistory;
import org.telegram.schema.request.TLRequestMessagesReadMessageContents;
import org.telegram.schema.request.TLRequestMessagesReceivedMessages;
import org.telegram.schema.request.TLRequestMessagesReceivedQueue;
import org.telegram.schema.request.TLRequestMessagesReorderPinnedDialogs;
import org.telegram.schema.request.TLRequestMessagesReorderStickerSets;
import org.telegram.schema.request.TLRequestMessagesReportEncryptedSpam;
import org.telegram.schema.request.TLRequestMessagesReportSpam;
import org.telegram.schema.request.TLRequestMessagesRequestEncryption;
import org.telegram.schema.request.TLRequestMessagesSaveDraft;
import org.telegram.schema.request.TLRequestMessagesSaveGif;
import org.telegram.schema.request.TLRequestMessagesSaveRecentSticker;
import org.telegram.schema.request.TLRequestMessagesSearch;
import org.telegram.schema.request.TLRequestMessagesSearchGifs;
import org.telegram.schema.request.TLRequestMessagesSearchGlobal;
import org.telegram.schema.request.TLRequestMessagesSendEncrypted;
import org.telegram.schema.request.TLRequestMessagesSendEncryptedFile;
import org.telegram.schema.request.TLRequestMessagesSendEncryptedService;
import org.telegram.schema.request.TLRequestMessagesSendInlineBotResult;
import org.telegram.schema.request.TLRequestMessagesSendMedia;
import org.telegram.schema.request.TLRequestMessagesSendMessage;
import org.telegram.schema.request.TLRequestMessagesSetBotCallbackAnswer;
import org.telegram.schema.request.TLRequestMessagesSetBotPrecheckoutResults;
import org.telegram.schema.request.TLRequestMessagesSetBotShippingResults;
import org.telegram.schema.request.TLRequestMessagesSetEncryptedTyping;
import org.telegram.schema.request.TLRequestMessagesSetGameScore;
import org.telegram.schema.request.TLRequestMessagesSetInlineBotResults;
import org.telegram.schema.request.TLRequestMessagesSetInlineGameScore;
import org.telegram.schema.request.TLRequestMessagesSetTyping;
import org.telegram.schema.request.TLRequestMessagesStartBot;
import org.telegram.schema.request.TLRequestMessagesToggleChatAdmins;
import org.telegram.schema.request.TLRequestMessagesToggleDialogPin;
import org.telegram.schema.request.TLRequestMessagesUninstallStickerSet;
import org.telegram.schema.request.TLRequestPaymentsClearSavedInfo;
import org.telegram.schema.request.TLRequestPaymentsGetPaymentForm;
import org.telegram.schema.request.TLRequestPaymentsGetPaymentReceipt;
import org.telegram.schema.request.TLRequestPaymentsGetSavedInfo;
import org.telegram.schema.request.TLRequestPaymentsSendPaymentForm;
import org.telegram.schema.request.TLRequestPaymentsValidateRequestedInfo;
import org.telegram.schema.request.TLRequestPhoneAcceptCall;
import org.telegram.schema.request.TLRequestPhoneConfirmCall;
import org.telegram.schema.request.TLRequestPhoneDiscardCall;
import org.telegram.schema.request.TLRequestPhoneGetCallConfig;
import org.telegram.schema.request.TLRequestPhoneReceivedCall;
import org.telegram.schema.request.TLRequestPhoneRequestCall;
import org.telegram.schema.request.TLRequestPhoneSaveCallDebug;
import org.telegram.schema.request.TLRequestPhoneSetCallRating;
import org.telegram.schema.request.TLRequestPhotosDeletePhotos;
import org.telegram.schema.request.TLRequestPhotosGetUserPhotos;
import org.telegram.schema.request.TLRequestPhotosUpdateProfilePhoto;
import org.telegram.schema.request.TLRequestPhotosUploadProfilePhoto;
import org.telegram.schema.request.TLRequestUpdatesGetChannelDifference;
import org.telegram.schema.request.TLRequestUpdatesGetDifference;
import org.telegram.schema.request.TLRequestUpdatesGetState;
import org.telegram.schema.request.TLRequestUploadGetCdnFile;
import org.telegram.schema.request.TLRequestUploadGetFile;
import org.telegram.schema.request.TLRequestUploadGetWebFile;
import org.telegram.schema.request.TLRequestUploadReuploadCdnFile;
import org.telegram.schema.request.TLRequestUploadSaveBigFilePart;
import org.telegram.schema.request.TLRequestUploadSaveFilePart;
import org.telegram.schema.request.TLRequestUsersGetFullUser;
import org.telegram.schema.request.TLRequestUsersGetUsers;
import org.telegram.schema.updates.TLAbsChannelDifference;
import org.telegram.schema.updates.TLAbsDifference;
import org.telegram.schema.updates.TLState;
import org.telegram.schema.upload.TLAbsCdnFile;
import org.telegram.schema.upload.TLAbsFile;
import org.telegram.schema.upload.TLWebFile;

/**
 * @author Telegram Schema Generator
 */
@SuppressWarnings({"unused", "unchecked", "RedundantCast"})
public abstract class TelegramApiWrapper implements TelegramApi {
    public abstract <T extends TLObject> T executeRpcQuery(TLMethod<T> method) throws RpcException,
            IOException;

    @Override
    public TLAbsUser accountChangePhone(String phoneNumber, String phoneCodeHash, String phoneCode)
            throws RpcException, IOException {
        return (TLAbsUser) executeRpcQuery(new TLRequestAccountChangePhone(phoneNumber, phoneCodeHash, phoneCode));
    }

    @Override
    public TLBool accountCheckUsername(String username) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestAccountCheckUsername(username));
    }

    @Override
    public TLBool accountConfirmPhone(String phoneCodeHash, String phoneCode) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestAccountConfirmPhone(phoneCodeHash, phoneCode));
    }

    @Override
    public TLBool accountDeleteAccount(String reason) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestAccountDeleteAccount(reason));
    }

    @Override
    public TLAccountDaysTTL accountGetAccountTTL() throws RpcException, IOException {
        return (TLAccountDaysTTL) executeRpcQuery(new TLRequestAccountGetAccountTTL());
    }

    @Override
    public TLAuthorizations accountGetAuthorizations() throws RpcException, IOException {
        return (TLAuthorizations) executeRpcQuery(new TLRequestAccountGetAuthorizations());
    }

    @Override
    public TLAbsPeerNotifySettings accountGetNotifySettings(TLAbsInputNotifyPeer peer) throws
            RpcException, IOException {
        return (TLAbsPeerNotifySettings) executeRpcQuery(new TLRequestAccountGetNotifySettings(peer));
    }

    @Override
    public TLAbsPassword accountGetPassword() throws RpcException, IOException {
        return (TLAbsPassword) executeRpcQuery(new TLRequestAccountGetPassword());
    }

    @Override
    public TLPasswordSettings accountGetPasswordSettings(TLBytes currentPasswordHash) throws
            RpcException, IOException {
        return (TLPasswordSettings) executeRpcQuery(new TLRequestAccountGetPasswordSettings(currentPasswordHash));
    }

    @Override
    public TLPrivacyRules accountGetPrivacy(TLAbsInputPrivacyKey key) throws RpcException,
            IOException {
        return (TLPrivacyRules) executeRpcQuery(new TLRequestAccountGetPrivacy(key));
    }

    @Override
    public TLTmpPassword accountGetTmpPassword(TLBytes passwordHash, int period) throws
            RpcException, IOException {
        return (TLTmpPassword) executeRpcQuery(new TLRequestAccountGetTmpPassword(passwordHash, period));
    }

    @Override
    public TLVector<TLAbsWallPaper> accountGetWallPapers() throws RpcException, IOException {
        return (TLVector<TLAbsWallPaper>) executeRpcQuery(new TLRequestAccountGetWallPapers());
    }

    @Override
    public TLBool accountRegisterDevice(int tokenType, String token) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestAccountRegisterDevice(tokenType, token));
    }

    @Override
    public TLBool accountReportPeer(TLAbsInputPeer peer, TLAbsReportReason reason) throws
            RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestAccountReportPeer(peer, reason));
    }

    @Override
    public TLBool accountResetAuthorization(long hash) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestAccountResetAuthorization(hash));
    }

    @Override
    public TLBool accountResetNotifySettings() throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestAccountResetNotifySettings());
    }

    @Override
    public TLSentCode accountSendChangePhoneCode(boolean allowFlashcall, String phoneNumber,
            boolean currentNumber) throws RpcException, IOException {
        return (TLSentCode) executeRpcQuery(new TLRequestAccountSendChangePhoneCode(allowFlashcall, phoneNumber, currentNumber));
    }

    @Override
    public TLSentCode accountSendConfirmPhoneCode(boolean allowFlashcall, String hash,
            boolean currentNumber) throws RpcException, IOException {
        return (TLSentCode) executeRpcQuery(new TLRequestAccountSendConfirmPhoneCode(allowFlashcall, hash, currentNumber));
    }

    @Override
    public TLBool accountSetAccountTTL(TLAccountDaysTTL ttl) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestAccountSetAccountTTL(ttl));
    }

    @Override
    public TLPrivacyRules accountSetPrivacy(TLAbsInputPrivacyKey key,
            TLVector<TLAbsInputPrivacyRule> rules) throws RpcException, IOException {
        return (TLPrivacyRules) executeRpcQuery(new TLRequestAccountSetPrivacy(key, rules));
    }

    @Override
    public TLBool accountUnregisterDevice(int tokenType, String token) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestAccountUnregisterDevice(tokenType, token));
    }

    @Override
    public TLBool accountUpdateDeviceLocked(int period) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestAccountUpdateDeviceLocked(period));
    }

    @Override
    public TLBool accountUpdateNotifySettings(TLAbsInputNotifyPeer peer,
            TLInputPeerNotifySettings settings) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestAccountUpdateNotifySettings(peer, settings));
    }

    @Override
    public TLBool accountUpdatePasswordSettings(TLBytes currentPasswordHash,
            TLPasswordInputSettings newSettings) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestAccountUpdatePasswordSettings(currentPasswordHash, newSettings));
    }

    @Override
    public TLAbsUser accountUpdateProfile(String firstName, String lastName, String about) throws
            RpcException, IOException {
        return (TLAbsUser) executeRpcQuery(new TLRequestAccountUpdateProfile(firstName, lastName, about));
    }

    @Override
    public TLBool accountUpdateStatus(boolean offline) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestAccountUpdateStatus(offline));
    }

    @Override
    public TLAbsUser accountUpdateUsername(String username) throws RpcException, IOException {
        return (TLAbsUser) executeRpcQuery(new TLRequestAccountUpdateUsername(username));
    }

    @Override
    public TLBool authBindTempAuthKey(long permAuthKeyId, long nonce, int expiresAt,
            TLBytes encryptedMessage) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestAuthBindTempAuthKey(permAuthKeyId, nonce, expiresAt, encryptedMessage));
    }

    @Override
    public TLBool authCancelCode(String phoneNumber, String phoneCodeHash) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestAuthCancelCode(phoneNumber, phoneCodeHash));
    }

    @Override
    public TLAuthorization authCheckPassword(TLBytes passwordHash) throws RpcException,
            IOException {
        return (TLAuthorization) executeRpcQuery(new TLRequestAuthCheckPassword(passwordHash));
    }

    @Override
    public TLCheckedPhone authCheckPhone(String phoneNumber) throws RpcException, IOException {
        return (TLCheckedPhone) executeRpcQuery(new TLRequestAuthCheckPhone(phoneNumber));
    }

    @Override
    public TLBool authDropTempAuthKeys(TLLongVector exceptAuthKeys) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestAuthDropTempAuthKeys(exceptAuthKeys));
    }

    @Override
    public TLExportedAuthorization authExportAuthorization(int dcId) throws RpcException,
            IOException {
        return (TLExportedAuthorization) executeRpcQuery(new TLRequestAuthExportAuthorization(dcId));
    }

    @Override
    public TLAuthorization authImportAuthorization(int id, TLBytes bytes) throws RpcException,
            IOException {
        return (TLAuthorization) executeRpcQuery(new TLRequestAuthImportAuthorization(id, bytes));
    }

    @Override
    public TLAuthorization authImportBotAuthorization(int flags, int apiId, String apiHash,
            String botAuthToken) throws RpcException, IOException {
        return (TLAuthorization) executeRpcQuery(new TLRequestAuthImportBotAuthorization(flags, apiId, apiHash, botAuthToken));
    }

    @Override
    public TLBool authLogOut() throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestAuthLogOut());
    }

    @Override
    public TLAuthorization authRecoverPassword(String code) throws RpcException, IOException {
        return (TLAuthorization) executeRpcQuery(new TLRequestAuthRecoverPassword(code));
    }

    @Override
    public TLPasswordRecovery authRequestPasswordRecovery() throws RpcException, IOException {
        return (TLPasswordRecovery) executeRpcQuery(new TLRequestAuthRequestPasswordRecovery());
    }

    @Override
    public TLSentCode authResendCode(String phoneNumber, String phoneCodeHash) throws RpcException,
            IOException {
        return (TLSentCode) executeRpcQuery(new TLRequestAuthResendCode(phoneNumber, phoneCodeHash));
    }

    @Override
    public TLBool authResetAuthorizations() throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestAuthResetAuthorizations());
    }

    @Override
    public TLSentCode authSendCode(boolean allowFlashcall, String phoneNumber,
            boolean currentNumber, int apiId, String apiHash) throws RpcException, IOException {
        return (TLSentCode) executeRpcQuery(new TLRequestAuthSendCode(allowFlashcall, phoneNumber, currentNumber, apiId, apiHash));
    }

    @Override
    public TLBool authSendInvites(TLStringVector phoneNumbers, String message) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestAuthSendInvites(phoneNumbers, message));
    }

    @Override
    public TLAuthorization authSignIn(String phoneNumber, String phoneCodeHash, String phoneCode)
            throws RpcException, IOException {
        return (TLAuthorization) executeRpcQuery(new TLRequestAuthSignIn(phoneNumber, phoneCodeHash, phoneCode));
    }

    @Override
    public TLAuthorization authSignUp(String phoneNumber, String phoneCodeHash, String phoneCode,
            String firstName, String lastName) throws RpcException, IOException {
        return (TLAuthorization) executeRpcQuery(new TLRequestAuthSignUp(phoneNumber, phoneCodeHash, phoneCode, firstName, lastName));
    }

    @Override
    public TLBool botsAnswerWebhookJSONQuery(long queryId, TLDataJSON data) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestBotsAnswerWebhookJSONQuery(queryId, data));
    }

    @Override
    public TLDataJSON botsSendCustomRequest(String customMethod, TLDataJSON params) throws
            RpcException, IOException {
        return (TLDataJSON) executeRpcQuery(new TLRequestBotsSendCustomRequest(customMethod, params));
    }

    @Override
    public TLBool channelsCheckUsername(TLAbsInputChannel channel, String username) throws
            RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestChannelsCheckUsername(channel, username));
    }

    @Override
    public TLAbsUpdates channelsCreateChannel(boolean broadcast, boolean megagroup, String title,
            String about) throws RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestChannelsCreateChannel(broadcast, megagroup, title, about));
    }

    @Override
    public TLAbsUpdates channelsDeleteChannel(TLAbsInputChannel channel) throws RpcException,
            IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestChannelsDeleteChannel(channel));
    }

    @Override
    public TLAffectedMessages channelsDeleteMessages(TLAbsInputChannel channel, TLIntVector id)
            throws RpcException, IOException {
        return (TLAffectedMessages) executeRpcQuery(new TLRequestChannelsDeleteMessages(channel, id));
    }

    @Override
    public TLAffectedHistory channelsDeleteUserHistory(TLAbsInputChannel channel,
            TLAbsInputUser userId) throws RpcException, IOException {
        return (TLAffectedHistory) executeRpcQuery(new TLRequestChannelsDeleteUserHistory(channel, userId));
    }

    @Override
    public TLBool channelsEditAbout(TLAbsInputChannel channel, String about) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestChannelsEditAbout(channel, about));
    }

    @Override
    public TLAbsUpdates channelsEditAdmin(TLAbsInputChannel channel, TLAbsInputUser userId,
            TLAbsChannelParticipantRole role) throws RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestChannelsEditAdmin(channel, userId, role));
    }

    @Override
    public TLAbsUpdates channelsEditPhoto(TLAbsInputChannel channel, TLAbsInputChatPhoto photo)
            throws RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestChannelsEditPhoto(channel, photo));
    }

    @Override
    public TLAbsUpdates channelsEditTitle(TLAbsInputChannel channel, String title) throws
            RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestChannelsEditTitle(channel, title));
    }

    @Override
    public TLAbsExportedChatInvite channelsExportInvite(TLAbsInputChannel channel) throws
            RpcException, IOException {
        return (TLAbsExportedChatInvite) executeRpcQuery(new TLRequestChannelsExportInvite(channel));
    }

    @Override
    public TLExportedMessageLink channelsExportMessageLink(TLAbsInputChannel channel, int id) throws
            RpcException, IOException {
        return (TLExportedMessageLink) executeRpcQuery(new TLRequestChannelsExportMessageLink(channel, id));
    }

    @Override
    public TLAbsChats channelsGetAdminedPublicChannels() throws RpcException, IOException {
        return (TLAbsChats) executeRpcQuery(new TLRequestChannelsGetAdminedPublicChannels());
    }

    @Override
    public TLAbsChats channelsGetChannels(TLVector<TLAbsInputChannel> id) throws RpcException,
            IOException {
        return (TLAbsChats) executeRpcQuery(new TLRequestChannelsGetChannels(id));
    }

    @Override
    public TLChatFull channelsGetFullChannel(TLAbsInputChannel channel) throws RpcException,
            IOException {
        return (TLChatFull) executeRpcQuery(new TLRequestChannelsGetFullChannel(channel));
    }

    @Override
    public TLAbsMessages channelsGetMessages(TLAbsInputChannel channel, TLIntVector id) throws
            RpcException, IOException {
        return (TLAbsMessages) executeRpcQuery(new TLRequestChannelsGetMessages(channel, id));
    }

    @Override
    public TLChannelParticipant channelsGetParticipant(TLAbsInputChannel channel,
            TLAbsInputUser userId) throws RpcException, IOException {
        return (TLChannelParticipant) executeRpcQuery(new TLRequestChannelsGetParticipant(channel, userId));
    }

    @Override
    public TLChannelParticipants channelsGetParticipants(TLAbsInputChannel channel,
            TLAbsChannelParticipantsFilter filter, int offset, int limit) throws RpcException,
            IOException {
        return (TLChannelParticipants) executeRpcQuery(new TLRequestChannelsGetParticipants(channel, filter, offset, limit));
    }

    @Override
    public TLAbsUpdates channelsInviteToChannel(TLAbsInputChannel channel,
            TLVector<TLAbsInputUser> users) throws RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestChannelsInviteToChannel(channel, users));
    }

    @Override
    public TLAbsUpdates channelsJoinChannel(TLAbsInputChannel channel) throws RpcException,
            IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestChannelsJoinChannel(channel));
    }

    @Override
    public TLAbsUpdates channelsKickFromChannel(TLAbsInputChannel channel, TLAbsInputUser userId,
            boolean kicked) throws RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestChannelsKickFromChannel(channel, userId, kicked));
    }

    @Override
    public TLAbsUpdates channelsLeaveChannel(TLAbsInputChannel channel) throws RpcException,
            IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestChannelsLeaveChannel(channel));
    }

    @Override
    public TLBool channelsReadHistory(TLAbsInputChannel channel, int maxId) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestChannelsReadHistory(channel, maxId));
    }

    @Override
    public TLBool channelsReportSpam(TLAbsInputChannel channel, TLAbsInputUser userId,
            TLIntVector id) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestChannelsReportSpam(channel, userId, id));
    }

    @Override
    public TLAbsUpdates channelsToggleInvites(TLAbsInputChannel channel, boolean enabled) throws
            RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestChannelsToggleInvites(channel, enabled));
    }

    @Override
    public TLAbsUpdates channelsToggleSignatures(TLAbsInputChannel channel, boolean enabled) throws
            RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestChannelsToggleSignatures(channel, enabled));
    }

    @Override
    public TLAbsUpdates channelsUpdatePinnedMessage(boolean silent, TLAbsInputChannel channel,
            int id) throws RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestChannelsUpdatePinnedMessage(silent, channel, id));
    }

    @Override
    public TLBool channelsUpdateUsername(TLAbsInputChannel channel, String username) throws
            RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestChannelsUpdateUsername(channel, username));
    }

    @Override
    public TLBool contactsBlock(TLAbsInputUser id) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestContactsBlock(id));
    }

    @Override
    public TLLink contactsDeleteContact(TLAbsInputUser id) throws RpcException, IOException {
        return (TLLink) executeRpcQuery(new TLRequestContactsDeleteContact(id));
    }

    @Override
    public TLBool contactsDeleteContacts(TLVector<TLAbsInputUser> id) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestContactsDeleteContacts(id));
    }

    @Override
    public TLIntVector contactsExportCard() throws RpcException, IOException {
        return (TLIntVector) executeRpcQuery(new TLRequestContactsExportCard());
    }

    @Override
    public TLAbsBlocked contactsGetBlocked(int offset, int limit) throws RpcException, IOException {
        return (TLAbsBlocked) executeRpcQuery(new TLRequestContactsGetBlocked(offset, limit));
    }

    @Override
    public TLAbsContacts contactsGetContacts(String hash) throws RpcException, IOException {
        return (TLAbsContacts) executeRpcQuery(new TLRequestContactsGetContacts(hash));
    }

    @Override
    public TLVector<TLContactStatus> contactsGetStatuses() throws RpcException, IOException {
        return (TLVector<TLContactStatus>) executeRpcQuery(new TLRequestContactsGetStatuses());
    }

    @Override
    public TLAbsTopPeers contactsGetTopPeers(boolean correspondents, boolean botsPm,
            boolean botsInline, boolean groups, boolean channels, int offset, int limit, int hash)
            throws RpcException, IOException {
        return (TLAbsTopPeers) executeRpcQuery(new TLRequestContactsGetTopPeers(correspondents, botsPm, botsInline, groups, channels, offset, limit, hash));
    }

    @Override
    public TLAbsUser contactsImportCard(TLIntVector exportCard) throws RpcException, IOException {
        return (TLAbsUser) executeRpcQuery(new TLRequestContactsImportCard(exportCard));
    }

    @Override
    public TLImportedContacts contactsImportContacts(TLVector<TLInputPhoneContact> contacts,
            boolean replace) throws RpcException, IOException {
        return (TLImportedContacts) executeRpcQuery(new TLRequestContactsImportContacts(contacts, replace));
    }

    @Override
    public TLBool contactsResetTopPeerRating(TLAbsTopPeerCategory category, TLAbsInputPeer peer)
            throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestContactsResetTopPeerRating(category, peer));
    }

    @Override
    public TLResolvedPeer contactsResolveUsername(String username) throws RpcException,
            IOException {
        return (TLResolvedPeer) executeRpcQuery(new TLRequestContactsResolveUsername(username));
    }

    @Override
    public TLFound contactsSearch(String q, int limit) throws RpcException, IOException {
        return (TLFound) executeRpcQuery(new TLRequestContactsSearch(q, limit));
    }

    @Override
    public TLBool contactsUnblock(TLAbsInputUser id) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestContactsUnblock(id));
    }

    @Override
    public TLBool contestSaveDeveloperInfo(int vkId, String name, String phoneNumber, int age,
            String city) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestContestSaveDeveloperInfo(vkId, name, phoneNumber, age, city));
    }

    @Override
    public TLAbsUpdates helpGetAppChangelog(String prevAppVersion) throws RpcException,
            IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestHelpGetAppChangelog(prevAppVersion));
    }

    @Override
    public TLAbsAppUpdate helpGetAppUpdate() throws RpcException, IOException {
        return (TLAbsAppUpdate) executeRpcQuery(new TLRequestHelpGetAppUpdate());
    }

    @Override
    public TLCdnConfig helpGetCdnConfig() throws RpcException, IOException {
        return (TLCdnConfig) executeRpcQuery(new TLRequestHelpGetCdnConfig());
    }

    @Override
    public TLConfig helpGetConfig() throws RpcException, IOException {
        return (TLConfig) executeRpcQuery(new TLRequestHelpGetConfig());
    }

    @Override
    public TLInviteText helpGetInviteText() throws RpcException, IOException {
        return (TLInviteText) executeRpcQuery(new TLRequestHelpGetInviteText());
    }

    @Override
    public TLNearestDc helpGetNearestDc() throws RpcException, IOException {
        return (TLNearestDc) executeRpcQuery(new TLRequestHelpGetNearestDc());
    }

    @Override
    public TLSupport helpGetSupport() throws RpcException, IOException {
        return (TLSupport) executeRpcQuery(new TLRequestHelpGetSupport());
    }

    @Override
    public TLTermsOfService helpGetTermsOfService() throws RpcException, IOException {
        return (TLTermsOfService) executeRpcQuery(new TLRequestHelpGetTermsOfService());
    }

    @Override
    public TLBool helpSaveAppLog(TLVector<TLInputAppEvent> events) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestHelpSaveAppLog(events));
    }

    @Override
    public TLBool helpSetBotUpdatesStatus(int pendingUpdatesCount, String message) throws
            RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestHelpSetBotUpdatesStatus(pendingUpdatesCount, message));
    }

    @Override
    public <T extends TLObject> T initConnection(int apiId, String deviceModel,
            String systemVersion, String appVersion, String langCode, TLMethod<T> query) throws
            RpcException, IOException {
        return (T) executeRpcQuery(new TLRequestInitConnection(apiId, deviceModel, systemVersion, appVersion, langCode, query));
    }

    @Override
    public <T extends TLObject> T invokeAfterMsg(long msgId, TLMethod<T> query) throws RpcException,
            IOException {
        return (T) executeRpcQuery(new TLRequestInvokeAfterMsg(msgId, query));
    }

    @Override
    public <T extends TLObject> T invokeAfterMsgs(TLLongVector msgIds, TLMethod<T> query) throws
            RpcException, IOException {
        return (T) executeRpcQuery(new TLRequestInvokeAfterMsgs(msgIds, query));
    }

    @Override
    public <T extends TLObject> T invokeWithLayer(int layer, TLMethod<T> query) throws RpcException,
            IOException {
        return (T) executeRpcQuery(new TLRequestInvokeWithLayer(layer, query));
    }

    @Override
    public <T extends TLObject> T invokeWithoutUpdates(TLMethod<T> query) throws RpcException,
            IOException {
        return (T) executeRpcQuery(new TLRequestInvokeWithoutUpdates(query));
    }

    @Override
    public TLAbsEncryptedChat messagesAcceptEncryption(TLInputEncryptedChat peer, TLBytes gB,
            long keyFingerprint) throws RpcException, IOException {
        return (TLAbsEncryptedChat) executeRpcQuery(new TLRequestMessagesAcceptEncryption(peer, gB, keyFingerprint));
    }

    @Override
    public TLAbsUpdates messagesAddChatUser(int chatId, TLAbsInputUser userId, int fwdLimit) throws
            RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesAddChatUser(chatId, userId, fwdLimit));
    }

    @Override
    public TLAbsChatInvite messagesCheckChatInvite(String hash) throws RpcException, IOException {
        return (TLAbsChatInvite) executeRpcQuery(new TLRequestMessagesCheckChatInvite(hash));
    }

    @Override
    public TLBool messagesClearRecentStickers(boolean attached) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesClearRecentStickers(attached));
    }

    @Override
    public TLAbsUpdates messagesCreateChat(TLVector<TLAbsInputUser> users, String title) throws
            RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesCreateChat(users, title));
    }

    @Override
    public TLAbsUpdates messagesDeleteChatUser(int chatId, TLAbsInputUser userId) throws
            RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesDeleteChatUser(chatId, userId));
    }

    @Override
    public TLAffectedHistory messagesDeleteHistory(boolean justClear, TLAbsInputPeer peer,
            int maxId) throws RpcException, IOException {
        return (TLAffectedHistory) executeRpcQuery(new TLRequestMessagesDeleteHistory(justClear, peer, maxId));
    }

    @Override
    public TLAffectedMessages messagesDeleteMessages(boolean revoke, TLIntVector id) throws
            RpcException, IOException {
        return (TLAffectedMessages) executeRpcQuery(new TLRequestMessagesDeleteMessages(revoke, id));
    }

    @Override
    public TLBool messagesDiscardEncryption(int chatId) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesDiscardEncryption(chatId));
    }

    @Override
    public TLBool messagesEditChatAdmin(int chatId, TLAbsInputUser userId, boolean isAdmin) throws
            RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesEditChatAdmin(chatId, userId, isAdmin));
    }

    @Override
    public TLAbsUpdates messagesEditChatPhoto(int chatId, TLAbsInputChatPhoto photo) throws
            RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesEditChatPhoto(chatId, photo));
    }

    @Override
    public TLAbsUpdates messagesEditChatTitle(int chatId, String title) throws RpcException,
            IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesEditChatTitle(chatId, title));
    }

    @Override
    public TLBool messagesEditInlineBotMessage(boolean noWebpage, TLInputBotInlineMessageID id,
            String message, TLAbsReplyMarkup replyMarkup, TLVector<TLAbsMessageEntity> entities)
            throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesEditInlineBotMessage(noWebpage, id, message, replyMarkup, entities));
    }

    @Override
    public TLAbsUpdates messagesEditMessage(boolean noWebpage, TLAbsInputPeer peer, int id,
            String message, TLAbsReplyMarkup replyMarkup, TLVector<TLAbsMessageEntity> entities)
            throws RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesEditMessage(noWebpage, peer, id, message, replyMarkup, entities));
    }

    @Override
    public TLAbsExportedChatInvite messagesExportChatInvite(int chatId) throws RpcException,
            IOException {
        return (TLAbsExportedChatInvite) executeRpcQuery(new TLRequestMessagesExportChatInvite(chatId));
    }

    @Override
    public TLAbsUpdates messagesForwardMessage(TLAbsInputPeer peer, int id, long randomId) throws
            RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesForwardMessage(peer, id, randomId));
    }

    @Override
    public TLAbsUpdates messagesForwardMessages(boolean silent, boolean background,
            boolean withMyScore, TLAbsInputPeer fromPeer, TLIntVector id, TLLongVector randomId,
            TLAbsInputPeer toPeer) throws RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesForwardMessages(silent, background, withMyScore, fromPeer, id, randomId, toPeer));
    }

    @Override
    public TLAbsChats messagesGetAllChats(TLIntVector exceptIds) throws RpcException, IOException {
        return (TLAbsChats) executeRpcQuery(new TLRequestMessagesGetAllChats(exceptIds));
    }

    @Override
    public TLAbsUpdates messagesGetAllDrafts() throws RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesGetAllDrafts());
    }

    @Override
    public TLAbsAllStickers messagesGetAllStickers(int hash) throws RpcException, IOException {
        return (TLAbsAllStickers) executeRpcQuery(new TLRequestMessagesGetAllStickers(hash));
    }

    @Override
    public TLArchivedStickers messagesGetArchivedStickers(boolean masks, long offsetId, int limit)
            throws RpcException, IOException {
        return (TLArchivedStickers) executeRpcQuery(new TLRequestMessagesGetArchivedStickers(masks, offsetId, limit));
    }

    @Override
    public TLVector<TLAbsStickerSetCovered> messagesGetAttachedStickers(
            TLAbsInputStickeredMedia media) throws RpcException, IOException {
        return (TLVector<TLAbsStickerSetCovered>) executeRpcQuery(new TLRequestMessagesGetAttachedStickers(media));
    }

    @Override
    public TLBotCallbackAnswer messagesGetBotCallbackAnswer(boolean game, TLAbsInputPeer peer,
            int msgId, TLBytes data) throws RpcException, IOException {
        return (TLBotCallbackAnswer) executeRpcQuery(new TLRequestMessagesGetBotCallbackAnswer(game, peer, msgId, data));
    }

    @Override
    public TLAbsChats messagesGetChats(TLIntVector id) throws RpcException, IOException {
        return (TLAbsChats) executeRpcQuery(new TLRequestMessagesGetChats(id));
    }

    @Override
    public TLAbsChats messagesGetCommonChats(TLAbsInputUser userId, int maxId, int limit) throws
            RpcException, IOException {
        return (TLAbsChats) executeRpcQuery(new TLRequestMessagesGetCommonChats(userId, maxId, limit));
    }

    @Override
    public TLAbsDhConfig messagesGetDhConfig(int version, int randomLength) throws RpcException,
            IOException {
        return (TLAbsDhConfig) executeRpcQuery(new TLRequestMessagesGetDhConfig(version, randomLength));
    }

    @Override
    public TLAbsDialogs messagesGetDialogs(boolean excludePinned, int offsetDate, int offsetId,
            TLAbsInputPeer offsetPeer, int limit) throws RpcException, IOException {
        return (TLAbsDialogs) executeRpcQuery(new TLRequestMessagesGetDialogs(excludePinned, offsetDate, offsetId, offsetPeer, limit));
    }

    @Override
    public TLAbsDocument messagesGetDocumentByHash(TLBytes sha256, int size, String mimeType) throws
            RpcException, IOException {
        return (TLAbsDocument) executeRpcQuery(new TLRequestMessagesGetDocumentByHash(sha256, size, mimeType));
    }

    @Override
    public TLAbsFeaturedStickers messagesGetFeaturedStickers(int hash) throws RpcException,
            IOException {
        return (TLAbsFeaturedStickers) executeRpcQuery(new TLRequestMessagesGetFeaturedStickers(hash));
    }

    @Override
    public TLChatFull messagesGetFullChat(int chatId) throws RpcException, IOException {
        return (TLChatFull) executeRpcQuery(new TLRequestMessagesGetFullChat(chatId));
    }

    @Override
    public TLHighScores messagesGetGameHighScores(TLAbsInputPeer peer, int id,
            TLAbsInputUser userId) throws RpcException, IOException {
        return (TLHighScores) executeRpcQuery(new TLRequestMessagesGetGameHighScores(peer, id, userId));
    }

    @Override
    public TLAbsMessages messagesGetHistory(TLAbsInputPeer peer, int offsetId, int offsetDate,
            int addOffset, int limit, int maxId, int minId) throws RpcException, IOException {
        return (TLAbsMessages) executeRpcQuery(new TLRequestMessagesGetHistory(peer, offsetId, offsetDate, addOffset, limit, maxId, minId));
    }

    @Override
    public TLBotResults messagesGetInlineBotResults(TLAbsInputUser bot, TLAbsInputPeer peer,
            TLAbsInputGeoPoint geoPoint, String query, String offset) throws RpcException,
            IOException {
        return (TLBotResults) executeRpcQuery(new TLRequestMessagesGetInlineBotResults(bot, peer, geoPoint, query, offset));
    }

    @Override
    public TLHighScores messagesGetInlineGameHighScores(TLInputBotInlineMessageID id,
            TLAbsInputUser userId) throws RpcException, IOException {
        return (TLHighScores) executeRpcQuery(new TLRequestMessagesGetInlineGameHighScores(id, userId));
    }

    @Override
    public TLAbsAllStickers messagesGetMaskStickers(int hash) throws RpcException, IOException {
        return (TLAbsAllStickers) executeRpcQuery(new TLRequestMessagesGetMaskStickers(hash));
    }

    @Override
    public TLMessageEditData messagesGetMessageEditData(TLAbsInputPeer peer, int id) throws
            RpcException, IOException {
        return (TLMessageEditData) executeRpcQuery(new TLRequestMessagesGetMessageEditData(peer, id));
    }

    @Override
    public TLAbsMessages messagesGetMessages(TLIntVector id) throws RpcException, IOException {
        return (TLAbsMessages) executeRpcQuery(new TLRequestMessagesGetMessages(id));
    }

    @Override
    public TLIntVector messagesGetMessagesViews(TLAbsInputPeer peer, TLIntVector id,
            boolean increment) throws RpcException, IOException {
        return (TLIntVector) executeRpcQuery(new TLRequestMessagesGetMessagesViews(peer, id, increment));
    }

    @Override
    public TLPeerDialogs messagesGetPeerDialogs(TLVector<TLAbsInputPeer> peers) throws RpcException,
            IOException {
        return (TLPeerDialogs) executeRpcQuery(new TLRequestMessagesGetPeerDialogs(peers));
    }

    @Override
    public TLPeerSettings messagesGetPeerSettings(TLAbsInputPeer peer) throws RpcException,
            IOException {
        return (TLPeerSettings) executeRpcQuery(new TLRequestMessagesGetPeerSettings(peer));
    }

    @Override
    public TLPeerDialogs messagesGetPinnedDialogs() throws RpcException, IOException {
        return (TLPeerDialogs) executeRpcQuery(new TLRequestMessagesGetPinnedDialogs());
    }

    @Override
    public TLAbsRecentStickers messagesGetRecentStickers(boolean attached, int hash) throws
            RpcException, IOException {
        return (TLAbsRecentStickers) executeRpcQuery(new TLRequestMessagesGetRecentStickers(attached, hash));
    }

    @Override
    public TLAbsSavedGifs messagesGetSavedGifs(int hash) throws RpcException, IOException {
        return (TLAbsSavedGifs) executeRpcQuery(new TLRequestMessagesGetSavedGifs(hash));
    }

    @Override
    public TLStickerSet messagesGetStickerSet(TLAbsInputStickerSet stickerset) throws RpcException,
            IOException {
        return (TLStickerSet) executeRpcQuery(new TLRequestMessagesGetStickerSet(stickerset));
    }

    @Override
    public TLAbsWebPage messagesGetWebPage(String url, int hash) throws RpcException, IOException {
        return (TLAbsWebPage) executeRpcQuery(new TLRequestMessagesGetWebPage(url, hash));
    }

    @Override
    public TLAbsMessageMedia messagesGetWebPagePreview(String message) throws RpcException,
            IOException {
        return (TLAbsMessageMedia) executeRpcQuery(new TLRequestMessagesGetWebPagePreview(message));
    }

    @Override
    public TLBool messagesHideReportSpam(TLAbsInputPeer peer) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesHideReportSpam(peer));
    }

    @Override
    public TLAbsUpdates messagesImportChatInvite(String hash) throws RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesImportChatInvite(hash));
    }

    @Override
    public TLAbsStickerSetInstallResult messagesInstallStickerSet(TLAbsInputStickerSet stickerset,
            boolean archived) throws RpcException, IOException {
        return (TLAbsStickerSetInstallResult) executeRpcQuery(new TLRequestMessagesInstallStickerSet(stickerset, archived));
    }

    @Override
    public TLAbsUpdates messagesMigrateChat(int chatId) throws RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesMigrateChat(chatId));
    }

    @Override
    public TLBool messagesReadEncryptedHistory(TLInputEncryptedChat peer, int maxDate) throws
            RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesReadEncryptedHistory(peer, maxDate));
    }

    @Override
    public TLBool messagesReadFeaturedStickers(TLLongVector id) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesReadFeaturedStickers(id));
    }

    @Override
    public TLAffectedMessages messagesReadHistory(TLAbsInputPeer peer, int maxId) throws
            RpcException, IOException {
        return (TLAffectedMessages) executeRpcQuery(new TLRequestMessagesReadHistory(peer, maxId));
    }

    @Override
    public TLAffectedMessages messagesReadMessageContents(TLIntVector id) throws RpcException,
            IOException {
        return (TLAffectedMessages) executeRpcQuery(new TLRequestMessagesReadMessageContents(id));
    }

    @Override
    public TLVector<TLReceivedNotifyMessage> messagesReceivedMessages(int maxId) throws
            RpcException, IOException {
        return (TLVector<TLReceivedNotifyMessage>) executeRpcQuery(new TLRequestMessagesReceivedMessages(maxId));
    }

    @Override
    public TLLongVector messagesReceivedQueue(int maxQts) throws RpcException, IOException {
        return (TLLongVector) executeRpcQuery(new TLRequestMessagesReceivedQueue(maxQts));
    }

    @Override
    public TLBool messagesReorderPinnedDialogs(boolean force, TLVector<TLAbsInputPeer> order) throws
            RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesReorderPinnedDialogs(force, order));
    }

    @Override
    public TLBool messagesReorderStickerSets(boolean masks, TLLongVector order) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesReorderStickerSets(masks, order));
    }

    @Override
    public TLBool messagesReportEncryptedSpam(TLInputEncryptedChat peer) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesReportEncryptedSpam(peer));
    }

    @Override
    public TLBool messagesReportSpam(TLAbsInputPeer peer) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesReportSpam(peer));
    }

    @Override
    public TLAbsEncryptedChat messagesRequestEncryption(TLAbsInputUser userId, int randomId,
            TLBytes gA) throws RpcException, IOException {
        return (TLAbsEncryptedChat) executeRpcQuery(new TLRequestMessagesRequestEncryption(userId, randomId, gA));
    }

    @Override
    public TLBool messagesSaveDraft(boolean noWebpage, Integer replyToMsgId, TLAbsInputPeer peer,
            String message, TLVector<TLAbsMessageEntity> entities) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesSaveDraft(noWebpage, replyToMsgId, peer, message, entities));
    }

    @Override
    public TLBool messagesSaveGif(TLAbsInputDocument id, boolean unsave) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesSaveGif(id, unsave));
    }

    @Override
    public TLBool messagesSaveRecentSticker(boolean attached, TLAbsInputDocument id, boolean unsave)
            throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesSaveRecentSticker(attached, id, unsave));
    }

    @Override
    public TLAbsMessages messagesSearch(TLAbsInputPeer peer, String q, TLAbsMessagesFilter filter,
            int minDate, int maxDate, int offset, int maxId, int limit) throws RpcException,
            IOException {
        return (TLAbsMessages) executeRpcQuery(new TLRequestMessagesSearch(peer, q, filter, minDate, maxDate, offset, maxId, limit));
    }

    @Override
    public TLFoundGifs messagesSearchGifs(String q, int offset) throws RpcException, IOException {
        return (TLFoundGifs) executeRpcQuery(new TLRequestMessagesSearchGifs(q, offset));
    }

    @Override
    public TLAbsMessages messagesSearchGlobal(String q, int offsetDate, TLAbsInputPeer offsetPeer,
            int offsetId, int limit) throws RpcException, IOException {
        return (TLAbsMessages) executeRpcQuery(new TLRequestMessagesSearchGlobal(q, offsetDate, offsetPeer, offsetId, limit));
    }

    @Override
    public TLAbsSentEncryptedMessage messagesSendEncrypted(TLInputEncryptedChat peer, long randomId,
            TLBytes data) throws RpcException, IOException {
        return (TLAbsSentEncryptedMessage) executeRpcQuery(new TLRequestMessagesSendEncrypted(peer, randomId, data));
    }

    @Override
    public TLAbsSentEncryptedMessage messagesSendEncryptedFile(TLInputEncryptedChat peer,
            long randomId, TLBytes data, TLAbsInputEncryptedFile file) throws RpcException,
            IOException {
        return (TLAbsSentEncryptedMessage) executeRpcQuery(new TLRequestMessagesSendEncryptedFile(peer, randomId, data, file));
    }

    @Override
    public TLAbsSentEncryptedMessage messagesSendEncryptedService(TLInputEncryptedChat peer,
            long randomId, TLBytes data) throws RpcException, IOException {
        return (TLAbsSentEncryptedMessage) executeRpcQuery(new TLRequestMessagesSendEncryptedService(peer, randomId, data));
    }

    @Override
    public TLAbsUpdates messagesSendInlineBotResult(boolean silent, boolean background,
            boolean clearDraft, TLAbsInputPeer peer, Integer replyToMsgId, long randomId,
            long queryId, String id) throws RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesSendInlineBotResult(silent, background, clearDraft, peer, replyToMsgId, randomId, queryId, id));
    }

    @Override
    public TLAbsUpdates messagesSendMedia(boolean silent, boolean background, boolean clearDraft,
            TLAbsInputPeer peer, Integer replyToMsgId, TLAbsInputMedia media, long randomId,
            TLAbsReplyMarkup replyMarkup) throws RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesSendMedia(silent, background, clearDraft, peer, replyToMsgId, media, randomId, replyMarkup));
    }

    @Override
    public TLAbsUpdates messagesSendMessage(boolean noWebpage, boolean silent, boolean background,
            boolean clearDraft, TLAbsInputPeer peer, Integer replyToMsgId, String message,
            long randomId, TLAbsReplyMarkup replyMarkup, TLVector<TLAbsMessageEntity> entities)
            throws RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesSendMessage(noWebpage, silent, background, clearDraft, peer, replyToMsgId, message, randomId, replyMarkup, entities));
    }

    @Override
    public TLBool messagesSetBotCallbackAnswer(boolean alert, long queryId, String message,
            String url, int cacheTime) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesSetBotCallbackAnswer(alert, queryId, message, url, cacheTime));
    }

    @Override
    public TLBool messagesSetBotPrecheckoutResults(boolean success, long queryId, String error)
            throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesSetBotPrecheckoutResults(success, queryId, error));
    }

    @Override
    public TLBool messagesSetBotShippingResults(long queryId, String error,
            TLVector<TLShippingOption> shippingOptions) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesSetBotShippingResults(queryId, error, shippingOptions));
    }

    @Override
    public TLBool messagesSetEncryptedTyping(TLInputEncryptedChat peer, boolean typing) throws
            RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesSetEncryptedTyping(peer, typing));
    }

    @Override
    public TLAbsUpdates messagesSetGameScore(boolean editMessage, boolean force,
            TLAbsInputPeer peer, int id, TLAbsInputUser userId, int score) throws RpcException,
            IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesSetGameScore(editMessage, force, peer, id, userId, score));
    }

    @Override
    public TLBool messagesSetInlineBotResults(boolean gallery, boolean _private, long queryId,
            TLVector<TLAbsInputBotInlineResult> results, int cacheTime, String nextOffset,
            TLInlineBotSwitchPM switchPm) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesSetInlineBotResults(gallery, _private, queryId, results, cacheTime, nextOffset, switchPm));
    }

    @Override
    public TLBool messagesSetInlineGameScore(boolean editMessage, boolean force,
            TLInputBotInlineMessageID id, TLAbsInputUser userId, int score) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesSetInlineGameScore(editMessage, force, id, userId, score));
    }

    @Override
    public TLBool messagesSetTyping(TLAbsInputPeer peer, TLAbsSendMessageAction action) throws
            RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesSetTyping(peer, action));
    }

    @Override
    public TLAbsUpdates messagesStartBot(TLAbsInputUser bot, TLAbsInputPeer peer, long randomId,
            String startParam) throws RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesStartBot(bot, peer, randomId, startParam));
    }

    @Override
    public TLAbsUpdates messagesToggleChatAdmins(int chatId, boolean enabled) throws RpcException,
            IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestMessagesToggleChatAdmins(chatId, enabled));
    }

    @Override
    public TLBool messagesToggleDialogPin(boolean pinned, TLAbsInputPeer peer) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesToggleDialogPin(pinned, peer));
    }

    @Override
    public TLBool messagesUninstallStickerSet(TLAbsInputStickerSet stickerset) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestMessagesUninstallStickerSet(stickerset));
    }

    @Override
    public TLBool paymentsClearSavedInfo(boolean credentials, boolean info) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestPaymentsClearSavedInfo(credentials, info));
    }

    @Override
    public TLPaymentForm paymentsGetPaymentForm(int msgId) throws RpcException, IOException {
        return (TLPaymentForm) executeRpcQuery(new TLRequestPaymentsGetPaymentForm(msgId));
    }

    @Override
    public TLPaymentReceipt paymentsGetPaymentReceipt(int msgId) throws RpcException, IOException {
        return (TLPaymentReceipt) executeRpcQuery(new TLRequestPaymentsGetPaymentReceipt(msgId));
    }

    @Override
    public TLSavedInfo paymentsGetSavedInfo() throws RpcException, IOException {
        return (TLSavedInfo) executeRpcQuery(new TLRequestPaymentsGetSavedInfo());
    }

    @Override
    public TLAbsPaymentResult paymentsSendPaymentForm(int msgId, String requestedInfoId,
            String shippingOptionId, TLAbsInputPaymentCredentials credentials) throws RpcException,
            IOException {
        return (TLAbsPaymentResult) executeRpcQuery(new TLRequestPaymentsSendPaymentForm(msgId, requestedInfoId, shippingOptionId, credentials));
    }

    @Override
    public TLValidatedRequestedInfo paymentsValidateRequestedInfo(boolean save, int msgId,
            TLPaymentRequestedInfo info) throws RpcException, IOException {
        return (TLValidatedRequestedInfo) executeRpcQuery(new TLRequestPaymentsValidateRequestedInfo(save, msgId, info));
    }

    @Override
    public TLPhoneCall phoneAcceptCall(TLInputPhoneCall peer, TLBytes gB,
            TLPhoneCallProtocol protocol) throws RpcException, IOException {
        return (TLPhoneCall) executeRpcQuery(new TLRequestPhoneAcceptCall(peer, gB, protocol));
    }

    @Override
    public TLPhoneCall phoneConfirmCall(TLInputPhoneCall peer, TLBytes gA, long keyFingerprint,
            TLPhoneCallProtocol protocol) throws RpcException, IOException {
        return (TLPhoneCall) executeRpcQuery(new TLRequestPhoneConfirmCall(peer, gA, keyFingerprint, protocol));
    }

    @Override
    public TLAbsUpdates phoneDiscardCall(TLInputPhoneCall peer, int duration,
            TLAbsPhoneCallDiscardReason reason, long connectionId) throws RpcException,
            IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestPhoneDiscardCall(peer, duration, reason, connectionId));
    }

    @Override
    public TLDataJSON phoneGetCallConfig() throws RpcException, IOException {
        return (TLDataJSON) executeRpcQuery(new TLRequestPhoneGetCallConfig());
    }

    @Override
    public TLBool phoneReceivedCall(TLInputPhoneCall peer) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestPhoneReceivedCall(peer));
    }

    @Override
    public TLPhoneCall phoneRequestCall(TLAbsInputUser userId, int randomId, TLBytes gAHash,
            TLPhoneCallProtocol protocol) throws RpcException, IOException {
        return (TLPhoneCall) executeRpcQuery(new TLRequestPhoneRequestCall(userId, randomId, gAHash, protocol));
    }

    @Override
    public TLBool phoneSaveCallDebug(TLInputPhoneCall peer, TLDataJSON debug) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestPhoneSaveCallDebug(peer, debug));
    }

    @Override
    public TLAbsUpdates phoneSetCallRating(TLInputPhoneCall peer, int rating, String comment) throws
            RpcException, IOException {
        return (TLAbsUpdates) executeRpcQuery(new TLRequestPhoneSetCallRating(peer, rating, comment));
    }

    @Override
    public TLLongVector photosDeletePhotos(TLVector<TLAbsInputPhoto> id) throws RpcException,
            IOException {
        return (TLLongVector) executeRpcQuery(new TLRequestPhotosDeletePhotos(id));
    }

    @Override
    public TLAbsPhotos photosGetUserPhotos(TLAbsInputUser userId, int offset, long maxId, int limit)
            throws RpcException, IOException {
        return (TLAbsPhotos) executeRpcQuery(new TLRequestPhotosGetUserPhotos(userId, offset, maxId, limit));
    }

    @Override
    public TLAbsUserProfilePhoto photosUpdateProfilePhoto(TLAbsInputPhoto id) throws RpcException,
            IOException {
        return (TLAbsUserProfilePhoto) executeRpcQuery(new TLRequestPhotosUpdateProfilePhoto(id));
    }

    @Override
    public TLPhoto photosUploadProfilePhoto(TLAbsInputFile file) throws RpcException, IOException {
        return (TLPhoto) executeRpcQuery(new TLRequestPhotosUploadProfilePhoto(file));
    }

    @Override
    public TLAbsChannelDifference updatesGetChannelDifference(boolean force,
            TLAbsInputChannel channel, TLAbsChannelMessagesFilter filter, int pts, int limit) throws
            RpcException, IOException {
        return (TLAbsChannelDifference) executeRpcQuery(new TLRequestUpdatesGetChannelDifference(force, channel, filter, pts, limit));
    }

    @Override
    public TLAbsDifference updatesGetDifference(int pts, Integer ptsTotalLimit, int date, int qts)
            throws RpcException, IOException {
        return (TLAbsDifference) executeRpcQuery(new TLRequestUpdatesGetDifference(pts, ptsTotalLimit, date, qts));
    }

    @Override
    public TLState updatesGetState() throws RpcException, IOException {
        return (TLState) executeRpcQuery(new TLRequestUpdatesGetState());
    }

    @Override
    public TLAbsCdnFile uploadGetCdnFile(TLBytes fileToken, int offset, int limit) throws
            RpcException, IOException {
        return (TLAbsCdnFile) executeRpcQuery(new TLRequestUploadGetCdnFile(fileToken, offset, limit));
    }

    @Override
    public TLAbsFile uploadGetFile(TLAbsInputFileLocation location, int offset, int limit) throws
            RpcException, IOException {
        return (TLAbsFile) executeRpcQuery(new TLRequestUploadGetFile(location, offset, limit));
    }

    @Override
    public TLWebFile uploadGetWebFile(TLInputWebFileLocation location, int offset, int limit) throws
            RpcException, IOException {
        return (TLWebFile) executeRpcQuery(new TLRequestUploadGetWebFile(location, offset, limit));
    }

    @Override
    public TLBool uploadReuploadCdnFile(TLBytes fileToken, TLBytes requestToken) throws
            RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestUploadReuploadCdnFile(fileToken, requestToken));
    }

    @Override
    public TLBool uploadSaveBigFilePart(long fileId, int filePart, int fileTotalParts,
            TLBytes bytes) throws RpcException, IOException {
        return (TLBool) executeRpcQuery(new TLRequestUploadSaveBigFilePart(fileId, filePart, fileTotalParts, bytes));
    }

    @Override
    public TLBool uploadSaveFilePart(long fileId, int filePart, TLBytes bytes) throws RpcException,
            IOException {
        return (TLBool) executeRpcQuery(new TLRequestUploadSaveFilePart(fileId, filePart, bytes));
    }

    @Override
    public TLUserFull usersGetFullUser(TLAbsInputUser id) throws RpcException, IOException {
        return (TLUserFull) executeRpcQuery(new TLRequestUsersGetFullUser(id));
    }

    @Override
    public TLVector<TLAbsUser> usersGetUsers(TLVector<TLAbsInputUser> id) throws RpcException,
            IOException {
        return (TLVector<TLAbsUser>) executeRpcQuery(new TLRequestUsersGetUsers(id));
    }
}
