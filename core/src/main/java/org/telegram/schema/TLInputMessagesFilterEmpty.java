package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputMessagesFilterEmpty extends TLAbsMessagesFilter {
    public static final int CLASS_ID = 0x57e2f66c;

    public TLInputMessagesFilterEmpty() {
    }

    @Override
    public String toString() {
        return "inputMessagesFilterEmpty#57e2f66c";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
