package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLPageBlockUnsupported extends TLAbsPageBlock {
    public static final int CLASS_ID = 0x13567e8a;

    public TLPageBlockUnsupported() {
    }

    @Override
    public String toString() {
        return "pageBlockUnsupported#13567e8a";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
