package org.telegram.schema.photos;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsPhoto;
import org.telegram.schema.TLAbsUser;

/**
 * @author Telegram Schema Generator
 */
public class TLPhotosSlice extends TLAbsPhotos {
    public static final int CLASS_ID = 0x15051f54;

    protected int count;

    protected TLVector<TLAbsPhoto> photos;

    protected TLVector<TLAbsUser> users;

    public TLPhotosSlice() {
    }

    public TLPhotosSlice(int count, TLVector<TLAbsPhoto> photos, TLVector<TLAbsUser> users) {
        this.count = count;
        this.photos = photos;
        this.users = users;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(count, stream);
        writeTLVector(photos, stream);
        writeTLVector(users, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        count = readInt(stream);
        photos = readTLVector(stream, context);
        users = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += photos.computeSerializedSize();
        size += users.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "photos.photosSlice#15051f54";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public TLVector<TLAbsPhoto> getPhotos() {
        return photos;
    }

    public void setPhotos(TLVector<TLAbsPhoto> photos) {
        this.photos = photos;
    }

    public TLVector<TLAbsUser> getUsers() {
        return users;
    }

    public void setUsers(TLVector<TLAbsUser> users) {
        this.users = users;
    }
}
