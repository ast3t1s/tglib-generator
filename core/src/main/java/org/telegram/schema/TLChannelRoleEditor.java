package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLChannelRoleEditor extends TLAbsChannelParticipantRole {
    public static final int CLASS_ID = 0x820bfe8c;

    public TLChannelRoleEditor() {
    }

    @Override
    public String toString() {
        return "channelRoleEditor#820bfe8c";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
