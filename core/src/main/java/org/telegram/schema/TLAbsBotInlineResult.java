package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLBotInlineMediaResult: botInlineMediaResult#17db940b</li>
 * <li>{@link TLBotInlineResult: botInlineResult#9bebaeb9</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsBotInlineResult extends TLObject {
    protected TLAbsBotInlineMessage sendMessage;

    public TLAbsBotInlineResult() {
    }

    public TLAbsBotInlineMessage getSendMessage() {
        return sendMessage;
    }

    public void setSendMessage(TLAbsBotInlineMessage sendMessage) {
        this.sendMessage = sendMessage;
    }
}
