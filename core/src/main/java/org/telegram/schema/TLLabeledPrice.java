package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLLabeledPrice extends TLObject {
    public static final int CLASS_ID = 0xcb296bf8;

    protected String label;

    protected long amount;

    public TLLabeledPrice() {
    }

    public TLLabeledPrice(String label, long amount) {
        this.label = label;
        this.amount = amount;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(label, stream);
        writeLong(amount, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        label = readTLString(stream);
        amount = readLong(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(label);
        size += SIZE_INT64;
        return size;
    }

    @Override
    public String toString() {
        return "labeledPrice#cb296bf8";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
