package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLMessageActionEmpty extends TLAbsMessageAction {
    public static final int CLASS_ID = 0xb6aef7b0;

    public TLMessageActionEmpty() {
    }

    @Override
    public String toString() {
        return "messageActionEmpty#b6aef7b0";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
