package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLContactLinkContact: contactLinkContact#d502c2d0</li>
 * <li>{@link TLContactLinkHasPhone: contactLinkHasPhone#268f3f59</li>
 * <li>{@link TLContactLinkNone: contactLinkNone#feedd3ad</li>
 * <li>{@link TLContactLinkUnknown: contactLinkUnknown#5f4f9247</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsContactLink extends TLObject {
    public TLAbsContactLink() {
    }
}
