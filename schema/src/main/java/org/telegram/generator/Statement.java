package org.telegram.generator;

public interface Statement<T, P> {

    public String createStatement(T type, P param);
}
