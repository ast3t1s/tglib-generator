package org.telegram.schema.auth;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLCodeTypeSms extends TLAbsCodeType {
    public static final int CLASS_ID = 0x72a3158c;

    public TLCodeTypeSms() {
    }

    @Override
    public String toString() {
        return "auth.codeTypeSms#72a3158c";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
