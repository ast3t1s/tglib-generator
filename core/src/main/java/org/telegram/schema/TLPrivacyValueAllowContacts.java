package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLPrivacyValueAllowContacts extends TLAbsPrivacyRule {
    public static final int CLASS_ID = 0xfffe1bac;

    public TLPrivacyValueAllowContacts() {
    }

    @Override
    public String toString() {
        return "privacyValueAllowContacts#fffe1bac";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
