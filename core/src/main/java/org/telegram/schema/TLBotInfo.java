package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;

/**
 * @author Telegram Schema Generator
 */
public class TLBotInfo extends TLObject {
    public static final int CLASS_ID = 0x98e81d3a;

    protected int userId;

    protected String description;

    protected TLVector<TLBotCommand> commands;

    public TLBotInfo() {
    }

    public TLBotInfo(int userId, String description, TLVector<TLBotCommand> commands) {
        this.userId = userId;
        this.description = description;
        this.commands = commands;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(userId, stream);
        writeString(description, stream);
        writeTLVector(commands, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        userId = readInt(stream);
        description = readTLString(stream);
        commands = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += computeTLStringSerializedSize(description);
        size += commands.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "botInfo#98e81d3a";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TLVector<TLBotCommand> getCommands() {
        return commands;
    }

    public void setCommands(TLVector<TLBotCommand> commands) {
        this.commands = commands;
    }
}
