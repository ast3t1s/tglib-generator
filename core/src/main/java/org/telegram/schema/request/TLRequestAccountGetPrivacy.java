package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLAbsInputPrivacyKey;
import org.telegram.schema.account.TLPrivacyRules;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestAccountGetPrivacy extends TLMethod<TLPrivacyRules> {
    public static final int CLASS_ID = 0xdadbc950;

    protected TLAbsInputPrivacyKey key;

    public TLRequestAccountGetPrivacy() {
    }

    public TLRequestAccountGetPrivacy(TLAbsInputPrivacyKey key) {
        this.key = key;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLPrivacyRules deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLPrivacyRules)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLPrivacyRules) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(key, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        key = readTLObject(stream, context, TLAbsInputPrivacyKey.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += key.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "account.getPrivacy#dadbc950";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsInputPrivacyKey getKey() {
        return key;
    }

    public void setKey(TLAbsInputPrivacyKey key) {
        this.key = key;
    }
}
