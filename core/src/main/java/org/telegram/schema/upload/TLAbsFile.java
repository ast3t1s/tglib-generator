package org.telegram.schema.upload;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLFile: upload.file#96a18d5</li>
 * <li>{@link TLFileCdnRedirect: upload.fileCdnRedirect#1508485a</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsFile extends TLObject {
    public TLAbsFile() {
    }
}
