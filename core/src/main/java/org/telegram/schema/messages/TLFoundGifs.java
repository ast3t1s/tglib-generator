package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsFoundGif;

/**
 * @author Telegram Schema Generator
 */
public class TLFoundGifs extends TLObject {
    public static final int CLASS_ID = 0x450a1c0a;

    protected int nextOffset;

    protected TLVector<TLAbsFoundGif> results;

    public TLFoundGifs() {
    }

    public TLFoundGifs(int nextOffset, TLVector<TLAbsFoundGif> results) {
        this.nextOffset = nextOffset;
        this.results = results;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(nextOffset, stream);
        writeTLVector(results, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        nextOffset = readInt(stream);
        results = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += results.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messages.foundGifs#450a1c0a";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getNextOffset() {
        return nextOffset;
    }

    public void setNextOffset(int nextOffset) {
        this.nextOffset = nextOffset;
    }

    public TLVector<TLAbsFoundGif> getResults() {
        return results;
    }

    public void setResults(TLVector<TLAbsFoundGif> results) {
        this.results = results;
    }
}
