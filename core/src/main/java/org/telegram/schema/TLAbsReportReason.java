package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputReportReasonOther: inputReportReasonOther#e1746d0a</li>
 * <li>{@link TLInputReportReasonPornography: inputReportReasonPornography#2e59d922</li>
 * <li>{@link TLInputReportReasonSpam: inputReportReasonSpam#58dbcab8</li>
 * <li>{@link TLInputReportReasonViolence: inputReportReasonViolence#1e22c78d</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsReportReason extends TLObject {
    public TLAbsReportReason() {
    }
}
