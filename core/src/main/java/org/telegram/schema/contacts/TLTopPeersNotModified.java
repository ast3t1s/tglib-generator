package org.telegram.schema.contacts;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLTopPeersNotModified extends TLAbsTopPeers {
    public static final int CLASS_ID = 0xde266ef5;

    public TLTopPeersNotModified() {
    }

    @Override
    public String toString() {
        return "contacts.topPeersNotModified#de266ef5";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
