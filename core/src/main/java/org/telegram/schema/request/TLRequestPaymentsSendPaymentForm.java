package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLAbsInputPaymentCredentials;
import org.telegram.schema.payments.TLAbsPaymentResult;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestPaymentsSendPaymentForm extends TLMethod<TLAbsPaymentResult> {
    public static final int CLASS_ID = 0x2b8879b3;

    protected int flags;

    protected int msgId;

    protected String requestedInfoId;

    protected String shippingOptionId;

    protected TLAbsInputPaymentCredentials credentials;

    public TLRequestPaymentsSendPaymentForm() {
    }

    public TLRequestPaymentsSendPaymentForm(int msgId, String requestedInfoId,
            String shippingOptionId, TLAbsInputPaymentCredentials credentials) {
        this.msgId = msgId;
        this.requestedInfoId = requestedInfoId;
        this.shippingOptionId = shippingOptionId;
        this.credentials = credentials;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLAbsPaymentResult deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLAbsPaymentResult)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLAbsPaymentResult) response;
    }

    private void computeFlags() {
        flags = 0;
        flags = requestedInfoId != null ? (flags | 1) : (flags & ~1);
        flags = shippingOptionId != null ? (flags | 2) : (flags & ~2);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeInt(msgId, stream);
        if ((flags & 1) != 0) {
            if (requestedInfoId == null) throwNullFieldException("requestedInfoId" , flags);
            writeString(requestedInfoId, stream);
        }
        if ((flags & 2) != 0) {
            if (shippingOptionId == null) throwNullFieldException("shippingOptionId" , flags);
            writeString(shippingOptionId, stream);
        }
        writeTLObject(credentials, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        msgId = readInt(stream);
        requestedInfoId = (flags & 1) != 0 ? readTLString(stream) : null;
        shippingOptionId = (flags & 2) != 0 ? readTLString(stream) : null;
        credentials = readTLObject(stream, context, TLAbsInputPaymentCredentials.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT32;
        if ((flags & 1) != 0) {
            if (requestedInfoId == null) throwNullFieldException("requestedInfoId" , flags);
            size += computeTLStringSerializedSize(requestedInfoId);
        }
        if ((flags & 2) != 0) {
            if (shippingOptionId == null) throwNullFieldException("shippingOptionId" , flags);
            size += computeTLStringSerializedSize(shippingOptionId);
        }
        size += credentials.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "payments.sendPaymentForm#2b8879b3";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getMsgId() {
        return msgId;
    }

    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }

    public String getRequestedInfoId() {
        return requestedInfoId;
    }

    public void setRequestedInfoId(String requestedInfoId) {
        this.requestedInfoId = requestedInfoId;
    }

    public String getShippingOptionId() {
        return shippingOptionId;
    }

    public void setShippingOptionId(String shippingOptionId) {
        this.shippingOptionId = shippingOptionId;
    }

    public TLAbsInputPaymentCredentials getCredentials() {
        return credentials;
    }

    public void setCredentials(TLAbsInputPaymentCredentials credentials) {
        this.credentials = credentials;
    }
}
