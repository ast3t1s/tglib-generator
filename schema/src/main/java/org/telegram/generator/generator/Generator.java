package org.telegram.generator.generator;

import com.squareup.javapoet.*;
import org.telegram.generator.CodeFactory;
import org.telegram.generator.model.*;
import org.telegram.generator.util.Util;

import javax.lang.model.element.Modifier;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static javax.lang.model.element.Modifier.PUBLIC;
import static org.telegram.generator.Main.OUTPUT;
import static org.telegram.generator.model.TLDefTypeModel.*;
import static org.telegram.generator.generator.GeneratorClassConst.*;
import static org.telegram.generator.util.TLStringUtils.*;

public class Generator {

    private TLDef tlDefinition = new TLDefinition(emptyMap(), emptyList(), emptyList());

    private final CodeFactory codeFactory = new CodeFactory();

    private final TLBuilderContext context;

    private int totalGenerated;

    private boolean printStatistic;

    public Generator() {
        context = new TLBuilderContext(new GeneratedCallback());
    }

    public boolean isPrintStatistic() {
        return printStatistic;
    }

    public void setPrintStatistic(boolean printStatistic) {
        this.printStatistic = printStatistic;
    }

    public void generateSchema(TLDef tlDefinition) throws IOException {
        this.tlDefinition = tlDefinition;

        System.out.println(tlDefinition.getTypes().size() + " types");

        System.out.println();
        System.out.println("Generating constructors...");
        generateConstructors();

        System.out.println();
        System.out.println("Generating methods...");
        generateMethods();

        System.out.println();
        System.out.println("Generating context...");
        generateContext();

        System.out.println();
        if (printStatistic) {
            System.out.println("Generated total count " + totalGenerated);
        }
    }

    private void generateConstructors() throws IOException {
        List<TLConstructor> constructors = tlDefinition.getConstructors();
        Collections.sort(constructors);
        System.out.println(constructors.size() + " constructors");

        HashMap<TLType, Integer> typeOccurrences = new HashMap<>(tlDefinition.getTypes().size());
        constructors.stream().map(TLConstructor::getTlType)
                .forEach(tlType -> typeOccurrences.put(tlType, typeOccurrences.getOrDefault(tlType, 0) + 1));

        List<TLConstructor> nonAbstractedConstructors = constructors.stream()
                .filter(tlConstructor -> typeOccurrences.get(tlConstructor.getTlType()) == 1)
                .collect(Collectors.toList());
        List<TLConstructor> abstractedConstructors = constructors.stream()
                .filter(tlConstructor -> typeOccurrences.get(tlConstructor.getTlType()) > 1)
                .collect(Collectors.toList());

        List<TLAbsConstructor> abstractConstructors = new ArrayList<>();

        context.fillContextConstructors(nonAbstractedConstructors);
        context.fillContextConstructors(abstractedConstructors);

        for (TLDefTypeModel.TLTypeRaw abstractType : abstractedConstructors.stream()
                .map(TLConstructor::getTlType).distinct().collect(Collectors.toList())) {
            List<TLConstructor> typedConstructors = abstractedConstructors.stream()
                    .filter(tlConstructor -> tlConstructor.getTlType() == abstractType)
                    .collect(Collectors.toList());

            boolean isEmptyAbstractConstructor = typedConstructors.size() == 2 && typedConstructors.stream()
                    .map(tlConstructor -> tlConstructor.getName().toLowerCase().endsWith("empty"))
                    .collect(Collectors.toList()).contains(true);

            if (isEmptyAbstractConstructor)
                typedConstructors.forEach(tlConstructor -> context.addEmptyAbstractedConstructor(tlConstructor, true));

            ArrayList<TLParameter> commonParameters = typedConstructors.stream()
                    .map(TLConstructor::getParameters)
                    .reduce((tlParameters, tlParameters2) -> new ArrayList<>(tlParameters.stream()
                            .filter(tlParameters2::contains).collect(Collectors.toList()))).get();

            abstractConstructors.add(new TLAbsConstructor(abstractType.getName(), commonParameters, abstractType, isEmptyAbstractConstructor));
            commonParameters.forEach(tlParameter -> tlParameter.setInherited(true));

            typedConstructors.stream().map((Function<TLConstructor, List<TLParameter>>) TLConstructor::getParameters)
                    .flatMap((Function<List<TLParameter>, Stream<?>>) tlParameters -> tlParameters.stream()
                            .peek(tlParameter -> { tlParameter.setInherited(true); }));
        }

        totalGenerated += nonAbstractedConstructors.size();
        totalGenerated += abstractConstructors.size();
        totalGenerated += abstractedConstructors.size();

        if (isPrintStatistic()) {
            System.out.println(nonAbstractedConstructors.size() + " abstract classes");
            System.out.println(abstractConstructors.size() + " \"direct\" classes classes");
            System.out.println(abstractedConstructors.size() + " child classes");
            System.out.println("Total " + constructors.size() + abstractConstructors.size() + " constructors related classes");
        }

        for (TLConstructor constructor : nonAbstractedConstructors) {
            ClassName clazzName = ClassName.get(createPackageName(constructor.getName()), className(constructor.getName(), false, false));
            context.addTypeClass(constructor.getTlType(), clazzName);
            context.addMapConstructor(constructor, clazzName);
        }

        for (TLAbsConstructor constructor : abstractConstructors) {
            context.addTypeClass(constructor.getTlType(), ClassName.get(createPackageName(constructor.getName()), className(constructor.getName(), true, false)));
        }

        for (TLConstructor constructor : abstractedConstructors) {
            context.addMapConstructor(constructor, ClassName.get(createPackageName(constructor.getName()), className(constructor.getName(), false, false)));
        }

        for (TLConstructor constructor : nonAbstractedConstructors) {
            writeClassToFile(createPackageName(constructor.getName()), context.generateConstructorClass(constructor, TYPE_TL_OBJECT), OUTPUT);
        }

        for (TLAbsConstructor constructor : abstractConstructors) {
            writeClassToFile(createPackageName(constructor.getName()), context.generateAbstractConstructorClass(constructor), OUTPUT);
        }

        for (TLConstructor constructor : abstractedConstructors) {
            writeClassToFile(createPackageName(constructor.getName()), context.generateConstructorClass(constructor, context.getClassType(constructor.getTlType())), OUTPUT);
        }
    }

    private void generateMethods() throws IOException {
        List<TLMethod> methods = tlDefinition.getMethods();
        Collections.sort(methods);

        totalGenerated += methods.size();

        if (isPrintStatistic())
        System.out.println(methods.size() + " Methods related classes");

        context.updateApiWrapperClazz();

        for (TLMethod method : methods) {
            writeClassToFile(PACKAGE_TL_API_REQUEST, context.generateMethodClass(method), OUTPUT);
        }

        writeClassToFile(PACKAGE_TL_API, context.getClazzTypeSpec(), OUTPUT);
        writeClassToFile(PACKAGE_TL_API, context.getClazzWrapperSpec(), OUTPUT);
        totalGenerated += 2;
    }

    private void generateContext() throws IOException {
        TypeSpec.Builder contextClazz = TypeSpec.classBuilder(TL_API_CONTEXT)
                .addModifiers(PUBLIC)
                .addJavadoc(DEFAULT_AUTHOR)
                .addAnnotation(AnnotationSpec.builder(SuppressWarnings.class).addMember("value", "\"unused\"")
                        .build())
                .superclass(TYPE_TL_CONTEXT);

        contextClazz.addMethod(MethodSpec.constructorBuilder()
                .addModifiers(PUBLIC)
                .addStatement("super($L)", context.getCountOfConstructors())
                .build());

        contextClazz.addField(TYPE_TL_API_CONTEXT, "instance", Modifier.PRIVATE, Modifier.STATIC);

        contextClazz.addMethod(MethodSpec.methodBuilder("getInstance")
                .addModifiers(PUBLIC, Modifier.STATIC)
                .returns(TYPE_TL_API_CONTEXT)
                .beginControlFlow("if (instance == null)")
                .addStatement("instance = new $T()", TYPE_TL_API_CONTEXT)
                .endControlFlow()
                .addStatement("return instance")
                .build());

        MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("init")
                .addModifiers(PUBLIC)
                .addAnnotation(Override.class);

        List<TLConstructor> sortedConstructors = context.getSortedConstructors();

        for (TLConstructor constructor : sortedConstructors) {
            ClassName clazzName = context.getClassConstructor(constructor);
            methodBuilder.addStatement("registerClass($T.CLASS_ID, $T.class)", clazzName, clazzName);
        }

        contextClazz.addMethod(methodBuilder.build());

        writeClassToFile(PACKAGE_TL_API, contextClazz.build(), OUTPUT);
        totalGenerated++;
    }

    private void generateClassCommon(TypeSpec.Builder clazz, ClassName clazzTypeName, String name, Integer id, List<TLParameter> parameters) {
        if (id != null) {
            clazz.addField(FieldSpec.builder(TypeName.INT, "CLASS_ID", PUBLIC, Modifier.STATIC, Modifier.FINAL)
                    .initializer("0x" + Util.hex(id))
                    .build());
        }

        clazz.addMethod(MethodSpec.constructorBuilder()
                .addModifiers(PUBLIC)
                .build());

        MethodSpec.Builder constructorBuilder = MethodSpec.constructorBuilder().addModifiers(PUBLIC);

        MethodSpec.Builder serializeMethod = MethodSpec.methodBuilder("serializeBody")
                .addModifiers(PUBLIC)
                .addAnnotation(Override.class)
                .addException(IOException.class)
                .addParameter(OutputStream.class, "stream");

        MethodSpec.Builder deserializeMethod = MethodSpec.methodBuilder("deserializeBody")
                .addModifiers(PUBLIC)
                .addAnnotation(Override.class)
                .addAnnotation(AnnotationSpec.builder(SuppressWarnings.class)
                        .addMember("value", "{\"unchecked\", \"SimplifiableConditionalExpression\"}")
                        .build())
                .addException(IOException.class)
                .addParameter(InputStream.class, "stream")
                .addParameter(TYPE_TL_CONTEXT, "context");

        MethodSpec.Builder computeSizeMethod = MethodSpec.methodBuilder("computeSerializedSize")
                .addModifiers(PUBLIC)
                .addAnnotation(Override.class)
                .returns(TypeName.INT);

        List<String> equalsStatements = new ArrayList<>();
        MethodSpec.Builder equalsMethod = MethodSpec.methodBuilder("equals")
                .addModifiers(PUBLIC)
                .addAnnotation(Override.class)
                .addAnnotation(AnnotationSpec.builder(SuppressWarnings.class)
                        .addMember("value", "$S", "PointlessBooleanExpression")
                        .build())
                .returns(TypeName.BOOLEAN)
                .addParameter(Object.class, "object")
                .addStatement("if (!(object instanceof $L)) return false", clazzTypeName.simpleName())
                .addStatement("if (object == this) return true")
                .addCode("\n")
                .addStatement("$T o = ($T) object", clazzTypeName, clazzTypeName)
                .addCode("\n");

        List<TLParameter> condParameters = parameters.stream()
                .filter(tlParameter -> tlParameter.getTlType() instanceof TLTypeConditional).collect(Collectors.toList());

        if (!condParameters.isEmpty() && id != null) {
            MethodSpec.Builder computeFlagsMethod = MethodSpec.methodBuilder("computeFlags")
                    .addModifiers(Modifier.PRIVATE);

            List<TLParameter> condBoolean = new ArrayList<>();
            computeFlagsMethod.addStatement("flags = 0");
            for (TLParameter parameter : condParameters) {
                TLTypeConditional tlType = (TLTypeConditional) parameter.getTlType();
                TLType realType = tlType.getRealType();
                String fieldName = Util.javaEscape(Util.lCamelCase(parameter.getName()));

                if (realType instanceof TLDefTypeModel.TLTypeRaw && Arrays.asList("true", "false").contains(((TLDefTypeModel.TLTypeRaw) realType).getName())) {
                    computeFlagsMethod.addStatement("flags = $L ? (flags | $L) : (flags & ~$L)", fieldName, tlType.pow2Value(),  tlType.pow2Value());
                    if (condParameters.stream().anyMatch(tlParameter -> tlParameter != parameter
                            && (!((((TLTypeConditional) tlParameter.getTlType()).getRealType() instanceof TLDefTypeModel.TLTypeRaw)
                            && (((TLTypeRaw) ((TLTypeConditional) tlParameter.getTlType()).getRealType()).getName().equals("Bool")))
                            && (((TLTypeConditional) tlParameter.getTlType()).getValue() == tlType.getValue())))) {
                        condBoolean.add(parameter);
                    }
                } else {
                    if (realType instanceof TLTypeRaw && ((TLTypeRaw) realType).getName().equals("Bool")
                            && condParameters.stream().anyMatch(tlParameter -> ((TLTypeConditional) tlParameter.getTlType()).getValue() == tlType.getValue())) {
                        computeFlagsMethod.addCode("// If field is not serialized force it to false\n");
                        computeFlagsMethod.addStatement("if ($L && (flags & $L) == 0) $L = false", fieldName, tlType.pow2Value(), fieldName);
                    } else {
                        computeFlagsMethod.addStatement("flags = $L != null ? (flags | $L) : (flags & ~$L)", fieldName, tlType.pow2Value(), tlType.pow2Value());
                    }
                }
            }

            if (!condBoolean.isEmpty()) {
                computeFlagsMethod.addCode("\n// Following parameters might be forced to true by another field that updated the flags\n");
                for (TLParameter parameter : condBoolean) {
                    computeFlagsMethod.addStatement("$L = (flags & $L) != 0", Util.javaEscape(Util.lCamelCase(parameter.getName())), ((TLDefTypeModel.TLTypeConditional) parameter.getTlType()).pow2Value());
                }
            }

            clazz.addMethod(computeFlagsMethod.build());

            serializeMethod.addStatement("computeFlags()");
            serializeMethod.addCode("\n");

            computeSizeMethod.addStatement("computeFlags()");
            computeSizeMethod.addCode("\n");
        }

        computeSizeMethod.addStatement("int size = SIZE_CLASS_ID");

        List<MethodSpec> accessors = new ArrayList<>();
        for (TLParameter parameter : parameters) {
            TLType fieldTlType = parameter.getTlType();
            TypeName fieldType = context.getType(fieldTlType, false);
            if (fieldType instanceof ParameterizedTypeName && (fieldTlType instanceof TLTypeGeneric || (fieldTlType instanceof TLTypeConditional && ((TLTypeConditional) fieldTlType).getRealType() instanceof TLTypeGeneric))) {
                TypeName typeArg = ((ParameterizedTypeName) fieldType).typeArguments.stream().findFirst().get();
                fieldType = ParameterizedTypeName.get(((ParameterizedTypeName) fieldType).rawType, typeArg);
            }

            String fieldName = Util.javaEscape(Util.lCamelCase(parameter.getName()));
            if (!parameter.isInherited() || id == null) {
                FieldSpec.Builder fieldBuilder = FieldSpec.builder(fieldType, fieldName, Modifier.PROTECTED);
                clazz.addField(fieldBuilder.build());
            }

            if (fieldTlType.serializable()) {
                serializeMethod.addCode(codeFactory.getCodeBlock(CodeFactory.SERIALIZE).createStatement(fieldTlType, fieldName) + "\n");
                computeSizeMethod.addCode(codeFactory.getCodeBlock(CodeFactory.COMPUTE_SIZE).createStatement(fieldTlType, fieldName) + "\n");
            }
            String deserializeStatement = fieldName + " = " + codeFactory.getCodeBlock(CodeFactory.DESERIALIZE).createStatement(fieldTlType, fieldType);
            int count = deserializeStatement.split("\\$T").length - 1;
            if (count == 0)
                deserializeMethod.addStatement(deserializeStatement);
            else if (count == 1)
                deserializeMethod.addStatement(deserializeStatement, fieldType);
            else if (count == 2)
                deserializeMethod.addStatement(deserializeStatement, fieldType, fieldType);

            equalsStatements.add(createEqualsStatement(fieldName, fieldType));

            if (!(fieldTlType instanceof TLTypeFlag)) {
                accessors.add(context.generateGetter(parameter.getName(), fieldName, fieldType));
                accessors.add(context.generateSetter(parameter.getName(), fieldName, fieldType));

                constructorBuilder.addParameter(fieldType, fieldName);
                constructorBuilder.addStatement("this.$L = $L", fieldName, fieldName);

                context.updateApiWrapperState(fieldTlType, fieldType, fieldName);
            }
        }

        computeSizeMethod.addStatement("return size");

        if (id != null) {
            if (!parameters.isEmpty()) {
                clazz.addMethod(constructorBuilder.build());
                clazz.addMethod(serializeMethod.build());
                clazz.addMethod(deserializeMethod.build());
                clazz.addMethod(computeSizeMethod.build());
            }

            clazz.addMethod(MethodSpec.methodBuilder("toString")
                    .addAnnotation(Override.class)
                    .addModifiers(PUBLIC)
                    .returns(String.class)
                    .addStatement("return $S",  name + "#" + Util.hex(id))
                    .build());

            clazz.addMethod(MethodSpec.methodBuilder("getClassID")
                    .addAnnotation(Override.class)
                    .addModifiers(PUBLIC)
                    .returns(TypeName.INT)
                    .addStatement("return CLASS_ID")
                    .build());

            if (equalsStatements.isEmpty()) {
                equalsMethod.addStatement("return true");
            } else {
                StringJoiner joiner = new StringJoiner("\n&&", "return ", "");
                for (String equalsParam : equalsStatements) {
                    joiner.add(equalsParam);
                }
                equalsMethod.addStatement(joiner.toString());
            }
        }
        clazz.addMethods(accessors);
    }

    private void writeClassToFile(String packageName, TypeSpec clazz, String output) throws IOException {
        JavaFile.builder(packageName, clazz)
                .addStaticImport(TYPE_STREAM_UTILS, "*")
                .addStaticImport(TYPE_TLOBJECT_UTILS, "*")
                .indent("    ")
                .build().writeTo(new File(output));
    }

    private class GeneratedCallback implements ContextCallback {

        @Override
        public void onGeneratedClass(TypeSpec.Builder clazz, ClassName clazzTypeName, String name, Integer id, List<TLParameter> parameters) {
            generateClassCommon(clazz, clazzTypeName, name, id, parameters);
        }
    }
} 
