package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLPhoto: photo#9288dd29</li>
 * <li>{@link TLPhotoEmpty: photoEmpty#2331b22d</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsPhoto extends TLObject {
    public TLAbsPhoto() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLPhoto getAsPhoto() {
        return null;
    }
}
