package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBool;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLAbsInputUser;
import org.telegram.schema.TLInputBotInlineMessageID;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestMessagesSetInlineGameScore extends TLMethod<TLBool> {
    public static final int CLASS_ID = 0x15ad9f64;

    protected int flags;

    protected boolean editMessage;

    protected boolean force;

    protected TLInputBotInlineMessageID id;

    protected TLAbsInputUser userId;

    protected int score;

    public TLRequestMessagesSetInlineGameScore() {
    }

    public TLRequestMessagesSetInlineGameScore(boolean editMessage, boolean force,
            TLInputBotInlineMessageID id, TLAbsInputUser userId, int score) {
        this.editMessage = editMessage;
        this.force = force;
        this.id = id;
        this.userId = userId;
        this.score = score;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLBool deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLBool)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLBool) response;
    }

    private void computeFlags() {
        flags = 0;
        flags = editMessage ? (flags | 1) : (flags & ~1);
        flags = force ? (flags | 2) : (flags & ~2);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeTLObject(id, stream);
        writeTLObject(userId, stream);
        writeInt(score, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        editMessage = (flags & 1) != 0;
        force = (flags & 2) != 0;
        id = readTLObject(stream, context, TLInputBotInlineMessageID.class, TLInputBotInlineMessageID.CLASS_ID);
        userId = readTLObject(stream, context, TLAbsInputUser.class, -1);
        score = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += id.computeSerializedSize();
        size += userId.computeSerializedSize();
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "messages.setInlineGameScore#15ad9f64";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getEditMessage() {
        return editMessage;
    }

    public void setEditMessage(boolean editMessage) {
        this.editMessage = editMessage;
    }

    public boolean getForce() {
        return force;
    }

    public void setForce(boolean force) {
        this.force = force;
    }

    public TLInputBotInlineMessageID getId() {
        return id;
    }

    public void setId(TLInputBotInlineMessageID id) {
        this.id = id;
    }

    public TLAbsInputUser getUserId() {
        return userId;
    }

    public void setUserId(TLAbsInputUser userId) {
        this.userId = userId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
