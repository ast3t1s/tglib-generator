package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBool;
import org.telegram.core.type.TLBytes;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.account.TLPasswordInputSettings;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestAccountUpdatePasswordSettings extends TLMethod<TLBool> {
    public static final int CLASS_ID = 0xfa7c4b86;

    protected TLBytes currentPasswordHash;

    protected TLPasswordInputSettings newSettings;

    public TLRequestAccountUpdatePasswordSettings() {
    }

    public TLRequestAccountUpdatePasswordSettings(TLBytes currentPasswordHash,
            TLPasswordInputSettings newSettings) {
        this.currentPasswordHash = currentPasswordHash;
        this.newSettings = newSettings;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLBool deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLBool)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLBool) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLBytes(currentPasswordHash, stream);
        writeTLObject(newSettings, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        currentPasswordHash = readTLBytes(stream, context);
        newSettings = readTLObject(stream, context, TLPasswordInputSettings.class, TLPasswordInputSettings.CLASS_ID);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLBytesSerializedSize(currentPasswordHash);
        size += newSettings.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "account.updatePasswordSettings#fa7c4b86";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLBytes getCurrentPasswordHash() {
        return currentPasswordHash;
    }

    public void setCurrentPasswordHash(TLBytes currentPasswordHash) {
        this.currentPasswordHash = currentPasswordHash;
    }

    public TLPasswordInputSettings getNewSettings() {
        return newSettings;
    }

    public void setNewSettings(TLPasswordInputSettings newSettings) {
        this.newSettings = newSettings;
    }
}
