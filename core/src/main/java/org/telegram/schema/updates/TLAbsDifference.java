package org.telegram.schema.updates;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLDifference: updates.difference#f49ca0</li>
 * <li>{@link TLDifferenceEmpty: updates.differenceEmpty#5d75a138</li>
 * <li>{@link TLDifferenceSlice: updates.differenceSlice#a8fb1981</li>
 * <li>{@link TLDifferenceTooLong: updates.differenceTooLong#4afe8f6d</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsDifference extends TLObject {
    public TLAbsDifference() {
    }
}
