package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLDhConfig: messages.dhConfig#2c221edd</li>
 * <li>{@link TLDhConfigNotModified: messages.dhConfigNotModified#c0e24635</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsDhConfig extends TLObject {
    public TLAbsDhConfig() {
    }
}
