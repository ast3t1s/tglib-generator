package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLStickerSet;

/**
 * @author Telegram Schema Generator
 */
public class TLAllStickers extends TLAbsAllStickers {
    public static final int CLASS_ID = 0xedfd405f;

    protected int hash;

    protected TLVector<TLStickerSet> sets;

    public TLAllStickers() {
    }

    public TLAllStickers(int hash, TLVector<TLStickerSet> sets) {
        this.hash = hash;
        this.sets = sets;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(hash, stream);
        writeTLVector(sets, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        hash = readInt(stream);
        sets = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += sets.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messages.allStickers#edfd405f";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getHash() {
        return hash;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }

    public TLVector<TLStickerSet> getSets() {
        return sets;
    }

    public void setSets(TLVector<TLStickerSet> sets) {
        this.sets = sets;
    }
}
