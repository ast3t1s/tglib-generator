package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBytes;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateBotShippingQuery extends TLAbsUpdate {
    public static final int CLASS_ID = 0xe0cdc940;

    protected long queryId;

    protected int userId;

    protected TLBytes payload;

    protected TLPostAddress shippingAddress;

    public TLUpdateBotShippingQuery() {
    }

    public TLUpdateBotShippingQuery(long queryId, int userId, TLBytes payload,
            TLPostAddress shippingAddress) {
        this.queryId = queryId;
        this.userId = userId;
        this.payload = payload;
        this.shippingAddress = shippingAddress;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeLong(queryId, stream);
        writeInt(userId, stream);
        writeTLBytes(payload, stream);
        writeTLObject(shippingAddress, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        queryId = readLong(stream);
        userId = readInt(stream);
        payload = readTLBytes(stream, context);
        shippingAddress = readTLObject(stream, context, TLPostAddress.class, TLPostAddress.CLASS_ID);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT64;
        size += SIZE_INT32;
        size += computeTLBytesSerializedSize(payload);
        size += shippingAddress.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "updateBotShippingQuery#e0cdc940";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public TLBytes getPayload() {
        return payload;
    }

    public void setPayload(TLBytes payload) {
        this.payload = payload;
    }

    public TLPostAddress getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(TLPostAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }
}
