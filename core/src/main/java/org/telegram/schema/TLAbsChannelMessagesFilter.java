package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLChannelMessagesFilter: channelMessagesFilter#cd77d957</li>
 * <li>{@link TLChannelMessagesFilterEmpty: channelMessagesFilterEmpty#94d42ee7</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsChannelMessagesFilter extends TLObject {
    public TLAbsChannelMessagesFilter() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLChannelMessagesFilter getAsChannelMessagesFilter() {
        return null;
    }
}
