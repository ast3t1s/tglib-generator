package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateNewEncryptedMessage extends TLAbsUpdate {
    public static final int CLASS_ID = 0x12bcbd9a;

    protected TLAbsEncryptedMessage message;

    protected int qts;

    public TLUpdateNewEncryptedMessage() {
    }

    public TLUpdateNewEncryptedMessage(TLAbsEncryptedMessage message, int qts) {
        this.message = message;
        this.qts = qts;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(message, stream);
        writeInt(qts, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        message = readTLObject(stream, context, TLAbsEncryptedMessage.class, -1);
        qts = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += message.computeSerializedSize();
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "updateNewEncryptedMessage#12bcbd9a";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsEncryptedMessage getMessage() {
        return message;
    }

    public void setMessage(TLAbsEncryptedMessage message) {
        this.message = message;
    }

    public int getQts() {
        return qts;
    }

    public void setQts(int qts) {
        this.qts = qts;
    }
}
