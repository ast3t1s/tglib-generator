package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLLongVector;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateStickerSetsOrder extends TLAbsUpdate {
    public static final int CLASS_ID = 0xbb2d201;

    protected int flags;

    protected boolean masks;

    protected TLLongVector order;

    public TLUpdateStickerSetsOrder() {
    }

    public TLUpdateStickerSetsOrder(boolean masks, TLLongVector order) {
        this.masks = masks;
        this.order = order;
    }

    private void computeFlags() {
        flags = 0;
        flags = masks ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeTLVector(order, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        masks = (flags & 1) != 0;
        order = readTLLongVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += order.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "updateStickerSetsOrder#bb2d201";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getMasks() {
        return masks;
    }

    public void setMasks(boolean masks) {
        this.masks = masks;
    }

    public TLLongVector getOrder() {
        return order;
    }

    public void setOrder(TLLongVector order) {
        this.order = order;
    }
}
