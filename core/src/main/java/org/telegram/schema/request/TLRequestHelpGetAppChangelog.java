package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLAbsUpdates;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestHelpGetAppChangelog extends TLMethod<TLAbsUpdates> {
    public static final int CLASS_ID = 0x9010ef6f;

    protected String prevAppVersion;

    public TLRequestHelpGetAppChangelog() {
    }

    public TLRequestHelpGetAppChangelog(String prevAppVersion) {
        this.prevAppVersion = prevAppVersion;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLAbsUpdates deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLAbsUpdates)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLAbsUpdates) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(prevAppVersion, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        prevAppVersion = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(prevAppVersion);
        return size;
    }

    @Override
    public String toString() {
        return "help.getAppChangelog#9010ef6f";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getPrevAppVersion() {
        return prevAppVersion;
    }

    public void setPrevAppVersion(String prevAppVersion) {
        this.prevAppVersion = prevAppVersion;
    }
}
