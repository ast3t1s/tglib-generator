package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLChannelParticipantsBots extends TLAbsChannelParticipantsFilter {
    public static final int CLASS_ID = 0xb0d1865b;

    public TLChannelParticipantsBots() {
    }

    @Override
    public String toString() {
        return "channelParticipantsBots#b0d1865b";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
