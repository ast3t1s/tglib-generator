package org.telegram.generator;

import java.util.Arrays;
import java.util.List;

public class TLConst {

    public static final List<String> INGORED_TYPES = Arrays.asList("Bool", "Vector t", "Null", "Error", "True");
    public static final List<String> SUPPORTED = Arrays.asList("Vector");
    public static final List<String> BUILT_IN = Arrays.asList("Bool", "true", "Vector", "int", "long", "double", "string", "bytes");

} 
