package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputMessagesFilterMusic extends TLAbsMessagesFilter {
    public static final int CLASS_ID = 0x3751b49e;

    public TLInputMessagesFilterMusic() {
    }

    @Override
    public String toString() {
        return "inputMessagesFilterMusic#3751b49e";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
