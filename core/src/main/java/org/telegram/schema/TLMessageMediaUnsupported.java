package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLMessageMediaUnsupported extends TLAbsMessageMedia {
    public static final int CLASS_ID = 0x9f84f49e;

    public TLMessageMediaUnsupported() {
    }

    @Override
    public String toString() {
        return "messageMediaUnsupported#9f84f49e";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
