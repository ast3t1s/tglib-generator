package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLSendMessageChooseContactAction extends TLAbsSendMessageAction {
    public static final int CLASS_ID = 0x628cbc6f;

    public TLSendMessageChooseContactAction() {
    }

    @Override
    public String toString() {
        return "sendMessageChooseContactAction#628cbc6f";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
