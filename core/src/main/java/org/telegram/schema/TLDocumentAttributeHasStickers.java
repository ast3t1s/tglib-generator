package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLDocumentAttributeHasStickers extends TLAbsDocumentAttribute {
    public static final int CLASS_ID = 0x9801d2f7;

    public TLDocumentAttributeHasStickers() {
    }

    @Override
    public String toString() {
        return "documentAttributeHasStickers#9801d2f7";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
