package org.telegram.generator;

import org.telegram.generator.generator.TLSchemaGenerator;

public class Main {

    private static final int TL_SCHEMA_LEVEL = 66;
    private static final String TL_SCHEMA_PATH = "./tg-schema-generator/schema/src/main/resources/tl-schema-" + TL_SCHEMA_LEVEL + ".json";

    public static final String OUTPUT = "./tg-schema-generator/core/src/main/java/";

    public static void main(String[] args) {
        TLSchemaGenerator schemaGenerator = new TLSchemaGenerator.Builder()
                .setPrintStatistic(true)
                .setOutPutPath(OUTPUT)
                .setSchemaPath(TL_SCHEMA_PATH).build();
        schemaGenerator.start();
    }

}
