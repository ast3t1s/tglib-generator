package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLContactLinkContact extends TLAbsContactLink {
    public static final int CLASS_ID = 0xd502c2d0;

    public TLContactLinkContact() {
    }

    @Override
    public String toString() {
        return "contactLinkContact#d502c2d0";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
