package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLPrivacyKeyPhoneCall extends TLAbsPrivacyKey {
    public static final int CLASS_ID = 0x3d662b7b;

    public TLPrivacyKeyPhoneCall() {
    }

    @Override
    public String toString() {
        return "privacyKeyPhoneCall#3d662b7b";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
