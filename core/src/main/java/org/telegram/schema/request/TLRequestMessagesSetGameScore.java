package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLAbsInputPeer;
import org.telegram.schema.TLAbsInputUser;
import org.telegram.schema.TLAbsUpdates;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestMessagesSetGameScore extends TLMethod<TLAbsUpdates> {
    public static final int CLASS_ID = 0x8ef8ecc0;

    protected int flags;

    protected boolean editMessage;

    protected boolean force;

    protected TLAbsInputPeer peer;

    protected int id;

    protected TLAbsInputUser userId;

    protected int score;

    public TLRequestMessagesSetGameScore() {
    }

    public TLRequestMessagesSetGameScore(boolean editMessage, boolean force, TLAbsInputPeer peer,
            int id, TLAbsInputUser userId, int score) {
        this.editMessage = editMessage;
        this.force = force;
        this.peer = peer;
        this.id = id;
        this.userId = userId;
        this.score = score;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLAbsUpdates deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLAbsUpdates)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLAbsUpdates) response;
    }

    private void computeFlags() {
        flags = 0;
        flags = editMessage ? (flags | 1) : (flags & ~1);
        flags = force ? (flags | 2) : (flags & ~2);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeTLObject(peer, stream);
        writeInt(id, stream);
        writeTLObject(userId, stream);
        writeInt(score, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        editMessage = (flags & 1) != 0;
        force = (flags & 2) != 0;
        peer = readTLObject(stream, context, TLAbsInputPeer.class, -1);
        id = readInt(stream);
        userId = readTLObject(stream, context, TLAbsInputUser.class, -1);
        score = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += peer.computeSerializedSize();
        size += SIZE_INT32;
        size += userId.computeSerializedSize();
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "messages.setGameScore#8ef8ecc0";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getEditMessage() {
        return editMessage;
    }

    public void setEditMessage(boolean editMessage) {
        this.editMessage = editMessage;
    }

    public boolean getForce() {
        return force;
    }

    public void setForce(boolean force) {
        this.force = force;
    }

    public TLAbsInputPeer getPeer() {
        return peer;
    }

    public void setPeer(TLAbsInputPeer peer) {
        this.peer = peer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TLAbsInputUser getUserId() {
        return userId;
    }

    public void setUserId(TLAbsInputUser userId) {
        this.userId = userId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
