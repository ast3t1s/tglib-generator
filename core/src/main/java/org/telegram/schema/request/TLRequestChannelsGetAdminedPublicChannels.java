package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.messages.TLAbsChats;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestChannelsGetAdminedPublicChannels extends TLMethod<TLAbsChats> {
    public static final int CLASS_ID = 0x8d8d82d7;

    public TLRequestChannelsGetAdminedPublicChannels() {
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLAbsChats deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLAbsChats)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLAbsChats) response;
    }

    @Override
    public String toString() {
        return "channels.getAdminedPublicChannels#8d8d82d7";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
