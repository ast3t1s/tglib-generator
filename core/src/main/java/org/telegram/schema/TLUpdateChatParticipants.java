package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateChatParticipants extends TLAbsUpdate {
    public static final int CLASS_ID = 0x7761198;

    protected TLAbsChatParticipants participants;

    public TLUpdateChatParticipants() {
    }

    public TLUpdateChatParticipants(TLAbsChatParticipants participants) {
        this.participants = participants;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(participants, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        participants = readTLObject(stream, context, TLAbsChatParticipants.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += participants.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "updateChatParticipants#7761198";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsChatParticipants getParticipants() {
        return participants;
    }

    public void setParticipants(TLAbsChatParticipants participants) {
        this.participants = participants;
    }
}
