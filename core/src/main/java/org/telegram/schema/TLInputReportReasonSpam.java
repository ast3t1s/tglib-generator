package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputReportReasonSpam extends TLAbsReportReason {
    public static final int CLASS_ID = 0x58dbcab8;

    public TLInputReportReasonSpam() {
    }

    @Override
    public String toString() {
        return "inputReportReasonSpam#58dbcab8";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
