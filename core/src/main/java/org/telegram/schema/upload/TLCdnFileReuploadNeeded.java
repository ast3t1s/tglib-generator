package org.telegram.schema.upload;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBytes;

/**
 * @author Telegram Schema Generator
 */
public class TLCdnFileReuploadNeeded extends TLAbsCdnFile {
    public static final int CLASS_ID = 0xeea8e46e;

    protected TLBytes requestToken;

    public TLCdnFileReuploadNeeded() {
    }

    public TLCdnFileReuploadNeeded(TLBytes requestToken) {
        this.requestToken = requestToken;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLBytes(requestToken, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        requestToken = readTLBytes(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLBytesSerializedSize(requestToken);
        return size;
    }

    @Override
    public String toString() {
        return "upload.cdnFileReuploadNeeded#eea8e46e";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLBytes getRequestToken() {
        return requestToken;
    }

    public void setRequestToken(TLBytes requestToken) {
        this.requestToken = requestToken;
    }
}
