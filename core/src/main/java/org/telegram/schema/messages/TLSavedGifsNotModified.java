package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLSavedGifsNotModified extends TLAbsSavedGifs {
    public static final int CLASS_ID = 0xe8025ca2;

    public TLSavedGifsNotModified() {
    }

    @Override
    public String toString() {
        return "messages.savedGifsNotModified#e8025ca2";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
