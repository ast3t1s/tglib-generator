package org.telegram.schema.storage;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLFileMp4 extends TLAbsFileType {
    public static final int CLASS_ID = 0xb3cea0e4;

    public TLFileMp4() {
    }

    @Override
    public String toString() {
        return "storage.fileMp4#b3cea0e4";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
