package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLAccountDaysTTL extends TLObject {
    public static final int CLASS_ID = 0xb8d0afdf;

    protected int days;

    public TLAccountDaysTTL() {
    }

    public TLAccountDaysTTL(int days) {
        this.days = days;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(days, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        days = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "accountDaysTTL#b8d0afdf";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }
}
