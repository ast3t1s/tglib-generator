package org.telegram.generator.util;

import com.squareup.javapoet.TypeName;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.telegram.generator.generator.GeneratorClassConst.PACKAGE_TL_API;

public final class TLStringUtils {

    private TLStringUtils() {}

    public static String createEqualsStatement(String fieldName, TypeName fieldType) {
        if (fieldType == TypeName.INT
                || fieldType == TypeName.LONG
                || fieldType == TypeName.FLOAT
                || fieldType == TypeName.DOUBLE
                || fieldType == TypeName.BOOLEAN
                || fieldType == TypeName.CHAR) {
            return String.format("%s == o.%s", fieldName, fieldName);
        } else {
            return String.format("(%s == o.%s || (%s != null && o.%s != null && %s.equals(o.%s)))", fieldName, fieldName, fieldName, fieldName, fieldName, fieldName);
        }
    }

    public static String createPackageName(String typeName) {
        String suffix = "";
        String[] parts = typeName.split("\\.");
        String[] result = Arrays.copyOf(parts, parts.length > 0 ? parts.length - 1 : parts.length);
        String subPackage = Arrays.stream(result).collect(Collectors.joining("."));
        if (!subPackage.isEmpty()) suffix += "." + subPackage;
        return PACKAGE_TL_API + suffix;
    }

    public static String className(String typeName, boolean isAbstract, boolean isRequest) {
        if (isAbstract) {
            return "TLAbs" + Util.uCamelCase(getLastElement(typeName.split("\\.")));
        } else if (isRequest) {
            return "TLRequest" + Util.uCamelCase(typeName);
        } else {
            return "TL" + Util.uFirstLetter(getLastElement(typeName.split("\\.")));
        }
    }

    public static String getLastElement(String[] parts) {
        return parts[parts.length - 1];
    }
} 
