package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateBotWebhookJSONQuery extends TLAbsUpdate {
    public static final int CLASS_ID = 0x9b9240a6;

    protected long queryId;

    protected TLDataJSON data;

    protected int timeout;

    public TLUpdateBotWebhookJSONQuery() {
    }

    public TLUpdateBotWebhookJSONQuery(long queryId, TLDataJSON data, int timeout) {
        this.queryId = queryId;
        this.data = data;
        this.timeout = timeout;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeLong(queryId, stream);
        writeTLObject(data, stream);
        writeInt(timeout, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        queryId = readLong(stream);
        data = readTLObject(stream, context, TLDataJSON.class, TLDataJSON.CLASS_ID);
        timeout = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT64;
        size += data.computeSerializedSize();
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "updateBotWebhookJSONQuery#9b9240a6";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public TLDataJSON getData() {
        return data;
    }

    public void setData(TLDataJSON data) {
        this.data = data;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }
}
