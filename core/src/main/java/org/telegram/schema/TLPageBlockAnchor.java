package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLPageBlockAnchor extends TLAbsPageBlock {
    public static final int CLASS_ID = 0xce0d37b0;

    protected String name;

    public TLPageBlockAnchor() {
    }

    public TLPageBlockAnchor(String name) {
        this.name = name;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(name, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        name = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(name);
        return size;
    }

    @Override
    public String toString() {
        return "pageBlockAnchor#ce0d37b0";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
