package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLChannelForbidden extends TLAbsChat {
    public static final int CLASS_ID = 0x8537784f;

    protected int flags;

    protected boolean broadcast;

    protected boolean megagroup;

    protected int id;

    protected long accessHash;

    protected String title;

    public TLChannelForbidden() {
    }

    public TLChannelForbidden(boolean broadcast, boolean megagroup, int id, long accessHash,
            String title) {
        this.broadcast = broadcast;
        this.megagroup = megagroup;
        this.id = id;
        this.accessHash = accessHash;
        this.title = title;
    }

    private void computeFlags() {
        flags = 0;
        flags = broadcast ? (flags | 32) : (flags & ~32);
        flags = megagroup ? (flags | 256) : (flags & ~256);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeInt(id, stream);
        writeLong(accessHash, stream);
        writeString(title, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        broadcast = (flags & 32) != 0;
        megagroup = (flags & 256) != 0;
        id = readInt(stream);
        accessHash = readLong(stream);
        title = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT32;
        size += SIZE_INT64;
        size += computeTLStringSerializedSize(title);
        return size;
    }

    @Override
    public String toString() {
        return "channelForbidden#8537784f";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getBroadcast() {
        return broadcast;
    }

    public void setBroadcast(boolean broadcast) {
        this.broadcast = broadcast;
    }

    public boolean getMegagroup() {
        return megagroup;
    }

    public void setMegagroup(boolean megagroup) {
        this.megagroup = megagroup;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getAccessHash() {
        return accessHash;
    }

    public void setAccessHash(long accessHash) {
        this.accessHash = accessHash;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
