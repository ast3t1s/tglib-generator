package org.telegram.schema.storage;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLFileWebp extends TLAbsFileType {
    public static final int CLASS_ID = 0x1081464c;

    public TLFileWebp() {
    }

    @Override
    public String toString() {
        return "storage.fileWebp#1081464c";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
