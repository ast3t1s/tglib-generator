package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLVector;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdatePinnedDialogs extends TLAbsUpdate {
    public static final int CLASS_ID = 0xd8caf68d;

    protected int flags;

    protected TLVector<TLAbsPeer> order;

    public TLUpdatePinnedDialogs() {
    }

    public TLUpdatePinnedDialogs(TLVector<TLAbsPeer> order) {
        this.order = order;
    }

    private void computeFlags() {
        flags = 0;
        flags = order != null ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        if ((flags & 1) != 0) {
            if (order == null) throwNullFieldException("order" , flags);
            writeTLVector(order, stream);
        }
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        order = (flags & 1) != 0 ? readTLVector(stream, context) : null;
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        if ((flags & 1) != 0) {
            if (order == null) throwNullFieldException("order" , flags);
            size += order.computeSerializedSize();
        }
        return size;
    }

    @Override
    public String toString() {
        return "updatePinnedDialogs#d8caf68d";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLVector<TLAbsPeer> getOrder() {
        return order;
    }

    public void setOrder(TLVector<TLAbsPeer> order) {
        this.order = order;
    }
}
