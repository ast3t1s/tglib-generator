package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestInvokeAfterMsg<T extends TLObject> extends TLMethod<T> {
    public static final int CLASS_ID = 0xcb9f372d;

    protected long msgId;

    protected TLMethod<T> query;

    public TLRequestInvokeAfterMsg() {
    }

    public TLRequestInvokeAfterMsg(long msgId, TLMethod<T> query) {
        this.msgId = msgId;
        this.query = query;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public T deserializeResponse(InputStream stream, TLContext context) throws IOException {
        return query.deserializeResponse(stream, context);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeLong(msgId, stream);
        writeTLMethod(query, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        msgId = readLong(stream);
        query = readTLMethod(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT64;
        size += query.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "invokeAfterMsg#cb9f372d";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public long getMsgId() {
        return msgId;
    }

    public void setMsgId(long msgId) {
        this.msgId = msgId;
    }

    public TLMethod<T> getQuery() {
        return query;
    }

    public void setQuery(TLMethod<T> query) {
        this.query = query;
    }
}
