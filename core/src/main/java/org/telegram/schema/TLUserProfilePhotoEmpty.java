package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLUserProfilePhotoEmpty extends TLAbsUserProfilePhoto {
    public static final int CLASS_ID = 0x4f11bae1;

    public TLUserProfilePhotoEmpty() {
    }

    @Override
    public String toString() {
        return "userProfilePhotoEmpty#4f11bae1";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    @Override
    public final boolean isEmpty() {
        return true;
    }

    @Override
    public final boolean isNotEmpty() {
        return false;
    }
}
