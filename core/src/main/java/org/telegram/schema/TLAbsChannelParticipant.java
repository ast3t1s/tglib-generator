package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLChannelParticipant: channelParticipant#15ebac1d</li>
 * <li>{@link TLChannelParticipantCreator: channelParticipantCreator#e3e2e1f9</li>
 * <li>{@link TLChannelParticipantEditor: channelParticipantEditor#98192d61</li>
 * <li>{@link TLChannelParticipantKicked: channelParticipantKicked#8cc5e69a</li>
 * <li>{@link TLChannelParticipantModerator: channelParticipantModerator#91057fef</li>
 * <li>{@link TLChannelParticipantSelf: channelParticipantSelf#a3289a6d</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsChannelParticipant extends TLObject {
    public TLAbsChannelParticipant() {
    }
}
