package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateChannelTooLong extends TLAbsUpdate {
    public static final int CLASS_ID = 0xeb0467fb;

    protected int flags;

    protected int channelId;

    protected Integer pts;

    public TLUpdateChannelTooLong() {
    }

    public TLUpdateChannelTooLong(int channelId, Integer pts) {
        this.channelId = channelId;
        this.pts = pts;
    }

    private void computeFlags() {
        flags = 0;
        flags = pts != null ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeInt(channelId, stream);
        if ((flags & 1) != 0) {
            if (pts == null) throwNullFieldException("pts" , flags);
            writeInt(pts, stream);
        }
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        channelId = readInt(stream);
        pts = (flags & 1) != 0 ? readInt(stream) : null;
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT32;
        if ((flags & 1) != 0) {
            if (pts == null) throwNullFieldException("pts" , flags);
            size += SIZE_INT32;
        }
        return size;
    }

    @Override
    public String toString() {
        return "updateChannelTooLong#eb0467fb";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public Integer getPts() {
        return pts;
    }

    public void setPts(Integer pts) {
        this.pts = pts;
    }
}
