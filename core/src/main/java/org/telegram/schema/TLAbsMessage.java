package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLMessage: message#c09be45f</li>
 * <li>{@link TLMessageEmpty: messageEmpty#83e5de54</li>
 * <li>{@link TLMessageService: messageService#9e19a1f6</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsMessage extends TLObject {
    public TLAbsMessage() {
    }
}
