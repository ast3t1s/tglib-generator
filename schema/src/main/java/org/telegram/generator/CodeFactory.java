package org.telegram.generator;

import org.telegram.generator.statement.ComputeSizeStatementStatement;
import org.telegram.generator.statement.DeserializeStatementStatement;
import org.telegram.generator.statement.SerializeStatementStatement;

public class CodeFactory {

    public static final String COMPUTE_SIZE = "compute_size";
    public static final String DESERIALIZE = "deserialize";
    public static final String SERIALIZE = "serialize";

    public Statement getCodeBlock(String blockType) {
        if (blockType == null)
            return null;
        if (blockType.equalsIgnoreCase(COMPUTE_SIZE))
            return new ComputeSizeStatementStatement();
        if (blockType.equalsIgnoreCase(DESERIALIZE))
            return new DeserializeStatementStatement();
        if (blockType.equalsIgnoreCase(SERIALIZE))
            return new SerializeStatementStatement();
        return null;
    }
} 
