package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLInputPeerNotifySettings extends TLObject {
    public static final int CLASS_ID = 0x38935eb2;

    protected int flags;

    protected boolean showPreviews;

    protected boolean silent;

    protected int muteUntil;

    protected String sound;

    public TLInputPeerNotifySettings() {
    }

    public TLInputPeerNotifySettings(boolean showPreviews, boolean silent, int muteUntil,
            String sound) {
        this.showPreviews = showPreviews;
        this.silent = silent;
        this.muteUntil = muteUntil;
        this.sound = sound;
    }

    private void computeFlags() {
        flags = 0;
        flags = showPreviews ? (flags | 1) : (flags & ~1);
        flags = silent ? (flags | 2) : (flags & ~2);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeInt(muteUntil, stream);
        writeString(sound, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        showPreviews = (flags & 1) != 0;
        silent = (flags & 2) != 0;
        muteUntil = readInt(stream);
        sound = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT32;
        size += computeTLStringSerializedSize(sound);
        return size;
    }

    @Override
    public String toString() {
        return "inputPeerNotifySettings#38935eb2";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getShowPreviews() {
        return showPreviews;
    }

    public void setShowPreviews(boolean showPreviews) {
        this.showPreviews = showPreviews;
    }

    public boolean getSilent() {
        return silent;
    }

    public void setSilent(boolean silent) {
        this.silent = silent;
    }

    public int getMuteUntil() {
        return muteUntil;
    }

    public void setMuteUntil(int muteUntil) {
        this.muteUntil = muteUntil;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }
}
