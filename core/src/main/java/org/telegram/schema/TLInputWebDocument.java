package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;

/**
 * @author Telegram Schema Generator
 */
public class TLInputWebDocument extends TLObject {
    public static final int CLASS_ID = 0x9bed434d;

    protected String url;

    protected int size;

    protected String mimeType;

    protected TLVector<TLAbsDocumentAttribute> attributes;

    public TLInputWebDocument() {
    }

    public TLInputWebDocument(String url, int size, String mimeType,
            TLVector<TLAbsDocumentAttribute> attributes) {
        this.url = url;
        this.size = size;
        this.mimeType = mimeType;
        this.attributes = attributes;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(url, stream);
        writeInt(size, stream);
        writeString(mimeType, stream);
        writeTLVector(attributes, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        url = readTLString(stream);
        size = readInt(stream);
        mimeType = readTLString(stream);
        attributes = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(url);
        size += SIZE_INT32;
        size += computeTLStringSerializedSize(mimeType);
        size += attributes.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "inputWebDocument#9bed434d";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public TLVector<TLAbsDocumentAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(TLVector<TLAbsDocumentAttribute> attributes) {
        this.attributes = attributes;
    }
}
