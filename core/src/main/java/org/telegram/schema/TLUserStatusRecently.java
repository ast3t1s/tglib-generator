package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLUserStatusRecently extends TLAbsUserStatus {
    public static final int CLASS_ID = 0xe26f42f1;

    public TLUserStatusRecently() {
    }

    @Override
    public String toString() {
        return "userStatusRecently#e26f42f1";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
