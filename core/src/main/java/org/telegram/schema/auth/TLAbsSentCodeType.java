package org.telegram.schema.auth;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLSentCodeTypeApp: auth.sentCodeTypeApp#3dbb5986</li>
 * <li>{@link TLSentCodeTypeCall: auth.sentCodeTypeCall#5353e5a7</li>
 * <li>{@link TLSentCodeTypeFlashCall: auth.sentCodeTypeFlashCall#ab03c6d9</li>
 * <li>{@link TLSentCodeTypeSms: auth.sentCodeTypeSms#c000bba2</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsSentCodeType extends TLObject {
    public TLAbsSentCodeType() {
    }
}
