package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsUser;
import org.telegram.schema.TLHighScore;

/**
 * @author Telegram Schema Generator
 */
public class TLHighScores extends TLObject {
    public static final int CLASS_ID = 0x9a3bfd99;

    protected TLVector<TLHighScore> scores;

    protected TLVector<TLAbsUser> users;

    public TLHighScores() {
    }

    public TLHighScores(TLVector<TLHighScore> scores, TLVector<TLAbsUser> users) {
        this.scores = scores;
        this.users = users;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(scores, stream);
        writeTLVector(users, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        scores = readTLVector(stream, context);
        users = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += scores.computeSerializedSize();
        size += users.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messages.highScores#9a3bfd99";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLVector<TLHighScore> getScores() {
        return scores;
    }

    public void setScores(TLVector<TLHighScore> scores) {
        this.scores = scores;
    }

    public TLVector<TLAbsUser> getUsers() {
        return users;
    }

    public void setUsers(TLVector<TLAbsUser> users) {
        this.users = users;
    }
}
