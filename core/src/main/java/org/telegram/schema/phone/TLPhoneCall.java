package org.telegram.schema.phone;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsPhoneCall;
import org.telegram.schema.TLAbsUser;

/**
 * @author Telegram Schema Generator
 */
public class TLPhoneCall extends TLObject {
    public static final int CLASS_ID = 0xec82e140;

    protected TLAbsPhoneCall phoneCall;

    protected TLVector<TLAbsUser> users;

    public TLPhoneCall() {
    }

    public TLPhoneCall(TLAbsPhoneCall phoneCall, TLVector<TLAbsUser> users) {
        this.phoneCall = phoneCall;
        this.users = users;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(phoneCall, stream);
        writeTLVector(users, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        phoneCall = readTLObject(stream, context, TLAbsPhoneCall.class, -1);
        users = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += phoneCall.computeSerializedSize();
        size += users.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "phone.phoneCall#ec82e140";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsPhoneCall getPhoneCall() {
        return phoneCall;
    }

    public void setPhoneCall(TLAbsPhoneCall phoneCall) {
        this.phoneCall = phoneCall;
    }

    public TLVector<TLAbsUser> getUsers() {
        return users;
    }

    public void setUsers(TLVector<TLAbsUser> users) {
        this.users = users;
    }
}
