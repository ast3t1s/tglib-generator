package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLDraftMessage: draftMessage#fd8e711f</li>
 * <li>{@link TLDraftMessageEmpty: draftMessageEmpty#ba4baec5</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsDraftMessage extends TLObject {
    public TLAbsDraftMessage() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLDraftMessage getAsDraftMessage() {
        return null;
    }
}
