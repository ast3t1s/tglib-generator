package org.telegram.generator.model;

public class TLParameter implements Comparable<TLParameter> {

    private final String name;
    private final TLDefTypeModel.TLType tlType;

    private boolean inherited = false;

    public TLParameter(String name, TLDefTypeModel.TLType tlType) {
        this.name = name;
        this.tlType = tlType;
    }

    public String getName() {
        return name;
    }

    public TLDefTypeModel.TLType getTlType() {
        return tlType;
    }

    public boolean isInherited() {
        return inherited;
    }

    public void setInherited(boolean inherited) {
        this.inherited = inherited;
    }

    @Override
    public int compareTo(TLParameter o) {
        return name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return String.format("%s: %s", name, tlType);
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof TLParameter
                && ((TLParameter) other).getName().equals(name)
                && ((TLParameter) other).getTlType() == tlType;
    }
}
