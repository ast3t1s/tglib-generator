package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLInputBotInlineResultGame extends TLAbsInputBotInlineResult {
    public static final int CLASS_ID = 0x4fa417f2;

    protected String id;

    protected String shortName;

    protected TLAbsInputBotInlineMessage sendMessage;

    public TLInputBotInlineResultGame() {
    }

    public TLInputBotInlineResultGame(String id, String shortName,
            TLAbsInputBotInlineMessage sendMessage) {
        this.id = id;
        this.shortName = shortName;
        this.sendMessage = sendMessage;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(id, stream);
        writeString(shortName, stream);
        writeTLObject(sendMessage, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        id = readTLString(stream);
        shortName = readTLString(stream);
        sendMessage = readTLObject(stream, context, TLAbsInputBotInlineMessage.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(id);
        size += computeTLStringSerializedSize(shortName);
        size += sendMessage.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "inputBotInlineResultGame#4fa417f2";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public TLAbsInputBotInlineMessage getSendMessage() {
        return sendMessage;
    }

    public void setSendMessage(TLAbsInputBotInlineMessage sendMessage) {
        this.sendMessage = sendMessage;
    }
}
