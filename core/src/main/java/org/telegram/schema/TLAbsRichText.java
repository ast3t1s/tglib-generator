package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLTextBold: textBold#6724abc4</li>
 * <li>{@link TLTextConcat: textConcat#7e6260d7</li>
 * <li>{@link TLTextEmail: textEmail#de5a0dd6</li>
 * <li>{@link TLTextEmpty: textEmpty#dc3d824f</li>
 * <li>{@link TLTextFixed: textFixed#6c3f19b9</li>
 * <li>{@link TLTextItalic: textItalic#d912a59c</li>
 * <li>{@link TLTextPlain: textPlain#744694e0</li>
 * <li>{@link TLTextStrike: textStrike#9bf8bb95</li>
 * <li>{@link TLTextUnderline: textUnderline#c12622c4</li>
 * <li>{@link TLTextUrl: textUrl#3c2884c1</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsRichText extends TLObject {
    public TLAbsRichText() {
    }
}
