package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputMediaEmpty extends TLAbsInputMedia {
    public static final int CLASS_ID = 0x9664f57f;

    public TLInputMediaEmpty() {
    }

    @Override
    public String toString() {
        return "inputMediaEmpty#9664f57f";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
