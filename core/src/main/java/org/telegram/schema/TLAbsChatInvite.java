package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLChatInvite: chatInvite#db74f558</li>
 * <li>{@link TLChatInviteAlready: chatInviteAlready#5a686d7c</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsChatInvite extends TLObject {
    public TLAbsChatInvite() {
    }
}
