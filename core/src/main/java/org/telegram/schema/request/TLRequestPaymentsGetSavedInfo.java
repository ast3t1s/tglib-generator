package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.payments.TLSavedInfo;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestPaymentsGetSavedInfo extends TLMethod<TLSavedInfo> {
    public static final int CLASS_ID = 0x227d824b;

    public TLRequestPaymentsGetSavedInfo() {
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLSavedInfo deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLSavedInfo)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLSavedInfo) response;
    }

    @Override
    public String toString() {
        return "payments.getSavedInfo#227d824b";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
