package org.telegram.schema.storage;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLFileGif extends TLAbsFileType {
    public static final int CLASS_ID = 0xcae1aadf;

    public TLFileGif() {
    }

    @Override
    public String toString() {
        return "storage.fileGif#cae1aadf";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
