package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLDisabledFeature extends TLObject {
    public static final int CLASS_ID = 0xae636f24;

    protected String feature;

    protected String description;

    public TLDisabledFeature() {
    }

    public TLDisabledFeature(String feature, String description) {
        this.feature = feature;
        this.description = description;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(feature, stream);
        writeString(description, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        feature = readTLString(stream);
        description = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(feature);
        size += computeTLStringSerializedSize(description);
        return size;
    }

    @Override
    public String toString() {
        return "disabledFeature#ae636f24";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
