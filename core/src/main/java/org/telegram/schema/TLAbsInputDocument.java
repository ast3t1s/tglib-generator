package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputDocument: inputDocument#18798952</li>
 * <li>{@link TLInputDocumentEmpty: inputDocumentEmpty#72f0eaae</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputDocument extends TLObject {
    public TLAbsInputDocument() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLInputDocument getAsInputDocument() {
        return null;
    }

    public static TLInputDocumentEmpty newEmpty() {
        return new TLInputDocumentEmpty();
    }

    public static TLInputDocument newNotEmpty() {
        return new TLInputDocument();
    }
}
