package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBytes;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLAbsInputPeer;
import org.telegram.schema.messages.TLBotCallbackAnswer;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestMessagesGetBotCallbackAnswer extends TLMethod<TLBotCallbackAnswer> {
    public static final int CLASS_ID = 0x810a9fec;

    protected int flags;

    protected boolean game;

    protected TLAbsInputPeer peer;

    protected int msgId;

    protected TLBytes data;

    public TLRequestMessagesGetBotCallbackAnswer() {
    }

    public TLRequestMessagesGetBotCallbackAnswer(boolean game, TLAbsInputPeer peer, int msgId,
            TLBytes data) {
        this.game = game;
        this.peer = peer;
        this.msgId = msgId;
        this.data = data;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLBotCallbackAnswer deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLBotCallbackAnswer)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLBotCallbackAnswer) response;
    }

    private void computeFlags() {
        flags = 0;
        flags = game ? (flags | 2) : (flags & ~2);
        flags = data != null ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeTLObject(peer, stream);
        writeInt(msgId, stream);
        if ((flags & 1) != 0) {
            if (data == null) throwNullFieldException("data" , flags);
            writeTLBytes(data, stream);
        }
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        game = (flags & 2) != 0;
        peer = readTLObject(stream, context, TLAbsInputPeer.class, -1);
        msgId = readInt(stream);
        data = (flags & 1) != 0 ? readTLBytes(stream, context) : null;
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += peer.computeSerializedSize();
        size += SIZE_INT32;
        if ((flags & 1) != 0) {
            if (data == null) throwNullFieldException("data" , flags);
            size += computeTLBytesSerializedSize(data);
        }
        return size;
    }

    @Override
    public String toString() {
        return "messages.getBotCallbackAnswer#810a9fec";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getGame() {
        return game;
    }

    public void setGame(boolean game) {
        this.game = game;
    }

    public TLAbsInputPeer getPeer() {
        return peer;
    }

    public void setPeer(TLAbsInputPeer peer) {
        this.peer = peer;
    }

    public int getMsgId() {
        return msgId;
    }

    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }

    public TLBytes getData() {
        return data;
    }

    public void setData(TLBytes data) {
        this.data = data;
    }
}
