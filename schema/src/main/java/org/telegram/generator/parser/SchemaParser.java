package org.telegram.generator.parser;

import com.fasterxml.jackson.databind.JsonNode;
import org.telegram.generator.TLConst;
import org.telegram.generator.model.*;
import org.telegram.generator.model.TLDefTypeModel.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SchemaParser {

    private static final Pattern GENERIC = Pattern.compile("([a-zA-Z]+)<([a-zA-Z]+)>");
    private static final Pattern FLAG = Pattern.compile("([a-zA-Z]+).(\\d+)\\?([a-zA-Z<>.]+)");
    private static final Pattern RAW = Pattern.compile("[a-zA-Z].+");

    public TLDef buildFromJson(JsonNode root) {
        System.out.println("Reading TL-Schema...");
        JsonNode constructorsNode = root.get("constructors");

        JsonNode methodsNode = root.get("methods");

        Map<String, TLType> types = new HashMap<>();
        List<TLConstructor> constructors = new ArrayList<>();
        List<TLMethod> methods = new ArrayList<>();

        for (JsonNode node : constructorsNode) {
            if (TLConst.INGORED_TYPES.contains(node.get("type").textValue())) {
                continue;
            }
            TLTypeRaw typeRaw = createConstructorType(node.get("type").textValue());
            types.put(typeRaw.getName(), typeRaw);
        }

        System.out.println("Reading constructors...");
        for (JsonNode constructor : constructorsNode) {
            if (TLConst.INGORED_TYPES.contains(constructor.get("type").textValue())) {
                continue;
            }

            String name = constructor.get("predicate").textValue();
            int id;
            try {
                id = constructor.get("id").intValue();
            } catch (Exception e) {
                id = Integer.parseInt(constructor.get("id").textValue());
            }
            String type = constructor.get("type").textValue();
            TLType tlType = types.get(type);

            ArrayList<TLParameter> constructorParameters = new ArrayList<>();
            parseParameters(types, constructor, constructorParameters);

            constructors.add(new TLConstructor(id, name, constructorParameters, (TLTypeRaw) tlType));
        }

        System.out.println("Reading methods...");
        for (JsonNode method : methodsNode) {
            String name = method.get("method").textValue();
            int id;
            try {
                id = method.get("id").intValue();
            } catch (Exception e) {
                id = Integer.parseInt(method.get("id").textValue());
            }
            String type = method.get("type").textValue();
            TLType tlType = createTLArgType(type, types, false);

            ArrayList<TLParameter> methodParameters = new ArrayList<>();
            parseParameters(types, method, methodParameters);
            methods.add(new TLMethod(id, name, methodParameters, tlType));
        }

        return new TLDefinition(types, constructors, methods);
    }

    private void parseParameters(Map<String, TLType> types, JsonNode constructor, ArrayList<TLParameter> constructorParameters) {
        for (JsonNode p : constructor.get("params")) {
            String pName = p.get("name").textValue();
            String pType = p.get("type").textValue();
            TLType pTLType = createTLArgType(pType, types, true);
            constructorParameters.add(new TLParameter(pName, pTLType));
        }
    }

    private TLTypeRaw createConstructorType(String typeName) {
        if (RAW.matcher(typeName).matches())
            return new TLTypeRaw(typeName);
         else
             throw new RuntimeException("Unsupported type" + typeName + "for constructor/method}");
    }

    private TLType createTLArgType(String typeName, Map<String, TLType> types, boolean isParameter) {
        if (!isParameter && typeName.equals("X")) {
            return new TLTypeAny();
        } else if (isParameter && typeName.equals("!X")) {
            return new TLTypeFunctional();
        } else if (isParameter && typeName.equals("#")) {
            return new TLTypeFlag();
        } else if (isParameter && FLAG.matcher(typeName).matches()) {
            Matcher matcher = FLAG.matcher(typeName);
            matcher.matches();
            String maskName = matcher.group(1);
            int value = Integer.parseInt(matcher.group(2));
            String realType = matcher.group(3);

            if (!maskName.equals("flags"))
                throw new RuntimeException("Unsupported flags");

            return new TLTypeConditional(value, createTLArgType(realType, types, true));
        } else if (GENERIC.matcher(typeName).matches()) {
            Matcher matcher = GENERIC.matcher(typeName);
            matcher.matches();
            String tlName = matcher.group(1);
            String genericName = matcher.group(2);

            if (!TLConst.SUPPORTED.contains(tlName))
                throw new RuntimeException("Unsupported generic type");

            TLType[] generic = new TLType[]{createTLArgType(genericName, types, true)};
            return new TLTypeGeneric(generic, tlName);
        } else if (RAW.matcher(typeName).matches()) {
            if (TLConst.BUILT_IN.contains(typeName))
                return new TLTypeRaw(typeName);
            else if (types.containsKey(typeName))
                 return types.get(typeName);
            else
                throw new RuntimeException("Unknown type " + typeName);
        } else
            throw new RuntimeException("Unsupported type");
    }
}
