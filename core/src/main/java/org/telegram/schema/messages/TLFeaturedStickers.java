package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLLongVector;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsStickerSetCovered;

/**
 * @author Telegram Schema Generator
 */
public class TLFeaturedStickers extends TLAbsFeaturedStickers {
    public static final int CLASS_ID = 0xf89d88e5;

    protected int hash;

    protected TLVector<TLAbsStickerSetCovered> sets;

    protected TLLongVector unread;

    public TLFeaturedStickers() {
    }

    public TLFeaturedStickers(int hash, TLVector<TLAbsStickerSetCovered> sets,
            TLLongVector unread) {
        this.hash = hash;
        this.sets = sets;
        this.unread = unread;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(hash, stream);
        writeTLVector(sets, stream);
        writeTLVector(unread, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        hash = readInt(stream);
        sets = readTLVector(stream, context);
        unread = readTLLongVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += sets.computeSerializedSize();
        size += unread.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messages.featuredStickers#f89d88e5";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getHash() {
        return hash;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }

    public TLVector<TLAbsStickerSetCovered> getSets() {
        return sets;
    }

    public void setSets(TLVector<TLAbsStickerSetCovered> sets) {
        this.sets = sets;
    }

    public TLLongVector getUnread() {
        return unread;
    }

    public void setUnread(TLLongVector unread) {
        this.unread = unread;
    }
}
