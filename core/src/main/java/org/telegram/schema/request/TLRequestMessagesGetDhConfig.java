package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.messages.TLAbsDhConfig;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestMessagesGetDhConfig extends TLMethod<TLAbsDhConfig> {
    public static final int CLASS_ID = 0x26cf8950;

    protected int version;

    protected int randomLength;

    public TLRequestMessagesGetDhConfig() {
    }

    public TLRequestMessagesGetDhConfig(int version, int randomLength) {
        this.version = version;
        this.randomLength = randomLength;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLAbsDhConfig deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLAbsDhConfig)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLAbsDhConfig) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(version, stream);
        writeInt(randomLength, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        version = readInt(stream);
        randomLength = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "messages.getDhConfig#26cf8950";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getRandomLength() {
        return randomLength;
    }

    public void setRandomLength(int randomLength) {
        this.randomLength = randomLength;
    }
}
