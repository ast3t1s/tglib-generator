package org.telegram.generator.statement;

import org.telegram.generator.Statement;
import org.telegram.generator.model.TLDefTypeModel.*;

public class SerializeStatementStatement implements Statement<TLType, String> {

    @Override
    public String createStatement(TLType type, String fieldName) {
        if (type instanceof TLTypeFunctional) {
            return String.format("writeTLMethod(%s, stream);", fieldName);
        } else if (type instanceof TLTypeFlag) {
            return String.format("writeInt(%s, stream);", fieldName);
        } else if (type instanceof TLTypeConditional) {
            return createConditionalStatement(type, fieldName);
        } else if (type instanceof TLTypeGeneric) {
            return String.format("writeTLVector(%s, stream);", fieldName);
        } else if (type instanceof TLTypeRaw) {
            return createRawTypeStatement(((TLTypeRaw) type).getName(), fieldName);
        } else {
            throw new RuntimeException("Unsupported type " + type);
        }
    }

    private String createConditionalStatement(TLType type, String fieldName) {
        StringBuilder statement = new StringBuilder();
        statement.append("if ((flags & ").append(((TLTypeConditional) type).pow2Value()).append(") != 0) {\n")
                .append("    ");
        if (!(((TLTypeConditional) type).getRealType() instanceof TLTypeRaw) || !(((TLTypeRaw) ((TLTypeConditional) type).getRealType()).getName().equals("Bool"))) {
            statement.append("if (")
                    .append(fieldName).append(" == null) throwNullFieldException(\"").append(fieldName).append("\" , flags);")
                    .append("\n")
                    .append("    ");
        }
        statement.append(createStatement(((TLTypeConditional) type).getRealType(), fieldName)).append("\n")
                .append("}");
        return statement.toString();
    }

    private String createRawTypeStatement(String name, String fieldName) {
        switch (name) {
            case "int":
                return String.format("writeInt(%s, stream);", fieldName);
            case "long":
                return String.format("writeLong(%s, stream);", fieldName);
            case "double":
                return String.format("writeDouble(%s, stream);", fieldName);
            case "float":
                return String.format("writeFloat(%s, stream);", fieldName);
            case "string":
                return String.format("writeString(%s, stream);", fieldName);
            case "bytes":
                return String.format("writeTLBytes(%s, stream);", fieldName);
            case "Bool":
                return String.format("writeBoolean(%s, stream);", fieldName);
            case "true": case "false":
                return "";
            default:
                return String.format("writeTLObject(%s, stream);", fieldName);
        }
    }
}
