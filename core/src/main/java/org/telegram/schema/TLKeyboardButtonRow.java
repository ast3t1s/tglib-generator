package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;

/**
 * @author Telegram Schema Generator
 */
public class TLKeyboardButtonRow extends TLObject {
    public static final int CLASS_ID = 0x77608b83;

    protected TLVector<TLAbsKeyboardButton> buttons;

    public TLKeyboardButtonRow() {
    }

    public TLKeyboardButtonRow(TLVector<TLAbsKeyboardButton> buttons) {
        this.buttons = buttons;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(buttons, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        buttons = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += buttons.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "keyboardButtonRow#77608b83";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLVector<TLAbsKeyboardButton> getButtons() {
        return buttons;
    }

    public void setButtons(TLVector<TLAbsKeyboardButton> buttons) {
        this.buttons = buttons;
    }
}
