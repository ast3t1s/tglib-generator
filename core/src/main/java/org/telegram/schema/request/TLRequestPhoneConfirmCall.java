package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBytes;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLInputPhoneCall;
import org.telegram.schema.TLPhoneCallProtocol;
import org.telegram.schema.phone.TLPhoneCall;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestPhoneConfirmCall extends TLMethod<TLPhoneCall> {
    public static final int CLASS_ID = 0x2efe1722;

    protected TLInputPhoneCall peer;

    protected TLBytes gA;

    protected long keyFingerprint;

    protected TLPhoneCallProtocol protocol;

    public TLRequestPhoneConfirmCall() {
    }

    public TLRequestPhoneConfirmCall(TLInputPhoneCall peer, TLBytes gA, long keyFingerprint,
            TLPhoneCallProtocol protocol) {
        this.peer = peer;
        this.gA = gA;
        this.keyFingerprint = keyFingerprint;
        this.protocol = protocol;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLPhoneCall deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLPhoneCall)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLPhoneCall) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(peer, stream);
        writeTLBytes(gA, stream);
        writeLong(keyFingerprint, stream);
        writeTLObject(protocol, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        peer = readTLObject(stream, context, TLInputPhoneCall.class, TLInputPhoneCall.CLASS_ID);
        gA = readTLBytes(stream, context);
        keyFingerprint = readLong(stream);
        protocol = readTLObject(stream, context, TLPhoneCallProtocol.class, TLPhoneCallProtocol.CLASS_ID);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += peer.computeSerializedSize();
        size += computeTLBytesSerializedSize(gA);
        size += SIZE_INT64;
        size += protocol.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "phone.confirmCall#2efe1722";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLInputPhoneCall getPeer() {
        return peer;
    }

    public void setPeer(TLInputPhoneCall peer) {
        this.peer = peer;
    }

    public TLBytes getGA() {
        return gA;
    }

    public void setGA(TLBytes gA) {
        this.gA = gA;
    }

    public long getKeyFingerprint() {
        return keyFingerprint;
    }

    public void setKeyFingerprint(long keyFingerprint) {
        this.keyFingerprint = keyFingerprint;
    }

    public TLPhoneCallProtocol getProtocol() {
        return protocol;
    }

    public void setProtocol(TLPhoneCallProtocol protocol) {
        this.protocol = protocol;
    }
}
