package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLExportedMessageLink extends TLObject {
    public static final int CLASS_ID = 0x1f486803;

    protected String link;

    public TLExportedMessageLink() {
    }

    public TLExportedMessageLink(String link) {
        this.link = link;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(link, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        link = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(link);
        return size;
    }

    @Override
    public String toString() {
        return "exportedMessageLink#1f486803";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
