package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputMessagesFilterChatPhotos extends TLAbsMessagesFilter {
    public static final int CLASS_ID = 0x3a20ecb8;

    public TLInputMessagesFilterChatPhotos() {
    }

    @Override
    public String toString() {
        return "inputMessagesFilterChatPhotos#3a20ecb8";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
