package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLPrivacyKeyChatInvite: privacyKeyChatInvite#500e6dfa</li>
 * <li>{@link TLPrivacyKeyPhoneCall: privacyKeyPhoneCall#3d662b7b</li>
 * <li>{@link TLPrivacyKeyStatusTimestamp: privacyKeyStatusTimestamp#bc2eab30</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsPrivacyKey extends TLObject {
    public TLAbsPrivacyKey() {
    }
}
