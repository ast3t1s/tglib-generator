package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBool;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLAbsInputPeer;
import org.telegram.schema.TLAbsTopPeerCategory;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestContactsResetTopPeerRating extends TLMethod<TLBool> {
    public static final int CLASS_ID = 0x1ae373ac;

    protected TLAbsTopPeerCategory category;

    protected TLAbsInputPeer peer;

    public TLRequestContactsResetTopPeerRating() {
    }

    public TLRequestContactsResetTopPeerRating(TLAbsTopPeerCategory category, TLAbsInputPeer peer) {
        this.category = category;
        this.peer = peer;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLBool deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLBool)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLBool) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(category, stream);
        writeTLObject(peer, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        category = readTLObject(stream, context, TLAbsTopPeerCategory.class, -1);
        peer = readTLObject(stream, context, TLAbsInputPeer.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += category.computeSerializedSize();
        size += peer.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "contacts.resetTopPeerRating#1ae373ac";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsTopPeerCategory getCategory() {
        return category;
    }

    public void setCategory(TLAbsTopPeerCategory category) {
        this.category = category;
    }

    public TLAbsInputPeer getPeer() {
        return peer;
    }

    public void setPeer(TLAbsInputPeer peer) {
        this.peer = peer;
    }
}
