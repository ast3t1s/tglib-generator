package org.telegram.generator.model;

public final class TLDefTypeModel {

    public static abstract class TLType {
        public boolean serializable() {
            return true;
        }
    }

    public static class TLTypeRaw extends TLType {

        private final String name;

        public TLTypeRaw(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return name;
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof TLTypeRaw && ((TLTypeRaw) other).getName().equals(name);
        }

        @Override
        public int hashCode() {
            return toString().hashCode();
        }
    }

    public static class TLTypeGeneric extends TLType {

        private final TLType[] generics;
        private final String name;

        public TLTypeGeneric(TLType[] generics, String name) {
            this.generics = generics;
            this.name = name;
        }

        public TLType[] getGenerics() {
            return generics;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return String.format("Generic<%s>", name);
        }

        @Override
        public int hashCode() {
            return toString().hashCode();
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof TLTypeGeneric;
        }
    }

    public static class TLTypeFlag extends TLType {

        @Override
        public String toString() {
            return "#Flag";
        }
        @Override
        public int hashCode() {
            return toString().hashCode();
        }
        @Override
        public boolean equals(Object other) {
            return other instanceof TLTypeFlag;
        }
    }

    public static class TLTypeAny extends TLType {

        @Override
        public String toString() {
            return "#Any";
        }

        @Override
        public int hashCode() {
            return toString().hashCode();
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof TLTypeAny;
        }
    }

    public static class TLTypeFunctional extends TLType {

        @Override
        public String toString() {
            return "#Functional";
        }

        @Override
        public int hashCode() {
            return toString().hashCode();
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof TLTypeFunctional;
        }
    }

    public static class TLTypeConditional extends TLType {

        private final int value;
        private final TLType realType;

        public TLTypeConditional(int value, TLType realType) {
            this.value = value;
            this.realType = realType;
        }

        @Override
        public boolean serializable() {
            return !(realType instanceof TLTypeRaw && ((TLTypeRaw) realType).getName().equalsIgnoreCase("true"));
        }

        public int getValue() {
            return value;
        }

        public TLType getRealType() {
            return realType;
        }

        public int pow2Value() {
            return (int) Math.pow(2, value);
        }

        @Override
        public String toString() {
            return String.format("flag.%d?%s", value, realType.toString());
        }

        @Override
        public int hashCode() {
            return toString().hashCode();
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof TLTypeConditional
                    && ((TLTypeConditional) other).getValue() == value
                    && ((TLTypeConditional) other).getRealType() == realType;
        }
    }


} 
