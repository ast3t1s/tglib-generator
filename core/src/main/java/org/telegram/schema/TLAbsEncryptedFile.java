package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLEncryptedFile: encryptedFile#4a70994c</li>
 * <li>{@link TLEncryptedFileEmpty: encryptedFileEmpty#c21f497e</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsEncryptedFile extends TLObject {
    public TLAbsEncryptedFile() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLEncryptedFile getAsEncryptedFile() {
        return null;
    }
}
