package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLSendMessageRecordAudioAction extends TLAbsSendMessageAction {
    public static final int CLASS_ID = 0xd52f73f7;

    public TLSendMessageRecordAudioAction() {
    }

    @Override
    public String toString() {
        return "sendMessageRecordAudioAction#d52f73f7";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
