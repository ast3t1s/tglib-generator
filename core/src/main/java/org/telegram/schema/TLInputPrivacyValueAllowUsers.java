package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLVector;

/**
 * @author Telegram Schema Generator
 */
public class TLInputPrivacyValueAllowUsers extends TLAbsInputPrivacyRule {
    public static final int CLASS_ID = 0x131cc67f;

    protected TLVector<TLAbsInputUser> users;

    public TLInputPrivacyValueAllowUsers() {
    }

    public TLInputPrivacyValueAllowUsers(TLVector<TLAbsInputUser> users) {
        this.users = users;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(users, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        users = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += users.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "inputPrivacyValueAllowUsers#131cc67f";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLVector<TLAbsInputUser> getUsers() {
        return users;
    }

    public void setUsers(TLVector<TLAbsInputUser> users) {
        this.users = users;
    }
}
