package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputPrivacyValueDisallowContacts extends TLAbsInputPrivacyRule {
    public static final int CLASS_ID = 0xba52007;

    public TLInputPrivacyValueDisallowContacts() {
    }

    @Override
    public String toString() {
        return "inputPrivacyValueDisallowContacts#ba52007";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
