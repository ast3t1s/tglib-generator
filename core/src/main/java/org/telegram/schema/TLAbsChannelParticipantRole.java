package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLChannelRoleEditor: channelRoleEditor#820bfe8c</li>
 * <li>{@link TLChannelRoleEmpty: channelRoleEmpty#b285a0c6</li>
 * <li>{@link TLChannelRoleModerator: channelRoleModerator#9618d975</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsChannelParticipantRole extends TLObject {
    public TLAbsChannelParticipantRole() {
    }
}
