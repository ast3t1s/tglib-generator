package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLChannelParticipantsKicked extends TLAbsChannelParticipantsFilter {
    public static final int CLASS_ID = 0x3c37bb7a;

    public TLChannelParticipantsKicked() {
    }

    @Override
    public String toString() {
        return "channelParticipantsKicked#3c37bb7a";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
