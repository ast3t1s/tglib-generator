package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLSendMessageCancelAction extends TLAbsSendMessageAction {
    public static final int CLASS_ID = 0xfd5ec8f5;

    public TLSendMessageCancelAction() {
    }

    @Override
    public String toString() {
        return "sendMessageCancelAction#fd5ec8f5";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
