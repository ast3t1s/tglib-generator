package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLVector;

/**
 * @author Telegram Schema Generator
 */
public class TLPageBlockEmbedPost extends TLAbsPageBlock {
    public static final int CLASS_ID = 0x292c7be9;

    protected String url;

    protected long webpageId;

    protected long authorPhotoId;

    protected String author;

    protected int date;

    protected TLVector<TLAbsPageBlock> blocks;

    protected TLAbsRichText caption;

    public TLPageBlockEmbedPost() {
    }

    public TLPageBlockEmbedPost(String url, long webpageId, long authorPhotoId, String author,
            int date, TLVector<TLAbsPageBlock> blocks, TLAbsRichText caption) {
        this.url = url;
        this.webpageId = webpageId;
        this.authorPhotoId = authorPhotoId;
        this.author = author;
        this.date = date;
        this.blocks = blocks;
        this.caption = caption;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(url, stream);
        writeLong(webpageId, stream);
        writeLong(authorPhotoId, stream);
        writeString(author, stream);
        writeInt(date, stream);
        writeTLVector(blocks, stream);
        writeTLObject(caption, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        url = readTLString(stream);
        webpageId = readLong(stream);
        authorPhotoId = readLong(stream);
        author = readTLString(stream);
        date = readInt(stream);
        blocks = readTLVector(stream, context);
        caption = readTLObject(stream, context, TLAbsRichText.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(url);
        size += SIZE_INT64;
        size += SIZE_INT64;
        size += computeTLStringSerializedSize(author);
        size += SIZE_INT32;
        size += blocks.computeSerializedSize();
        size += caption.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "pageBlockEmbedPost#292c7be9";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getWebpageId() {
        return webpageId;
    }

    public void setWebpageId(long webpageId) {
        this.webpageId = webpageId;
    }

    public long getAuthorPhotoId() {
        return authorPhotoId;
    }

    public void setAuthorPhotoId(long authorPhotoId) {
        this.authorPhotoId = authorPhotoId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public TLVector<TLAbsPageBlock> getBlocks() {
        return blocks;
    }

    public void setBlocks(TLVector<TLAbsPageBlock> blocks) {
        this.blocks = blocks;
    }

    public TLAbsRichText getCaption() {
        return caption;
    }

    public void setCaption(TLAbsRichText caption) {
        this.caption = caption;
    }
}
