package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputChatPhotoEmpty extends TLAbsInputChatPhoto {
    public static final int CLASS_ID = 0x1ca48f57;

    public TLInputChatPhotoEmpty() {
    }

    @Override
    public String toString() {
        return "inputChatPhotoEmpty#1ca48f57";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
