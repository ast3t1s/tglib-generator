package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLContactStatus;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestContactsGetStatuses extends TLMethod<TLVector<TLContactStatus>> {
    public static final int CLASS_ID = 0xc4a353ee;

    public TLRequestContactsGetStatuses() {
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLVector<TLContactStatus> deserializeResponse(InputStream stream, TLContext context)
            throws IOException {
        return readTLVector(stream, context);
    }

    @Override
    public String toString() {
        return "contacts.getStatuses#c4a353ee";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
