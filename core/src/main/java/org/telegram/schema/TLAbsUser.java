package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLUser: user#2e13f4c3</li>
 * <li>{@link TLUserEmpty: userEmpty#200250ba</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsUser extends TLObject {
    public TLAbsUser() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLUser getAsUser() {
        return null;
    }
}
