package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputNotifyChats extends TLAbsInputNotifyPeer {
    public static final int CLASS_ID = 0x4a95e84e;

    public TLInputNotifyChats() {
    }

    @Override
    public String toString() {
        return "inputNotifyChats#4a95e84e";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
