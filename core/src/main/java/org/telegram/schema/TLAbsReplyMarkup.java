package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLReplyInlineMarkup: replyInlineMarkup#48a30254</li>
 * <li>{@link TLReplyKeyboardForceReply: replyKeyboardForceReply#f4108aa0</li>
 * <li>{@link TLReplyKeyboardHide: replyKeyboardHide#a03e5b85</li>
 * <li>{@link TLReplyKeyboardMarkup: replyKeyboardMarkup#3502758c</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsReplyMarkup extends TLObject {
    public TLAbsReplyMarkup() {
    }
}
