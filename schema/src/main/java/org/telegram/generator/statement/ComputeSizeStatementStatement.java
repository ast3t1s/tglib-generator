package org.telegram.generator.statement;

import org.telegram.generator.model.TLDefTypeModel.*;
import org.telegram.generator.Statement;

public class ComputeSizeStatementStatement implements Statement<TLType, String> {

    @Override
    public String createStatement(TLType type, String fieldName) {
        if (type instanceof TLTypeFunctional) {
            return String.format("size += %s.computeSerializedSize();", fieldName);
        } else if (type instanceof TLTypeFlag) {
            return "size += SIZE_INT32;";
        } else if (type instanceof TLTypeConditional) {
            return createTypeConditionalStatement(type, fieldName);
        } else if (type instanceof TLTypeGeneric) {
            return String.format("size += %s.computeSerializedSize();", fieldName);
        } else if (type instanceof TLTypeRaw) {
            return createTypeRawStatement(((TLTypeRaw) type).getName(), fieldName);
        } else {
            throw new RuntimeException("Unsupported type " + type);
        }
    }

    private String createTypeConditionalStatement(TLType type, String fieldName) {
        StringBuilder statement = new StringBuilder();
        statement.append("if ((flags & ")
                .append(((TLTypeConditional) type).pow2Value())
                .append(") != 0) {\n")
                .append("    ");
        if (!(((TLTypeConditional) type).getRealType() instanceof TLTypeRaw) || !(((TLTypeRaw) ((TLTypeConditional) type).getRealType()).getName().equalsIgnoreCase("Bool"))) {
            statement.append("if (")
                    .append(fieldName).append(" == null) throwNullFieldException(\"").append(fieldName).append("\" , flags);")
                    .append("\n")
                    .append("    ");
        }
        statement.append(createStatement(((TLTypeConditional) type).getRealType(), fieldName))
                .append("\n")
                .append("}");
        return statement.toString();
    }

    private String createTypeRawStatement(String name, String fieldName) {
        switch (name) {
            case "int":
                return "size += SIZE_INT32;";
            case "long":
                return "size += SIZE_INT64;";
            case "double":
                return "size += SIZE_DOUBLE;";
            case "float":
                return "size += SIZE_DOUBLE;";
            case "string":
                return String.format("size += computeTLStringSerializedSize(%s);", fieldName);
            case "bytes":
                return String.format("size += computeTLBytesSerializedSize(%s);", fieldName);
            case "Bool":
                return "size += SIZE_BOOLEAN;";
            default:
                return String.format("size += %s.computeSerializedSize();", fieldName);
        }
    }
}
