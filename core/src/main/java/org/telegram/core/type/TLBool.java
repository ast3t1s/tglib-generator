package org.telegram.core.type;

import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidClassException;
import java.io.OutputStream;

import static org.telegram.core.utils.StreamUtils.readInt;

public abstract class TLBool extends TLObject {

    public static final TLBool TRUE = new TLBoolTrue();
    public static final TLBool FALSE = new TLBoolFalse();

    public static final int TRUE_CLASS_ID = TLBoolTrue.CLASS_ID;
    public static final int FALSE_CLASS_ID = TLBoolFalse.CLASS_ID;

    public static TLBool get(boolean value) {
        return value ? TRUE : FALSE;
    }

    public static void serialize(boolean value, OutputStream stream) throws IOException {
        get(value).serialize(stream);
    }

    public static boolean deserialize(InputStream stream) throws IOException {
        int constructorId = readInt(stream);
        if (constructorId == TLBoolTrue.CLASS_ID)
            return true;
        if (constructorId == TLBoolFalse.CLASS_ID)
            return false;

        throw new InvalidClassException("Wrong TLBool constructor id. Found " + Integer.toHexString(constructorId)
                + ", expected: " + Integer.toHexString(TLBoolTrue.CLASS_ID)
                + " or " + Integer.toHexString(TLBoolFalse.CLASS_ID));
    }

    @Override
    public boolean equals(Object obj) {
        return this == obj; // Singleton, 1 instance
    }

    private static final class TLBoolTrue extends TLBool {

        public static final int CLASS_ID = 0x997275b5;

        private TLBoolTrue() {

        }

        @Override
        public int getClassID() {
            return CLASS_ID;
        }

        @Override
        public String toString() {
            return "boolTrue#997275b5";
        }
    }

    private static final class TLBoolFalse extends TLBool {

        public static final int CLASS_ID = 0xbc799737;

        private TLBoolFalse() {

        }

        @Override
        public int getClassID() {
            return CLASS_ID;
        }

        @Override
        public String toString() {
            return "boolFalse#bc799737";
        }
    }
} 
