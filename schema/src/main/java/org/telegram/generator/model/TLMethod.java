package org.telegram.generator.model;

import org.telegram.generator.util.Util;

import java.util.List;

public class TLMethod implements Comparable<TLMethod> {

    private final int id;
    private final String name;
    private final List<TLParameter> parameters;
    private final TLDefTypeModel.TLType tlType;

    public TLMethod(int id, String name, List<TLParameter> parameters, TLDefTypeModel.TLType tlType) {
        this.id = id;
        this.name = name;
        this.parameters = parameters;
        this.tlType = tlType;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<TLParameter> getParameters() {
        return parameters;
    }

    public TLDefTypeModel.TLType getTlType() {
        return tlType;
    }

    @Override
    public int compareTo(TLMethod o) {
        return name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return String.format("%s#%s", name, Util.hex(id));
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof TLMethod
                && ((TLMethod) other).getName().equals(name)
                && ((TLMethod) other).getId() == id;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
