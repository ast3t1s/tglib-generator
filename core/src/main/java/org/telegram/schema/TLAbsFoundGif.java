package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLFoundGif: foundGif#162ecc1f</li>
 * <li>{@link TLFoundGifCached: foundGifCached#9c750409</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsFoundGif extends TLObject {
    public TLAbsFoundGif() {
    }
}
