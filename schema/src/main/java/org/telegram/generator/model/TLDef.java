package org.telegram.generator.model;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public interface TLDef {

    public default Map<String, TLDefTypeModel.TLType> getTypes() {
        return Collections.emptyMap();
    }

    public default List<TLConstructor> getConstructors() {
        return Collections.emptyList();
    }

    public default List<TLMethod> getMethods() {
        return Collections.emptyList();
    }
}
