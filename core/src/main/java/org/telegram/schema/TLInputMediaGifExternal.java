package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLInputMediaGifExternal extends TLAbsInputMedia {
    public static final int CLASS_ID = 0x4843b0fd;

    protected String url;

    protected String q;

    public TLInputMediaGifExternal() {
    }

    public TLInputMediaGifExternal(String url, String q) {
        this.url = url;
        this.q = q;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(url, stream);
        writeString(q, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        url = readTLString(stream);
        q = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(url);
        size += computeTLStringSerializedSize(q);
        return size;
    }

    @Override
    public String toString() {
        return "inputMediaGifExternal#4843b0fd";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }
}
