package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLEncryptedMessage: encryptedMessage#ed18c118</li>
 * <li>{@link TLEncryptedMessageService: encryptedMessageService#23734b06</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsEncryptedMessage extends TLObject {
    public TLAbsEncryptedMessage() {
    }
}
