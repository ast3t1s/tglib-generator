package org.telegram.schema.auth;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLCodeTypeFlashCall extends TLAbsCodeType {
    public static final int CLASS_ID = 0x226ccefb;

    public TLCodeTypeFlashCall() {
    }

    @Override
    public String toString() {
        return "auth.codeTypeFlashCall#226ccefb";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
