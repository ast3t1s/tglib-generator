package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateRecentStickers extends TLAbsUpdate {
    public static final int CLASS_ID = 0x9a422c20;

    public TLUpdateRecentStickers() {
    }

    @Override
    public String toString() {
        return "updateRecentStickers#9a422c20";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
