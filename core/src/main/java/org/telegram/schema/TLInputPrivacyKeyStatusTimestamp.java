package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputPrivacyKeyStatusTimestamp extends TLAbsInputPrivacyKey {
    public static final int CLASS_ID = 0x4f96cb18;

    public TLInputPrivacyKeyStatusTimestamp() {
    }

    @Override
    public String toString() {
        return "inputPrivacyKeyStatusTimestamp#4f96cb18";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
