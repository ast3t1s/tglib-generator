package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputPeerNotifyEventsEmpty extends TLAbsInputPeerNotifyEvents {
    public static final int CLASS_ID = 0xf03064d8;

    public TLInputPeerNotifyEventsEmpty() {
    }

    @Override
    public String toString() {
        return "inputPeerNotifyEventsEmpty#f03064d8";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    @Override
    public final boolean isEmpty() {
        return true;
    }

    @Override
    public final boolean isNotEmpty() {
        return false;
    }
}
