package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLFileLocation: fileLocation#53d69076</li>
 * <li>{@link TLFileLocationUnavailable: fileLocationUnavailable#7c596b46</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsFileLocation extends TLObject {
    public TLAbsFileLocation() {
    }
}
