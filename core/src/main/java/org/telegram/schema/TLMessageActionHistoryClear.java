package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLMessageActionHistoryClear extends TLAbsMessageAction {
    public static final int CLASS_ID = 0x9fbab604;

    public TLMessageActionHistoryClear() {
    }

    @Override
    public String toString() {
        return "messageActionHistoryClear#9fbab604";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
