package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLPhoneCallDiscardReasonMissed extends TLAbsPhoneCallDiscardReason {
    public static final int CLASS_ID = 0x85e42301;

    public TLPhoneCallDiscardReasonMissed() {
    }

    @Override
    public String toString() {
        return "phoneCallDiscardReasonMissed#85e42301";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
