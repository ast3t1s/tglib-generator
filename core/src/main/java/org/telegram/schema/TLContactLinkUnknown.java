package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLContactLinkUnknown extends TLAbsContactLink {
    public static final int CLASS_ID = 0x5f4f9247;

    public TLContactLinkUnknown() {
    }

    @Override
    public String toString() {
        return "contactLinkUnknown#5f4f9247";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
