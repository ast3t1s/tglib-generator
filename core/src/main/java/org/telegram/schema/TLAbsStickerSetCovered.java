package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLStickerSetCovered: stickerSetCovered#6410a5d2</li>
 * <li>{@link TLStickerSetMultiCovered: stickerSetMultiCovered#3407e51b</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsStickerSetCovered extends TLObject {
    protected TLStickerSet set;

    public TLAbsStickerSetCovered() {
    }

    public TLStickerSet getSet() {
        return set;
    }

    public void setSet(TLStickerSet set) {
        this.set = set;
    }
}
