package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLInputAppEvent extends TLObject {
    public static final int CLASS_ID = 0x770656a8;

    protected double time;

    protected String type;

    protected long peer;

    protected String data;

    public TLInputAppEvent() {
    }

    public TLInputAppEvent(double time, String type, long peer, String data) {
        this.time = time;
        this.type = type;
        this.peer = peer;
        this.data = data;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeDouble(time, stream);
        writeString(type, stream);
        writeLong(peer, stream);
        writeString(data, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        time = readDouble(stream);
        type = readTLString(stream);
        peer = readLong(stream);
        data = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_DOUBLE;
        size += computeTLStringSerializedSize(type);
        size += SIZE_INT64;
        size += computeTLStringSerializedSize(data);
        return size;
    }

    @Override
    public String toString() {
        return "inputAppEvent#770656a8";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getPeer() {
        return peer;
    }

    public void setPeer(long peer) {
        this.peer = peer;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
