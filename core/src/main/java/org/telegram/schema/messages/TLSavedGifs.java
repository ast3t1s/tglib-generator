package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsDocument;

/**
 * @author Telegram Schema Generator
 */
public class TLSavedGifs extends TLAbsSavedGifs {
    public static final int CLASS_ID = 0x2e0709a5;

    protected int hash;

    protected TLVector<TLAbsDocument> gifs;

    public TLSavedGifs() {
    }

    public TLSavedGifs(int hash, TLVector<TLAbsDocument> gifs) {
        this.hash = hash;
        this.gifs = gifs;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(hash, stream);
        writeTLVector(gifs, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        hash = readInt(stream);
        gifs = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += gifs.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messages.savedGifs#2e0709a5";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getHash() {
        return hash;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }

    public TLVector<TLAbsDocument> getGifs() {
        return gifs;
    }

    public void setGifs(TLVector<TLAbsDocument> gifs) {
        this.gifs = gifs;
    }
}
