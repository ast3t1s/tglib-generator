package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputPeerNotifyEventsAll extends TLAbsInputPeerNotifyEvents {
    public static final int CLASS_ID = 0xe86a2c74;

    public TLInputPeerNotifyEventsAll() {
    }

    @Override
    public String toString() {
        return "inputPeerNotifyEventsAll#e86a2c74";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    @Override
    public final boolean isEmpty() {
        return false;
    }

    @Override
    public final boolean isNotEmpty() {
        return true;
    }

    @Override
    public final TLInputPeerNotifyEventsAll getAsInputPeerNotifyEventsAll() {
        return this;
    }
}
