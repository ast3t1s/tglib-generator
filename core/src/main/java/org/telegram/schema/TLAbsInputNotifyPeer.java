package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputNotifyAll: inputNotifyAll#a429b886</li>
 * <li>{@link TLInputNotifyChats: inputNotifyChats#4a95e84e</li>
 * <li>{@link TLInputNotifyPeer: inputNotifyPeer#b8bc5b0c</li>
 * <li>{@link TLInputNotifyUsers: inputNotifyUsers#193b4417</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputNotifyPeer extends TLObject {
    public TLAbsInputNotifyPeer() {
    }
}
