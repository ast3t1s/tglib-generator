package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLDataJSON extends TLObject {
    public static final int CLASS_ID = 0x7d748d04;

    protected String data;

    public TLDataJSON() {
    }

    public TLDataJSON(String data) {
        this.data = data;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(data, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        data = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(data);
        return size;
    }

    @Override
    public String toString() {
        return "dataJSON#7d748d04";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
