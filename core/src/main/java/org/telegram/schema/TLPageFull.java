package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLVector;

/**
 * @author Telegram Schema Generator
 */
public class TLPageFull extends TLAbsPage {
    public static final int CLASS_ID = 0xd7a19d69;

    protected TLVector<TLAbsPageBlock> blocks;

    protected TLVector<TLAbsPhoto> photos;

    protected TLVector<TLAbsDocument> videos;

    public TLPageFull() {
    }

    public TLPageFull(TLVector<TLAbsPageBlock> blocks, TLVector<TLAbsPhoto> photos,
            TLVector<TLAbsDocument> videos) {
        this.blocks = blocks;
        this.photos = photos;
        this.videos = videos;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(blocks, stream);
        writeTLVector(photos, stream);
        writeTLVector(videos, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        blocks = readTLVector(stream, context);
        photos = readTLVector(stream, context);
        videos = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += blocks.computeSerializedSize();
        size += photos.computeSerializedSize();
        size += videos.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "pageFull#d7a19d69";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLVector<TLAbsPageBlock> getBlocks() {
        return blocks;
    }

    public void setBlocks(TLVector<TLAbsPageBlock> blocks) {
        this.blocks = blocks;
    }

    public TLVector<TLAbsPhoto> getPhotos() {
        return photos;
    }

    public void setPhotos(TLVector<TLAbsPhoto> photos) {
        this.photos = photos;
    }

    public TLVector<TLAbsDocument> getVideos() {
        return videos;
    }

    public void setVideos(TLVector<TLAbsDocument> videos) {
        this.videos = videos;
    }
}
