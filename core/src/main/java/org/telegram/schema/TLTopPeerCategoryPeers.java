package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;

/**
 * @author Telegram Schema Generator
 */
public class TLTopPeerCategoryPeers extends TLObject {
    public static final int CLASS_ID = 0xfb834291;

    protected TLAbsTopPeerCategory category;

    protected int count;

    protected TLVector<TLTopPeer> peers;

    public TLTopPeerCategoryPeers() {
    }

    public TLTopPeerCategoryPeers(TLAbsTopPeerCategory category, int count,
            TLVector<TLTopPeer> peers) {
        this.category = category;
        this.count = count;
        this.peers = peers;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(category, stream);
        writeInt(count, stream);
        writeTLVector(peers, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        category = readTLObject(stream, context, TLAbsTopPeerCategory.class, -1);
        count = readInt(stream);
        peers = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += category.computeSerializedSize();
        size += SIZE_INT32;
        size += peers.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "topPeerCategoryPeers#fb834291";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsTopPeerCategory getCategory() {
        return category;
    }

    public void setCategory(TLAbsTopPeerCategory category) {
        this.category = category;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public TLVector<TLTopPeer> getPeers() {
        return peers;
    }

    public void setPeers(TLVector<TLTopPeer> peers) {
        this.peers = peers;
    }
}
