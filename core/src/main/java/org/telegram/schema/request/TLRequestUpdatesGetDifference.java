package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.updates.TLAbsDifference;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestUpdatesGetDifference extends TLMethod<TLAbsDifference> {
    public static final int CLASS_ID = 0x25939651;

    protected int flags;

    protected int pts;

    protected Integer ptsTotalLimit;

    protected int date;

    protected int qts;

    public TLRequestUpdatesGetDifference() {
    }

    public TLRequestUpdatesGetDifference(int pts, Integer ptsTotalLimit, int date, int qts) {
        this.pts = pts;
        this.ptsTotalLimit = ptsTotalLimit;
        this.date = date;
        this.qts = qts;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLAbsDifference deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLAbsDifference)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLAbsDifference) response;
    }

    private void computeFlags() {
        flags = 0;
        flags = ptsTotalLimit != null ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeInt(pts, stream);
        if ((flags & 1) != 0) {
            if (ptsTotalLimit == null) throwNullFieldException("ptsTotalLimit" , flags);
            writeInt(ptsTotalLimit, stream);
        }
        writeInt(date, stream);
        writeInt(qts, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        pts = readInt(stream);
        ptsTotalLimit = (flags & 1) != 0 ? readInt(stream) : null;
        date = readInt(stream);
        qts = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT32;
        if ((flags & 1) != 0) {
            if (ptsTotalLimit == null) throwNullFieldException("ptsTotalLimit" , flags);
            size += SIZE_INT32;
        }
        size += SIZE_INT32;
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "updates.getDifference#25939651";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getPts() {
        return pts;
    }

    public void setPts(int pts) {
        this.pts = pts;
    }

    public Integer getPtsTotalLimit() {
        return ptsTotalLimit;
    }

    public void setPtsTotalLimit(Integer ptsTotalLimit) {
        this.ptsTotalLimit = ptsTotalLimit;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getQts() {
        return qts;
    }

    public void setQts(int qts) {
        this.qts = qts;
    }
}
