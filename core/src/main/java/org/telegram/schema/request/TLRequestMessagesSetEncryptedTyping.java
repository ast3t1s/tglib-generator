package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBool;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLInputEncryptedChat;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestMessagesSetEncryptedTyping extends TLMethod<TLBool> {
    public static final int CLASS_ID = 0x791451ed;

    protected TLInputEncryptedChat peer;

    protected boolean typing;

    public TLRequestMessagesSetEncryptedTyping() {
    }

    public TLRequestMessagesSetEncryptedTyping(TLInputEncryptedChat peer, boolean typing) {
        this.peer = peer;
        this.typing = typing;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLBool deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLBool)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLBool) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(peer, stream);
        writeBoolean(typing, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        peer = readTLObject(stream, context, TLInputEncryptedChat.class, TLInputEncryptedChat.CLASS_ID);
        typing = readTLBool(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += peer.computeSerializedSize();
        size += SIZE_BOOLEAN;
        return size;
    }

    @Override
    public String toString() {
        return "messages.setEncryptedTyping#791451ed";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLInputEncryptedChat getPeer() {
        return peer;
    }

    public void setPeer(TLInputEncryptedChat peer) {
        this.peer = peer;
    }

    public boolean getTyping() {
        return typing;
    }

    public void setTyping(boolean typing) {
        this.typing = typing;
    }
}
