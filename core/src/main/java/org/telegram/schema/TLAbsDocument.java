package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLDocument: document#87232bc7</li>
 * <li>{@link TLDocumentEmpty: documentEmpty#36f8c871</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsDocument extends TLObject {
    public TLAbsDocument() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLDocument getAsDocument() {
        return null;
    }
}
