package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLNotifyAll extends TLAbsNotifyPeer {
    public static final int CLASS_ID = 0x74d07c60;

    public TLNotifyAll() {
    }

    @Override
    public String toString() {
        return "notifyAll#74d07c60";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
