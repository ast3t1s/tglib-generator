package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLPhoneCall: phoneCall#ffe6ab67</li>
 * <li>{@link TLPhoneCallAccepted: phoneCallAccepted#6d003d3f</li>
 * <li>{@link TLPhoneCallDiscarded: phoneCallDiscarded#50ca4de1</li>
 * <li>{@link TLPhoneCallEmpty: phoneCallEmpty#5366c915</li>
 * <li>{@link TLPhoneCallRequested: phoneCallRequested#83761ce4</li>
 * <li>{@link TLPhoneCallWaiting: phoneCallWaiting#1b8f4ad1</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsPhoneCall extends TLObject {
    public TLAbsPhoneCall() {
    }
}
