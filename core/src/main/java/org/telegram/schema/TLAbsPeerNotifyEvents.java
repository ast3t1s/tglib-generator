package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLPeerNotifyEventsAll: peerNotifyEventsAll#6d1ded88</li>
 * <li>{@link TLPeerNotifyEventsEmpty: peerNotifyEventsEmpty#add53cb3</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsPeerNotifyEvents extends TLObject {
    public TLAbsPeerNotifyEvents() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLPeerNotifyEventsAll getAsPeerNotifyEventsAll() {
        return null;
    }
}
