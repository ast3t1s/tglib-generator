package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLUserStatusEmpty: userStatusEmpty#9d05049</li>
 * <li>{@link TLUserStatusLastMonth: userStatusLastMonth#77ebc742</li>
 * <li>{@link TLUserStatusLastWeek: userStatusLastWeek#7bf09fc</li>
 * <li>{@link TLUserStatusOffline: userStatusOffline#8c703f</li>
 * <li>{@link TLUserStatusOnline: userStatusOnline#edb93949</li>
 * <li>{@link TLUserStatusRecently: userStatusRecently#e26f42f1</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsUserStatus extends TLObject {
    public TLAbsUserStatus() {
    }
}
