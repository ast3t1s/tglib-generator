package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputPrivacyValueDisallowAll extends TLAbsInputPrivacyRule {
    public static final int CLASS_ID = 0xd66b66c9;

    public TLInputPrivacyValueDisallowAll() {
    }

    @Override
    public String toString() {
        return "inputPrivacyValueDisallowAll#d66b66c9";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
