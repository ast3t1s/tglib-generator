package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputUser: inputUser#d8292816</li>
 * <li>{@link TLInputUserEmpty: inputUserEmpty#b98886cf</li>
 * <li>{@link TLInputUserSelf: inputUserSelf#f7c1b13f</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputUser extends TLObject {
    public TLAbsInputUser() {
    }
}
