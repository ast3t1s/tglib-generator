package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLChatParticipants: chatParticipants#3f460fed</li>
 * <li>{@link TLChatParticipantsForbidden: chatParticipantsForbidden#fc900c2b</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsChatParticipants extends TLObject {
    public TLAbsChatParticipants() {
    }
}
