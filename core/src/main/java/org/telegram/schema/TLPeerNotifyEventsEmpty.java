package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLPeerNotifyEventsEmpty extends TLAbsPeerNotifyEvents {
    public static final int CLASS_ID = 0xadd53cb3;

    public TLPeerNotifyEventsEmpty() {
    }

    @Override
    public String toString() {
        return "peerNotifyEventsEmpty#add53cb3";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    @Override
    public final boolean isEmpty() {
        return true;
    }

    @Override
    public final boolean isNotEmpty() {
        return false;
    }
}
