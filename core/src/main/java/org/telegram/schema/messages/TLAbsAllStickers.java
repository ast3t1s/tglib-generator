package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLAllStickers: messages.allStickers#edfd405f</li>
 * <li>{@link TLAllStickersNotModified: messages.allStickersNotModified#e86602c3</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsAllStickers extends TLObject {
    public TLAbsAllStickers() {
    }
}
