package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLChats: messages.chats#64ff9fd5</li>
 * <li>{@link TLChatsSlice: messages.chatsSlice#9cd81144</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsChats extends TLObject {
    public TLAbsChats() {
    }
}
