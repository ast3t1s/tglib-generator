package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLEncryptedChat: encryptedChat#fa56ce36</li>
 * <li>{@link TLEncryptedChatDiscarded: encryptedChatDiscarded#13d6dd27</li>
 * <li>{@link TLEncryptedChatEmpty: encryptedChatEmpty#ab7ec0a0</li>
 * <li>{@link TLEncryptedChatRequested: encryptedChatRequested#c878527e</li>
 * <li>{@link TLEncryptedChatWaiting: encryptedChatWaiting#3bf703dc</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsEncryptedChat extends TLObject {
    public TLAbsEncryptedChat() {
    }
}
