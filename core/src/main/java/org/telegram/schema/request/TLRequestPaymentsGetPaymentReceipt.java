package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.payments.TLPaymentReceipt;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestPaymentsGetPaymentReceipt extends TLMethod<TLPaymentReceipt> {
    public static final int CLASS_ID = 0xa092a980;

    protected int msgId;

    public TLRequestPaymentsGetPaymentReceipt() {
    }

    public TLRequestPaymentsGetPaymentReceipt(int msgId) {
        this.msgId = msgId;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLPaymentReceipt deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLPaymentReceipt)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLPaymentReceipt) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(msgId, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        msgId = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "payments.getPaymentReceipt#a092a980";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getMsgId() {
        return msgId;
    }

    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }
}
