package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLDataJSON;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestBotsSendCustomRequest extends TLMethod<TLDataJSON> {
    public static final int CLASS_ID = 0xaa2769ed;

    protected String customMethod;

    protected TLDataJSON params;

    public TLRequestBotsSendCustomRequest() {
    }

    public TLRequestBotsSendCustomRequest(String customMethod, TLDataJSON params) {
        this.customMethod = customMethod;
        this.params = params;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLDataJSON deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLDataJSON)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLDataJSON) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(customMethod, stream);
        writeTLObject(params, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        customMethod = readTLString(stream);
        params = readTLObject(stream, context, TLDataJSON.class, TLDataJSON.CLASS_ID);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(customMethod);
        size += params.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "bots.sendCustomRequest#aa2769ed";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getCustomMethod() {
        return customMethod;
    }

    public void setCustomMethod(String customMethod) {
        this.customMethod = customMethod;
    }

    public TLDataJSON getParams() {
        return params;
    }

    public void setParams(TLDataJSON params) {
        this.params = params;
    }
}
