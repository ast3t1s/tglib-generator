package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLPeerChannel: peerChannel#bddde532</li>
 * <li>{@link TLPeerChat: peerChat#bad0e5bb</li>
 * <li>{@link TLPeerUser: peerUser#9db1bc6d</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsPeer extends TLObject {
    public TLAbsPeer() {
    }
}
