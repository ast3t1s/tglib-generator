package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLPhoneCallDiscardReasonDisconnect extends TLAbsPhoneCallDiscardReason {
    public static final int CLASS_ID = 0xe095c1a0;

    public TLPhoneCallDiscardReasonDisconnect() {
    }

    @Override
    public String toString() {
        return "phoneCallDiscardReasonDisconnect#e095c1a0";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
