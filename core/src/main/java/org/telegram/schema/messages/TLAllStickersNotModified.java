package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLAllStickersNotModified extends TLAbsAllStickers {
    public static final int CLASS_ID = 0xe86602c3;

    public TLAllStickersNotModified() {
    }

    @Override
    public String toString() {
        return "messages.allStickersNotModified#e86602c3";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
