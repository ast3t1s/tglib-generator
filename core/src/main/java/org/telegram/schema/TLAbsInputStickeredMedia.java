package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputStickeredMediaDocument: inputStickeredMediaDocument#438865b</li>
 * <li>{@link TLInputStickeredMediaPhoto: inputStickeredMediaPhoto#4a992157</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputStickeredMedia extends TLObject {
    public TLAbsInputStickeredMedia() {
    }
}
