package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateDialogPinned extends TLAbsUpdate {
    public static final int CLASS_ID = 0xd711a2cc;

    protected int flags;

    protected boolean pinned;

    protected TLAbsPeer peer;

    public TLUpdateDialogPinned() {
    }

    public TLUpdateDialogPinned(boolean pinned, TLAbsPeer peer) {
        this.pinned = pinned;
        this.peer = peer;
    }

    private void computeFlags() {
        flags = 0;
        flags = pinned ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeTLObject(peer, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        pinned = (flags & 1) != 0;
        peer = readTLObject(stream, context, TLAbsPeer.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += peer.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "updateDialogPinned#d711a2cc";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getPinned() {
        return pinned;
    }

    public void setPinned(boolean pinned) {
        this.pinned = pinned;
    }

    public TLAbsPeer getPeer() {
        return peer;
    }

    public void setPeer(TLAbsPeer peer) {
        this.peer = peer;
    }
}
