package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputMessagesFilterGif extends TLAbsMessagesFilter {
    public static final int CLASS_ID = 0xffc86587;

    public TLInputMessagesFilterGif() {
    }

    @Override
    public String toString() {
        return "inputMessagesFilterGif#ffc86587";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
