package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputMessagesFilterVideo extends TLAbsMessagesFilter {
    public static final int CLASS_ID = 0x9fc00e65;

    public TLInputMessagesFilterVideo() {
    }

    @Override
    public String toString() {
        return "inputMessagesFilterVideo#9fc00e65";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
