package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLMessageActionGameScore extends TLAbsMessageAction {
    public static final int CLASS_ID = 0x92a72876;

    protected long gameId;

    protected int score;

    public TLMessageActionGameScore() {
    }

    public TLMessageActionGameScore(long gameId, int score) {
        this.gameId = gameId;
        this.score = score;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeLong(gameId, stream);
        writeInt(score, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        gameId = readLong(stream);
        score = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT64;
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "messageActionGameScore#92a72876";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public long getGameId() {
        return gameId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
