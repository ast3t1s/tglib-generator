package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLPhoneCallDiscardReasonBusy extends TLAbsPhoneCallDiscardReason {
    public static final int CLASS_ID = 0xfaf7e8c9;

    public TLPhoneCallDiscardReasonBusy() {
    }

    @Override
    public String toString() {
        return "phoneCallDiscardReasonBusy#faf7e8c9";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
