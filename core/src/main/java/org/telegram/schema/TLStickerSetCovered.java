package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLStickerSetCovered extends TLAbsStickerSetCovered {
    public static final int CLASS_ID = 0x6410a5d2;

    protected TLAbsDocument cover;

    public TLStickerSetCovered() {
    }

    public TLStickerSetCovered(TLStickerSet set, TLAbsDocument cover) {
        this.set = set;
        this.cover = cover;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(set, stream);
        writeTLObject(cover, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        set = readTLObject(stream, context, TLStickerSet.class, TLStickerSet.CLASS_ID);
        cover = readTLObject(stream, context, TLAbsDocument.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += set.computeSerializedSize();
        size += cover.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "stickerSetCovered#6410a5d2";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLStickerSet getSet() {
        return set;
    }

    public void setSet(TLStickerSet set) {
        this.set = set;
    }

    public TLAbsDocument getCover() {
        return cover;
    }

    public void setCover(TLAbsDocument cover) {
        this.cover = cover;
    }
}
