package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLAbsUpdates;
import org.telegram.schema.TLInputPhoneCall;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestPhoneSetCallRating extends TLMethod<TLAbsUpdates> {
    public static final int CLASS_ID = 0x1c536a34;

    protected TLInputPhoneCall peer;

    protected int rating;

    protected String comment;

    public TLRequestPhoneSetCallRating() {
    }

    public TLRequestPhoneSetCallRating(TLInputPhoneCall peer, int rating, String comment) {
        this.peer = peer;
        this.rating = rating;
        this.comment = comment;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLAbsUpdates deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLAbsUpdates)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLAbsUpdates) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(peer, stream);
        writeInt(rating, stream);
        writeString(comment, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        peer = readTLObject(stream, context, TLInputPhoneCall.class, TLInputPhoneCall.CLASS_ID);
        rating = readInt(stream);
        comment = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += peer.computeSerializedSize();
        size += SIZE_INT32;
        size += computeTLStringSerializedSize(comment);
        return size;
    }

    @Override
    public String toString() {
        return "phone.setCallRating#1c536a34";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLInputPhoneCall getPeer() {
        return peer;
    }

    public void setPeer(TLInputPhoneCall peer) {
        this.peer = peer;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
