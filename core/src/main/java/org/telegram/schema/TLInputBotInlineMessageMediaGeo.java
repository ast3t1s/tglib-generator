package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLInputBotInlineMessageMediaGeo extends TLAbsInputBotInlineMessage {
    public static final int CLASS_ID = 0xf4a59de1;

    protected int flags;

    protected TLAbsInputGeoPoint geoPoint;

    protected TLAbsReplyMarkup replyMarkup;

    public TLInputBotInlineMessageMediaGeo() {
    }

    public TLInputBotInlineMessageMediaGeo(TLAbsInputGeoPoint geoPoint,
            TLAbsReplyMarkup replyMarkup) {
        this.geoPoint = geoPoint;
        this.replyMarkup = replyMarkup;
    }

    private void computeFlags() {
        flags = 0;
        flags = replyMarkup != null ? (flags | 4) : (flags & ~4);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeTLObject(geoPoint, stream);
        if ((flags & 4) != 0) {
            if (replyMarkup == null) throwNullFieldException("replyMarkup" , flags);
            writeTLObject(replyMarkup, stream);
        }
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        geoPoint = readTLObject(stream, context, TLAbsInputGeoPoint.class, -1);
        replyMarkup = (flags & 4) != 0 ? readTLObject(stream, context, TLAbsReplyMarkup.class, -1) : null;
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += geoPoint.computeSerializedSize();
        if ((flags & 4) != 0) {
            if (replyMarkup == null) throwNullFieldException("replyMarkup" , flags);
            size += replyMarkup.computeSerializedSize();
        }
        return size;
    }

    @Override
    public String toString() {
        return "inputBotInlineMessageMediaGeo#f4a59de1";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsInputGeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(TLAbsInputGeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public TLAbsReplyMarkup getReplyMarkup() {
        return replyMarkup;
    }

    public void setReplyMarkup(TLAbsReplyMarkup replyMarkup) {
        this.replyMarkup = replyMarkup;
    }
}
