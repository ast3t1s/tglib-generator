package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLAbsChannelParticipantRole;
import org.telegram.schema.TLAbsInputChannel;
import org.telegram.schema.TLAbsInputUser;
import org.telegram.schema.TLAbsUpdates;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestChannelsEditAdmin extends TLMethod<TLAbsUpdates> {
    public static final int CLASS_ID = 0xeb7611d0;

    protected TLAbsInputChannel channel;

    protected TLAbsInputUser userId;

    protected TLAbsChannelParticipantRole role;

    public TLRequestChannelsEditAdmin() {
    }

    public TLRequestChannelsEditAdmin(TLAbsInputChannel channel, TLAbsInputUser userId,
            TLAbsChannelParticipantRole role) {
        this.channel = channel;
        this.userId = userId;
        this.role = role;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLAbsUpdates deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLAbsUpdates)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLAbsUpdates) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(channel, stream);
        writeTLObject(userId, stream);
        writeTLObject(role, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        channel = readTLObject(stream, context, TLAbsInputChannel.class, -1);
        userId = readTLObject(stream, context, TLAbsInputUser.class, -1);
        role = readTLObject(stream, context, TLAbsChannelParticipantRole.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += channel.computeSerializedSize();
        size += userId.computeSerializedSize();
        size += role.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "channels.editAdmin#eb7611d0";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsInputChannel getChannel() {
        return channel;
    }

    public void setChannel(TLAbsInputChannel channel) {
        this.channel = channel;
    }

    public TLAbsInputUser getUserId() {
        return userId;
    }

    public void setUserId(TLAbsInputUser userId) {
        this.userId = userId;
    }

    public TLAbsChannelParticipantRole getRole() {
        return role;
    }

    public void setRole(TLAbsChannelParticipantRole role) {
        this.role = role;
    }
}
