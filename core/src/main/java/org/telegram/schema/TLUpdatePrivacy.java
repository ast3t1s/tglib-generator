package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLVector;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdatePrivacy extends TLAbsUpdate {
    public static final int CLASS_ID = 0xee3b272a;

    protected TLAbsPrivacyKey key;

    protected TLVector<TLAbsPrivacyRule> rules;

    public TLUpdatePrivacy() {
    }

    public TLUpdatePrivacy(TLAbsPrivacyKey key, TLVector<TLAbsPrivacyRule> rules) {
        this.key = key;
        this.rules = rules;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(key, stream);
        writeTLVector(rules, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        key = readTLObject(stream, context, TLAbsPrivacyKey.class, -1);
        rules = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += key.computeSerializedSize();
        size += rules.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "updatePrivacy#ee3b272a";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsPrivacyKey getKey() {
        return key;
    }

    public void setKey(TLAbsPrivacyKey key) {
        this.key = key;
    }

    public TLVector<TLAbsPrivacyRule> getRules() {
        return rules;
    }

    public void setRules(TLVector<TLAbsPrivacyRule> rules) {
        this.rules = rules;
    }
}
