package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLKeyboardButtonSwitchInline extends TLAbsKeyboardButton {
    public static final int CLASS_ID = 0x568a748;

    protected int flags;

    protected boolean samePeer;

    protected String text;

    protected String query;

    public TLKeyboardButtonSwitchInline() {
    }

    public TLKeyboardButtonSwitchInline(boolean samePeer, String text, String query) {
        this.samePeer = samePeer;
        this.text = text;
        this.query = query;
    }

    private void computeFlags() {
        flags = 0;
        flags = samePeer ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeString(text, stream);
        writeString(query, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        samePeer = (flags & 1) != 0;
        text = readTLString(stream);
        query = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += computeTLStringSerializedSize(text);
        size += computeTLStringSerializedSize(query);
        return size;
    }

    @Override
    public String toString() {
        return "keyboardButtonSwitchInline#568a748";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getSamePeer() {
        return samePeer;
    }

    public void setSamePeer(boolean samePeer) {
        this.samePeer = samePeer;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
