package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateConfig extends TLAbsUpdate {
    public static final int CLASS_ID = 0xa229dd06;

    public TLUpdateConfig() {
    }

    @Override
    public String toString() {
        return "updateConfig#a229dd06";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
