package org.telegram.schema.contacts;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLContactsNotModified extends TLAbsContacts {
    public static final int CLASS_ID = 0xb74ba9d2;

    public TLContactsNotModified() {
    }

    @Override
    public String toString() {
        return "contacts.contactsNotModified#b74ba9d2";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
