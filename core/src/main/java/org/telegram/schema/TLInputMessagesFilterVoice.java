package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputMessagesFilterVoice extends TLAbsMessagesFilter {
    public static final int CLASS_ID = 0x50f5c392;

    public TLInputMessagesFilterVoice() {
    }

    @Override
    public String toString() {
        return "inputMessagesFilterVoice#50f5c392";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
