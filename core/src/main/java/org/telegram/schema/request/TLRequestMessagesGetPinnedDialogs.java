package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.messages.TLPeerDialogs;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestMessagesGetPinnedDialogs extends TLMethod<TLPeerDialogs> {
    public static final int CLASS_ID = 0xe254d64e;

    public TLRequestMessagesGetPinnedDialogs() {
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLPeerDialogs deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLPeerDialogs)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLPeerDialogs) response;
    }

    @Override
    public String toString() {
        return "messages.getPinnedDialogs#e254d64e";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
