package org.telegram.schema.contacts;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLTopPeers: contacts.topPeers#70b772a8</li>
 * <li>{@link TLTopPeersNotModified: contacts.topPeersNotModified#de266ef5</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsTopPeers extends TLObject {
    public TLAbsTopPeers() {
    }
}
