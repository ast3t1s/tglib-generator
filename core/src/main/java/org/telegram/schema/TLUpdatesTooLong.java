package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdatesTooLong extends TLAbsUpdates {
    public static final int CLASS_ID = 0xe317af7e;

    public TLUpdatesTooLong() {
    }

    @Override
    public String toString() {
        return "updatesTooLong#e317af7e";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
