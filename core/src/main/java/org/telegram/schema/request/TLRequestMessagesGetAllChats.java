package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLIntVector;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.messages.TLAbsChats;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestMessagesGetAllChats extends TLMethod<TLAbsChats> {
    public static final int CLASS_ID = 0xeba80ff0;

    protected TLIntVector exceptIds;

    public TLRequestMessagesGetAllChats() {
    }

    public TLRequestMessagesGetAllChats(TLIntVector exceptIds) {
        this.exceptIds = exceptIds;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLAbsChats deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLAbsChats)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLAbsChats) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(exceptIds, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        exceptIds = readTLIntVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += exceptIds.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messages.getAllChats#eba80ff0";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLIntVector getExceptIds() {
        return exceptIds;
    }

    public void setExceptIds(TLIntVector exceptIds) {
        this.exceptIds = exceptIds;
    }
}
