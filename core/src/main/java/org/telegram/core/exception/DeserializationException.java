package org.telegram.core.exception;

import java.io.IOException;

public class DeserializationException extends IOException {
    public DeserializationException(String s) {
        super(s);
    }
} 
