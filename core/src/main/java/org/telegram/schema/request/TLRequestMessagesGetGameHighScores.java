package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLAbsInputPeer;
import org.telegram.schema.TLAbsInputUser;
import org.telegram.schema.messages.TLHighScores;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestMessagesGetGameHighScores extends TLMethod<TLHighScores> {
    public static final int CLASS_ID = 0xe822649d;

    protected TLAbsInputPeer peer;

    protected int id;

    protected TLAbsInputUser userId;

    public TLRequestMessagesGetGameHighScores() {
    }

    public TLRequestMessagesGetGameHighScores(TLAbsInputPeer peer, int id, TLAbsInputUser userId) {
        this.peer = peer;
        this.id = id;
        this.userId = userId;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLHighScores deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLHighScores)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLHighScores) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(peer, stream);
        writeInt(id, stream);
        writeTLObject(userId, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        peer = readTLObject(stream, context, TLAbsInputPeer.class, -1);
        id = readInt(stream);
        userId = readTLObject(stream, context, TLAbsInputUser.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += peer.computeSerializedSize();
        size += SIZE_INT32;
        size += userId.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messages.getGameHighScores#e822649d";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsInputPeer getPeer() {
        return peer;
    }

    public void setPeer(TLAbsInputPeer peer) {
        this.peer = peer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TLAbsInputUser getUserId() {
        return userId;
    }

    public void setUserId(TLAbsInputUser userId) {
        this.userId = userId;
    }
}
