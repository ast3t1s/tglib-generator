package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputPrivacyValueAllowAll extends TLAbsInputPrivacyRule {
    public static final int CLASS_ID = 0x184b35ce;

    public TLInputPrivacyValueAllowAll() {
    }

    @Override
    public String toString() {
        return "inputPrivacyValueAllowAll#184b35ce";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
