package org.telegram.schema.channels;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsChannelParticipant;
import org.telegram.schema.TLAbsUser;

/**
 * @author Telegram Schema Generator
 */
public class TLChannelParticipants extends TLObject {
    public static final int CLASS_ID = 0xf56ee2a8;

    protected int count;

    protected TLVector<TLAbsChannelParticipant> participants;

    protected TLVector<TLAbsUser> users;

    public TLChannelParticipants() {
    }

    public TLChannelParticipants(int count, TLVector<TLAbsChannelParticipant> participants,
            TLVector<TLAbsUser> users) {
        this.count = count;
        this.participants = participants;
        this.users = users;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(count, stream);
        writeTLVector(participants, stream);
        writeTLVector(users, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        count = readInt(stream);
        participants = readTLVector(stream, context);
        users = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += participants.computeSerializedSize();
        size += users.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "channels.channelParticipants#f56ee2a8";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public TLVector<TLAbsChannelParticipant> getParticipants() {
        return participants;
    }

    public void setParticipants(TLVector<TLAbsChannelParticipant> participants) {
        this.participants = participants;
    }

    public TLVector<TLAbsUser> getUsers() {
        return users;
    }

    public void setUsers(TLVector<TLAbsUser> users) {
        this.users = users;
    }
}
