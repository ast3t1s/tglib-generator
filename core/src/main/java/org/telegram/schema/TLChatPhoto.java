package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLChatPhoto extends TLAbsChatPhoto {
    public static final int CLASS_ID = 0x6153276a;

    protected TLAbsFileLocation photoSmall;

    protected TLAbsFileLocation photoBig;

    public TLChatPhoto() {
    }

    public TLChatPhoto(TLAbsFileLocation photoSmall, TLAbsFileLocation photoBig) {
        this.photoSmall = photoSmall;
        this.photoBig = photoBig;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(photoSmall, stream);
        writeTLObject(photoBig, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        photoSmall = readTLObject(stream, context, TLAbsFileLocation.class, -1);
        photoBig = readTLObject(stream, context, TLAbsFileLocation.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += photoSmall.computeSerializedSize();
        size += photoBig.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "chatPhoto#6153276a";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsFileLocation getPhotoSmall() {
        return photoSmall;
    }

    public void setPhotoSmall(TLAbsFileLocation photoSmall) {
        this.photoSmall = photoSmall;
    }

    public TLAbsFileLocation getPhotoBig() {
        return photoBig;
    }

    public void setPhotoBig(TLAbsFileLocation photoBig) {
        this.photoBig = photoBig;
    }

    @Override
    public final boolean isEmpty() {
        return false;
    }

    @Override
    public final boolean isNotEmpty() {
        return true;
    }

    @Override
    public final TLChatPhoto getAsChatPhoto() {
        return this;
    }
}
