package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBytes;

/**
 * @author Telegram Schema Generator
 */
public class TLInputMediaInvoice extends TLAbsInputMedia {
    public static final int CLASS_ID = 0x92153685;

    protected int flags;

    protected String title;

    protected String description;

    protected TLInputWebDocument photo;

    protected TLInvoice invoice;

    protected TLBytes payload;

    protected String provider;

    protected String startParam;

    public TLInputMediaInvoice() {
    }

    public TLInputMediaInvoice(String title, String description, TLInputWebDocument photo,
            TLInvoice invoice, TLBytes payload, String provider, String startParam) {
        this.title = title;
        this.description = description;
        this.photo = photo;
        this.invoice = invoice;
        this.payload = payload;
        this.provider = provider;
        this.startParam = startParam;
    }

    private void computeFlags() {
        flags = 0;
        flags = photo != null ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeString(title, stream);
        writeString(description, stream);
        if ((flags & 1) != 0) {
            if (photo == null) throwNullFieldException("photo" , flags);
            writeTLObject(photo, stream);
        }
        writeTLObject(invoice, stream);
        writeTLBytes(payload, stream);
        writeString(provider, stream);
        writeString(startParam, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        title = readTLString(stream);
        description = readTLString(stream);
        photo = (flags & 1) != 0 ? readTLObject(stream, context, TLInputWebDocument.class, TLInputWebDocument.CLASS_ID) : null;
        invoice = readTLObject(stream, context, TLInvoice.class, TLInvoice.CLASS_ID);
        payload = readTLBytes(stream, context);
        provider = readTLString(stream);
        startParam = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += computeTLStringSerializedSize(title);
        size += computeTLStringSerializedSize(description);
        if ((flags & 1) != 0) {
            if (photo == null) throwNullFieldException("photo" , flags);
            size += photo.computeSerializedSize();
        }
        size += invoice.computeSerializedSize();
        size += computeTLBytesSerializedSize(payload);
        size += computeTLStringSerializedSize(provider);
        size += computeTLStringSerializedSize(startParam);
        return size;
    }

    @Override
    public String toString() {
        return "inputMediaInvoice#92153685";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TLInputWebDocument getPhoto() {
        return photo;
    }

    public void setPhoto(TLInputWebDocument photo) {
        this.photo = photo;
    }

    public TLInvoice getInvoice() {
        return invoice;
    }

    public void setInvoice(TLInvoice invoice) {
        this.invoice = invoice;
    }

    public TLBytes getPayload() {
        return payload;
    }

    public void setPayload(TLBytes payload) {
        this.payload = payload;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getStartParam() {
        return startParam;
    }

    public void setStartParam(String startParam) {
        this.startParam = startParam;
    }
}
