package org.telegram.generator.model;

import java.util.List;

public class TLAbsConstructor implements Comparable<TLAbsConstructor> {

    private final String name;
    private final List<TLParameter> parameters;
    private final TLDefTypeModel.TLTypeRaw tlTypeRaw;
    private final boolean abstractEmptyConstructor;

    public TLAbsConstructor(String name, List<TLParameter> parameters, TLDefTypeModel.TLTypeRaw tlTypeRaw, boolean abstractEmptyConstructor) {
        this.name = name;
        this.parameters = parameters;
        this.tlTypeRaw = tlTypeRaw;
        this.abstractEmptyConstructor = abstractEmptyConstructor;
    }

    public String getName() {
        return name;
    }

    public List<TLParameter> getParameters() {
        return parameters;
    }

    public TLDefTypeModel.TLTypeRaw getTlType() {
        return tlTypeRaw;
    }

    public boolean isAbstractEmptyConstructor() {
        return abstractEmptyConstructor;
    }

    @Override
    public int compareTo(TLAbsConstructor o) {
        return name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return name + " -> " + tlTypeRaw;
    }

    @Override
    public boolean equals(Object other) {
        return other instanceof TLConstructor && ((TLConstructor) other).getName().equals(name);
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
