package org.telegram.schema.channels;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsChannelParticipant;
import org.telegram.schema.TLAbsUser;

/**
 * @author Telegram Schema Generator
 */
public class TLChannelParticipant extends TLObject {
    public static final int CLASS_ID = 0xd0d9b163;

    protected TLAbsChannelParticipant participant;

    protected TLVector<TLAbsUser> users;

    public TLChannelParticipant() {
    }

    public TLChannelParticipant(TLAbsChannelParticipant participant, TLVector<TLAbsUser> users) {
        this.participant = participant;
        this.users = users;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(participant, stream);
        writeTLVector(users, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        participant = readTLObject(stream, context, TLAbsChannelParticipant.class, -1);
        users = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += participant.computeSerializedSize();
        size += users.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "channels.channelParticipant#d0d9b163";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsChannelParticipant getParticipant() {
        return participant;
    }

    public void setParticipant(TLAbsChannelParticipant participant) {
        this.participant = participant;
    }

    public TLVector<TLAbsUser> getUsers() {
        return users;
    }

    public void setUsers(TLVector<TLAbsUser> users) {
        this.users = users;
    }
}
