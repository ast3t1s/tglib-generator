package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLUserStatusLastWeek extends TLAbsUserStatus {
    public static final int CLASS_ID = 0x7bf09fc;

    public TLUserStatusLastWeek() {
    }

    @Override
    public String toString() {
        return "userStatusLastWeek#7bf09fc";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
