package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLStickerSetInstallResultSuccess extends TLAbsStickerSetInstallResult {
    public static final int CLASS_ID = 0x38641628;

    public TLStickerSetInstallResultSuccess() {
    }

    @Override
    public String toString() {
        return "messages.stickerSetInstallResultSuccess#38641628";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
