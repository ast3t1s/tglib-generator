package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLUserStatusLastMonth extends TLAbsUserStatus {
    public static final int CLASS_ID = 0x77ebc742;

    public TLUserStatusLastMonth() {
    }

    @Override
    public String toString() {
        return "userStatusLastMonth#77ebc742";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
