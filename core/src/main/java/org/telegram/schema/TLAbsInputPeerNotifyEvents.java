package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputPeerNotifyEventsAll: inputPeerNotifyEventsAll#e86a2c74</li>
 * <li>{@link TLInputPeerNotifyEventsEmpty: inputPeerNotifyEventsEmpty#f03064d8</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputPeerNotifyEvents extends TLObject {
    public TLAbsInputPeerNotifyEvents() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLInputPeerNotifyEventsAll getAsInputPeerNotifyEventsAll() {
        return null;
    }

    public static TLInputPeerNotifyEventsEmpty newEmpty() {
        return new TLInputPeerNotifyEventsEmpty();
    }

    public static TLInputPeerNotifyEventsAll newNotEmpty() {
        return new TLInputPeerNotifyEventsAll();
    }
}
