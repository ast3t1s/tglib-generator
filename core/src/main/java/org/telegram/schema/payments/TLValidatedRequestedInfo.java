package org.telegram.schema.payments;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLShippingOption;

/**
 * @author Telegram Schema Generator
 */
public class TLValidatedRequestedInfo extends TLObject {
    public static final int CLASS_ID = 0xd1451883;

    protected int flags;

    protected String id;

    protected TLVector<TLShippingOption> shippingOptions;

    public TLValidatedRequestedInfo() {
    }

    public TLValidatedRequestedInfo(String id, TLVector<TLShippingOption> shippingOptions) {
        this.id = id;
        this.shippingOptions = shippingOptions;
    }

    private void computeFlags() {
        flags = 0;
        flags = id != null ? (flags | 1) : (flags & ~1);
        flags = shippingOptions != null ? (flags | 2) : (flags & ~2);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        if ((flags & 1) != 0) {
            if (id == null) throwNullFieldException("id" , flags);
            writeString(id, stream);
        }
        if ((flags & 2) != 0) {
            if (shippingOptions == null) throwNullFieldException("shippingOptions" , flags);
            writeTLVector(shippingOptions, stream);
        }
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        id = (flags & 1) != 0 ? readTLString(stream) : null;
        shippingOptions = (flags & 2) != 0 ? readTLVector(stream, context) : null;
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        if ((flags & 1) != 0) {
            if (id == null) throwNullFieldException("id" , flags);
            size += computeTLStringSerializedSize(id);
        }
        if ((flags & 2) != 0) {
            if (shippingOptions == null) throwNullFieldException("shippingOptions" , flags);
            size += shippingOptions.computeSerializedSize();
        }
        return size;
    }

    @Override
    public String toString() {
        return "payments.validatedRequestedInfo#d1451883";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TLVector<TLShippingOption> getShippingOptions() {
        return shippingOptions;
    }

    public void setShippingOptions(TLVector<TLShippingOption> shippingOptions) {
        this.shippingOptions = shippingOptions;
    }
}
