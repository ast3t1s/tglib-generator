package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputUserEmpty extends TLAbsInputUser {
    public static final int CLASS_ID = 0xb98886cf;

    public TLInputUserEmpty() {
    }

    @Override
    public String toString() {
        return "inputUserEmpty#b98886cf";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
