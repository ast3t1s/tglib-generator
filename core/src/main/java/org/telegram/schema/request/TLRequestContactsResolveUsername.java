package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.contacts.TLResolvedPeer;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestContactsResolveUsername extends TLMethod<TLResolvedPeer> {
    public static final int CLASS_ID = 0xf93ccba3;

    protected String username;

    public TLRequestContactsResolveUsername() {
    }

    public TLRequestContactsResolveUsername(String username) {
        this.username = username;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLResolvedPeer deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLResolvedPeer)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLResolvedPeer) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(username, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        username = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(username);
        return size;
    }

    @Override
    public String toString() {
        return "contacts.resolveUsername#f93ccba3";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
