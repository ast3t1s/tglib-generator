package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateDraftMessage extends TLAbsUpdate {
    public static final int CLASS_ID = 0xee2bb969;

    protected TLAbsPeer peer;

    protected TLAbsDraftMessage draft;

    public TLUpdateDraftMessage() {
    }

    public TLUpdateDraftMessage(TLAbsPeer peer, TLAbsDraftMessage draft) {
        this.peer = peer;
        this.draft = draft;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(peer, stream);
        writeTLObject(draft, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        peer = readTLObject(stream, context, TLAbsPeer.class, -1);
        draft = readTLObject(stream, context, TLAbsDraftMessage.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += peer.computeSerializedSize();
        size += draft.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "updateDraftMessage#ee2bb969";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsPeer getPeer() {
        return peer;
    }

    public void setPeer(TLAbsPeer peer) {
        this.peer = peer;
    }

    public TLAbsDraftMessage getDraft() {
        return draft;
    }

    public void setDraft(TLAbsDraftMessage draft) {
        this.draft = draft;
    }
}
