package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBool;
import org.telegram.core.type.TLBytes;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestAuthBindTempAuthKey extends TLMethod<TLBool> {
    public static final int CLASS_ID = 0xcdd42a05;

    protected long permAuthKeyId;

    protected long nonce;

    protected int expiresAt;

    protected TLBytes encryptedMessage;

    public TLRequestAuthBindTempAuthKey() {
    }

    public TLRequestAuthBindTempAuthKey(long permAuthKeyId, long nonce, int expiresAt,
            TLBytes encryptedMessage) {
        this.permAuthKeyId = permAuthKeyId;
        this.nonce = nonce;
        this.expiresAt = expiresAt;
        this.encryptedMessage = encryptedMessage;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLBool deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLBool)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLBool) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeLong(permAuthKeyId, stream);
        writeLong(nonce, stream);
        writeInt(expiresAt, stream);
        writeTLBytes(encryptedMessage, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        permAuthKeyId = readLong(stream);
        nonce = readLong(stream);
        expiresAt = readInt(stream);
        encryptedMessage = readTLBytes(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT64;
        size += SIZE_INT64;
        size += SIZE_INT32;
        size += computeTLBytesSerializedSize(encryptedMessage);
        return size;
    }

    @Override
    public String toString() {
        return "auth.bindTempAuthKey#cdd42a05";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public long getPermAuthKeyId() {
        return permAuthKeyId;
    }

    public void setPermAuthKeyId(long permAuthKeyId) {
        this.permAuthKeyId = permAuthKeyId;
    }

    public long getNonce() {
        return nonce;
    }

    public void setNonce(long nonce) {
        this.nonce = nonce;
    }

    public int getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(int expiresAt) {
        this.expiresAt = expiresAt;
    }

    public TLBytes getEncryptedMessage() {
        return encryptedMessage;
    }

    public void setEncryptedMessage(TLBytes encryptedMessage) {
        this.encryptedMessage = encryptedMessage;
    }
}
