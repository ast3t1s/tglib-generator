package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLChannel: channel#a14dca52</li>
 * <li>{@link TLChannelForbidden: channelForbidden#8537784f</li>
 * <li>{@link TLChat: chat#d91cdd54</li>
 * <li>{@link TLChatEmpty: chatEmpty#9ba2d800</li>
 * <li>{@link TLChatForbidden: chatForbidden#7328bdb</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsChat extends TLObject {
    public TLAbsChat() {
    }
}
