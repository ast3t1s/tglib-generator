package org.telegram.schema.photos;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLPhotos: photos.photos#8dca6aa5</li>
 * <li>{@link TLPhotosSlice: photos.photosSlice#15051f54</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsPhotos extends TLObject {
    public TLAbsPhotos() {
    }
}
