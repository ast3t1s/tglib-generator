package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLStickerSetInstallResultArchive: messages.stickerSetInstallResultArchive#35e410a8</li>
 * <li>{@link TLStickerSetInstallResultSuccess: messages.stickerSetInstallResultSuccess#38641628</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsStickerSetInstallResult extends TLObject {
    public TLAbsStickerSetInstallResult() {
    }
}
