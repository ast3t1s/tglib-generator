package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLPhotoCachedSize: photoCachedSize#e9a734fa</li>
 * <li>{@link TLPhotoSize: photoSize#77bfb61b</li>
 * <li>{@link TLPhotoSizeEmpty: photoSizeEmpty#e17e23c</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsPhotoSize extends TLObject {
    public TLAbsPhotoSize() {
    }
}
