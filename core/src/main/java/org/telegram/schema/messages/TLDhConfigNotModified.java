package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBytes;

/**
 * @author Telegram Schema Generator
 */
public class TLDhConfigNotModified extends TLAbsDhConfig {
    public static final int CLASS_ID = 0xc0e24635;

    protected TLBytes random;

    public TLDhConfigNotModified() {
    }

    public TLDhConfigNotModified(TLBytes random) {
        this.random = random;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLBytes(random, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        random = readTLBytes(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLBytesSerializedSize(random);
        return size;
    }

    @Override
    public String toString() {
        return "messages.dhConfigNotModified#c0e24635";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLBytes getRandom() {
        return random;
    }

    public void setRandom(TLBytes random) {
        this.random = random;
    }
}
