package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLConfig;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestHelpGetConfig extends TLMethod<TLConfig> {
    public static final int CLASS_ID = 0xc4f9186b;

    public TLRequestHelpGetConfig() {
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLConfig deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLConfig)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLConfig) response;
    }

    @Override
    public String toString() {
        return "help.getConfig#c4f9186b";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
