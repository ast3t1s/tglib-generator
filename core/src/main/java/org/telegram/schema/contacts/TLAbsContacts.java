package org.telegram.schema.contacts;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLContacts: contacts.contacts#6f8b8cb2</li>
 * <li>{@link TLContactsNotModified: contacts.contactsNotModified#b74ba9d2</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsContacts extends TLObject {
    public TLAbsContacts() {
    }
}
