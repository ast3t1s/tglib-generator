package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputPrivacyKeyChatInvite: inputPrivacyKeyChatInvite#bdfb0426</li>
 * <li>{@link TLInputPrivacyKeyPhoneCall: inputPrivacyKeyPhoneCall#fabadc5f</li>
 * <li>{@link TLInputPrivacyKeyStatusTimestamp: inputPrivacyKeyStatusTimestamp#4f96cb18</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputPrivacyKey extends TLObject {
    public TLAbsInputPrivacyKey() {
    }
}
