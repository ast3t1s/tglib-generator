package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputPrivacyKeyChatInvite extends TLAbsInputPrivacyKey {
    public static final int CLASS_ID = 0xbdfb0426;

    public TLInputPrivacyKeyChatInvite() {
    }

    @Override
    public String toString() {
        return "inputPrivacyKeyChatInvite#bdfb0426";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
