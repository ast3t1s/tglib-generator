package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLWebPageNotModified extends TLAbsWebPage {
    public static final int CLASS_ID = 0x85849473;

    public TLWebPageNotModified() {
    }

    @Override
    public String toString() {
        return "webPageNotModified#85849473";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
