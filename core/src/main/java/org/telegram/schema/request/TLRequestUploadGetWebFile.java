package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLInputWebFileLocation;
import org.telegram.schema.upload.TLWebFile;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestUploadGetWebFile extends TLMethod<TLWebFile> {
    public static final int CLASS_ID = 0x24e6818d;

    protected TLInputWebFileLocation location;

    protected int offset;

    protected int limit;

    public TLRequestUploadGetWebFile() {
    }

    public TLRequestUploadGetWebFile(TLInputWebFileLocation location, int offset, int limit) {
        this.location = location;
        this.offset = offset;
        this.limit = limit;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLWebFile deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLWebFile)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLWebFile) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(location, stream);
        writeInt(offset, stream);
        writeInt(limit, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        location = readTLObject(stream, context, TLInputWebFileLocation.class, TLInputWebFileLocation.CLASS_ID);
        offset = readInt(stream);
        limit = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += location.computeSerializedSize();
        size += SIZE_INT32;
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "upload.getWebFile#24e6818d";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLInputWebFileLocation getLocation() {
        return location;
    }

    public void setLocation(TLInputWebFileLocation location) {
        this.location = location;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
