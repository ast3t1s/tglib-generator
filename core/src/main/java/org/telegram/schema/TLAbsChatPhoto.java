package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLChatPhoto: chatPhoto#6153276a</li>
 * <li>{@link TLChatPhotoEmpty: chatPhotoEmpty#37c1011c</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsChatPhoto extends TLObject {
    public TLAbsChatPhoto() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLChatPhoto getAsChatPhoto() {
        return null;
    }
}
