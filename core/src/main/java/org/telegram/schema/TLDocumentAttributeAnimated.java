package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLDocumentAttributeAnimated extends TLAbsDocumentAttribute {
    public static final int CLASS_ID = 0x11b58939;

    public TLDocumentAttributeAnimated() {
    }

    @Override
    public String toString() {
        return "documentAttributeAnimated#11b58939";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
