package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateSavedGifs extends TLAbsUpdate {
    public static final int CLASS_ID = 0x9375341e;

    public TLUpdateSavedGifs() {
    }

    @Override
    public String toString() {
        return "updateSavedGifs#9375341e";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
