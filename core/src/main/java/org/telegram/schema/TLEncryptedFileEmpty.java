package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLEncryptedFileEmpty extends TLAbsEncryptedFile {
    public static final int CLASS_ID = 0xc21f497e;

    public TLEncryptedFileEmpty() {
    }

    @Override
    public String toString() {
        return "encryptedFileEmpty#c21f497e";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    @Override
    public final boolean isEmpty() {
        return true;
    }

    @Override
    public final boolean isNotEmpty() {
        return false;
    }
}
