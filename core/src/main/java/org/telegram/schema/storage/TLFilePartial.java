package org.telegram.schema.storage;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLFilePartial extends TLAbsFileType {
    public static final int CLASS_ID = 0x40bc6f52;

    public TLFilePartial() {
    }

    @Override
    public String toString() {
        return "storage.filePartial#40bc6f52";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
