package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLStickersNotModified extends TLAbsStickers {
    public static final int CLASS_ID = 0xf1749a22;

    public TLStickersNotModified() {
    }

    @Override
    public String toString() {
        return "messages.stickersNotModified#f1749a22";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
