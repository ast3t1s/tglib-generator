package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLNotifyUsers extends TLAbsNotifyPeer {
    public static final int CLASS_ID = 0xb4c83b4c;

    public TLNotifyUsers() {
    }

    @Override
    public String toString() {
        return "notifyUsers#b4c83b4c";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
