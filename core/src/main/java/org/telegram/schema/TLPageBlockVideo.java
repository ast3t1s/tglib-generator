package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLPageBlockVideo extends TLAbsPageBlock {
    public static final int CLASS_ID = 0xd9d71866;

    protected int flags;

    protected boolean autoplay;

    protected boolean loop;

    protected long videoId;

    protected TLAbsRichText caption;

    public TLPageBlockVideo() {
    }

    public TLPageBlockVideo(boolean autoplay, boolean loop, long videoId, TLAbsRichText caption) {
        this.autoplay = autoplay;
        this.loop = loop;
        this.videoId = videoId;
        this.caption = caption;
    }

    private void computeFlags() {
        flags = 0;
        flags = autoplay ? (flags | 1) : (flags & ~1);
        flags = loop ? (flags | 2) : (flags & ~2);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeLong(videoId, stream);
        writeTLObject(caption, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        autoplay = (flags & 1) != 0;
        loop = (flags & 2) != 0;
        videoId = readLong(stream);
        caption = readTLObject(stream, context, TLAbsRichText.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT64;
        size += caption.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "pageBlockVideo#d9d71866";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getAutoplay() {
        return autoplay;
    }

    public void setAutoplay(boolean autoplay) {
        this.autoplay = autoplay;
    }

    public boolean getLoop() {
        return loop;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public long getVideoId() {
        return videoId;
    }

    public void setVideoId(long videoId) {
        this.videoId = videoId;
    }

    public TLAbsRichText getCaption() {
        return caption;
    }

    public void setCaption(TLAbsRichText caption) {
        this.caption = caption;
    }
}
