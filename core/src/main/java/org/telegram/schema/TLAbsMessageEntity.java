package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputMessageEntityMentionName: inputMessageEntityMentionName#208e68c9</li>
 * <li>{@link TLMessageEntityBold: messageEntityBold#bd610bc9</li>
 * <li>{@link TLMessageEntityBotCommand: messageEntityBotCommand#6cef8ac7</li>
 * <li>{@link TLMessageEntityCode: messageEntityCode#28a20571</li>
 * <li>{@link TLMessageEntityEmail: messageEntityEmail#64e475c2</li>
 * <li>{@link TLMessageEntityHashtag: messageEntityHashtag#6f635b0d</li>
 * <li>{@link TLMessageEntityItalic: messageEntityItalic#826f8b60</li>
 * <li>{@link TLMessageEntityMention: messageEntityMention#fa04579d</li>
 * <li>{@link TLMessageEntityMentionName: messageEntityMentionName#352dca58</li>
 * <li>{@link TLMessageEntityPre: messageEntityPre#73924be0</li>
 * <li>{@link TLMessageEntityTextUrl: messageEntityTextUrl#76a6d327</li>
 * <li>{@link TLMessageEntityUnknown: messageEntityUnknown#bb92ba95</li>
 * <li>{@link TLMessageEntityUrl: messageEntityUrl#6ed02538</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsMessageEntity extends TLObject {
    public TLAbsMessageEntity() {
    }
}
