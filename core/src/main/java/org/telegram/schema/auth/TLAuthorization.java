package org.telegram.schema.auth;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLAbsUser;

/**
 * @author Telegram Schema Generator
 */
public class TLAuthorization extends TLObject {
    public static final int CLASS_ID = 0xcd050916;

    protected int flags;

    protected Integer tmpSessions;

    protected TLAbsUser user;

    public TLAuthorization() {
    }

    public TLAuthorization(Integer tmpSessions, TLAbsUser user) {
        this.tmpSessions = tmpSessions;
        this.user = user;
    }

    private void computeFlags() {
        flags = 0;
        flags = tmpSessions != null ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        if ((flags & 1) != 0) {
            if (tmpSessions == null) throwNullFieldException("tmpSessions" , flags);
            writeInt(tmpSessions, stream);
        }
        writeTLObject(user, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        tmpSessions = (flags & 1) != 0 ? readInt(stream) : null;
        user = readTLObject(stream, context, TLAbsUser.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        if ((flags & 1) != 0) {
            if (tmpSessions == null) throwNullFieldException("tmpSessions" , flags);
            size += SIZE_INT32;
        }
        size += user.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "auth.authorization#cd050916";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public Integer getTmpSessions() {
        return tmpSessions;
    }

    public void setTmpSessions(Integer tmpSessions) {
        this.tmpSessions = tmpSessions;
    }

    public TLAbsUser getUser() {
        return user;
    }

    public void setUser(TLAbsUser user) {
        this.user = user;
    }
}
