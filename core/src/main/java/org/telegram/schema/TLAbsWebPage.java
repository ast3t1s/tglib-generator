package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLWebPage: webPage#5f07b4bc</li>
 * <li>{@link TLWebPageEmpty: webPageEmpty#eb1477e8</li>
 * <li>{@link TLWebPageNotModified: webPageNotModified#85849473</li>
 * <li>{@link TLWebPagePending: webPagePending#c586da1c</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsWebPage extends TLObject {
    public TLAbsWebPage() {
    }
}
