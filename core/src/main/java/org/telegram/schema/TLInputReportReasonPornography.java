package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputReportReasonPornography extends TLAbsReportReason {
    public static final int CLASS_ID = 0x2e59d922;

    public TLInputReportReasonPornography() {
    }

    @Override
    public String toString() {
        return "inputReportReasonPornography#2e59d922";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
