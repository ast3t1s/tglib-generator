package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputMessagesFilterDocument extends TLAbsMessagesFilter {
    public static final int CLASS_ID = 0x9eddf188;

    public TLInputMessagesFilterDocument() {
    }

    @Override
    public String toString() {
        return "inputMessagesFilterDocument#9eddf188";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
