package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLFeaturedStickers: messages.featuredStickers#f89d88e5</li>
 * <li>{@link TLFeaturedStickersNotModified: messages.featuredStickersNotModified#4ede3cf</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsFeaturedStickers extends TLObject {
    public TLAbsFeaturedStickers() {
    }
}
