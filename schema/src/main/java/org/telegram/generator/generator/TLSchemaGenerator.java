package org.telegram.generator.generator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.telegram.generator.model.TLDef;
import org.telegram.generator.parser.SchemaParser;

import java.io.File;
import java.io.IOException;

public class TLSchemaGenerator {

    private final SchemaParser parser = new SchemaParser();
    private final Generator generator = new Generator();

    private final String schemaPath;
    private final String outputPath;

    private TLSchemaGenerator(Builder builder) {
        this.schemaPath = builder.schemaPath;
        this.outputPath = builder.outputPath;
        if (builder.printStatistic) {
            generator.setPrintStatistic(true);
        }
    }

    public void start() {
        if (schemaPath == null || schemaPath.isEmpty()) {
            throw new IllegalArgumentException("Schema path must not be empty.");
        }
        if (outputPath == null || outputPath.isEmpty()) {
            throw new IllegalArgumentException("Output path must not be empty.");
        }
        System.out.println("Parse JSON schema....");
        TLDef tlDefinition;
        try {
            tlDefinition = getDefinitionFromSchema();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Error schema parse...");
        }
        System.out.println("Generating java classes....");
        try {
            generator.generateSchema(tlDefinition);
        } catch (IOException e) {
            throw new RuntimeException("Error schema generating...");
        }
    }

    private TLDef getDefinitionFromSchema() throws IOException {
        JsonNode tlSchemaNode = new ObjectMapper().readValue(new File(schemaPath), JsonNode.class);
        return parser.buildFromJson(tlSchemaNode);
    }

    public static class Builder {

        private boolean printStatistic;
        private String schemaPath;
        private String outputPath;

        public Builder setSchemaPath(String schemaPath) {
            this.schemaPath = schemaPath;
            return this;
        }

        public Builder setOutPutPath(String outputPath) {
            this.outputPath = outputPath;
            return this;
        }

        public Builder setPrintStatistic(boolean printStatistic) {
            this.printStatistic = printStatistic;
            return this;
        }

        public TLSchemaGenerator build() {
            return new TLSchemaGenerator(this);
        }
    }

}
