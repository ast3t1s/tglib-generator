package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLInputGameShortName extends TLAbsInputGame {
    public static final int CLASS_ID = 0xc331e80a;

    protected TLAbsInputUser botId;

    protected String shortName;

    public TLInputGameShortName() {
    }

    public TLInputGameShortName(TLAbsInputUser botId, String shortName) {
        this.botId = botId;
        this.shortName = shortName;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(botId, stream);
        writeString(shortName, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        botId = readTLObject(stream, context, TLAbsInputUser.class, -1);
        shortName = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += botId.computeSerializedSize();
        size += computeTLStringSerializedSize(shortName);
        return size;
    }

    @Override
    public String toString() {
        return "inputGameShortName#c331e80a";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsInputUser getBotId() {
        return botId;
    }

    public void setBotId(TLAbsInputUser botId) {
        this.botId = botId;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
