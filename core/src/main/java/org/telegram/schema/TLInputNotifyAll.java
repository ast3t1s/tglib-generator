package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputNotifyAll extends TLAbsInputNotifyPeer {
    public static final int CLASS_ID = 0xa429b886;

    public TLInputNotifyAll() {
    }

    @Override
    public String toString() {
        return "inputNotifyAll#a429b886";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
