package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateStickerSets extends TLAbsUpdate {
    public static final int CLASS_ID = 0x43ae3dec;

    public TLUpdateStickerSets() {
    }

    @Override
    public String toString() {
        return "updateStickerSets#43ae3dec";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
