package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputUserSelf extends TLAbsInputUser {
    public static final int CLASS_ID = 0xf7c1b13f;

    public TLInputUserSelf() {
    }

    @Override
    public String toString() {
        return "inputUserSelf#f7c1b13f";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
