package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLDocumentAttributeVideo extends TLAbsDocumentAttribute {
    public static final int CLASS_ID = 0xef02ce6;

    protected int flags;

    protected boolean roundMessage;

    protected int duration;

    protected int w;

    protected int h;

    public TLDocumentAttributeVideo() {
    }

    public TLDocumentAttributeVideo(boolean roundMessage, int duration, int w, int h) {
        this.roundMessage = roundMessage;
        this.duration = duration;
        this.w = w;
        this.h = h;
    }

    private void computeFlags() {
        flags = 0;
        flags = roundMessage ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeInt(duration, stream);
        writeInt(w, stream);
        writeInt(h, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        roundMessage = (flags & 1) != 0;
        duration = readInt(stream);
        w = readInt(stream);
        h = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT32;
        size += SIZE_INT32;
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "documentAttributeVideo#ef02ce6";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getRoundMessage() {
        return roundMessage;
    }

    public void setRoundMessage(boolean roundMessage) {
        this.roundMessage = roundMessage;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }
}
