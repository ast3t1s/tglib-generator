package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputMessagesFilterPhotoVideoDocuments extends TLAbsMessagesFilter {
    public static final int CLASS_ID = 0xd95e73bb;

    public TLInputMessagesFilterPhotoVideoDocuments() {
    }

    @Override
    public String toString() {
        return "inputMessagesFilterPhotoVideoDocuments#d95e73bb";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
