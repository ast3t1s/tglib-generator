package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLGeoPoint extends TLAbsGeoPoint {
    public static final int CLASS_ID = 0x2049d70c;

    protected double _long;

    protected double lat;

    public TLGeoPoint() {
    }

    public TLGeoPoint(double _long, double lat) {
        this._long = _long;
        this.lat = lat;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeDouble(_long, stream);
        writeDouble(lat, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        _long = readDouble(stream);
        lat = readDouble(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_DOUBLE;
        size += SIZE_DOUBLE;
        return size;
    }

    @Override
    public String toString() {
        return "geoPoint#2049d70c";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public double getLong() {
        return _long;
    }

    public void setLong(double _long) {
        this._long = _long;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    @Override
    public final boolean isEmpty() {
        return false;
    }

    @Override
    public final boolean isNotEmpty() {
        return true;
    }

    @Override
    public final TLGeoPoint getAsGeoPoint() {
        return this;
    }
}
