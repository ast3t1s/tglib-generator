package org.telegram.schema.storage;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLFileMov extends TLAbsFileType {
    public static final int CLASS_ID = 0x4b09ebbc;

    public TLFileMov() {
    }

    @Override
    public String toString() {
        return "storage.fileMov#4b09ebbc";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
