package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLRecentStickersNotModified extends TLAbsRecentStickers {
    public static final int CLASS_ID = 0xb17f890;

    public TLRecentStickersNotModified() {
    }

    @Override
    public String toString() {
        return "messages.recentStickersNotModified#b17f890";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
