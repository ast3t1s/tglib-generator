package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBool;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLStringVector;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestAuthSendInvites extends TLMethod<TLBool> {
    public static final int CLASS_ID = 0x771c1d97;

    protected TLStringVector phoneNumbers;

    protected String message;

    public TLRequestAuthSendInvites() {
    }

    public TLRequestAuthSendInvites(TLStringVector phoneNumbers, String message) {
        this.phoneNumbers = phoneNumbers;
        this.message = message;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLBool deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLBool)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLBool) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(phoneNumbers, stream);
        writeString(message, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        phoneNumbers = readTLStringVector(stream, context);
        message = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += phoneNumbers.computeSerializedSize();
        size += computeTLStringSerializedSize(message);
        return size;
    }

    @Override
    public String toString() {
        return "auth.sendInvites#771c1d97";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLStringVector getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(TLStringVector phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
