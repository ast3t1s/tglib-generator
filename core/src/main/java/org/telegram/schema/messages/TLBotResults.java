package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsBotInlineResult;
import org.telegram.schema.TLInlineBotSwitchPM;

/**
 * @author Telegram Schema Generator
 */
public class TLBotResults extends TLObject {
    public static final int CLASS_ID = 0xccd3563d;

    protected int flags;

    protected boolean gallery;

    protected long queryId;

    protected String nextOffset;

    protected TLInlineBotSwitchPM switchPm;

    protected TLVector<TLAbsBotInlineResult> results;

    protected int cacheTime;

    public TLBotResults() {
    }

    public TLBotResults(boolean gallery, long queryId, String nextOffset,
            TLInlineBotSwitchPM switchPm, TLVector<TLAbsBotInlineResult> results, int cacheTime) {
        this.gallery = gallery;
        this.queryId = queryId;
        this.nextOffset = nextOffset;
        this.switchPm = switchPm;
        this.results = results;
        this.cacheTime = cacheTime;
    }

    private void computeFlags() {
        flags = 0;
        flags = gallery ? (flags | 1) : (flags & ~1);
        flags = nextOffset != null ? (flags | 2) : (flags & ~2);
        flags = switchPm != null ? (flags | 4) : (flags & ~4);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeLong(queryId, stream);
        if ((flags & 2) != 0) {
            if (nextOffset == null) throwNullFieldException("nextOffset" , flags);
            writeString(nextOffset, stream);
        }
        if ((flags & 4) != 0) {
            if (switchPm == null) throwNullFieldException("switchPm" , flags);
            writeTLObject(switchPm, stream);
        }
        writeTLVector(results, stream);
        writeInt(cacheTime, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        gallery = (flags & 1) != 0;
        queryId = readLong(stream);
        nextOffset = (flags & 2) != 0 ? readTLString(stream) : null;
        switchPm = (flags & 4) != 0 ? readTLObject(stream, context, TLInlineBotSwitchPM.class, TLInlineBotSwitchPM.CLASS_ID) : null;
        results = readTLVector(stream, context);
        cacheTime = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT64;
        if ((flags & 2) != 0) {
            if (nextOffset == null) throwNullFieldException("nextOffset" , flags);
            size += computeTLStringSerializedSize(nextOffset);
        }
        if ((flags & 4) != 0) {
            if (switchPm == null) throwNullFieldException("switchPm" , flags);
            size += switchPm.computeSerializedSize();
        }
        size += results.computeSerializedSize();
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "messages.botResults#ccd3563d";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getGallery() {
        return gallery;
    }

    public void setGallery(boolean gallery) {
        this.gallery = gallery;
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public String getNextOffset() {
        return nextOffset;
    }

    public void setNextOffset(String nextOffset) {
        this.nextOffset = nextOffset;
    }

    public TLInlineBotSwitchPM getSwitchPm() {
        return switchPm;
    }

    public void setSwitchPm(TLInlineBotSwitchPM switchPm) {
        this.switchPm = switchPm;
    }

    public TLVector<TLAbsBotInlineResult> getResults() {
        return results;
    }

    public void setResults(TLVector<TLAbsBotInlineResult> results) {
        this.results = results;
    }

    public int getCacheTime() {
        return cacheTime;
    }

    public void setCacheTime(int cacheTime) {
        this.cacheTime = cacheTime;
    }
}
