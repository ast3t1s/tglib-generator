package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLStickers: messages.stickers#8a8ecd32</li>
 * <li>{@link TLStickersNotModified: messages.stickersNotModified#f1749a22</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsStickers extends TLObject {
    public TLAbsStickers() {
    }
}
