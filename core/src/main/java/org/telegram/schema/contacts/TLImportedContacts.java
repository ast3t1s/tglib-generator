package org.telegram.schema.contacts;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLLongVector;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsUser;
import org.telegram.schema.TLImportedContact;

/**
 * @author Telegram Schema Generator
 */
public class TLImportedContacts extends TLObject {
    public static final int CLASS_ID = 0xad524315;

    protected TLVector<TLImportedContact> imported;

    protected TLLongVector retryContacts;

    protected TLVector<TLAbsUser> users;

    public TLImportedContacts() {
    }

    public TLImportedContacts(TLVector<TLImportedContact> imported, TLLongVector retryContacts,
            TLVector<TLAbsUser> users) {
        this.imported = imported;
        this.retryContacts = retryContacts;
        this.users = users;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(imported, stream);
        writeTLVector(retryContacts, stream);
        writeTLVector(users, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        imported = readTLVector(stream, context);
        retryContacts = readTLLongVector(stream, context);
        users = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += imported.computeSerializedSize();
        size += retryContacts.computeSerializedSize();
        size += users.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "contacts.importedContacts#ad524315";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLVector<TLImportedContact> getImported() {
        return imported;
    }

    public void setImported(TLVector<TLImportedContact> imported) {
        this.imported = imported;
    }

    public TLLongVector getRetryContacts() {
        return retryContacts;
    }

    public void setRetryContacts(TLLongVector retryContacts) {
        this.retryContacts = retryContacts;
    }

    public TLVector<TLAbsUser> getUsers() {
        return users;
    }

    public void setUsers(TLVector<TLAbsUser> users) {
        this.users = users;
    }
}
