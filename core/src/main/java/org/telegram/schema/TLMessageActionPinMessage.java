package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLMessageActionPinMessage extends TLAbsMessageAction {
    public static final int CLASS_ID = 0x94bd38ed;

    public TLMessageActionPinMessage() {
    }

    @Override
    public String toString() {
        return "messageActionPinMessage#94bd38ed";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
