package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLPrivacyKeyStatusTimestamp extends TLAbsPrivacyKey {
    public static final int CLASS_ID = 0xbc2eab30;

    public TLPrivacyKeyStatusTimestamp() {
    }

    @Override
    public String toString() {
        return "privacyKeyStatusTimestamp#bc2eab30";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
