package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputStickerSetEmpty: inputStickerSetEmpty#ffb62b95</li>
 * <li>{@link TLInputStickerSetID: inputStickerSetID#9de7a269</li>
 * <li>{@link TLInputStickerSetShortName: inputStickerSetShortName#861cc8a0</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputStickerSet extends TLObject {
    public TLAbsInputStickerSet() {
    }
}
