package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLRecentStickers: messages.recentStickers#5ce20970</li>
 * <li>{@link TLRecentStickersNotModified: messages.recentStickersNotModified#b17f890</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsRecentStickers extends TLObject {
    public TLAbsRecentStickers() {
    }
}
