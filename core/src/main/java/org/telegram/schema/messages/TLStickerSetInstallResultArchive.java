package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsStickerSetCovered;

/**
 * @author Telegram Schema Generator
 */
public class TLStickerSetInstallResultArchive extends TLAbsStickerSetInstallResult {
    public static final int CLASS_ID = 0x35e410a8;

    protected TLVector<TLAbsStickerSetCovered> sets;

    public TLStickerSetInstallResultArchive() {
    }

    public TLStickerSetInstallResultArchive(TLVector<TLAbsStickerSetCovered> sets) {
        this.sets = sets;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(sets, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        sets = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += sets.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messages.stickerSetInstallResultArchive#35e410a8";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLVector<TLAbsStickerSetCovered> getSets() {
        return sets;
    }

    public void setSets(TLVector<TLAbsStickerSetCovered> sets) {
        this.sets = sets;
    }
}
