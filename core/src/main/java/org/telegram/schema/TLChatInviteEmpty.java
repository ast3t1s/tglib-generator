package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLChatInviteEmpty extends TLAbsExportedChatInvite {
    public static final int CLASS_ID = 0x69df3769;

    public TLChatInviteEmpty() {
    }

    @Override
    public String toString() {
        return "chatInviteEmpty#69df3769";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    @Override
    public final boolean isEmpty() {
        return true;
    }

    @Override
    public final boolean isNotEmpty() {
        return false;
    }
}
