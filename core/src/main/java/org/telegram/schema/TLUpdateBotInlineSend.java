package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateBotInlineSend extends TLAbsUpdate {
    public static final int CLASS_ID = 0xe48f964;

    protected int flags;

    protected int userId;

    protected String query;

    protected TLAbsGeoPoint geo;

    protected String id;

    protected TLInputBotInlineMessageID msgId;

    public TLUpdateBotInlineSend() {
    }

    public TLUpdateBotInlineSend(int userId, String query, TLAbsGeoPoint geo, String id,
            TLInputBotInlineMessageID msgId) {
        this.userId = userId;
        this.query = query;
        this.geo = geo;
        this.id = id;
        this.msgId = msgId;
    }

    private void computeFlags() {
        flags = 0;
        flags = geo != null ? (flags | 1) : (flags & ~1);
        flags = msgId != null ? (flags | 2) : (flags & ~2);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeInt(userId, stream);
        writeString(query, stream);
        if ((flags & 1) != 0) {
            if (geo == null) throwNullFieldException("geo" , flags);
            writeTLObject(geo, stream);
        }
        writeString(id, stream);
        if ((flags & 2) != 0) {
            if (msgId == null) throwNullFieldException("msgId" , flags);
            writeTLObject(msgId, stream);
        }
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        userId = readInt(stream);
        query = readTLString(stream);
        geo = (flags & 1) != 0 ? readTLObject(stream, context, TLAbsGeoPoint.class, -1) : null;
        id = readTLString(stream);
        msgId = (flags & 2) != 0 ? readTLObject(stream, context, TLInputBotInlineMessageID.class, TLInputBotInlineMessageID.CLASS_ID) : null;
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT32;
        size += computeTLStringSerializedSize(query);
        if ((flags & 1) != 0) {
            if (geo == null) throwNullFieldException("geo" , flags);
            size += geo.computeSerializedSize();
        }
        size += computeTLStringSerializedSize(id);
        if ((flags & 2) != 0) {
            if (msgId == null) throwNullFieldException("msgId" , flags);
            size += msgId.computeSerializedSize();
        }
        return size;
    }

    @Override
    public String toString() {
        return "updateBotInlineSend#e48f964";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public TLAbsGeoPoint getGeo() {
        return geo;
    }

    public void setGeo(TLAbsGeoPoint geo) {
        this.geo = geo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TLInputBotInlineMessageID getMsgId() {
        return msgId;
    }

    public void setMsgId(TLInputBotInlineMessageID msgId) {
        this.msgId = msgId;
    }
}
