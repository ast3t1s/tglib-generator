package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsStickerSetCovered;

/**
 * @author Telegram Schema Generator
 */
public class TLArchivedStickers extends TLObject {
    public static final int CLASS_ID = 0x4fcba9c8;

    protected int count;

    protected TLVector<TLAbsStickerSetCovered> sets;

    public TLArchivedStickers() {
    }

    public TLArchivedStickers(int count, TLVector<TLAbsStickerSetCovered> sets) {
        this.count = count;
        this.sets = sets;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(count, stream);
        writeTLVector(sets, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        count = readInt(stream);
        sets = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += sets.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messages.archivedStickers#4fcba9c8";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public TLVector<TLAbsStickerSetCovered> getSets() {
        return sets;
    }

    public void setSets(TLVector<TLAbsStickerSetCovered> sets) {
        this.sets = sets;
    }
}
