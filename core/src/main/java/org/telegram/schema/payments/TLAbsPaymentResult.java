package org.telegram.schema.payments;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLPaymentResult: payments.paymentResult#4e5f810d</li>
 * <li>{@link TLPaymentVerficationNeeded: payments.paymentVerficationNeeded#6b56b921</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsPaymentResult extends TLObject {
    public TLAbsPaymentResult() {
    }
}
