package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLInputEncryptedFileUploaded extends TLAbsInputEncryptedFile {
    public static final int CLASS_ID = 0x64bd0306;

    protected long id;

    protected int parts;

    protected String md5Checksum;

    protected int keyFingerprint;

    public TLInputEncryptedFileUploaded() {
    }

    public TLInputEncryptedFileUploaded(long id, int parts, String md5Checksum,
            int keyFingerprint) {
        this.id = id;
        this.parts = parts;
        this.md5Checksum = md5Checksum;
        this.keyFingerprint = keyFingerprint;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeLong(id, stream);
        writeInt(parts, stream);
        writeString(md5Checksum, stream);
        writeInt(keyFingerprint, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        id = readLong(stream);
        parts = readInt(stream);
        md5Checksum = readTLString(stream);
        keyFingerprint = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT64;
        size += SIZE_INT32;
        size += computeTLStringSerializedSize(md5Checksum);
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "inputEncryptedFileUploaded#64bd0306";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getParts() {
        return parts;
    }

    public void setParts(int parts) {
        this.parts = parts;
    }

    public String getMd5Checksum() {
        return md5Checksum;
    }

    public void setMd5Checksum(String md5Checksum) {
        this.md5Checksum = md5Checksum;
    }

    public int getKeyFingerprint() {
        return keyFingerprint;
    }

    public void setKeyFingerprint(int keyFingerprint) {
        this.keyFingerprint = keyFingerprint;
    }
}
