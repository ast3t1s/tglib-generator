package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputPeerSelf extends TLAbsInputPeer {
    public static final int CLASS_ID = 0x7da07ec9;

    public TLInputPeerSelf() {
    }

    @Override
    public String toString() {
        return "inputPeerSelf#7da07ec9";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
