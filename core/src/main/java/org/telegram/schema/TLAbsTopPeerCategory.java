package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLTopPeerCategoryBotsInline: topPeerCategoryBotsInline#148677e2</li>
 * <li>{@link TLTopPeerCategoryBotsPM: topPeerCategoryBotsPM#ab661b5b</li>
 * <li>{@link TLTopPeerCategoryChannels: topPeerCategoryChannels#161d9628</li>
 * <li>{@link TLTopPeerCategoryCorrespondents: topPeerCategoryCorrespondents#637b7ed</li>
 * <li>{@link TLTopPeerCategoryGroups: topPeerCategoryGroups#bd17a14a</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsTopPeerCategory extends TLObject {
    public TLAbsTopPeerCategory() {
    }
}
