package org.telegram.generator.model;

import java.util.*;

public class TLDefinition implements TLDef {

    private final Map<String, TLDefTypeModel.TLType> types = new HashMap<>();
    private final List<TLConstructor> constructors = new ArrayList<>();
    private final List<TLMethod> methods = new ArrayList<>();

    public TLDefinition() {}

    public TLDefinition(Map<String, TLDefTypeModel.TLType> types, List<TLConstructor> constructors, List<TLMethod> methods) {
        this.types.putAll(types);
        this.constructors.addAll(constructors);
        this.methods.addAll(methods);
    }

    public void addType(String typeName, TLDefTypeModel.TLType type) {
        types.putIfAbsent(typeName, type);
    }

    public void addConstructor(TLConstructor constructor) {
        constructors.add(constructor);
    }

    public void addMethod(TLMethod method) {
        methods.add(method);
    }

    public void setTypes(Map<String, TLDefTypeModel.TLType> types) {
        this.types.putAll(types);
    }

    public void setConstructors(List<TLConstructor> constructors) {
        this.constructors.addAll(constructors);
    }

    public void setMethods(List<TLMethod> methods) {
        this.methods.addAll(methods);
    }

    @Override
    public Map<String, TLDefTypeModel.TLType> getTypes() {
        return types;
    }

    @Override
    public List<TLConstructor> getConstructors() {
        return constructors;
    }

    @Override
    public List<TLMethod> getMethods() {
        return methods;
    }
}
