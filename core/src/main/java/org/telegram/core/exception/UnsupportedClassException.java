package org.telegram.core.exception;

public class UnsupportedClassException extends DeserializationException {
    public UnsupportedClassException(int classID) {
        super("Unsupported class: #" + Integer.toHexString(classID));
    }
} 
