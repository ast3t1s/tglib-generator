package org.telegram.schema.storage;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLFilePng extends TLAbsFileType {
    public static final int CLASS_ID = 0xa4f63c0;

    public TLFilePng() {
    }

    @Override
    public String toString() {
        return "storage.filePng#a4f63c0";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
