package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputMessagesFilterPhotos extends TLAbsMessagesFilter {
    public static final int CLASS_ID = 0x9609a51c;

    public TLInputMessagesFilterPhotos() {
    }

    @Override
    public String toString() {
        return "inputMessagesFilterPhotos#9609a51c";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
