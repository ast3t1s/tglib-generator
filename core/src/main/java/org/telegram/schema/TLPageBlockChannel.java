package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLPageBlockChannel extends TLAbsPageBlock {
    public static final int CLASS_ID = 0xef1751b5;

    protected TLAbsChat channel;

    public TLPageBlockChannel() {
    }

    public TLPageBlockChannel(TLAbsChat channel) {
        this.channel = channel;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(channel, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        channel = readTLObject(stream, context, TLAbsChat.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += channel.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "pageBlockChannel#ef1751b5";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsChat getChannel() {
        return channel;
    }

    public void setChannel(TLAbsChat channel) {
        this.channel = channel;
    }
}
