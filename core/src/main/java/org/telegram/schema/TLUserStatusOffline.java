package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLUserStatusOffline extends TLAbsUserStatus {
    public static final int CLASS_ID = 0x8c703f;

    protected int wasOnline;

    public TLUserStatusOffline() {
    }

    public TLUserStatusOffline(int wasOnline) {
        this.wasOnline = wasOnline;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(wasOnline, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        wasOnline = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "userStatusOffline#8c703f";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getWasOnline() {
        return wasOnline;
    }

    public void setWasOnline(int wasOnline) {
        this.wasOnline = wasOnline;
    }
}
