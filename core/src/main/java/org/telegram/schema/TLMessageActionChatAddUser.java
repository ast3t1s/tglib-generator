package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLIntVector;

/**
 * @author Telegram Schema Generator
 */
public class TLMessageActionChatAddUser extends TLAbsMessageAction {
    public static final int CLASS_ID = 0x488a7337;

    protected TLIntVector users;

    public TLMessageActionChatAddUser() {
    }

    public TLMessageActionChatAddUser(TLIntVector users) {
        this.users = users;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(users, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        users = readTLIntVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += users.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messageActionChatAddUser#488a7337";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLIntVector getUsers() {
        return users;
    }

    public void setUsers(TLIntVector users) {
        this.users = users;
    }
}
