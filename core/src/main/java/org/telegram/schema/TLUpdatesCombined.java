package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLVector;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdatesCombined extends TLAbsUpdates {
    public static final int CLASS_ID = 0x725b04c3;

    protected TLVector<TLAbsUpdate> updates;

    protected TLVector<TLAbsUser> users;

    protected TLVector<TLAbsChat> chats;

    protected int date;

    protected int seqStart;

    protected int seq;

    public TLUpdatesCombined() {
    }

    public TLUpdatesCombined(TLVector<TLAbsUpdate> updates, TLVector<TLAbsUser> users,
            TLVector<TLAbsChat> chats, int date, int seqStart, int seq) {
        this.updates = updates;
        this.users = users;
        this.chats = chats;
        this.date = date;
        this.seqStart = seqStart;
        this.seq = seq;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(updates, stream);
        writeTLVector(users, stream);
        writeTLVector(chats, stream);
        writeInt(date, stream);
        writeInt(seqStart, stream);
        writeInt(seq, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        updates = readTLVector(stream, context);
        users = readTLVector(stream, context);
        chats = readTLVector(stream, context);
        date = readInt(stream);
        seqStart = readInt(stream);
        seq = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += updates.computeSerializedSize();
        size += users.computeSerializedSize();
        size += chats.computeSerializedSize();
        size += SIZE_INT32;
        size += SIZE_INT32;
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "updatesCombined#725b04c3";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLVector<TLAbsUpdate> getUpdates() {
        return updates;
    }

    public void setUpdates(TLVector<TLAbsUpdate> updates) {
        this.updates = updates;
    }

    public TLVector<TLAbsUser> getUsers() {
        return users;
    }

    public void setUsers(TLVector<TLAbsUser> users) {
        this.users = users;
    }

    public TLVector<TLAbsChat> getChats() {
        return chats;
    }

    public void setChats(TLVector<TLAbsChat> chats) {
        this.chats = chats;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getSeqStart() {
        return seqStart;
    }

    public void setSeqStart(int seqStart) {
        this.seqStart = seqStart;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }
}
