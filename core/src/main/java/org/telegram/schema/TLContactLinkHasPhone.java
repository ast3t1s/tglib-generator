package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLContactLinkHasPhone extends TLAbsContactLink {
    public static final int CLASS_ID = 0x268f3f59;

    public TLContactLinkHasPhone() {
    }

    @Override
    public String toString() {
        return "contactLinkHasPhone#268f3f59";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
