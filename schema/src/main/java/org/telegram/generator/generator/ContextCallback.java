package org.telegram.generator.generator;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeSpec;
import org.telegram.generator.model.TLParameter;

import java.util.List;

public interface ContextCallback {

    public void onGeneratedClass(TypeSpec.Builder clazz, ClassName clazzTypeName, String name, Integer id, List<TLParameter> parameters);

}
