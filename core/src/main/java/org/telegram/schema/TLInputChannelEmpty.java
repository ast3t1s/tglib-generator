package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputChannelEmpty extends TLAbsInputChannel {
    public static final int CLASS_ID = 0xee8c1e86;

    public TLInputChannelEmpty() {
    }

    @Override
    public String toString() {
        return "inputChannelEmpty#ee8c1e86";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    @Override
    public final boolean isEmpty() {
        return true;
    }

    @Override
    public final boolean isNotEmpty() {
        return false;
    }
}
