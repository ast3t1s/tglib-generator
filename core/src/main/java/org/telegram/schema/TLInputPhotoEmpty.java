package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputPhotoEmpty extends TLAbsInputPhoto {
    public static final int CLASS_ID = 0x1cd7bf0d;

    public TLInputPhotoEmpty() {
    }

    @Override
    public String toString() {
        return "inputPhotoEmpty#1cd7bf0d";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    @Override
    public final boolean isEmpty() {
        return true;
    }

    @Override
    public final boolean isNotEmpty() {
        return false;
    }
}
