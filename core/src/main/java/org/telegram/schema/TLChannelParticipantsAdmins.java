package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLChannelParticipantsAdmins extends TLAbsChannelParticipantsFilter {
    public static final int CLASS_ID = 0xb4608969;

    public TLChannelParticipantsAdmins() {
    }

    @Override
    public String toString() {
        return "channelParticipantsAdmins#b4608969";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
