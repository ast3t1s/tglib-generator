package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLPageBlockAuthorDate extends TLAbsPageBlock {
    public static final int CLASS_ID = 0xbaafe5e0;

    protected TLAbsRichText author;

    protected int publishedDate;

    public TLPageBlockAuthorDate() {
    }

    public TLPageBlockAuthorDate(TLAbsRichText author, int publishedDate) {
        this.author = author;
        this.publishedDate = publishedDate;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(author, stream);
        writeInt(publishedDate, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        author = readTLObject(stream, context, TLAbsRichText.class, -1);
        publishedDate = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += author.computeSerializedSize();
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "pageBlockAuthorDate#baafe5e0";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsRichText getAuthor() {
        return author;
    }

    public void setAuthor(TLAbsRichText author) {
        this.author = author;
    }

    public int getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(int publishedDate) {
        this.publishedDate = publishedDate;
    }
}
