package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLMessageActionChatDeletePhoto extends TLAbsMessageAction {
    public static final int CLASS_ID = 0x95e3fbef;

    public TLMessageActionChatDeletePhoto() {
    }

    @Override
    public String toString() {
        return "messageActionChatDeletePhoto#95e3fbef";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
