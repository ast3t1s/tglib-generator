package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLGeoPointEmpty extends TLAbsGeoPoint {
    public static final int CLASS_ID = 0x1117dd5f;

    public TLGeoPointEmpty() {
    }

    @Override
    public String toString() {
        return "geoPointEmpty#1117dd5f";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    @Override
    public final boolean isEmpty() {
        return true;
    }

    @Override
    public final boolean isNotEmpty() {
        return false;
    }
}
