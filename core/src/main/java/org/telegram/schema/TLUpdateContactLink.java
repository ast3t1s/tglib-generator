package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateContactLink extends TLAbsUpdate {
    public static final int CLASS_ID = 0x9d2e67c5;

    protected int userId;

    protected TLAbsContactLink myLink;

    protected TLAbsContactLink foreignLink;

    public TLUpdateContactLink() {
    }

    public TLUpdateContactLink(int userId, TLAbsContactLink myLink, TLAbsContactLink foreignLink) {
        this.userId = userId;
        this.myLink = myLink;
        this.foreignLink = foreignLink;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(userId, stream);
        writeTLObject(myLink, stream);
        writeTLObject(foreignLink, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        userId = readInt(stream);
        myLink = readTLObject(stream, context, TLAbsContactLink.class, -1);
        foreignLink = readTLObject(stream, context, TLAbsContactLink.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += myLink.computeSerializedSize();
        size += foreignLink.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "updateContactLink#9d2e67c5";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public TLAbsContactLink getMyLink() {
        return myLink;
    }

    public void setMyLink(TLAbsContactLink myLink) {
        this.myLink = myLink;
    }

    public TLAbsContactLink getForeignLink() {
        return foreignLink;
    }

    public void setForeignLink(TLAbsContactLink foreignLink) {
        this.foreignLink = foreignLink;
    }
}
