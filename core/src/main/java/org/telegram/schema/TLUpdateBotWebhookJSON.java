package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLUpdateBotWebhookJSON extends TLAbsUpdate {
    public static final int CLASS_ID = 0x8317c0c3;

    protected TLDataJSON data;

    public TLUpdateBotWebhookJSON() {
    }

    public TLUpdateBotWebhookJSON(TLDataJSON data) {
        this.data = data;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(data, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        data = readTLObject(stream, context, TLDataJSON.class, TLDataJSON.CLASS_ID);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += data.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "updateBotWebhookJSON#8317c0c3";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLDataJSON getData() {
        return data;
    }

    public void setData(TLDataJSON data) {
        this.data = data;
    }
}
