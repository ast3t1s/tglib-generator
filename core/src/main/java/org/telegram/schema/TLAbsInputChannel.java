package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputChannel: inputChannel#afeb712e</li>
 * <li>{@link TLInputChannelEmpty: inputChannelEmpty#ee8c1e86</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputChannel extends TLObject {
    public TLAbsInputChannel() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLInputChannel getAsInputChannel() {
        return null;
    }

    public static TLInputChannelEmpty newEmpty() {
        return new TLInputChannelEmpty();
    }

    public static TLInputChannel newNotEmpty() {
        return new TLInputChannel();
    }
}
