package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestInvokeWithoutUpdates<T extends TLObject> extends TLMethod<T> {
    public static final int CLASS_ID = 0xbf9459b7;

    protected TLMethod<T> query;

    public TLRequestInvokeWithoutUpdates() {
    }

    public TLRequestInvokeWithoutUpdates(TLMethod<T> query) {
        this.query = query;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public T deserializeResponse(InputStream stream, TLContext context) throws IOException {
        return query.deserializeResponse(stream, context);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLMethod(query, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        query = readTLMethod(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += query.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "invokeWithoutUpdates#bf9459b7";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLMethod<T> getQuery() {
        return query;
    }

    public void setQuery(TLMethod<T> query) {
        this.query = query;
    }
}
