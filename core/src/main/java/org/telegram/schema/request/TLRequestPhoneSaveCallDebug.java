package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBool;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLDataJSON;
import org.telegram.schema.TLInputPhoneCall;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestPhoneSaveCallDebug extends TLMethod<TLBool> {
    public static final int CLASS_ID = 0x277add7e;

    protected TLInputPhoneCall peer;

    protected TLDataJSON debug;

    public TLRequestPhoneSaveCallDebug() {
    }

    public TLRequestPhoneSaveCallDebug(TLInputPhoneCall peer, TLDataJSON debug) {
        this.peer = peer;
        this.debug = debug;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLBool deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLBool)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLBool) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(peer, stream);
        writeTLObject(debug, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        peer = readTLObject(stream, context, TLInputPhoneCall.class, TLInputPhoneCall.CLASS_ID);
        debug = readTLObject(stream, context, TLDataJSON.class, TLDataJSON.CLASS_ID);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += peer.computeSerializedSize();
        size += debug.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "phone.saveCallDebug#277add7e";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLInputPhoneCall getPeer() {
        return peer;
    }

    public void setPeer(TLInputPhoneCall peer) {
        this.peer = peer;
    }

    public TLDataJSON getDebug() {
        return debug;
    }

    public void setDebug(TLDataJSON debug) {
        this.debug = debug;
    }
}
