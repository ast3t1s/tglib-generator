package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputBotInlineMessageGame: inputBotInlineMessageGame#4b425864</li>
 * <li>{@link TLInputBotInlineMessageMediaAuto: inputBotInlineMessageMediaAuto#292fed13</li>
 * <li>{@link TLInputBotInlineMessageMediaContact: inputBotInlineMessageMediaContact#2daf01a7</li>
 * <li>{@link TLInputBotInlineMessageMediaGeo: inputBotInlineMessageMediaGeo#f4a59de1</li>
 * <li>{@link TLInputBotInlineMessageMediaVenue: inputBotInlineMessageMediaVenue#aaafadc8</li>
 * <li>{@link TLInputBotInlineMessageText: inputBotInlineMessageText#3dcd7a87</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputBotInlineMessage extends TLObject {
    public TLAbsInputBotInlineMessage() {
    }
}
