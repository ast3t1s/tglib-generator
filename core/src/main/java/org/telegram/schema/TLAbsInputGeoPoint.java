package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputGeoPoint: inputGeoPoint#f3b7acc9</li>
 * <li>{@link TLInputGeoPointEmpty: inputGeoPointEmpty#e4c123d6</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputGeoPoint extends TLObject {
    public TLAbsInputGeoPoint() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLInputGeoPoint getAsInputGeoPoint() {
        return null;
    }

    public static TLInputGeoPointEmpty newEmpty() {
        return new TLInputGeoPointEmpty();
    }

    public static TLInputGeoPoint newNotEmpty() {
        return new TLInputGeoPoint();
    }
}
