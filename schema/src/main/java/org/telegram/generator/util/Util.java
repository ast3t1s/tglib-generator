package org.telegram.generator.util;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Util {

    public static String uFirstLetter(String str) {
        if (Character.isUpperCase(str.charAt(0))) return str;
        else return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    public static String lFirstLetter(String str) {
        if (Character.isLowerCase(str.charAt(0))) return str;
        else return str.substring(0, 1).toLowerCase() + str.substring(1);
    }

    public static String lCamelCase(String value) {
        List<String> words = divideByWords(value);
        String first = words.remove(0);
        return lFirstLetter(first) + words.stream().map(Util::uFirstLetter).collect(Collectors.joining());
    }

    public static String javaEscape(String value) {
        if (value.equals("private") || value.equals("public") || value.equals("long") || value.equals("assert") || value.equals("final")) {
            return "_" + value;
        } else {
            return value;
        }
    }

    public static String last(String[] arr) {
        return arr[arr.length - 1];
    }

    public static String uCamelCase(String value) {
        List<String> words = divideByWords(value);
        return words.stream().map(Util::uFirstLetter).collect(Collectors.joining());
    }

    public static String hex(int number) {
        return Integer.toHexString(number);
    }

    private static List<String> divideByWords(String value) {
        char[] dividers = new char[] {'.', '_'};

        String workingValue = value;
        List<String> words = new LinkedList<>();

        outer: while (!workingValue.equals("")) {
            for (int i = 0; i < workingValue.length() - 1; i++) {
                for (char div : dividers) {
                    try {
                        if (workingValue.charAt(i) == div) {
                            if (i > 0) {
                                words.add(workingValue.substring(0, i));
                                workingValue = workingValue.substring(i + 1);
                            } else {
                                workingValue = workingValue.substring(1);
                            }

                            continue outer;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (i > 0) {
                    if (Character.isLowerCase(workingValue.charAt(i - 1)) && Character.isUpperCase(workingValue.charAt(i))) {
                        words.add(workingValue.substring(0, i));
                        workingValue = workingValue.substring(i);

                        continue outer;
                    }
                }
            }

            if (!workingValue.equals("")) {
                words.add(workingValue);
                workingValue = "";
            }
        }
        return words;
    }
} 
