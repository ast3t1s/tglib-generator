package org.telegram.generator.statement;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeName;
import org.telegram.generator.model.TLDefTypeModel.*;
import org.telegram.generator.Statement;

import java.util.Arrays;

public class DeserializeStatementStatement implements Statement<TLType, TypeName> {

    @Override
    public String createStatement(TLType type, TypeName fieldType) {
        if (type instanceof TLTypeFunctional) {
            return "readTLMethod(stream, context)";
        } else if (type instanceof TLTypeFlag) {
            return "readInt(stream)";
        } else if (type instanceof TLTypeConditional) {
            return createConditionalStatement(type, fieldType);
        } else if (type instanceof TLTypeGeneric) {
            String name = ((TLTypeRaw) (Arrays.stream(((TLTypeGeneric) type)
                    .getGenerics()).findFirst().get())).getName();
            return createGenericStatement(name);
        } else if (type instanceof TLTypeRaw) {
            String name = ((TLTypeRaw) type).getName();
            return createRawStatement(fieldType, name);
        } else {
            throw new RuntimeException("Unsupported type " + type);
        }
    }

    private String createConditionalStatement(TLType type, TypeName fieldType) {
        String prefix = String.format("(flags & %d) != 0", ((TLTypeConditional) type).pow2Value());
        String suffix = (((TLTypeConditional) type).getRealType() instanceof TLTypeRaw && "Bool".equalsIgnoreCase(((TLTypeRaw) ((TLTypeConditional) type).getRealType()).getName()))
                ? "false" : "null";
        if (((TLTypeConditional) type).getRealType() instanceof TLTypeRaw && Arrays.asList("true", "false").contains(((TLTypeRaw) ((TLTypeConditional) type).getRealType()).getName()))
            return prefix;
        else
            return prefix + " ? " + createStatement(((TLTypeConditional) type).getRealType(), fieldType) + " : " + suffix;
    }

    private String createRawStatement(TypeName fieldType, String name) {
        switch (name) {
            case "int":
                return "readInt(stream)";
            case "long":
                return "readLong(stream)";
            case "double":
                return "readDouble(stream)";
            case "float":
                return "readFloat(stream)";
            case "string":
                return "readTLString(stream)";
            case "bytes":
                return "readTLBytes(stream, context)";
            case "Bool":
                return "readTLBool(stream)";
            default:
                String className = ((ClassName) fieldType).simpleName();
                boolean isAbstract = className.toLowerCase().startsWith("TLAbs".toLowerCase());
                return isAbstract ? "readTLObject(stream, context, $T.class, -1)" : "readTLObject(stream, context, $T.class, $T.CLASS_ID)";
        }
    }

    private String createGenericStatement(String name) {
        switch (name) {
            case "int":
                return "readTLIntVector(stream, context)";
            case "long":
                return "readTLLongVector(stream, context)";
            case "string":
                return "readTLStringVector(stream, context)";
            default:
                return "readTLVector(stream, context)";
        }
    }
}
