package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLMessageActionPaymentSent extends TLAbsMessageAction {
    public static final int CLASS_ID = 0x40699cd0;

    protected String currency;

    protected long totalAmount;

    public TLMessageActionPaymentSent() {
    }

    public TLMessageActionPaymentSent(String currency, long totalAmount) {
        this.currency = currency;
        this.totalAmount = totalAmount;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(currency, stream);
        writeLong(totalAmount, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        currency = readTLString(stream);
        totalAmount = readLong(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(currency);
        size += SIZE_INT64;
        return size;
    }

    @Override
    public String toString() {
        return "messageActionPaymentSent#40699cd0";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(long totalAmount) {
        this.totalAmount = totalAmount;
    }
}
