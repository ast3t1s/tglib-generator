package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLMessageMediaGame extends TLAbsMessageMedia {
    public static final int CLASS_ID = 0xfdb19008;

    protected TLGame game;

    public TLMessageMediaGame() {
    }

    public TLMessageMediaGame(TLGame game) {
        this.game = game;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(game, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        game = readTLObject(stream, context, TLGame.class, TLGame.CLASS_ID);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += game.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messageMediaGame#fdb19008";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLGame getGame() {
        return game;
    }

    public void setGame(TLGame game) {
        this.game = game;
    }
}
