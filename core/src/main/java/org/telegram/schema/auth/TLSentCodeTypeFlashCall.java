package org.telegram.schema.auth;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLSentCodeTypeFlashCall extends TLAbsSentCodeType {
    public static final int CLASS_ID = 0xab03c6d9;

    protected String pattern;

    public TLSentCodeTypeFlashCall() {
    }

    public TLSentCodeTypeFlashCall(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(pattern, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        pattern = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(pattern);
        return size;
    }

    @Override
    public String toString() {
        return "auth.sentCodeTypeFlashCall#ab03c6d9";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
