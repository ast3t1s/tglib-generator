package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLPeerNotifyEventsAll extends TLAbsPeerNotifyEvents {
    public static final int CLASS_ID = 0x6d1ded88;

    public TLPeerNotifyEventsAll() {
    }

    @Override
    public String toString() {
        return "peerNotifyEventsAll#6d1ded88";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    @Override
    public final boolean isEmpty() {
        return false;
    }

    @Override
    public final boolean isNotEmpty() {
        return true;
    }

    @Override
    public final TLPeerNotifyEventsAll getAsPeerNotifyEventsAll() {
        return this;
    }
}
