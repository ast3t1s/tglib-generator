package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLPrivacyValueDisallowAll extends TLAbsPrivacyRule {
    public static final int CLASS_ID = 0x8b73e763;

    public TLPrivacyValueDisallowAll() {
    }

    @Override
    public String toString() {
        return "privacyValueDisallowAll#8b73e763";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
