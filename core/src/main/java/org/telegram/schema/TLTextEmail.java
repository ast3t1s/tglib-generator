package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLTextEmail extends TLAbsRichText {
    public static final int CLASS_ID = 0xde5a0dd6;

    protected TLAbsRichText text;

    protected String email;

    public TLTextEmail() {
    }

    public TLTextEmail(TLAbsRichText text, String email) {
        this.text = text;
        this.email = email;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(text, stream);
        writeString(email, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        text = readTLObject(stream, context, TLAbsRichText.class, -1);
        email = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += text.computeSerializedSize();
        size += computeTLStringSerializedSize(email);
        return size;
    }

    @Override
    public String toString() {
        return "textEmail#de5a0dd6";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsRichText getText() {
        return text;
    }

    public void setText(TLAbsRichText text) {
        this.text = text;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
