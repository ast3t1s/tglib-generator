package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLSentEncryptedFile: messages.sentEncryptedFile#9493ff32</li>
 * <li>{@link TLSentEncryptedMessage: messages.sentEncryptedMessage#560f8935</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsSentEncryptedMessage extends TLObject {
    public TLAbsSentEncryptedMessage() {
    }
}
