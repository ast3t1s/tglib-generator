package org.telegram.schema.payments;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLPaymentRequestedInfo;

/**
 * @author Telegram Schema Generator
 */
public class TLSavedInfo extends TLObject {
    public static final int CLASS_ID = 0xfb8fe43c;

    protected int flags;

    protected boolean hasSavedCredentials;

    protected TLPaymentRequestedInfo savedInfo;

    public TLSavedInfo() {
    }

    public TLSavedInfo(boolean hasSavedCredentials, TLPaymentRequestedInfo savedInfo) {
        this.hasSavedCredentials = hasSavedCredentials;
        this.savedInfo = savedInfo;
    }

    private void computeFlags() {
        flags = 0;
        flags = hasSavedCredentials ? (flags | 2) : (flags & ~2);
        flags = savedInfo != null ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        if ((flags & 1) != 0) {
            if (savedInfo == null) throwNullFieldException("savedInfo" , flags);
            writeTLObject(savedInfo, stream);
        }
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        hasSavedCredentials = (flags & 2) != 0;
        savedInfo = (flags & 1) != 0 ? readTLObject(stream, context, TLPaymentRequestedInfo.class, TLPaymentRequestedInfo.CLASS_ID) : null;
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        if ((flags & 1) != 0) {
            if (savedInfo == null) throwNullFieldException("savedInfo" , flags);
            size += savedInfo.computeSerializedSize();
        }
        return size;
    }

    @Override
    public String toString() {
        return "payments.savedInfo#fb8fe43c";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getHasSavedCredentials() {
        return hasSavedCredentials;
    }

    public void setHasSavedCredentials(boolean hasSavedCredentials) {
        this.hasSavedCredentials = hasSavedCredentials;
    }

    public TLPaymentRequestedInfo getSavedInfo() {
        return savedInfo;
    }

    public void setSavedInfo(TLPaymentRequestedInfo savedInfo) {
        this.savedInfo = savedInfo;
    }
}
