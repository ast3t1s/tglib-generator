package org.telegram.schema.contacts;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsUser;
import org.telegram.schema.TLContact;

/**
 * @author Telegram Schema Generator
 */
public class TLContacts extends TLAbsContacts {
    public static final int CLASS_ID = 0x6f8b8cb2;

    protected TLVector<TLContact> contacts;

    protected TLVector<TLAbsUser> users;

    public TLContacts() {
    }

    public TLContacts(TLVector<TLContact> contacts, TLVector<TLAbsUser> users) {
        this.contacts = contacts;
        this.users = users;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(contacts, stream);
        writeTLVector(users, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        contacts = readTLVector(stream, context);
        users = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += contacts.computeSerializedSize();
        size += users.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "contacts.contacts#6f8b8cb2";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLVector<TLContact> getContacts() {
        return contacts;
    }

    public void setContacts(TLVector<TLContact> contacts) {
        this.contacts = contacts;
    }

    public TLVector<TLAbsUser> getUsers() {
        return users;
    }

    public void setUsers(TLVector<TLAbsUser> users) {
        this.users = users;
    }
}
