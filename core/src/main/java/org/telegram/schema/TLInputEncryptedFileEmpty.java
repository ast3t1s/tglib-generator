package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputEncryptedFileEmpty extends TLAbsInputEncryptedFile {
    public static final int CLASS_ID = 0x1837c364;

    public TLInputEncryptedFileEmpty() {
    }

    @Override
    public String toString() {
        return "inputEncryptedFileEmpty#1837c364";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
