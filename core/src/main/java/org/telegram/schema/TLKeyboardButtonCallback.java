package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBytes;

/**
 * @author Telegram Schema Generator
 */
public class TLKeyboardButtonCallback extends TLAbsKeyboardButton {
    public static final int CLASS_ID = 0x683a5e46;

    protected String text;

    protected TLBytes data;

    public TLKeyboardButtonCallback() {
    }

    public TLKeyboardButtonCallback(String text, TLBytes data) {
        this.text = text;
        this.data = data;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(text, stream);
        writeTLBytes(data, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        text = readTLString(stream);
        data = readTLBytes(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(text);
        size += computeTLBytesSerializedSize(data);
        return size;
    }

    @Override
    public String toString() {
        return "keyboardButtonCallback#683a5e46";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public TLBytes getData() {
        return data;
    }

    public void setData(TLBytes data) {
        this.data = data;
    }
}
