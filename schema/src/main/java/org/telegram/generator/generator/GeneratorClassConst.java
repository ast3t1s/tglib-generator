package org.telegram.generator.generator;

import com.squareup.javapoet.ClassName;

public final class GeneratorClassConst {

    public static final String PACKAGE_TL = "org.telegram";
    public static final String PACKAGE_TL_CORE = PACKAGE_TL + ".core";
    public static final String PACKAGE_TL_API = PACKAGE_TL + ".schema";
    public static final String PACKAGE_TL_API_REQUEST = PACKAGE_TL_API + ".request";
    public static final String PACKAGE_TL_EXCEPTION = PACKAGE_TL_CORE + ".exception";
    public static final String PACKAGE_TL_TYPE = PACKAGE_TL_CORE + ".type";
    public static final String PACKAGE_TL_UTILS = PACKAGE_TL_CORE + ".utils";

    public static final String TELEGRAM_API_INTERFACE = "TelegramApi";
    public static final String TELEGRAM_API_WRAPPER = "TelegramApiWrapper";
    public static final String TL_API_CONTEXT = "TLApiContext";

    public static final ClassName TYPE_TELEGRAM_API = ClassName.get(PACKAGE_TL_API, TELEGRAM_API_INTERFACE);
    public static final ClassName TYPE_TL_API_CONTEXT = ClassName.get(PACKAGE_TL_API, TL_API_CONTEXT);

    public static final ClassName TYPE_STREAM_UTILS = ClassName.get(PACKAGE_TL_UTILS, "StreamUtils");
    public static final ClassName TYPE_TL_CONTEXT = ClassName.get(PACKAGE_TL_CORE, "TLContext");
    public static final ClassName TYPE_TLOBJECT_UTILS = ClassName.get(PACKAGE_TL_UTILS, "TypeUtils");
    public static final ClassName TYPE_RPC_EXCEPTION = ClassName.get(PACKAGE_TL_EXCEPTION, "RpcException");

    public static final ClassName TYPE_TL_BOOL = ClassName.get(PACKAGE_TL_TYPE, "TLBool");
    public static final ClassName TYPE_TL_BYTES = ClassName.get(PACKAGE_TL_TYPE, "TLBytes");
    public static final ClassName TYPE_TL_INT_VECTOR = ClassName.get(PACKAGE_TL_TYPE, "TLIntVector");
    public static final ClassName TYPE_TL_LONG_VECTOR = ClassName.get(PACKAGE_TL_TYPE, "TLLongVector");
    public static final ClassName TYPE_TL_STRING_VECTOR = ClassName.get(PACKAGE_TL_TYPE, "TLStringVector");
    public static final ClassName TYPE_TL_VECTOR = ClassName.get(PACKAGE_TL_TYPE, "TLVector");
    public static final ClassName TYPE_TL_METHOD = ClassName.get(PACKAGE_TL_TYPE, "TLMethod");
    public static final ClassName TYPE_TL_OBJECT = ClassName.get(PACKAGE_TL_TYPE, "TLObject");

    public static final String DEFAULT_AUTHOR = "@author Telegram Schema Generator\n";
} 
