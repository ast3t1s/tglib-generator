package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsInputStickeredMedia;
import org.telegram.schema.TLAbsStickerSetCovered;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestMessagesGetAttachedStickers extends TLMethod<TLVector<TLAbsStickerSetCovered>> {
    public static final int CLASS_ID = 0xcc5b67cc;

    protected TLAbsInputStickeredMedia media;

    public TLRequestMessagesGetAttachedStickers() {
    }

    public TLRequestMessagesGetAttachedStickers(TLAbsInputStickeredMedia media) {
        this.media = media;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLVector<TLAbsStickerSetCovered> deserializeResponse(InputStream stream,
            TLContext context) throws IOException {
        return readTLVector(stream, context);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(media, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        media = readTLObject(stream, context, TLAbsInputStickeredMedia.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += media.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messages.getAttachedStickers#cc5b67cc";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsInputStickeredMedia getMedia() {
        return media;
    }

    public void setMedia(TLAbsInputStickeredMedia media) {
        this.media = media;
    }
}
