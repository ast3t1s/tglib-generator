package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBool;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLShippingOption;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestMessagesSetBotShippingResults extends TLMethod<TLBool> {
    public static final int CLASS_ID = 0xe5f672fa;

    protected int flags;

    protected long queryId;

    protected String error;

    protected TLVector<TLShippingOption> shippingOptions;

    public TLRequestMessagesSetBotShippingResults() {
    }

    public TLRequestMessagesSetBotShippingResults(long queryId, String error,
            TLVector<TLShippingOption> shippingOptions) {
        this.queryId = queryId;
        this.error = error;
        this.shippingOptions = shippingOptions;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLBool deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLBool)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLBool) response;
    }

    private void computeFlags() {
        flags = 0;
        flags = error != null ? (flags | 1) : (flags & ~1);
        flags = shippingOptions != null ? (flags | 2) : (flags & ~2);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeLong(queryId, stream);
        if ((flags & 1) != 0) {
            if (error == null) throwNullFieldException("error" , flags);
            writeString(error, stream);
        }
        if ((flags & 2) != 0) {
            if (shippingOptions == null) throwNullFieldException("shippingOptions" , flags);
            writeTLVector(shippingOptions, stream);
        }
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        queryId = readLong(stream);
        error = (flags & 1) != 0 ? readTLString(stream) : null;
        shippingOptions = (flags & 2) != 0 ? readTLVector(stream, context) : null;
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT64;
        if ((flags & 1) != 0) {
            if (error == null) throwNullFieldException("error" , flags);
            size += computeTLStringSerializedSize(error);
        }
        if ((flags & 2) != 0) {
            if (shippingOptions == null) throwNullFieldException("shippingOptions" , flags);
            size += shippingOptions.computeSerializedSize();
        }
        return size;
    }

    @Override
    public String toString() {
        return "messages.setBotShippingResults#e5f672fa";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public long getQueryId() {
        return queryId;
    }

    public void setQueryId(long queryId) {
        this.queryId = queryId;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public TLVector<TLShippingOption> getShippingOptions() {
        return shippingOptions;
    }

    public void setShippingOptions(TLVector<TLShippingOption> shippingOptions) {
        this.shippingOptions = shippingOptions;
    }
}
