package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputPhoto: inputPhoto#fb95c6c4</li>
 * <li>{@link TLInputPhotoEmpty: inputPhotoEmpty#1cd7bf0d</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputPhoto extends TLObject {
    public TLAbsInputPhoto() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLInputPhoto getAsInputPhoto() {
        return null;
    }

    public static TLInputPhotoEmpty newEmpty() {
        return new TLInputPhotoEmpty();
    }

    public static TLInputPhoto newNotEmpty() {
        return new TLInputPhoto();
    }
}
