package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputNotifyUsers extends TLAbsInputNotifyPeer {
    public static final int CLASS_ID = 0x193b4417;

    public TLInputNotifyUsers() {
    }

    @Override
    public String toString() {
        return "inputNotifyUsers#193b4417";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
