package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsWallPaper;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestAccountGetWallPapers extends TLMethod<TLVector<TLAbsWallPaper>> {
    public static final int CLASS_ID = 0xc04cfac2;

    public TLRequestAccountGetWallPapers() {
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLVector<TLAbsWallPaper> deserializeResponse(InputStream stream, TLContext context)
            throws IOException {
        return readTLVector(stream, context);
    }

    @Override
    public String toString() {
        return "account.getWallPapers#c04cfac2";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
