package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLSendMessageRecordVideoAction extends TLAbsSendMessageAction {
    public static final int CLASS_ID = 0xa187d66f;

    public TLSendMessageRecordVideoAction() {
    }

    @Override
    public String toString() {
        return "sendMessageRecordVideoAction#a187d66f";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
