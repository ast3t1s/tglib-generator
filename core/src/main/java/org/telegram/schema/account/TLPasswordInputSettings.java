package org.telegram.schema.account;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBytes;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLPasswordInputSettings extends TLObject {
    public static final int CLASS_ID = 0x86916deb;

    protected int flags;

    protected TLBytes newSalt;

    protected TLBytes newPasswordHash;

    protected String hint;

    protected String email;

    public TLPasswordInputSettings() {
    }

    public TLPasswordInputSettings(TLBytes newSalt, TLBytes newPasswordHash, String hint,
            String email) {
        this.newSalt = newSalt;
        this.newPasswordHash = newPasswordHash;
        this.hint = hint;
        this.email = email;
    }

    private void computeFlags() {
        flags = 0;
        flags = newSalt != null ? (flags | 1) : (flags & ~1);
        flags = newPasswordHash != null ? (flags | 1) : (flags & ~1);
        flags = hint != null ? (flags | 1) : (flags & ~1);
        flags = email != null ? (flags | 2) : (flags & ~2);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        if ((flags & 1) != 0) {
            if (newSalt == null) throwNullFieldException("newSalt" , flags);
            writeTLBytes(newSalt, stream);
        }
        if ((flags & 1) != 0) {
            if (newPasswordHash == null) throwNullFieldException("newPasswordHash" , flags);
            writeTLBytes(newPasswordHash, stream);
        }
        if ((flags & 1) != 0) {
            if (hint == null) throwNullFieldException("hint" , flags);
            writeString(hint, stream);
        }
        if ((flags & 2) != 0) {
            if (email == null) throwNullFieldException("email" , flags);
            writeString(email, stream);
        }
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        newSalt = (flags & 1) != 0 ? readTLBytes(stream, context) : null;
        newPasswordHash = (flags & 1) != 0 ? readTLBytes(stream, context) : null;
        hint = (flags & 1) != 0 ? readTLString(stream) : null;
        email = (flags & 2) != 0 ? readTLString(stream) : null;
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        if ((flags & 1) != 0) {
            if (newSalt == null) throwNullFieldException("newSalt" , flags);
            size += computeTLBytesSerializedSize(newSalt);
        }
        if ((flags & 1) != 0) {
            if (newPasswordHash == null) throwNullFieldException("newPasswordHash" , flags);
            size += computeTLBytesSerializedSize(newPasswordHash);
        }
        if ((flags & 1) != 0) {
            if (hint == null) throwNullFieldException("hint" , flags);
            size += computeTLStringSerializedSize(hint);
        }
        if ((flags & 2) != 0) {
            if (email == null) throwNullFieldException("email" , flags);
            size += computeTLStringSerializedSize(email);
        }
        return size;
    }

    @Override
    public String toString() {
        return "account.passwordInputSettings#86916deb";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLBytes getNewSalt() {
        return newSalt;
    }

    public void setNewSalt(TLBytes newSalt) {
        this.newSalt = newSalt;
    }

    public TLBytes getNewPasswordHash() {
        return newPasswordHash;
    }

    public void setNewPasswordHash(TLBytes newPasswordHash) {
        this.newPasswordHash = newPasswordHash;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
