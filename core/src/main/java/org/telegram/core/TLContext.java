package org.telegram.core;

import org.telegram.core.exception.*;
import org.telegram.core.exception.InvalidClassException;
import org.telegram.core.exception.UnsupportedClassException;
import org.telegram.core.type.*;
import org.telegram.core.utils.StreamUtils;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;

public abstract class TLContext {

    private final HashMap<Integer, Class> registeredClasses = new HashMap<>();

    public TLContext(int size) {
        init();
    }

    public abstract void init();

    public final boolean isSupportedObject(TLObject object) {
        return isSupportedObject(object.getClassID());
    }

    public final boolean isSupportedObject(int constructorId) {
        return registeredClasses.containsKey(constructorId);
    }

    public final <T extends TLObject> void registerClass(int classId, Class<T> clazz) {
        registeredClasses.put(classId, clazz);
    }

    public final TLObject deserializeMessage(byte[] data) throws IOException {
        return deserializeMessage(data, null, -1);
    }

    public final <T extends TLObject> T deserializeMessage(byte[] data, Class<T> clazz, int constructorId) throws IOException {
        return deserializeMessage(new ByteArrayInputStream(data), clazz, constructorId);
    }

    public final TLObject deserializeMessage(InputStream stream) throws IOException {
        return deserializeMessage(stream, null, -1);
    }

    /**
     * Deserialize a TLObject from the given input stream
     *
     * @param stream        input stream ready to by read
     * @param clazz         expected TLObject clazz or null
     * @param classId expected constructorId or -1 if unknown
     * @param <T>           expected TLObject's type
     * @return the deserialized TLObject
     * @throws IOException
     */
    @SuppressWarnings({"unchecked", "DuplicateThrows"})
    public final <T extends TLObject> T deserializeMessage(InputStream stream, Class<T> clazz, int classId) throws DeserializationException, IOException {
        int realClassId = StreamUtils.readInt(stream);
        if (classId != -1 && realClassId != classId) {
            throw new InvalidClassException(realClassId, classId);
        } else if (classId == -1) {
            classId = realClassId;
            clazz = null;
        }

        if (classId == TLGzipObject.CLASS_ID)
            return (T) deserializeMessage(unzipStream(stream));
        if (classId == TLBool.TRUE_CLASS_ID)
            return (T) TLBool.TRUE;
        if (classId == TLBool.FALSE_CLASS_ID)
            return (T) TLBool.FALSE;
        if (classId == TLVector.CLASS_ID) {
            /* Vector should be deserialized via the appropriate method, a vector was not expected,
             we must assume it's not any of vector<int>, vector<long>, vector<string> */
            return (T) deserializeVectorBody(stream, new TLVector<>());
        }

        try {
            if (clazz == null) {
                clazz = registeredClasses.get(classId);
                if (clazz == null)
                    throw new UnsupportedClassException(classId);
            }

            T message = clazz.getConstructor().newInstance();
            message.deserializeBody(stream, this);
            return message;
        } catch (ReflectiveOperationException e) {
            // !! Should never happen
            throw new RuntimeException("Unable to deserialize data. This error should not happen", e);
        }
    }

    public final TLVector deserializeVector(InputStream stream) throws IOException {
        return deserializeVector(stream, new TLVector<>());
    }

    public final TLIntVector deserializeIntVector(InputStream stream) throws IOException {
        return (TLIntVector) deserializeVector(stream, new TLIntVector());
    }

    public final TLLongVector deserializeLongVector(InputStream stream) throws IOException {
        return (TLLongVector) deserializeVector(stream, new TLLongVector());
    }

    public final TLStringVector deserializeStringVector(InputStream stream) throws IOException {
        return (TLStringVector) deserializeVector(stream, new TLStringVector());
    }

    private TLVector<?> deserializeVector(InputStream stream, TLVector<?> vector) throws IOException {
        int classId = StreamUtils.readInt(stream);
        if (classId == TLGzipObject.CLASS_ID)
            return deserializeVector(unzipStream(stream));

        if (classId == TLVector.CLASS_ID)
            return deserializeVectorBody(stream, vector);

        throw new InvalidClassException(classId, TLVector.CLASS_ID);
    }

    private TLVector<?> deserializeVectorBody(InputStream stream, TLVector<?> vector) throws IOException {
        vector.deserializeBody(stream, this);
        return vector;
    }

    private InputStream unzipStream(InputStream stream) throws IOException {
        TLGzipObject obj = new TLGzipObject();
        obj.deserializeBody(stream, this);
        ByteArrayInputStream packedDataStream = new ByteArrayInputStream(obj.getPackedData());
        return new BufferedInputStream(new GZIPInputStream(packedDataStream));
    }
} 
