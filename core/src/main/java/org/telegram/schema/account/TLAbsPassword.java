package org.telegram.schema.account;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLNoPassword: account.noPassword#96dabc18</li>
 * <li>{@link TLPassword: account.password#7c18141c</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsPassword extends TLObject {
    public TLAbsPassword() {
    }
}
