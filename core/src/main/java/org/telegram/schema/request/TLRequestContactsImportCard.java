package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLIntVector;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLAbsUser;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestContactsImportCard extends TLMethod<TLAbsUser> {
    public static final int CLASS_ID = 0x4fe196fe;

    protected TLIntVector exportCard;

    public TLRequestContactsImportCard() {
    }

    public TLRequestContactsImportCard(TLIntVector exportCard) {
        this.exportCard = exportCard;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLAbsUser deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLAbsUser)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLAbsUser) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(exportCard, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        exportCard = readTLIntVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += exportCard.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "contacts.importCard#4fe196fe";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLIntVector getExportCard() {
        return exportCard;
    }

    public void setExportCard(TLIntVector exportCard) {
        this.exportCard = exportCard;
    }
}
