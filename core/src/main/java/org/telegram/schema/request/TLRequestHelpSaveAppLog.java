package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBool;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLInputAppEvent;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestHelpSaveAppLog extends TLMethod<TLBool> {
    public static final int CLASS_ID = 0x6f02f748;

    protected TLVector<TLInputAppEvent> events;

    public TLRequestHelpSaveAppLog() {
    }

    public TLRequestHelpSaveAppLog(TLVector<TLInputAppEvent> events) {
        this.events = events;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLBool deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLBool)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLBool) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(events, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        events = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += events.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "help.saveAppLog#6f02f748";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLVector<TLInputAppEvent> getEvents() {
        return events;
    }

    public void setEvents(TLVector<TLInputAppEvent> events) {
        this.events = events;
    }
}
