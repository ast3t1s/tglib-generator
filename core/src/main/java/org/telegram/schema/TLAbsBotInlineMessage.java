package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLBotInlineMessageMediaAuto: botInlineMessageMediaAuto#a74b15b</li>
 * <li>{@link TLBotInlineMessageMediaContact: botInlineMessageMediaContact#35edb4d4</li>
 * <li>{@link TLBotInlineMessageMediaGeo: botInlineMessageMediaGeo#3a8fd8b8</li>
 * <li>{@link TLBotInlineMessageMediaVenue: botInlineMessageMediaVenue#4366232e</li>
 * <li>{@link TLBotInlineMessageText: botInlineMessageText#8c7f65e2</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsBotInlineMessage extends TLObject {
    public TLAbsBotInlineMessage() {
    }
}
