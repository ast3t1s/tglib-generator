package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputGeoPointEmpty extends TLAbsInputGeoPoint {
    public static final int CLASS_ID = 0xe4c123d6;

    public TLInputGeoPointEmpty() {
    }

    @Override
    public String toString() {
        return "inputGeoPointEmpty#e4c123d6";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    @Override
    public final boolean isEmpty() {
        return true;
    }

    @Override
    public final boolean isNotEmpty() {
        return false;
    }
}
