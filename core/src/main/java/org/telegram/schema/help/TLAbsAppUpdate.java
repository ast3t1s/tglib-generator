package org.telegram.schema.help;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLAppUpdate: help.appUpdate#8987f311</li>
 * <li>{@link TLNoAppUpdate: help.noAppUpdate#c45a6536</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsAppUpdate extends TLObject {
    public TLAbsAppUpdate() {
    }
}
