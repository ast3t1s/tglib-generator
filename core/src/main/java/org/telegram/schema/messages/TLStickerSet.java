package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsDocument;
import org.telegram.schema.TLStickerPack;

/**
 * @author Telegram Schema Generator
 */
public class TLStickerSet extends TLObject {
    public static final int CLASS_ID = 0xb60a24a6;

    protected org.telegram.schema.TLStickerSet set;

    protected TLVector<TLStickerPack> packs;

    protected TLVector<TLAbsDocument> documents;

    public TLStickerSet() {
    }

    public TLStickerSet(org.telegram.schema.TLStickerSet set, TLVector<TLStickerPack> packs,
            TLVector<TLAbsDocument> documents) {
        this.set = set;
        this.packs = packs;
        this.documents = documents;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(set, stream);
        writeTLVector(packs, stream);
        writeTLVector(documents, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        set = readTLObject(stream, context, org.telegram.schema.TLStickerSet.class, org.telegram.schema.TLStickerSet.CLASS_ID);
        packs = readTLVector(stream, context);
        documents = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += set.computeSerializedSize();
        size += packs.computeSerializedSize();
        size += documents.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messages.stickerSet#b60a24a6";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public org.telegram.schema.TLStickerSet getSet() {
        return set;
    }

    public void setSet(org.telegram.schema.TLStickerSet set) {
        this.set = set;
    }

    public TLVector<TLStickerPack> getPacks() {
        return packs;
    }

    public void setPacks(TLVector<TLStickerPack> packs) {
        this.packs = packs;
    }

    public TLVector<TLAbsDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(TLVector<TLAbsDocument> documents) {
        this.documents = documents;
    }
}
