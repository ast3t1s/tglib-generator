package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLDocumentAttributeSticker extends TLAbsDocumentAttribute {
    public static final int CLASS_ID = 0x6319d612;

    protected int flags;

    protected boolean mask;

    protected String alt;

    protected TLAbsInputStickerSet stickerset;

    protected TLMaskCoords maskCoords;

    public TLDocumentAttributeSticker() {
    }

    public TLDocumentAttributeSticker(boolean mask, String alt, TLAbsInputStickerSet stickerset,
            TLMaskCoords maskCoords) {
        this.mask = mask;
        this.alt = alt;
        this.stickerset = stickerset;
        this.maskCoords = maskCoords;
    }

    private void computeFlags() {
        flags = 0;
        flags = mask ? (flags | 2) : (flags & ~2);
        flags = maskCoords != null ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeString(alt, stream);
        writeTLObject(stickerset, stream);
        if ((flags & 1) != 0) {
            if (maskCoords == null) throwNullFieldException("maskCoords" , flags);
            writeTLObject(maskCoords, stream);
        }
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        mask = (flags & 2) != 0;
        alt = readTLString(stream);
        stickerset = readTLObject(stream, context, TLAbsInputStickerSet.class, -1);
        maskCoords = (flags & 1) != 0 ? readTLObject(stream, context, TLMaskCoords.class, TLMaskCoords.CLASS_ID) : null;
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += computeTLStringSerializedSize(alt);
        size += stickerset.computeSerializedSize();
        if ((flags & 1) != 0) {
            if (maskCoords == null) throwNullFieldException("maskCoords" , flags);
            size += maskCoords.computeSerializedSize();
        }
        return size;
    }

    @Override
    public String toString() {
        return "documentAttributeSticker#6319d612";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getMask() {
        return mask;
    }

    public void setMask(boolean mask) {
        this.mask = mask;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public TLAbsInputStickerSet getStickerset() {
        return stickerset;
    }

    public void setStickerset(TLAbsInputStickerSet stickerset) {
        this.stickerset = stickerset;
    }

    public TLMaskCoords getMaskCoords() {
        return maskCoords;
    }

    public void setMaskCoords(TLMaskCoords maskCoords) {
        this.maskCoords = maskCoords;
    }
}
