package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLUserProfilePhoto: userProfilePhoto#d559d8c8</li>
 * <li>{@link TLUserProfilePhotoEmpty: userProfilePhotoEmpty#4f11bae1</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsUserProfilePhoto extends TLObject {
    public TLAbsUserProfilePhoto() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLUserProfilePhoto getAsUserProfilePhoto() {
        return null;
    }
}
