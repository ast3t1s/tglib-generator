package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLGeoPoint: geoPoint#2049d70c</li>
 * <li>{@link TLGeoPointEmpty: geoPointEmpty#1117dd5f</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsGeoPoint extends TLObject {
    public TLAbsGeoPoint() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLGeoPoint getAsGeoPoint() {
        return null;
    }
}
