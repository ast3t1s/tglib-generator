package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLPrivacyValueDisallowContacts extends TLAbsPrivacyRule {
    public static final int CLASS_ID = 0xf888fa1a;

    public TLPrivacyValueDisallowContacts() {
    }

    @Override
    public String toString() {
        return "privacyValueDisallowContacts#f888fa1a";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
