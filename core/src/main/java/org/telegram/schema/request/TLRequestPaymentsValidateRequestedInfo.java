package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.TLPaymentRequestedInfo;
import org.telegram.schema.payments.TLValidatedRequestedInfo;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestPaymentsValidateRequestedInfo extends TLMethod<TLValidatedRequestedInfo> {
    public static final int CLASS_ID = 0x770a8e74;

    protected int flags;

    protected boolean save;

    protected int msgId;

    protected TLPaymentRequestedInfo info;

    public TLRequestPaymentsValidateRequestedInfo() {
    }

    public TLRequestPaymentsValidateRequestedInfo(boolean save, int msgId,
            TLPaymentRequestedInfo info) {
        this.save = save;
        this.msgId = msgId;
        this.info = info;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLValidatedRequestedInfo deserializeResponse(InputStream stream, TLContext context)
            throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLValidatedRequestedInfo)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLValidatedRequestedInfo) response;
    }

    private void computeFlags() {
        flags = 0;
        flags = save ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeInt(msgId, stream);
        writeTLObject(info, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        save = (flags & 1) != 0;
        msgId = readInt(stream);
        info = readTLObject(stream, context, TLPaymentRequestedInfo.class, TLPaymentRequestedInfo.CLASS_ID);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT32;
        size += info.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "payments.validateRequestedInfo#770a8e74";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }

    public int getMsgId() {
        return msgId;
    }

    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }

    public TLPaymentRequestedInfo getInfo() {
        return info;
    }

    public void setInfo(TLPaymentRequestedInfo info) {
        this.info = info;
    }
}
