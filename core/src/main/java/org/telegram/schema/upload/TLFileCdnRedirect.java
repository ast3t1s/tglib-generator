package org.telegram.schema.upload;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBytes;

/**
 * @author Telegram Schema Generator
 */
public class TLFileCdnRedirect extends TLAbsFile {
    public static final int CLASS_ID = 0x1508485a;

    protected int dcId;

    protected TLBytes fileToken;

    protected TLBytes encryptionKey;

    protected TLBytes encryptionIv;

    public TLFileCdnRedirect() {
    }

    public TLFileCdnRedirect(int dcId, TLBytes fileToken, TLBytes encryptionKey,
            TLBytes encryptionIv) {
        this.dcId = dcId;
        this.fileToken = fileToken;
        this.encryptionKey = encryptionKey;
        this.encryptionIv = encryptionIv;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(dcId, stream);
        writeTLBytes(fileToken, stream);
        writeTLBytes(encryptionKey, stream);
        writeTLBytes(encryptionIv, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        dcId = readInt(stream);
        fileToken = readTLBytes(stream, context);
        encryptionKey = readTLBytes(stream, context);
        encryptionIv = readTLBytes(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += computeTLBytesSerializedSize(fileToken);
        size += computeTLBytesSerializedSize(encryptionKey);
        size += computeTLBytesSerializedSize(encryptionIv);
        return size;
    }

    @Override
    public String toString() {
        return "upload.fileCdnRedirect#1508485a";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getDcId() {
        return dcId;
    }

    public void setDcId(int dcId) {
        this.dcId = dcId;
    }

    public TLBytes getFileToken() {
        return fileToken;
    }

    public void setFileToken(TLBytes fileToken) {
        this.fileToken = fileToken;
    }

    public TLBytes getEncryptionKey() {
        return encryptionKey;
    }

    public void setEncryptionKey(TLBytes encryptionKey) {
        this.encryptionKey = encryptionKey;
    }

    public TLBytes getEncryptionIv() {
        return encryptionIv;
    }

    public void setEncryptionIv(TLBytes encryptionIv) {
        this.encryptionIv = encryptionIv;
    }
}
