package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLUserStatusEmpty extends TLAbsUserStatus {
    public static final int CLASS_ID = 0x9d05049;

    public TLUserStatusEmpty() {
    }

    @Override
    public String toString() {
        return "userStatusEmpty#9d05049";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
