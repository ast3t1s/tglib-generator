package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLNotifyChats extends TLAbsNotifyPeer {
    public static final int CLASS_ID = 0xc007cec3;

    public TLNotifyChats() {
    }

    @Override
    public String toString() {
        return "notifyChats#c007cec3";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
