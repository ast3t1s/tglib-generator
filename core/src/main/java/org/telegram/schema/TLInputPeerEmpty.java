package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputPeerEmpty extends TLAbsInputPeer {
    public static final int CLASS_ID = 0x7f3b18ea;

    public TLInputPeerEmpty() {
    }

    @Override
    public String toString() {
        return "inputPeerEmpty#7f3b18ea";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
