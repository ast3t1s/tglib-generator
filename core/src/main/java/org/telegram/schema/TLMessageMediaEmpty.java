package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLMessageMediaEmpty extends TLAbsMessageMedia {
    public static final int CLASS_ID = 0x3ded6320;

    public TLMessageMediaEmpty() {
    }

    @Override
    public String toString() {
        return "messageMediaEmpty#3ded6320";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
