package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLPageBlockDivider extends TLAbsPageBlock {
    public static final int CLASS_ID = 0xdb20b188;

    public TLPageBlockDivider() {
    }

    @Override
    public String toString() {
        return "pageBlockDivider#db20b188";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
