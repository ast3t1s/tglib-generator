package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLInputPaymentCredentials extends TLAbsInputPaymentCredentials {
    public static final int CLASS_ID = 0x3417d728;

    protected int flags;

    protected boolean save;

    protected TLDataJSON data;

    public TLInputPaymentCredentials() {
    }

    public TLInputPaymentCredentials(boolean save, TLDataJSON data) {
        this.save = save;
        this.data = data;
    }

    private void computeFlags() {
        flags = 0;
        flags = save ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeTLObject(data, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        save = (flags & 1) != 0;
        data = readTLObject(stream, context, TLDataJSON.class, TLDataJSON.CLASS_ID);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += data.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "inputPaymentCredentials#3417d728";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }

    public TLDataJSON getData() {
        return data;
    }

    public void setData(TLDataJSON data) {
        this.data = data;
    }
}
