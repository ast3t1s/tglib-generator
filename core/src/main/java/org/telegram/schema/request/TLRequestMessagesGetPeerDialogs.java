package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;
import org.telegram.schema.TLAbsInputPeer;
import org.telegram.schema.messages.TLPeerDialogs;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestMessagesGetPeerDialogs extends TLMethod<TLPeerDialogs> {
    public static final int CLASS_ID = 0x2d9776b9;

    protected TLVector<TLAbsInputPeer> peers;

    public TLRequestMessagesGetPeerDialogs() {
    }

    public TLRequestMessagesGetPeerDialogs(TLVector<TLAbsInputPeer> peers) {
        this.peers = peers;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLPeerDialogs deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLPeerDialogs)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLPeerDialogs) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(peers, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        peers = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += peers.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "messages.getPeerDialogs#2d9776b9";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLVector<TLAbsInputPeer> getPeers() {
        return peers;
    }

    public void setPeers(TLVector<TLAbsInputPeer> peers) {
        this.peers = peers;
    }
}
