package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLTextEmpty extends TLAbsRichText {
    public static final int CLASS_ID = 0xdc3d824f;

    public TLTextEmpty() {
    }

    @Override
    public String toString() {
        return "textEmpty#dc3d824f";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
