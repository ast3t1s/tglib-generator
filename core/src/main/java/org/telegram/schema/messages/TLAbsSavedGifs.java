package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLSavedGifs: messages.savedGifs#2e0709a5</li>
 * <li>{@link TLSavedGifsNotModified: messages.savedGifsNotModified#e8025ca2</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsSavedGifs extends TLObject {
    public TLAbsSavedGifs() {
    }
}
