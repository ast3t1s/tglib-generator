package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputPrivacyKeyPhoneCall extends TLAbsInputPrivacyKey {
    public static final int CLASS_ID = 0xfabadc5f;

    public TLInputPrivacyKeyPhoneCall() {
    }

    @Override
    public String toString() {
        return "inputPrivacyKeyPhoneCall#fabadc5f";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
