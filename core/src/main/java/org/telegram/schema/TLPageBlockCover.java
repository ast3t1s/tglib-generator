package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLPageBlockCover extends TLAbsPageBlock {
    public static final int CLASS_ID = 0x39f23300;

    protected TLAbsPageBlock cover;

    public TLPageBlockCover() {
    }

    public TLPageBlockCover(TLAbsPageBlock cover) {
        this.cover = cover;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(cover, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        cover = readTLObject(stream, context, TLAbsPageBlock.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += cover.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "pageBlockCover#39f23300";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsPageBlock getCover() {
        return cover;
    }

    public void setCover(TLAbsPageBlock cover) {
        this.cover = cover;
    }
}
