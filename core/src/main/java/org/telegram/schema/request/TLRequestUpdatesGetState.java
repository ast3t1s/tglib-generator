package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.updates.TLState;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestUpdatesGetState extends TLMethod<TLState> {
    public static final int CLASS_ID = 0xedd4882a;

    public TLRequestUpdatesGetState() {
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLState deserializeResponse(InputStream stream, TLContext context) throws IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLState)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLState) response;
    }

    @Override
    public String toString() {
        return "updates.getState#edd4882a";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
