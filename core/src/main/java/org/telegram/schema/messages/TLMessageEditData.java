package org.telegram.schema.messages;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLMessageEditData extends TLObject {
    public static final int CLASS_ID = 0x26b5dde6;

    protected int flags;

    protected boolean caption;

    public TLMessageEditData() {
    }

    public TLMessageEditData(boolean caption) {
        this.caption = caption;
    }

    private void computeFlags() {
        flags = 0;
        flags = caption ? (flags | 1) : (flags & ~1);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        caption = (flags & 1) != 0;
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "messages.messageEditData#26b5dde6";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getCaption() {
        return caption;
    }

    public void setCaption(boolean caption) {
        this.caption = caption;
    }
}
