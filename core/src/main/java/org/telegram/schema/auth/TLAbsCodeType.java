package org.telegram.schema.auth;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLCodeTypeCall: auth.codeTypeCall#741cd3e3</li>
 * <li>{@link TLCodeTypeFlashCall: auth.codeTypeFlashCall#226ccefb</li>
 * <li>{@link TLCodeTypeSms: auth.codeTypeSms#72a3158c</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsCodeType extends TLObject {
    public TLAbsCodeType() {
    }
}
