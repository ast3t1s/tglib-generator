package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLInputGameID: inputGameID#32c3e77</li>
 * <li>{@link TLInputGameShortName: inputGameShortName#c331e80a</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsInputGame extends TLObject {
    public TLAbsInputGame() {
    }
}
