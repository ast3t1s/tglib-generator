package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;
import org.telegram.schema.auth.TLAuthorization;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestAuthImportBotAuthorization extends TLMethod<TLAuthorization> {
    public static final int CLASS_ID = 0x67a3ff2c;

    protected int flags;

    protected int apiId;

    protected String apiHash;

    protected String botAuthToken;

    public TLRequestAuthImportBotAuthorization() {
    }

    public TLRequestAuthImportBotAuthorization(int flags, int apiId, String apiHash,
            String botAuthToken) {
        this.flags = flags;
        this.apiId = apiId;
        this.apiHash = apiHash;
        this.botAuthToken = botAuthToken;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public TLAuthorization deserializeResponse(InputStream stream, TLContext context) throws
            IOException {
        final TLObject response = readTLObject(stream, context);
        if (response == null) {
            throw new IOException("Unable to parse response");
        }
        if (!(response instanceof TLAuthorization)) {
            throw new IOException("Incorrect response type, expected " + getClass().getCanonicalName() + ", found " + response.getClass().getCanonicalName());
        }
        return (TLAuthorization) response;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeInt(flags, stream);
        writeInt(apiId, stream);
        writeString(apiHash, stream);
        writeString(botAuthToken, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        apiId = readInt(stream);
        apiHash = readTLString(stream);
        botAuthToken = readTLString(stream);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT32;
        size += computeTLStringSerializedSize(apiHash);
        size += computeTLStringSerializedSize(botAuthToken);
        return size;
    }

    @Override
    public String toString() {
        return "auth.importBotAuthorization#67a3ff2c";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public int getFlags() {
        return flags;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }

    public int getApiId() {
        return apiId;
    }

    public void setApiId(int apiId) {
        this.apiId = apiId;
    }

    public String getApiHash() {
        return apiHash;
    }

    public void setApiHash(String apiHash) {
        this.apiHash = apiHash;
    }

    public String getBotAuthToken() {
        return botAuthToken;
    }

    public void setBotAuthToken(String botAuthToken) {
        this.botAuthToken = botAuthToken;
    }
}
