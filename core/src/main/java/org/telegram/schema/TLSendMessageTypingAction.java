package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLSendMessageTypingAction extends TLAbsSendMessageAction {
    public static final int CLASS_ID = 0x16bf744e;

    public TLSendMessageTypingAction() {
    }

    @Override
    public String toString() {
        return "sendMessageTypingAction#16bf744e";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
