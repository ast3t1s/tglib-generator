package org.telegram.schema.contacts;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLBlocked: contacts.blocked#1c138d15</li>
 * <li>{@link TLBlockedSlice: contacts.blockedSlice#900802a1</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsBlocked extends TLObject {
    public TLAbsBlocked() {
    }
}
