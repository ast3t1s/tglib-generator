package org.telegram.schema.request;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLLongVector;
import org.telegram.core.type.TLMethod;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLRequestInvokeAfterMsgs<T extends TLObject> extends TLMethod<T> {
    public static final int CLASS_ID = 0x3dc4b4f0;

    protected TLLongVector msgIds;

    protected TLMethod<T> query;

    public TLRequestInvokeAfterMsgs() {
    }

    public TLRequestInvokeAfterMsgs(TLLongVector msgIds, TLMethod<T> query) {
        this.msgIds = msgIds;
        this.query = query;
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public T deserializeResponse(InputStream stream, TLContext context) throws IOException {
        return query.deserializeResponse(stream, context);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(msgIds, stream);
        writeTLMethod(query, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        msgIds = readTLLongVector(stream, context);
        query = readTLMethod(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += msgIds.computeSerializedSize();
        size += query.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "invokeAfterMsgs#3dc4b4f0";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLLongVector getMsgIds() {
        return msgIds;
    }

    public void setMsgIds(TLLongVector msgIds) {
        this.msgIds = msgIds;
    }

    public TLMethod<T> getQuery() {
        return query;
    }

    public void setQuery(TLMethod<T> query) {
        this.query = query;
    }
}
