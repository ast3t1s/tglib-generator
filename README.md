Telegram Schema Generator
===========

[Type Language](http://core.telegram.org/mtproto/TL) compiler for working with api of [Telegram](http://telegram.org) project.

Converts json telegram schema (http://core.telegram.org/schema) to generated classes for serializing and deserializing api messages and methods.

Usage
-----------
1. Download Telegram JSON schema and put it to resource folder of schema module
2. Run TLGenerator 
3. Copy classes from core module with TL base classes and schema.
-----------

More information
----------------
#### Type Language documentation

English: http://core.telegram.org/mtproto/TL

License
----------------
Compiler uses [MIT License](LICENSE)