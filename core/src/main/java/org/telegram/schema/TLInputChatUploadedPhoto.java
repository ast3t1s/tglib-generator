package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLInputChatUploadedPhoto extends TLAbsInputChatPhoto {
    public static final int CLASS_ID = 0x927c55b4;

    protected TLAbsInputFile file;

    public TLInputChatUploadedPhoto() {
    }

    public TLInputChatUploadedPhoto(TLAbsInputFile file) {
        this.file = file;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLObject(file, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        file = readTLObject(stream, context, TLAbsInputFile.class, -1);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += file.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "inputChatUploadedPhoto#927c55b4";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLAbsInputFile getFile() {
        return file;
    }

    public void setFile(TLAbsInputFile file) {
        this.file = file;
    }
}
