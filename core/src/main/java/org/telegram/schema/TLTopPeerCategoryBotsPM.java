package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLTopPeerCategoryBotsPM extends TLAbsTopPeerCategory {
    public static final int CLASS_ID = 0xab661b5b;

    public TLTopPeerCategoryBotsPM() {
    }

    @Override
    public String toString() {
        return "topPeerCategoryBotsPM#ab661b5b";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
