package org.telegram.schema.auth;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLCodeTypeCall extends TLAbsCodeType {
    public static final int CLASS_ID = 0x741cd3e3;

    public TLCodeTypeCall() {
    }

    @Override
    public String toString() {
        return "auth.codeTypeCall#741cd3e3";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
