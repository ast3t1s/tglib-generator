package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLVector;

/**
 * @author Telegram Schema Generator
 */
public class TLTextConcat extends TLAbsRichText {
    public static final int CLASS_ID = 0x7e6260d7;

    protected TLVector<TLAbsRichText> texts;

    public TLTextConcat() {
    }

    public TLTextConcat(TLVector<TLAbsRichText> texts) {
        this.texts = texts;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeTLVector(texts, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        texts = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += texts.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "textConcat#7e6260d7";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public TLVector<TLAbsRichText> getTexts() {
        return texts;
    }

    public void setTexts(TLVector<TLAbsRichText> texts) {
        this.texts = texts;
    }
}
