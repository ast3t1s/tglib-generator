package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLBytes;

/**
 * @author Telegram Schema Generator
 */
public class TLInputPaymentCredentialsSaved extends TLAbsInputPaymentCredentials {
    public static final int CLASS_ID = 0xc10eb2cf;

    protected String id;

    protected TLBytes tmpPassword;

    public TLInputPaymentCredentialsSaved() {
    }

    public TLInputPaymentCredentialsSaved(String id, TLBytes tmpPassword) {
        this.id = id;
        this.tmpPassword = tmpPassword;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(id, stream);
        writeTLBytes(tmpPassword, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        id = readTLString(stream);
        tmpPassword = readTLBytes(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(id);
        size += computeTLBytesSerializedSize(tmpPassword);
        return size;
    }

    @Override
    public String toString() {
        return "inputPaymentCredentialsSaved#c10eb2cf";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TLBytes getTmpPassword() {
        return tmpPassword;
    }

    public void setTmpPassword(TLBytes tmpPassword) {
        this.tmpPassword = tmpPassword;
    }
}
