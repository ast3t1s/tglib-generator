package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLWallPaper: wallPaper#ccb03657</li>
 * <li>{@link TLWallPaperSolid: wallPaperSolid#63117f24</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsWallPaper extends TLObject {
    public TLAbsWallPaper() {
    }
}
