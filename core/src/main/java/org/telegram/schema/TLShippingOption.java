package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;
import org.telegram.core.type.TLVector;

/**
 * @author Telegram Schema Generator
 */
public class TLShippingOption extends TLObject {
    public static final int CLASS_ID = 0xb6213cdf;

    protected String id;

    protected String title;

    protected TLVector<TLLabeledPrice> prices;

    public TLShippingOption() {
    }

    public TLShippingOption(String id, String title, TLVector<TLLabeledPrice> prices) {
        this.id = id;
        this.title = title;
        this.prices = prices;
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        writeString(id, stream);
        writeString(title, stream);
        writeTLVector(prices, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        id = readTLString(stream);
        title = readTLString(stream);
        prices = readTLVector(stream, context);
    }

    @Override
    public int computeSerializedSize() {
        int size = SIZE_CLASS_ID;
        size += computeTLStringSerializedSize(id);
        size += computeTLStringSerializedSize(title);
        size += prices.computeSerializedSize();
        return size;
    }

    @Override
    public String toString() {
        return "shippingOption#b6213cdf";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TLVector<TLLabeledPrice> getPrices() {
        return prices;
    }

    public void setPrices(TLVector<TLLabeledPrice> prices) {
        this.prices = prices;
    }
}
