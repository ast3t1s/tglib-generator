package org.telegram.schema.updates;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;

/**
 * @author Telegram Schema Generator
 */
public class TLChannelDifferenceEmpty extends TLAbsChannelDifference {
    public static final int CLASS_ID = 0x3e11affb;

    protected int flags;

    protected boolean _final;

    protected int pts;

    protected Integer timeout;

    public TLChannelDifferenceEmpty() {
    }

    public TLChannelDifferenceEmpty(boolean _final, int pts, Integer timeout) {
        this._final = _final;
        this.pts = pts;
        this.timeout = timeout;
    }

    private void computeFlags() {
        flags = 0;
        flags = _final ? (flags | 1) : (flags & ~1);
        flags = timeout != null ? (flags | 2) : (flags & ~2);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeInt(pts, stream);
        if ((flags & 2) != 0) {
            if (timeout == null) throwNullFieldException("timeout" , flags);
            writeInt(timeout, stream);
        }
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        _final = (flags & 1) != 0;
        pts = readInt(stream);
        timeout = (flags & 2) != 0 ? readInt(stream) : null;
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT32;
        if ((flags & 2) != 0) {
            if (timeout == null) throwNullFieldException("timeout" , flags);
            size += SIZE_INT32;
        }
        return size;
    }

    @Override
    public String toString() {
        return "updates.channelDifferenceEmpty#3e11affb";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getFinal() {
        return _final;
    }

    public void setFinal(boolean _final) {
        this._final = _final;
    }

    public int getPts() {
        return pts;
    }

    public void setPts(int pts) {
        this.pts = pts;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }
}
