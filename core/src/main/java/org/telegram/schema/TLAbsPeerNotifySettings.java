package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLPeerNotifySettings: peerNotifySettings#9acda4c0</li>
 * <li>{@link TLPeerNotifySettingsEmpty: peerNotifySettingsEmpty#70a68512</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsPeerNotifySettings extends TLObject {
    public TLAbsPeerNotifySettings() {
    }

    public abstract boolean isEmpty();

    public abstract boolean isNotEmpty();

    public TLPeerNotifySettings getAsPeerNotifySettings() {
        return null;
    }
}
