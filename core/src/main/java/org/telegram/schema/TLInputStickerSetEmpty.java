package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLInputStickerSetEmpty extends TLAbsInputStickerSet {
    public static final int CLASS_ID = 0xffb62b95;

    public TLInputStickerSetEmpty() {
    }

    @Override
    public String toString() {
        return "inputStickerSetEmpty#ffb62b95";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
