package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import org.telegram.core.TLContext;
import org.telegram.core.type.TLObject;

/**
 * @author Telegram Schema Generator
 */
public class TLPhoneCallProtocol extends TLObject {
    public static final int CLASS_ID = 0xa2bb35cb;

    protected int flags;

    protected boolean udpP2p;

    protected boolean udpReflector;

    protected int minLayer;

    protected int maxLayer;

    public TLPhoneCallProtocol() {
    }

    public TLPhoneCallProtocol(boolean udpP2p, boolean udpReflector, int minLayer, int maxLayer) {
        this.udpP2p = udpP2p;
        this.udpReflector = udpReflector;
        this.minLayer = minLayer;
        this.maxLayer = maxLayer;
    }

    private void computeFlags() {
        flags = 0;
        flags = udpP2p ? (flags | 1) : (flags & ~1);
        flags = udpReflector ? (flags | 2) : (flags & ~2);
    }

    @Override
    public void serializeBody(OutputStream stream) throws IOException {
        computeFlags();

        writeInt(flags, stream);
        writeInt(minLayer, stream);
        writeInt(maxLayer, stream);
    }

    @Override
    @SuppressWarnings({"unchecked", "SimplifiableConditionalExpression"})
    public void deserializeBody(InputStream stream, TLContext context) throws IOException {
        flags = readInt(stream);
        udpP2p = (flags & 1) != 0;
        udpReflector = (flags & 2) != 0;
        minLayer = readInt(stream);
        maxLayer = readInt(stream);
    }

    @Override
    public int computeSerializedSize() {
        computeFlags();

        int size = SIZE_CLASS_ID;
        size += SIZE_INT32;
        size += SIZE_INT32;
        size += SIZE_INT32;
        return size;
    }

    @Override
    public String toString() {
        return "phoneCallProtocol#a2bb35cb";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }

    public boolean getUdpP2p() {
        return udpP2p;
    }

    public void setUdpP2p(boolean udpP2p) {
        this.udpP2p = udpP2p;
    }

    public boolean getUdpReflector() {
        return udpReflector;
    }

    public void setUdpReflector(boolean udpReflector) {
        this.udpReflector = udpReflector;
    }

    public int getMinLayer() {
        return minLayer;
    }

    public void setMinLayer(int minLayer) {
        this.minLayer = minLayer;
    }

    public int getMaxLayer() {
        return maxLayer;
    }

    public void setMaxLayer(int maxLayer) {
        this.maxLayer = maxLayer;
    }
}
