package org.telegram.schema.storage;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import java.lang.Override;
import java.lang.String;

/**
 * @author Telegram Schema Generator
 */
public class TLFilePdf extends TLAbsFileType {
    public static final int CLASS_ID = 0xae1e508d;

    public TLFilePdf() {
    }

    @Override
    public String toString() {
        return "storage.filePdf#ae1e508d";
    }

    @Override
    public int getClassID() {
        return CLASS_ID;
    }
}
