package org.telegram.schema;

import static org.telegram.core.utils.StreamUtils.*;
import static org.telegram.core.utils.TypeUtils.*;

import org.telegram.core.type.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLNotifyAll: notifyAll#74d07c60</li>
 * <li>{@link TLNotifyChats: notifyChats#c007cec3</li>
 * <li>{@link TLNotifyPeer: notifyPeer#9fd40bd8</li>
 * <li>{@link TLNotifyUsers: notifyUsers#b4c83b4c</li>
 * </ul>
 *
 * @author Telegram Schema Generator
 */
public abstract class TLAbsNotifyPeer extends TLObject {
    public TLAbsNotifyPeer() {
    }
}
